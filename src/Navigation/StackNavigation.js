import React from "react";
import { View, Text } from "react-native";
import SignUp from "../Auth/SignUp";
import WelcomeScreen from "../Auth/WelcomeScreen";
import FindPassword from "../Auth/FindPassword";
import ChangePassword from "../Auth/ChangePassword";
import Login from "../Auth/Login";
import MyTabs from "./TabNavigation";

import Profile from "../Screens/Profile";
import EditProfile from "../Screens/EditProfile";
// import Dashboard from "../Screens/Dashboard";

import GetStarted from "../Screens/MHS/GetStarted";
import SelectOptions from "../Screens/MHS/SelectOptions";
import BasicInformation from "../Screens/MHS/BasicInformation";
import DepressionScreening from "../Screens/MHS/DepressionScreening";
import BipolarScreening from "../Screens/MHS/BipolarScreening";
import AnxietyScreening from "../Screens/MHS/AnxietyScreening";
import PTSDScreening from "../Screens/MHS/PTSDScreening";
import SubstanceUseTest from "../Screens/MHS/SubstanceUseTest";
import Result from "../Screens/MHS/Result";
import SplashScreen from "../Screens/SplashScreen";
import { createStackNavigator } from "@react-navigation/stack";
import { getFocusedRouteNameFromRoute } from "@react-navigation/native";

const Stack = createStackNavigator();

export default function MyStack({ navigation, route }) {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="SplashScreen" component={SplashScreen} />
      <Stack.Screen name="Screening" component={MyTabs} />
      {/* <Stack.Screen name="WelcomeScreen" component={WelcomeScreen} /> */}
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="SignUp" component={SignUp} />
      <Stack.Screen name="FindPassword" component={FindPassword} />
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="EditProfile" component={EditProfile} />
      <Stack.Screen name="ChangePassword" component={ChangePassword} />
      <Stack.Screen name="GetStarted" component={GetStarted} />
      <Stack.Screen name="SelectOptions" component={SelectOptions} />
      <Stack.Screen name="BasicInformation" component={BasicInformation} />
      <Stack.Screen
        name="DepressionScreening"
        component={DepressionScreening}
      />
      <Stack.Screen name="BipolarScreening" component={BipolarScreening} />
      <Stack.Screen name="AnxietyScreening" component={AnxietyScreening} />
      <Stack.Screen name="PTSDScreening" component={PTSDScreening} />
      <Stack.Screen name="SubstanceUseTest" component={SubstanceUseTest} />
      <Stack.Screen name="Result" component={Result} />
    </Stack.Navigator>
  );
}
