import React, { useState, useEffect } from "react";
import { View, Text } from "react-native";
import SignUp from "../Auth/SignUp";
import WelcomeScreen from "../Auth/WelcomeScreen";
import FindPassword from "../Auth/FindPassword";
import ChangePassword from "../Auth/ChangePassword";
import Login from "../Auth/Login";
import MyTabs from "./TabNavigation";

import Profile from "../Screens/Profile";
import EditProfile from "../Screens/EditProfile";
// import Dashboard from "../Screens/Dashboard";

import GetStarted from "../Screens/MHS/GetStarted";
import SelectOptions from "../Screens/MHS/SelectOptions";
import BasicInformation from "../Screens/MHS/BasicInformation";
import DepressionScreening from "../Screens/MHS/DepressionScreening";
import BipolarScreening from "../Screens/MHS/BipolarScreening";
import AnxietyScreening from "../Screens/MHS/AnxietyScreening";
import PTSDScreening from "../Screens/MHS/PTSDScreening";
import SubstanceUseTest from "../Screens/MHS/SubstanceUseTest";
import Result from "../Screens/MHS/Result";
import ExistResult from "../Screens/MHS/ExistResult";
import { createStackNavigator } from "@react-navigation/stack";
const ScreenStack = createStackNavigator();

export default function ScreeningStack({ navigation, route }) {
  return (
    <ScreenStack.Navigator screenOptions={{ headerShown: false }}>
      {/* <ScreenStack.Screen name="WelcomeScreen" component={WelcomeScreen} /> */}
      <ScreenStack.Screen name="GetStarted" component={GetStarted} />
      <ScreenStack.Screen name="SelectOptions" component={SelectOptions} />
      <ScreenStack.Screen
        name="BasicInformation"
        component={BasicInformation}
      />
      <ScreenStack.Screen
        name="DepressionScreening"
        component={DepressionScreening}
      />
      <ScreenStack.Screen
        name="BipolarScreening"
        component={BipolarScreening}
      />
      <ScreenStack.Screen
        name="AnxietyScreening"
        component={AnxietyScreening}
      />
      <ScreenStack.Screen name="PTSDScreening" component={PTSDScreening} />
      <ScreenStack.Screen
        name="SubstanceUseTest"
        component={SubstanceUseTest}
      />
      <ScreenStack.Screen name="Result" component={Result} />
      {/* <ScreenStack.Screen name="Login" component={Login} /> */}
      {/* <ScreenStack.Screen name="SignUp" component={SignUp} /> */}
      {/* <ScreenStack.Screen name="FindPassword" component={FindPassword} /> */}
      {/* <Stack.Screen name="Dashboard" component={Dashboard} /> */}
      <ScreenStack.Screen name="Profile" component={Profile} />
      <ScreenStack.Screen name="EditProfile" component={EditProfile} />
      <ScreenStack.Screen name="ChangePassword" component={ChangePassword} />
      <ScreenStack.Screen name="ExistResult" component={ExistResult} />

      {/* <ScreenStack.Screen name="Welcome" component={Welcome} /> */}
    </ScreenStack.Navigator>
  );
}
