import React from "react";
import { View, Text } from "react-native";
import Default from "../Screens/WR/Default";
import Description from "../Screens/WR/Description";
import { createStackNavigator } from "@react-navigation/stack";

const ResourcesStack = createStackNavigator();

export default function ResourceStack() {
  return (
    <ResourcesStack.Navigator screenOptions={{ headerShown: false }}>
      <ResourcesStack.Screen name="Default" component={Default} />
      <ResourcesStack.Screen name="Description" component={Description} />
    </ResourcesStack.Navigator>
  );
}
