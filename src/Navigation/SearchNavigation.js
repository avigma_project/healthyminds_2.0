import React from "react";
import { View, Text } from "react-native";
import Welcome from "../Screens/FMHP/Welcome";
import FindHealthProvider from "../Screens/FMHP/FindHealthProvider";
import WebViewList from "../Screens/FMHP/WebViewList";
import WelcomeScreen from "../Auth/WelcomeScreen";
import SignUp from "../Auth/SignUp";
import FindPassword from "../Auth/FindPassword";
import ChangePassword from "../Auth/ChangePassword";
import Login from "../Auth/Login";
import MyTabs from "./TabNavigation";
import Profile from "../Screens/Profile";
import EditProfile from "../Screens/EditProfile";
import { createStackNavigator } from "@react-navigation/stack";
const SearchStack = createStackNavigator();

export default function Searchstack() {
  return (
    <SearchStack.Navigator screenOptions={{ headerShown: false }}>
      {/* <SearchStack.Screen name="WelcomeScreen" component={WelcomeScreen} /> */}
      <SearchStack.Screen name="Welcome" component={Welcome} />
      <SearchStack.Screen
        name="FindHealthProvider"
        component={FindHealthProvider}
      />
      <SearchStack.Screen name="WebViewList" component={WebViewList} />
      <SearchStack.Screen name="Login" component={Login} />
      <SearchStack.Screen name="SignUp" component={SignUp} />
      <SearchStack.Screen name="FindPassword" component={FindPassword} />
      {/* <Stack.Screen name="Dashboard" component={Dashboard} /> */}
      <SearchStack.Screen name="Profile" component={Profile} />
      <SearchStack.Screen name="EditProfile" component={EditProfile} />
      <SearchStack.Screen name="ChangePassword" component={ChangePassword} />
    </SearchStack.Navigator>
  );
}
