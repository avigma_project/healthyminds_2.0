import React from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Image,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import strings from "../Language/Language";
import ScreeningStack from "./ScreeningNavigation";
import Searchstack from "./SearchNavigation";
import ResourceStack from "./ResourcesNavigation";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const Tab = createBottomTabNavigator();

export default function MyTabs({ navigation }) {
  let heightbar;
  if (deviceHeight > 700) {
    heightbar = deviceHeight / 10.5;
  } else {
    heightbar = 49;
  }
  return (
    <Tab.Navigator
      // initialRouteName="Search"
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let image;
          if (route.name === "Search") {
            image = focused
              ? require("../Images/GroupSearchColor.png")
              : require("../Images/GroupSearch.png");
          } else if (route.name === "Screening") {
            image = focused
              ? require("../Images/GroupScreeningColor.png")
              : require("../Images/GroupScreening.png");
          } else if (route.name === "Resources") {
            image = focused
              ? require("../Images/GroupResourcesColor.png")
              : require("../Images/GroupResources.png");
          }
          return (
            <Image
              source={image}
              style={{ height: 20, width: 20, resizeMode: "stretch" }}
            />
          );
        },
      })}
      tabBarOptions={{
        activeTintColor: "#377867",
        inactiveTintColor: "#828282",
        keyboardHidesTabBar: true,
        style: {
          backgroundColor: "#FFFFFF",
          height: Platform.OS === "ios" ? heightbar : 55,
        },
      }}
    >
      <Tab.Screen
        name="Search"
        component={Searchstack}
        listeners={({ navigation, route }) => ({
          tabPress: async (e) => {
            navigation.navigate("Search", {
              screen: "Welcome",
            });
          },
        })}
      />
      <Tab.Screen
        name="Screening"
        component={ScreeningStack}
        listeners={({ navigation, route }) => ({
          tabPress: async (e) => {
            navigation.navigate("Screening", {
              screen: "GetStarted",
            });
          },
        })}
      />
      <Tab.Screen name="Resources" component={ResourceStack} />
    </Tab.Navigator>
  );
}
