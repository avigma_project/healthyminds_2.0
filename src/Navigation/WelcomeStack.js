import React from "react";
import WelcomeScreen from "../Auth/WelcomeScreen";
import { createStackNavigator } from "@react-navigation/stack";
const WelcomeStackNav = createStackNavigator();

export default function WelcomeStack() {
  return (
    <WelcomeStackNav.Navigator screenOptions={{ headerShown: false }}>
      <WelcomeStackNav.Screen name="WelcomeScreen" component={WelcomeScreen} />
    </WelcomeStackNav.Navigator>
  );
}
