// ES6 module syntax
import LocalizedStrings from "react-native-localization";

// CommonJS syntax
// let LocalizedStrings  = require ('react-native-localization');

let strings = new LocalizedStrings({
  en: {
    clickhereforeval: "Click here for Evaluation",
    nearcenter: "health centers near",
    showing: "Showing",
    visitwebsite: "Visit Website",
    expertise: "Expertise",
    miles: "miles",
    direction: "Direction",
    emal: "Email",
    dbirth: "Date of Birth",
    nam: "Name",
    editprofile: "Edit Profile",
    confrimnewpass: "Confirm your new Password",
    setupnewpass: "Set up your new Password",
    currpass: "Current password",
    atleast: "Contains at least 6 letters or numbers",
    changepass: "Change Password",
    allcomplete: "You have completed all tests.",
    ptsdcomplete:
      "Your PTSD screening is complete. Next, please select a test you are interested in taking.",
    anxcomplete:
      "Your Anxiety screening is complete. Next, please select a test you are interested in taking",
    bipcomplete:
      "Your Bipolar screening is complete. Next, please select a test you are interested in taking.",
    nextwe: "Next we need to ask you some questions regarding Depression.",
    depcomplete:
      "Your Depression test is complete. Next, please select a test you are interested in taking.",
    resources: "Resources",
    start: "Start",
    save: "Save",
    result: "Result",
    retake: "Retake Test",
    constint: "You are likely consistent with",
    neededstr: "A complete evaluation is strongly recommended",
    youlikely:
      "You are unlikely consistent with any mental issues. Please check back to screening again in 3-6 months",
    hi: "Hi",
    myprofile: "My profile",
    tellus:
      "Tell us more details about these subtance(s) you checked above in the past three months",
    nosubst: "No, I’ve never used substances above",
    used: "used? (Non-medical use only). Check all that apply",
    inurlife: "In your life, which of the following substances have you",
    sut: "Substance Use test",
    ever: "ever",
    pleasetell: "Please tell us about how you have felt most days in the",
    ptsddesc:
      "If at any time you have experienced or witnessed a traumatic event, which involves loss of life, serious injury or threat of either",
    ptsdscreeen: "PTSD screening",
    pw: "past week",
    plese: "Please tell us how you have felt most days in the",
    as: "Anxiety screening",
    psm: "past six months",
    howmuch:
      "How much of a problem did any of these cause you - like being unable to work; having family, money or legal troubles; getting into arguments or fights",
    ifyes:
      "You checked YES to more than one of the above. Have several of these ever happened during the same period of time",
    bipodesc:
      "Has there ever been a period of time when you were not your usual self and",
    biposcreen: "Bipolar screening",
    ovprw: "over the past two weeks",
    biodesc: "Please tell us how you have felt",
    toast1: "Please fill all details",
    finish: "Finish",
    getstart: "Get started",
    des1:
      "Click below to complete an anonymous screening to evaluate your mental health",
    des2:
      "The screenings cover Depression, Anxiety, Post-traumatic Stress Disorder, Mood Disorder, and Substance Use",
    hithere: "Hi there",
    opttitle2: "First of all, let’s get to know you!",
    thank: "Thank you",
    basinform: "Basic information",
    dpsceen: "Depression screening",
    bipsceen: "Bipolar screening (optional)",
    axnsceen: "Anxiety screening (optional)",
    ptsdsceen: "P.T.S.D screening (optional)",
    ptsds: "P.T.S.D",
    subsuse: "Substance Use test (optional)",
    subsuse1: "Substance",
    ctr: "Continue to result",
    optiontitle: "First of all, update your Basic Information with any changes",
    ydob: "Your Date of Birth",
    upload: "Upload",
    edit: "Edit",
    camera: "Camera",
    gallery: "Gallery",
    listview: "List View",
    mapview: "Map View",
    biop: "Bi-Polar",
    anxt: "Anxiety Disorder",
    depres: "Depression",
    logsin: "Log in/Sign up",
    profile: "My Profile",
    title1: "Welcome to Mental Health Screening!",
    title: "Mental Health Check-Up",
    description1:
      "This is the start to managing your mental health.Please let us know what are you here for today:",
    description: "Thanks for downloading our application.",
    description2:
      "Please fill out screening questionnaire so that we can assist you.",
    contact: "Contact Us",
    button1: "Let's start",
    button2: "Find a Mental Health Provider",
    button3: "Mental Health Screening",
    button4: "Well Resources",
    login: "Log In",
    signup: "Sign Up",
    welcome: "Welcome back",
    email: "Your Email Address",
    password: "Your Password",
    forgetpassword: "Forgot your password?",
    eplaceholder: "How can we contact you?",
    pplaceholder: "Contains at least 6 letters or numbers",
    signuptitle: "We are glad to have you here",
    wecall: "1. What should we call you ?",
    isgender: "3. What is your gender identity?",
    isage: "2. What is your age?",
    name: "Your Name",
    nameplaceholder: "How can we call you?",
    passsetup: "Set up Your Password",
    passsetupplaceholder: "Contains at least 6 letters or numbers",
    confirmpass: "Comfirm Your Password",
    confirmpassplaceholder: "Re-enter your password",
    dashwelcome: "Welcome",
    dashwelcometext: "How can we help you today?",
    findpass: "Find Password",
    findtitle: "Don't worry,we got your back",
    findplaceholder: "Using your email to reset your password",
    sendlink: "Send Link to Email",
    gotoprofile: "Go to My Profile",
    logout: "Log out",
    HomeName: "What's your First name",
    HomeAge: "What's your Age",
    HomeZipcode: "What's your Zipcode",
    Next: "Next",
    //personal details
    basic: "Basic information",
    identity: "What is your identity",
    status: "What is your partnership status?",
    racial: "What is your racial/ethnic identity?",
    treatment: "Have you ever been treated for:(Check all that apply)",
    suicide: "Have you ever attempted suicide?",
    //drop1
    Female: "Non-binary",
    Female1: "Female",
    Male1: "Male",
    Transgender1: "Transgender",
    Male: "I prefer another identity",
    Transgender: "Prefer not to say",
    other: "Other",
    othTreat: "Enter other treatments",
    // drop2
    isstatus: "4. What is your partnership status?",
    married: "Married or registered domestic partnership",
    partner: "Living with my partner",
    living: "I am partnered, living separately",
    separated: "I am divorced or separated",
    single: "I am single",
    widowed: "I am widowed",
    // drop3
    isracial: "5. What is your racial/ethnic identity?",
    black: "Hispanic/Latino",
    indian: "Multiracial/ Ethnic",
    African: "Black or African American",
    native: "Native Hawaiian/Pacific Islander",
    white: "White",
    asian: "Asian",
    Alaska: "American Indian/Alaska Native",
    Hawaiian: "Native Hawaiian/Pacific Islander",
    mixed: "Mixed Race",
    other: "Other",
    identidad: "Other race/Identity",
    //radio
    previous: "Previous",
    bipolor1: "“Yes” to 7 of the 13 items in question number 1",
    bipolor2: "“Yes” to question number 2",
    bipolor3: "“Moderate” or “Serious” to question number 3",
    biponeg: "You are considered negative for a bipolar disorder evaluation",
    bipopos: "Symptoms are suggestive of bipolar disorder.",
    Depression: "Over the past two weeks, how often have you?",
    MQD:
      "Has there ever been a period of time when you were not your usual self and..",
    ADS:
      "These questions are to ask about things you may have felt most days in the past six  months!!",
    PTSD:
      "Please respond to these questions regarding your past shocking or painful event. How have you felt most days in the past week?",
    MQD1:
      "How much of a problem did any of these cause you - like being unable to work; having family, money or legal troubles; getting into arguments or fights?",

    problem: "No problem",
    problem1: "Minor problem",
    problem2: "Moderate problem",
    problem3: "Serious problem",

    Yes: "Yes",
    No: "No",
    selectOptions: "Please select an option...",
    issuicide: "8. Have you ever attempted suicide?",
    isuicide1: "9. Have you ever attempted suicide?",
    ismedication: "7. Did treatment include medication?",
    little: "None or little of the time",
    Some: "Some of the time",
    Most: "Most of the time",
    All: "All of the time",
    //Substance
    current: "I’m currently being treated",
    past: "I have received treatment in the past",
    never: "I have never been treated",
    n3months: "Yes, but not in the past 3 months",
    y3months: "Yes, in the past 3 months",
    m3never: "No, never",
    //result
    thankyou: "Thank you for your time.",
    details: "Details below:",
    result1:
      "This content is not intended to be a substitute for professional medical advice, diagnosis, or treatment.",
    result2: "Please close and restart the application to Fill new form.",

    SymptomsPTSD4: "Symptoms correspond to Post Traumatic Stress Disorder.",
    SymptomsPTSD23:
      "Symptoms may be consistent with Post Traumatic Stress Disorder.",
    SymptomsPTSD1:
      "Symptoms not consistent with Post Traumatic Stress Disorder.",

    DepEva:
      "In a self-selected population, the clinician should consider the possibility of the presence of an anxiety disorder instead of, or as well as, a depressive episode. Severity level is typically mild or moderate, depending upon the degree of impairment",
    DepEva1:
      "Presence of depressive disorder is very likely.  In this higher range, the severity level may be more severe and require immediate attention.",
    depressive: "Symptoms are not consistent with a depressive episode.",
    depressive1: "Symptoms are consistent with a depressive episode.",
    depressive2:
      "Symptoms are strongly consistent with criteria for a depressive episode.",
    Anxity: "Symptoms not suggestive of General Anxiety Disorder.",
    Anxity1: "Symptoms suggestive of General Anxiety Disorder.",
    tobacco1: "You are at a low risk for substance use problems.",
    tobacco2: "You are at a moderate risk for substance use problems.",
    tobacco3: "You are at a high risk for substance use problems.",
    tobalc1:
      "You are at a low risk of health and other problems from your current pattern of substance use.",
    tobalc2:
      "You are at a high risk of experiencing severe problems (health, social, financial, legal, relationships) as a result of your current pattern of use, and you are likely to be dependent.",
    Evaluation: "A complete Evaluation is strongly recommended",
    recommended: "Further evaluation is recommended",
    notRecommended: "A complete evaluation is NOT recommended. ",
    Evaluationrecommended: "A complete Evaluation is recommended",
    Need: "Need to be Evaluated for bipolar disorder.",
    Home: "Home",
    Search: "Search",
    Centers: "Health Centers",
    Screening: "Screening",
    Service: "Resources",
    Survey: "Survey",
    Resources: "Resources",
    skip: "Skip",
    continue: "Continue",
    emergency:
      "If you’re thinking about suicide or are worried about someone else, call the National Suicide Prevention Lifeline 1-800-273-TALK (8255), dial 911 or go/take them immediately to the nearest hospital emergency room for an evaluation",
    Return: "RETURN TO SCREENING",
    SearchWel:
      "Health E Livin is dedicated to providing mental health resources and access to care that is beneficial to your overall health.",
    Findmhs: "Find a health provider near you",
    searchcode: "Enter city name or zipcode",
    screen1_1: "Have you been feeling Sad, down or empty?",
    screen1:
      "Click Continue for DEPRESSION SCREENING or skip to go to next screening tool",

    screen2:
      "Click continue for BIPOLAR SCREEN or SKIP to go to next screening tool",
    screen2_2: "Have you been experiencing mood changes from very high to low?",

    screen3_3: "Are you constantly worried, nervous or stressed?",
    screen3:
      "Click Continue for GENERAL ANXIETY SCREENING or SKIP to go to next screening tool",

    screen4_4: "Troubled by a past shocking or painful event?",
    screen4:
      "Click Continue for POSTTRAUMATIC STRESS SCREEN or SKIP to go to conclude screenings",
    changeLanguage: "Do you want to change Language ?",
    botsign:
      "You can Sign up/ log in to save your results, and retake it anytime!",
    family:
      "Behavioral health conditions like this are treatable and most people find that they can make positive changes in their lives with professional help and the support of family and friends.",
    Never: "Never",
    Once: "Once or Twice",
    Monthly: "Monthly",
    Weekly: "Weekly",
    Daily: "Daily or Almost Daily",
  },

  sp: {
    clickhereforeval: "Haga clic aquí para evaluar",
    nearcenter: "centros de salud cercanos",
    showing: "Demostración",
    visitwebsite: "Visita la página web",
    expertise: "Pericia",
    miles: "millas",
    direction: "Dirección",
    emal: "Correo electrónico",
    dbirth: "Fecha de cumpleaños",
    nam: "Nombre",
    editprofile: "Editar perfil",
    confrimnewpass: "Confirma tu nueva contraseña",
    setupnewpass: "Configure su nueva contraseña",
    currpass: "Contraseña actual",
    atleast: "Contiene al menos 6 letras o números",
    changepass: "Cambiar la contraseña",
    allcomplete: "Ha completado todas las pruebas.",
    ptsdcomplete:
      "Su examen de PTSD está completo. A continuación, seleccione una prueba que le interese realizar.",
    anxcomplete:
      "Su examen de ansiedad está completo. A continuación, seleccione una prueba que le interese realizar",
    bipcomplete:
      "Su examen bipolar está completo. A continuación, seleccione una prueba que le interese realizar.",
    nextwe:
      "A continuación, debemos hacerle algunas preguntas sobre la depresión.",
    depcomplete:
      "Su prueba de depresión está completa. A continuación, seleccione una prueba que le interese realizar.",
    resources: "Recursos",
    start: "Comienzo",
    save: "Ahorrar",
    result: "Resultado",
    retake: "Volver a tomar la prueba",
    constint: "Probablemente seas consistente con",
    neededstr: "Se recomienda encarecidamente una evaluación completa",
    youlikely:
      "Es poco probable que sea consistente con algún problema mental. Vuelva a comprobarlo nuevamente en 3 a 6 meses",
    hi: "Hola",
    myprofile: "Mi perfil",
    tellus:
      "Cuéntenos más detalles sobre estas subvenciones que marcó anteriormente en los últimos tres meses",
    nosubst: "No, nunca he usado sustancias arriba",
    used: "¿usó? (Solo para uso no médico). Marque todo lo que corresponda",
    inurlife: "En su vida, ¿cuál de las siguientes sustancias le",
    sut: "Prueba de uso de sustancias",
    ever: "siempre",
    pleasetell: "Cuéntenos cómo se ha sentido la mayoría de los días en el",
    ptsddesc:
      "Si en algún momento ha experimentado o presenciado un evento traumático, que implique la pérdida de la vida, lesiones graves o amenaza de",
    ptsdscreeen: "Examen de PTSD",
    pw: "semana pasada",
    plese: "Díganos cómo se ha sentido la mayoría de los días en el",
    as: "Examen de ansiedad",
    psm: "últimos seis meses",
    howmuch:
      "¿Cuánto problema le causó alguno de estos? incapaz de trabajar; tener problemas familiares, económicos o legales; meterse en discusiones o peleas",
    ifyes:
      "Marcó SÍ en más de una de las opciones anteriores. ¿Han sucedido varios de estos durante el mismo período de tiempo",
    bipodesc:
      "¿Ha habido alguna vez un período de tiempo en el que no era su yo habitual y",
    biposcreen: "Cribado bipolar",
    ovprw: "durante las últimas dos semanas",
    biodesc: "Por favor, cuéntanos cómo te has sentido",
    toast1: "Por favor complete todos los detalles",
    finish: "Terminar",
    getstart: "Empezar",
    des1:
      "Haga clic a continuación para completar una prueba anónima para evaluar su salud mental",
    des2:
      "Las proyecciones cubren depresión, ansiedad, estrés postraumático Trastorno, trastorno del estado de ánimo y uso de sustancias",
    hithere: "Hola",
    opttitle2: "En primer lugar, ¡vamos a conocerte!",
    thank: "Gracias",
    basinform: "Información básica",
    dpsceen: "Examen de detección de depresión",
    bipsceen: "Cribado bipolar (opcional)",
    axnsceen: "Examen de ansiedad (opcional)",
    ptsdsceen: "Detección de P.T.S.D (opcional)",
    ptsds: "P.T.S.D",
    subsuse1: "sustancias",
    subsuse: "Prueba de uso de sustancias (opcional)",
    ctr: "Continuar resultando",
    optiontitle:
      "En primer lugar, actualice su información básica con cualquier cambio",
    ydob: "Tu fecha de nacimiento",
    upload: "Subir",
    edit: "Editar",
    camera: "Cámara",
    gallery: "Galería",
    listview: "Vista de la lista",
    mapview: "Vista del mapa",
    biop: "Bipolar",
    anxt: "Trastorno de ansiedad",
    depres: "Depresión",
    logsin: "Log in/Sign up",
    profile: "Mi perfil",
    screen1_1: "¿Te has sentido triste, deprimido o vacío?",
    screen1:
      "Haga clic en Continuar para EVALUAR DEPRESIÓN o saltar para pasar a la siguiente herramienta de evaluación",

    screen2_2: "¿Ha experimentado cambios de humor de muy alto a bajo?",
    screen2:
      "Haga clic en continuar para PANTALLA BIPOLAR o SALTAR para ir a la siguiente herramienta de detección",

    screen3_3: "¿Estás constantemente preocupado, nervioso o estresado?",
    screen3:
      "Haga clic en Continuar para EXAMEN DE ANSIEDAD GENERAL o SALTAR para ir a la siguiente herramienta de detección",

    screen4_4: "Preocupado por un evento pasado doloroso o impactante",
    screen4:
      "Haga clic en Continuar para la PANTALLA DE ESTRÉS POSTRAUMÁTICO o SALTAR para ir a las evaluaciones finales",

    title1: "¡Bienvenido a Mental Health Screening!",
    title: "Come vuoi il tuo uovo oggi?",
    contact: "Contáctanos",
    description: "Gracias por descargar nuestra aplicación.",
    description1:
      "Este es el comienzo para controlar su salud mental.Háganos saber para qué está aquí hoy:",
    description2:
      "Complete el cuestionario de selección para que podamos ayudarlo.",
    button1: "Empecemos",
    button2: "Encuéntrese un proveedor de salud mental",
    button3: "Tómese un prueba de salud mental",
    button4: "Recursos para la salud mental",
    login: "Acceso",
    signup: "Inscribirse",
    welcome: "Bienvenido de nuevo",
    email: "Su dirección de correo electrónico",
    password: "Tu contraseña",
    forgetpassword: "¿Olvidaste tu contraseña?",
    eplaceholder: "¿Como podemos contactarlo?",
    pplaceholder: "¿Como podemos contactarlo?",
    signuptitle: "Estamos encantados de contar contigo aquí",
    wecall: "1. ¿Cómo deberíamos llamarte?",
    isage: "2. ¿Qué edad tiene?",
    isgender: "3. ¿Cuál es su identidad de género?",
    name: "Tu nombre",
    nameplaceholder: "¿Cómo podemos llamarte?",
    passsetup: "Configura tu contraseña",
    confirmpass: "Confirme su contraseña",
    confirmpassplaceholder: "reingresa tu contraseña",
    passsetupplaceholder: "Contiene al menos 6 letras o números",
    dashwelcome: "Bienvenido",
    dashwelcometext: "¿Cómo podemos ayudarlo hoy?",
    logout: "Cerrar sesión",
    findpass: "Encontrar contraseña",
    findtitle: "No te preocupes, te apoyamos",
    findplaceholder:
      "Usando su correo electrónico para restablecer su contraseña",
    sendlink: "Enviar enlace a correo electrónico",
    gotoprofile: "Ir a mi perfil",
    HomeName: "Cuál es tu primer nombre",
    HomeAge: "Cual es tu edad",
    HomeZipcode: "Cuál es tu código ZIP",
    Next: "Próxima",
    //personal details
    identity: "Cual es tu identidad",
    basic: "Información básica",
    status: "¿Cuál es su estado de sociedad?",
    racial: "¿Cuál es su identidad racial / étnica?",
    treatment:
      "¿Alguna vez ha sido tratado por:(Marque todo lo que corresponda)",
    suicide: "¿Alguna vez ha intentado suicidarse?",
    //drop1
    Female: "No binario",
    Female1: "Hembra",
    Male1: "Masculino",
    Transgender1: "Transgénero",
    Male: "Prefiero otra identidad",
    Transgender: "Prefiero no decirlo",
    other: "Otra",
    othTreat: "Ingrese otros tratamientos",
    //drop2
    isstatus: "4. ¿Cuál es su estado de sociedad?",
    married: "Pareja de hecho casada o registrada",
    partner: "Viviendo con mi pareja",
    living: "Estoy en pareja, viviendo por separado",
    separated: "Estoy divorciado o separado",
    single: "Estoy soltero",
    widowed: "Soy viudo",
    //drop3
    isracial: "5. ¿Cuál es su identidad racial / étnica?",
    black: "Hispana / latina",
    indian: "Multirracial / étnico",
    African: "Negro o afroamericano",
    Alaska: "Indio americano / nativo de Alaska",
    native: "Nativo hawaiano / isleño del Pacífico",
    white: "Blanco",
    asian: "Asiático",
    Hawaiian: "Nativo hawaiano / isleño del Pacífico",
    mixed: "Mixed Race",
    other: "Otro",
    identidad: "otra raza / identidad",
    //radio
    previous: "Previo",
    bipolor1: "'Sí' a 7 de los 13 elementos de la pregunta número 1",
    bipolor2: "'Sí' a la pregunta número 2",
    bipolor3: "'Moderado' o 'Grave' a la pregunta número 3",
    biponeg:
      "Se le considera negativo para una evaluación de trastorno bipolar.",
    bipopos: "Los síntomas sugieren un trastorno bipolar.",
    Depression:
      "Durante las últimas dos semanas, ¿con qué frecuencia lo ha hecho?",
    MQD:
      "¿Ha habido alguna vez un período de tiempo en el que no era usted mismo y ...",
    ADS:
      "¡Estas preguntas son para preguntar sobre cosas que quizás haya sentido la mayoría de los días en los últimos seis meses!",
    PTSD:
      "Responda a estas preguntas sobre su pasado doloroso o impactante. Cómo ¿Ha sentido la mayoría de los días durante la semana pasada?",
    MQD1:
      "¿Cuánto problema le causó alguno de estos? Por ejemplo, no poder trabajar; tener problemas familiares, económicos o legales; metiéndose en discusiones o peleas?",

    problem: "No hay problema",
    problem1: "Problema menor",
    problem2: "Problema moderado",
    problem3: "Problema serio",

    Yes: "Si",
    No: "No",
    selectOptions: "Por favor seleccione una opción...",
    issuicide: "8. ¿Alguna vez ha intentado suicidarse?",
    isuicide1: "9. ¿Ha intentado suicidarse alguna vez?",
    ismedication: "7. ¿El tratamiento incluyó medicación?",
    little: "Ninguno o poco del tiempo",
    Some: "Algo de tiempo",
    Most: "La mayor parte del tiempo",
    All: "Todo el tiempo",
    //Substance
    current: "Actualmente estoy siendo tratado",
    past: "He recibido tratamiento en el pasado",
    never: "Nunca he sido tratado",
    n3months: "Sí, pero no en los últimos 3 meses.",
    y3months: "Sí, en los últimos 3 meses",
    m3never: "No nunca",
    //result
    thankyou: "Gracias por tu tiempo.",
    details: "Detalles abajo:",
    result1:
      "Este contenido no pretende sustituir el asesoramiento, diagnóstico o tratamiento médico profesional.",
    result2:
      "Cierre y reinicie la aplicación para completar un nuevo formulario",
    Evaluation: "Se recomienda encarecidamente una evaluación completa",
    recommended: "Se recomienda una evaluación adicional",
    notRecommended: "No se recomienda una evaluación completa",
    Evaluationrecommended: "Se recomienda una evaluación completa",
    Need: "Necesita ser evaluado para el trastorno bipolar.",
    Home: "Casa",
    Search: "Buscar",
    Centers: "Centros de salud",
    Screening: "Poner en pantalla",
    Service: "Recursos",
    Survey: "Encuesta",
    Resources: "Recursos",
    skip: "omitir",
    continue: "Seguir",
    emergency:
      "Si está pensando en suicidarse o está preocupado por otra persona, llame a la Línea Nacional de Prevención del Suicidio al 1-800-273-TALK (8255), marque el 911 o llévelos de inmediato a la sala de emergencias del hospital más cercano para una evaluación.",
    Return: "VOLVER A PRUEBAS",
    SearchWel:
      "Health E Livin se dedica a brindar recursos de salud mental y acceso a atención que sea beneficiosa para su salud en general.",
    Findmhs: "Encuentre un proveedor de salud cerca de usted",
    searchcode: "Ingrese el nombre de la ciudad o el código postal",
    SymptomsPTSD4:
      "Los síntomas corresponden al trastorno de estrés postraumático.",
    SymptomsPTSD23:
      "Los síntomas pueden ser compatibles con el trastorno de estrés postraumático.",
    SymptomsPTSD1:
      "Los síntomas no son compatibles con el trastorno de estrés postraumático.",

    depressive: "Los síntomas no son compatibles con un episodio depresivo.",
    depressive1: "Los síntomas son consistentes con un episodio depresivo.",
    depressive2:
      "Los síntomas son muy consistentes con criterios para un episodio depresivo.",
    Anxity: "Síntomas que no sugieren un trastorno de ansiedad generalizada.",
    Anxity1: "Síntomas sugestivos de trastorno de ansiedad generalizada.",
    tobacco1: "Tiene un riesgo bajo de tener problemas de uso de sustancias.",
    tobacco2: "Tiene un riesgo moderado de problemas de uso de sustancias.",
    tobacco3: "Tiene un alto riesgo de tener problemas de uso de sustancias.",
    tobalc1:
      "Usted tiene un riesgo bajo de tener problemas de salud y de otro tipo debido a su patrón actual de consumo de sustancias.",
    tobalc2:
      "Usted corre un alto riesgo de experimentar problemas graves (de salud, sociales, financieros, legales, de relaciones) como resultado de su patrón actual de uso y es probable que sea dependiente.",
    DepEva:
      "En una población autoseleccionada, el médico debe considerar la posibilidad de la presencia de un trastorno de ansiedad en lugar de, o además, de un episodio depresivo. El nivel de gravedad suele ser leve o moderado, según el grado de deterioro.",
    DepEva1:
      "Es muy probable que exista un trastorno depresivo. En este rango más alto, el nivel de gravedad puede ser más severo y requerir atención inmediata.",
    changeLanguage: "¿Quieres cambiar de idioma?",
    botsign:
      "¡Puede registrarse / iniciar sesión para guardar sus resultados y volver a tomarlos en cualquier momento!",
    family:
      "Las condiciones de salud del comportamiento como esta son tratables y la mayoría de las personas descubren que pueden hacer cambios positivos en sus vidas con ayuda profesional y el apoyo de familiares y amigos.",
    Never: "Nunca",
    Once: "Una o dos veces",
    Monthly: "Mensual",
    Weekly: "Semanal",
    Daily: "Diariamente o casi a diario",
  },
});
// let stringsSpanish =({
//     sp:{
//         how:"Chequeo de salud mental",
//         boiledEgg:"Boiled egg",
//         softBoiledEgg:"Soft-boiled egg",
//         choice:"How to choose the egg"
//       },
// })

// export default  getLanguage = (selectedLang) =>{

//     if(selectedLang == "en"){
//         return stringsEN ;
//     }
//     else{
//         return stringsSpanish ;
//     }

// }

export default strings;
