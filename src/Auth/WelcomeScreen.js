import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  Linking,
  Platform,
  Dimensions,
  Alert,
  Image,
} from "react-native";
import CustomButton from "../CustomFolder/CustomButton";
import Header from "../CustomFolder/Header";
import strings from "../Language/Language";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Spinner from "react-native-loading-spinner-overlay";
import Geolocation from "react-native-geolocation-service";
import { Toast } from "native-base";
import axios from "axios";
import * as database from "../Database/allSchemas";
import { API } from "../Api/ApiUrl";
import { userprofile, getdata } from "../Api/function";
import appConfig from "../../app.json";
const deviceHeight = Dimensions.get("window").height;

export default class WelcomeScreen extends Component {
  state = {
    Ltype: 1,
    textValue: "Spanish",
    isLoading: false,
    firstname: null,
    token: "",
    // currentLatitude: 0,
    // currentLongitude: 0,
  };

  componentDidMount() {
    const { navigation, route } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      // this.setState({ isLoader: true }, () => this.getLocation());
    

      this.getToken();
    });
    this.getLanguage();
  }

  componentWillUnmount() {
    this._unsubscribe;
  }

  // hasLocationPermissionIOS = async () => {
  //   const openSetting = () => {
  //     Linking.openSettings().catch(() => {
  //       Alert.alert("Unable to open settings");
  //     });
  //   };
  //   const status = await Geolocation.requestAuthorization("whenInUse");
  //   console.log("Check");
  //   if (status === "granted") {
  //     console.log("granted");
  //     return true;
  //   }

  //   if (status === "denied") {
  //     Alert.alert(
  //       `Turn on Location Services to allow "${
  //         appConfig.displayName
  //       }" to determine your location.`,
  //       "",
  //       [
  //         {
  //           text: "Don't Use Location",
  //           onPress: () => {
  //             BackHandler.exitApp();
  //           },
  //         },
  //       ]
  //     );

  //     console.log("denied");
  //   }

  //   if (status === "disabled") {
  //     Alert.alert(
  //       `Turn on Location Services to allow "${
  //         appConfig.displayName
  //       }" to determine your location.`,
  //       "",
  //       [
  //         { text: "Go to Settings", onPress: openSetting },
  //         {
  //           text: "Don't Use Location",
  //           onPress: () => {
  //             BackHandler.exitApp();
  //           },
  //         },
  //       ]
  //     );

  //     console.log("disable");
  //   }

  //   return false;
  // };

  // hasLocationPermission = async () => {
  //   if (Platform.OS === "ios") {
  //     const hasPermission = await this.hasLocationPermissionIOS();
  //     return hasPermission;
  //   }

  //   if (Platform.OS === "android" && Platform.Version < 23) {
  //     return true;
  //   }

  //   const hasPermission = await PermissionsAndroid.check(
  //     PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
  //   );

  //   if (hasPermission) {
  //     return true;
  //   }

  //   const status = await PermissionsAndroid.request(
  //     PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
  //   );

  //   if (status === PermissionsAndroid.RESULTS.GRANTED) {
  //     return true;
  //   }

  //   if (status === PermissionsAndroid.RESULTS.DENIED) {
  //     ToastAndroid.show(
  //       "Location permission denied by user.",
  //       ToastAndroid.LONG
  //     );
  //   } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
  //     ToastAndroid.show(
  //       "Location permission revoked by user.",
  //       ToastAndroid.LONG
  //     );
  //   }

  //   return false;
  // };

  // getLocation = async () => {
  //   const hasLocationPermission = await this.hasLocationPermission();

  //   if (!hasLocationPermission) {
  //     console.log("POst");
  //     return;
  //   }

  //   this.setState({ loading: true }, () => {
  //     Geolocation.getCurrentPosition(
  //       (position) => {
  //         this.setState(
  //           {
  //             currentLatitude: position.coords.latitude,
  //             currentLongitude: position.coords.longitude,
  //             loading: false,
  //           },
  //           () => this.saveLocation()
  //         );
  //       },
  //       (error) => {
  //         this.setState({ loading: false });
  //         console.log(error);
  //       },
  //       {
  //         accuracy: {
  //           android: "high",
  //           ios: "best",
  //         },
  //         enableHighAccuracy: this.state.highAccuracy,
  //         timeout: 15000,
  //         maximumAge: 10000,
  //         distanceFilter: 0,
  //         forceRequestLocation: this.state.forceLocation,
  //         showLocationDialog: this.state.showLocationDialog,
  //       }
  //     );
  //   });
  // };

  // saveLocation = async () => {
  //   const { currentLatitude, currentLongitude } = this.state;
  //   try {
  //     const latitude = ["latitude", JSON.stringify(currentLatitude)];
  //     const longitude = ["longitude", JSON.stringify(currentLongitude)];
  //     await AsyncStorage.multiSet([latitude, longitude]);
  //     console.log("Set: ", latitude, longitude);
  //   } catch (error) {
  //     console.log(" Location error ", error);
  //   }
  // };

  async GetMasterDetails(token) {
    console.log("ltype data", this.state.Ltype);
    // const data = JSON.stringify({"Ltype":2})
    // console.log("Ashok",data)
    this.setState({ isLoading: true });
    var data = JSON.stringify({
      Ltype: this.state.Ltype,
      // HC_Lat: this.state.currentLatitude,
      // HC_Long: this.state.currentLongitude,
    });
    //debugger;
    try {
      getdata(data, token).then((response) => {
        console.log("response ---dfiffff", response);
        this.setState({ isLoading: false });
        database.deletePatientMaster().catch((error) => console.log(error));
        database.deleteFinal_Scores_Schema().catch((Error) => {
          console.log(Error);
        });
        database.deletePatientDepression().catch((error) => console.log(error));
        database
          .deletePatientDepression2()
          .catch((error) => console.log(error));
        database
          .deletePatientDepression3()
          .catch((error) => console.log(error));
        database
          .deletePatientDepression4()
          .catch((error) => console.log(error));
        database
          .deleteSubstanceUseMaster()
          .catch((error) => console.log(error));
        database
          .deleteSubstanceOptionMaster()
          .catch((error) => console.log(error));

        // database
        //   .deleteSubstanceHistoryMaster()
        //   .catch((error) => console.log(error));
        database
          .deletePatient_Treated_Schema()
          .catch((error) => console.log(error));
        database.deletePatientSurveyMaster_Schema().catch((error) => {
          console.log(error);
        });
        database.deletePatientHealthCenters_Schema().catch((error) => {
          console.log(error);
        });
        database.deletePatientTechniques_List_Schema_Schema().catch((error) => {
          console.log(error);
        });

        this.setState({ isLoading: false });

        if (response.data != "") {
          if (response.data[0] != "") {
            database
              .InsertPatientDepressionMaster(response.data[0])
              .catch((error) => console.log(error));

            // ToastAndroid.showWithGravity(
            //     "InsertPatientDepressionMaster",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
            console.log(response, "Spanish");
          }
          if (response.data[1] != "") {
            database
              .InsertPatientMoodDisorderMaster(response.data[1])
              .catch((error) => console.log(error));
            // ToastAndroid.showWithGravity(
            //     "InsertPatientMoodDisorderMaster",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
          }
          if (response.data[2] != "") {
            database
              .InsertPatientAnxietyDisorderMaster(response.data[2])
              .catch((error) => console.log(error));
            // ToastAndroid.showWithGravity(
            //     "InsertPatientAnxietyDisorderMaster",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
          }
          if (response.data[3] != "") {
            database
              .InsertPatientPTSDMaster(response.data[3])
              .catch((error) => console.log(error));
            // ToastAndroid.showWithGravity(
            //     "InsertPatientPTSDMaster",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
          }
          if (response.data[4] != "") {
            // console.log('repson 3', response.data[4]);
            database
              .InsertPatientTreatedMaster(response.data[4])
              .catch((error) => console.log(error));
            // ToastAndroid.showWithGravity(
            //     "InsertPatientTreatedMaster",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
          }
          if (response.data[5][0] != "") {
            // console.log('repson 3', response.data[5][0]);
            database
              .InsertPatientSurveyMaster(response.data[5][0])
              .catch((error) => console.log(error));
            // ToastAndroid.showWithGravity(
            //     "InsertPatientTreatedMaster",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
            // console.log("Ashok final response", response.data[5][0])
          }
          if (response.data[6][0] != "") {
            // console.log('repson 3', response.data[6][0]);
            database
              .InsertHealthCenterDetails(response.data[6][0])
              .catch((error) => console.log(error));
            // ToastAndroid.showWithGravity(
            //     "InsertHealthCenterDetails",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
            // console.log("Ashok  InsertHealthCenterDetails", response.data[6][0])
          }

          if (response.data[7][0] != "") {
            // console.log('repson 3', response.data[7][0]);
            database
              .InsertTechniques_List_SchemaDetails(response.data[7][0])
              .catch((error) => console.log(error));
            // ToastAndroid.showWithGravity(
            //     "InsertTechniques_List_SchemaDetails",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
            // console.log("Ashok  InsertTechniques_List_SchemaDetails", response.data[7][0])
          }
          if (response.data[8] != "") {
            database
              .InsertSubstanceOptionMaster(response.data[8])
              .catch((error) => console.log(error));
          }
          if (response.data[9] != "") {
            database
              .InsertSubstanceUseMaster(response.data[9])
              .catch((error) => console.log(error));
          }
          // if (response.data[10] != "") {
          //   // console.log(
          //   //   response.data[10],
          //   //   "response.data[10]auhashdiuhduihudhuihuihauidh"
          //   // );
          //   database
          //     .InsertPatientMaster(response.data[10])
          //     .catch((error) => console.log(error));
          // }
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      // console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
        this.setLanguage("en");
      } else {
        if (get_Data == "2") {
          strings.setLanguage("sp");
          this.setLanguage("sp");
        } else {
          strings.setLanguage("en");
          this.setLanguage("en");
        }
      }
    });
  }

  async setLanguage(languageCode) {
    // console.log(languageCode)
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        // console.log("refresh", get_Data);
        this.setState({
          Ltype: 1,
          language: false,
        });
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
        this.setState({
          Ltype: 2,
          language: false,
        });
      });
    }
    this.GetMasterDetails();
    // getLanguage(languageCode)
  }

  async componentDidUpdate() {
    // this.getLanguage()
    if (this.state.update === true) {
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log(get_Data, "Change language");
        if (get_Data === 1) {
          this.setState({ update: false });
          this.GetMasterDetails();
        } else {
          if (get_Data === 2) {
            this.setState({ update: false });
            this.GetMasterDetails();
          }
        }
      });
    }
  }

  onPress = () => {
    if (this.state.textValue == "Spanish") {
      this.setState({
        textValue: "English",
      });
      this.setLanguage("sp");
    } else {
      this.setState({
        textValue: "Spanish",
      });
      this.setLanguage("en");
    }
  };

  getUserData = async (token) => {
    this.setState({
      isLoading: true,
    });
    var data = JSON.stringify({
      Type: 2,
    });
    try {
      const res = await userprofile(data, token);
      console.log(res, "resssss");
      this.setState({
        firstname: res[0][0].User_Name,
        isLoading: false,
      });
      console.log(this.state.firstname, "namemmemmeme");
    } catch (error) {
      console.log("hihihihihihih", { e: error.response.data.error });
      let message = "";
      if (error.response) {
        this.setState({ isLoading: false });
      } else {
        message = "";
      }
      console.log({ message });
    }
  };

  getToken = async () => {
    let token;
    try {
      token = await AsyncStorage.getItem("token");
      if (token) {
        this.getUserData(token);
        this.GetMasterDetails(token);
        this.setState({ token: token });
      } else {
        console.log("no token found");
      }
    } catch (e) {
      console.log(e);
    }
  };
  logOut = async (token) => {
    await AsyncStorage.removeItem("token");
    this.setState({ token: token });
    delete axios.defaults.headers.common["TOKEN"];
    this.props.navigation.navigate("Login");
  };
  contact = () => {
    Linking.openURL("https://www.healthelivin.org/contact-us/");
  };

  render() {
    return (
      <SafeAreaView
        style={{
          height: "100%",
          backgroundColor: "#fff",
          // backgroundColor: "#E5E5E5"
        }}
      >
        <ScrollView contentContainerStyle={{ marginHorizontal: 16 }}>
          <View style={{}}>
            <Spinner visible={this.state.isLoading} />
            <View
              style={{
                width: "100%",
                // backgroundColor: 'red',
                justifyContent: "flex-end",
                flexDirection: "row",
                marginTop: 8,
                marginBottom: 16,
              }}
            >
              <TouchableOpacity
                style={{
                  backgroundColor: "#F8D470",
                  paddingHorizontal: 12,
                  paddingVertical: 8,
                  // width: '20%',
                  justifyContent: "center",
                  alignItems: "center",
                  borderRadius: 20,
                  marginRight: 20,
                }}
                onPress={this.onPress}
              >
                <Text
                  style={{
                    color: "#A1683A",
                    // fontWeight: "bold",
                    lineHeight: 20,
                    fontSize: 14,
                    fontFamily: "Quicksand-Regular",
                  }}
                >
                  {this.state.textValue}
                </Text>
              </TouchableOpacity>
            </View>
            {this.state.token != "" && this.state.token != undefined ? (
              <View
                style={{
                  justifyContent: "center",
                  // backgroundColor: 'red',
                  // alignItems: "center",
                  marginLeft: "5%",
                }}
              >
                <Text
                  style={{
                    fontSize: 25,
                    fontWeight: "700",
                    lineHeight: 32,
                    fontFamily: "Quicksand-Bold",
                  }}
                >
                  {strings.dashwelcome}, {this.state.firstname}!
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: "400",
                    lineHeight: 22,
                  }}
                >
                  {strings.dashwelcometext}
                </Text>
              </View>
            ) : (
              <View
                style={
                  {
                    // justifyContent: "center",
                    // backgroundColor: 'red',
                    // alignItems: "center",
                    // marginHorizontal: "8%",
                  }
                }
              >
                <Text
                  style={{
                    fontSize: 25,
                    fontWeight: "700",
                    lineHeight: 32,
                    fontFamily: "Quicksand-Bold",
                    // marginLeft: Platform.OS === "ios" ? "1%" : "6%",
                  }}
                >
                  {strings.title1}
                </Text>
                <Text
                  style={{
                    // marginLeft: "6%",
                    fontSize: 16,
                    fontWeight: "400",
                    lineHeight: 22,
                    // fontFamily: "OpenSansRoman-Light",
                    // textAlign: "center",
                    // right: Platform.OS === "ios" ? 15 : 0,
                  }}
                >
                  {strings.description1}
                </Text>
              </View>
            )}

            <View>
              <View style={{ marginTop: 32, alignItems: "center" }}>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("Search", {
                      screen: "Welcome",
                    })
                  }
                  style={{
                    backgroundColor: "#377867",
                    width: "95%",
                    height: 60,
                    justifyContent: "center",
                    alignItems: "center",
                    borderRadius: 10,
                  }}
                >
                  <Text
                    style={{
                      color: "#F8D470",
                      fontSize: 16,
                      fontWeight: "700",
                      lineHeight: 22,
                      left: this.state.Ltype != 1 ? 5 : 0,
                      fontFamily: "Quicksand-Bold",
                    }}
                  >
                    {strings.button2}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{ marginTop: 24 }}>
                <CustomButton
                  title={strings.button3}
                  redius={10}
                  width="95%"
                  height={60}
                  onPress={() => this.props.navigation.navigate("GetStarted")}
                />
              </View>
              <View style={{ marginTop: 24 }}>
                <CustomButton
                  title={strings.button4}
                  redius={10}
                  width="95%"
                  height={60}
                  onPress={() =>
                    this.props.navigation.navigate("Resources", {
                      screen: "Default",
                    })
                  }
                />
              </View>
            </View>
            {this.state.token != "" && this.state.token != undefined ? (
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  marginTop: "10%",
                }}
              >
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("Profile")}
                >
                  <Text
                    style={{
                      color: "#377867",
                      fontSize: 18,
                      fontWeight: "700",
                      lineHeight: 24,
                      // fontFamily: "Quicksand-Bold",
                    }}
                  >
                    {strings.gotoprofile}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ marginTop: 32 }}
                  onPress={() => this.logOut()}
                >
                  <Text
                    style={{
                      color: "#A1683B",
                      fontSize: 18,
                      fontWeight: "700",
                      lineHeight: 24,
                      // fontFamily: "Quicksand-Bold",
                    }}
                  >
                    {strings.logout}
                  </Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View
                style={{
                  flexDirection: "row",
                  // backgroundColor: 'red',
                  justifyContent: "center",
                  marginTop: 20,
                }}
              >
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("Login")}
                >
                  <Text
                    style={{
                      color: "#377867",
                      fontSize: 18,
                      fontWeight: "700",
                      lineHeight: 24,
                      // fontFamily: "Quicksand-Bold",
                      textDecorationLine: "underline",
                    }}
                  >
                    {strings.login} /{" "}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("SignUp")}
                >
                  <Text
                    style={{
                      color: "#377867",
                      fontSize: 18,
                      fontWeight: "700",
                      lineHeight: 24,
                      // fontFamily: "Quicksand-Bold",
                      textDecorationLine: "underline",
                    }}
                  >
                    {strings.signup}
                  </Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        </ScrollView>
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <TouchableOpacity
            style={{ position: "absolute", bottom: 32 }}
            onPress={() => this.contact()}
          >
            <Text
              style={{
                color: "#A1683B",
                fontWeight: "700",
                lineHeight: 24,
                // fontFamily: "Quicksand-Bold",
              }}
            >
              {strings.contact}
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-around",
            height: 49,
            borderTopColor: "lightgray",
            borderTopWidth: 0.5,
            alignItems: "center",
          }}
        >
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate("Search", {
                screen: "Welcome",
              })
            }
          >
            <Image
              source={require("../Images/GroupSearch.png")}
              style={{
                height: 20,
                width: 20,
                resizeMode: "stretch",
                alignSelf: "center",
              }}
            />
            <Text
              style={{
                color: "gray",
                fontSize: 10,
                // fontFamily: "Quicksand-Bold",
              }}
            >
              Search
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("GetStarted")}
          >
            <Image
              source={require("../Images/GroupScreening.png")}
              style={{
                height: 20,
                width: 20,
                resizeMode: "stretch",
                alignSelf: "center",
              }}
            />
            <Text
              style={{
                color: "gray",
                fontSize: 10,
                // fontFamily: "Quicksand-Bold",
              }}
            >
              Screening
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate("Resources", {
                screen: "Default",
              })
            }
          >
            <Image
              source={require("../Images/GroupResources.png")}
              style={{
                height: 20,
                width: 20,
                resizeMode: "stretch",
                alignSelf: "center",
              }}
            />
            <Text
              style={{
                color: "gray",
                fontSize: 10,
                // fontFamily: "Quicksand-Bold",
              }}
            >
              Resources
            </Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}
