import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  Linking,
} from "react-native";
import Header from "../CustomFolder/Header";
import CustomInput from "../CustomFolder/CustomInput";
import CustomButton from "../CustomFolder/CustomButton";
import { forgotpassword } from "../Api/function";
import strings from "../Language/Language";
import { Toast } from "native-base";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default class FindPassword extends Component {
  constructor() {
    super();
    this.state = {
      username: "",
      Type: 1,
      isLoading: false,
      ErrorUserName: null,
      msg: null,
      textValue: "Spanish",
    };
  }
  onUsernameChange = (username) => {
    this.setState({ username });
  };
  Validation = () => {
    this.setState({ isLoading: false });

    // console.log("hihihihiihihhi", this.state.username);
    // debugger;
    const invalidFields = [];

    if (!this.state.username) {
      invalidFields.push("username");
      this.setState({ ErrorUserName: "Email address is required" });
    } else {
      console.log("else");
      this.setState({ ErrorUserName: null });
    }
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(this.state.username) === false && this.state.username) {
      invalidFields.push("ErrorUserEmail");
      this.setState({ ErrorUserEmail: "Please enter valid email" });
    } else {
      this.setState({ ErrorUserEmail: null });
    }
    return invalidFields.length > 0;
  };
  ForgotPassword = async () => {
    const { username } = this.state;
    this.setState({
      ErrorUserName: null,
      isLoading: true,
      ErrorPassword: null,
    });
    let data = JSON.stringify({
      EmailID: username,
    });
    console.log("datadtaadtadag", data);
    if (username) {
      let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (reg.test(this.state.username) === false && this.state.username) {
        this.setState({
          ErrorUserEmail: "Please enter valid email",
          isLoading: false,
        });
      } else {
        this.setState({
          ErrorUserEmail: null,
          ErrorPassword: null,
          isLoading: true,
        });
        await forgotpassword(data)
          .then((res) => {
            console.log("res: ", res);
            this.setState({
              ErrorUserEmail: null,
              ErrorPassword: null,
              isLoading: false,
            });
            this.showMessage(
              "Link has been sent to your mentioned email address"
            );
            this.props.navigation.navigate("Login");
          })
          .catch((error) => {
            console.log("hihihihihihih", { e: error.response.data.error });
            let message = "";
            if (error.response) {
              const {
                data: { error_description },
              } = error.response;
              message = error_description;
              if (error.response.data.error === "-90") {
                this.showMessage("User is already exist with email");
              }
              this.setState({ isLoading: false });
            } else {
              message = "";
            }
            console.log({ message });
          });
      }
    } else {
      this.setState({
        ErrorUserName: "Email address is required",
        isLoading: false,
        ErrorPassword: null,
      });
      // this.setState({  });
    }
  };
  showMessage = (message) => {
    if (message !== "" && message !== null && message !== undefined) {
      Toast.show({
        text: message,
        // style: styles.toasttxt,
        duration: 5000,
      });
    }
  };

  componentDidMount() {
    this.getLanguage();
  }

  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
      } else {
        strings.setLanguage("sp");
      }
    });
  }

  async setLanguage(languageCode) {
    console.log({ languageCode });
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        this.setState({ Ltype: 1 });
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
        this.setState({ Ltype: 2 });
      });
    }
  }

  onPress = () => {
    // alert("hiiii");
    if (this.state.textValue == "Spanish") {
      this.setState({
        textValue: "English",
      });
      this.setLanguage("sp");
    } else {
      this.setState({
        textValue: "Spanish",
      });
      this.setLanguage("en");
    }
  };

  contact = () => {
    Linking.openURL("https://www.healthelivin.org/contact-us/");
  };

  render() {
    return (
      <SafeAreaView>
        <View
          style={{
            backgroundColor: "#fff",
            // backgroundColor: "#E5E5E5",
            height: "100%",
          }}
        >
          <View
            style={{
              width: "100%",
              // backgroundColor: 'red',
              justifyContent: "flex-end",
              flexDirection: "row",
              marginTop: 8,
              marginBottom: 16,
            }}
          >
            <TouchableOpacity
              style={{
                backgroundColor: "#F8D470",
                paddingHorizontal: 12,
                paddingVertical: 8,
                // width: '20%',
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 20,
                marginRight: 20,
              }}
              onPress={this.onPress}
            >
              <Text
                style={{
                  color: "#A1683A",
                  fontWeight: "bold",
                  lineHeight: 20,
                  fontSize: 14,
                  fontFamily: "Quicksand-Bold",
                }}
              >
                {this.state.textValue}
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              //   backgroundColor: 'red',
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text
              style={{
                fontWeight: "bold",
                marginBottom: 8,
                fontSize: 20,
                fontWeight: "700",
                lineHeight: 26,
                fontFamily: "Quicksand-Bold",
              }}
            >
              {strings.findpass}
            </Text>
            <Text
              style={{
                fontSize: 14,
                fontWeight: "400",
                lineHeight: 18,
                fontFamily: "Quicksand-Bold",
              }}
            >
              {strings.findtitle}
            </Text>
          </View>
          <View style={{ marginHorizontal: 16 }}>
            <CustomInput
              placeholder={strings.findplaceholder}
              heading={strings.email}
              placeholderTextColor="gray"
              value={this.state.username}
              onChangeText={this.onUsernameChange}
              keyboardType={"email-address"}
            />
          </View>
          <View style={{ width: "90%" }}>
            {this.state.ErrorUserName && (
              <Text style={styles.errortext}>{this.state.ErrorUserName}</Text>
            )}
          </View>
          <View style={{ width: "90%" }}>
            {this.state.ErrorUserEmail && (
              <Text style={styles.errortext}>{this.state.ErrorUserEmail}</Text>
            )}
          </View>
          <View style={{ marginTop: 35 }}>
            <CustomButton
              onPress={() => this.ForgotPassword()}
              title={strings.sendlink}
              redius={40}
              width="70%"
              height={40}
            />
          </View>

          <View
            style={{ position: "absolute", bottom: 20, alignSelf: "center" }}
          >
            <View>
              <TouchableOpacity onPress={() => this.contact()}>
                <Text style={{ color: "#A1683B" }}>{strings.contact}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  errortext: {
    color: "red",
    marginTop: 5,
    marginLeft: "10%",
  },
});
