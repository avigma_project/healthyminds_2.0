import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  Linking,
  Dimensions,
} from "react-native";
import Header from "../CustomFolder/Header";
import CustomInput from "../CustomFolder/CustomInput";
import CustomButton from "../CustomFolder/CustomButton";
import Spinner from "react-native-loading-spinner-overlay";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Feather from "react-native-vector-icons/Feather";
import { register } from "../Api/function";
import qs from "qs";
import axios from "axios";
import { Toast } from "native-base";
import strings from "../Language/Language";
const deviceHeight = Dimensions.get("window").height;

export default class SignUp extends Component {
  constructor() {
    super();
    this.state = {
      isShowPassword: true,
      firstname: null,
      username: null,
      password: null,
      access_token: "",
      clientid: 2,
      grant_type: "password",
      role: 2,
      isLoading: false,
      ErrorUserName: null,
      ErrorPassword: null,
      ErrorFirstName: null,
      msg: null,
      cpassword: null,
      textValue: "Spanish",
      hidePassword: true,
      hideConfirmPassword: true,
    };
  }
  onUsernameChange = (username) => {
    this.setState({ username });
  };

  onPasswordChange = (password) => {
    this.setState({ password });
  };
  onCPasswordChange = (cpassword) => {
    this.setState({ cpassword });
  };
  onFirstNameChange = (firstname) => {
    this.setState({ firstname });
  };
  managePasswordVisibility = () => {
    this.setState({ hidePassword: !this.state.hidePassword });
  };

  manageConfirmPasswordVisibility = () => {
    this.setState({ hideConfirmPassword: !this.state.hideConfirmPassword });
  };
  onPressRegister = async () => {
    console.log("onPressRegister");
    this.setState({ isLoading: false });
    const validate = this.Validation();
    console.log("validate", validate);
    if (!validate) {
      const {
        username,
        password,
        firstname,
        clientid,
        role,
        grant_type,
      } = this.state;

      this.setState({ isLoading: true });

      let data = qs.stringify({
        grant_type: grant_type,
        username: username,
        password: password,
        ClientId: clientid,
        FirstName: firstname,
        role: role,
      });
      console.log("data", data);
      await register(data)
        .then((res) => {
          console.log("res: ", res.access_token);
          this.setState({ isLoading: false });
          AsyncStorage.setItem("token", res.access_token);
          axios.defaults.headers.common["TOKEN"] = res.access_token;
          this.props.navigation.navigate("Search", {
            screen: "Welcome",
          });
        })
        .catch((error) => {
          console.log({ e: error.response.data.error });
          let message = "";
          if (error.response) {
            const {
              data: { error_description },
            } = error.response;
            message = error_description;
            if (error.response.data.error === "-99") {
              this.showMessage("User is already exist");
            }
            this.setState({ isLoading: false });
          } else {
            message = "";
          }
          console.log({ message });
        });
      // await register(data)
      //   .then(res => {
      //     console.log('res: positive', res, res.access_token);
      //     this.props.navigation.navigate('DashBoard');
      //     this.setState({isLoading: false, msg: 'Successfully register'});
      //   })
      //   .catch(error => {
      //     if (error.response) {
      //       if (error.response.data.error === '-99') {
      //         console.log('User is already existttttttttt');
      //         // this.showMessage('User is already exist');

      //         this.setState({isLoading: false, msg: 'User is already exist'});
      //       }
      //       console.log('responce_error', error.response);

      //       // this.showMessage(error.response);
      //       this.setState({isLoading: false});
      //     } else if (error.request) {
      //       // this.showMessage(error.request);
      //       this.setState({isLoading: false});
      //       console.log('request error', error.request);
      //     }
      //     // this.showMessage('Server Error');
      //     this.setState({isLoading: false});
      //   });
      // this.setState({isLoading: false});
    }
  };
  showMessage = (message) => {
    if (message !== "" && message !== null && message !== undefined) {
      Toast.show({
        text: message,
        // style: styles.toasttxt,
        duration: 5000,
      });
    }
  };
  Validation = () => {
    this.setState({ isLoading: false });
    // debugger;
    const invalidFields = [];
    if (!this.state.firstname) {
      invalidFields.push("firstname");
      this.setState({ ErrorFirstName: "First Name is required" });
    } else {
      console.log("else");
      this.setState({ ErrorFirstName: null });
    }
    let regtext = /^[a-zA-Z ]*$/;
    if (regtext.test(this.state.firstname) === false) {
      invalidFields.push("firstname");
      this.setState({ ErrorName: "Please enter valid Name" });
    } else {
      this.setState({ ErrorName: null });
    }
    if (!this.state.username) {
      invalidFields.push("username");
      this.setState({ ErrorUserName: "Email address is required" });
    } else {
      console.log("else");
      this.setState({ ErrorUserName: null });
    }

    if (!this.state.password) {
      invalidFields.push("password");
      this.setState({ ErrorPassword: "Password is required" });
    } else {
      this.setState({ ErrorPassword: null });
    }
    if (this.state.password.length < 6) {
      invalidFields.push("passwordlength");
      this.setState({
        ErrorPasswordLength: "Password length is must greater then 6",
      });
    } else {
      this.setState({ ErrorPasswordLength: null });
    }
    if (this.state.password !== this.state.cpassword) {
      invalidFields.push("cpassword");
      this.setState({
        ErrorPassword: "Password and Confirm Password does not match",
      });
    } else {
      this.setState({ ErrorPassword: null });
    }
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(this.state.username) === false && this.state.username) {
      invalidFields.push("ErrorUserEmail");
      this.setState({ ErrorUserEmail: "Please enter valid email" });
    } else {
      this.setState({ ErrorUserEmail: null });
    }

    return invalidFields.length > 0;
  };
  componentDidMount() {
    this.getLanguage();
  }

  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
      } else {
        strings.setLanguage("sp");
      }
    });
  }

  async setLanguage(languageCode) {
    console.log({ languageCode });
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        this.setState({ Ltype: 1 });
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
        this.setState({ Ltype: 2 });
      });
    }
  }

  onPress = () => {
    // alert("hiiii");
    if (this.state.textValue == "Spanish") {
      this.setState({
        textValue: "English",
      });
      this.setLanguage("sp");
    } else {
      this.setState({
        textValue: "Spanish",
      });
      this.setLanguage("en");
    }
  };
  contact = () => {
    Linking.openURL("https://www.healthelivin.org/contact-us/");
  };
  render() {
    return (
      <SafeAreaView>
        <View
          style={{
            height: "100%",
            backgroundColor: "#fff",
            // backgroundColor: "#E5E5E5"
          }}
        >
          <ScrollView
            keyboardShouldPersistTaps="handled"
            contentContainerStyle={{
              height: deviceHeight,
              backgroundColor: "#fff",
            }}
          >
            <View
              style={{
                width: "100%",
                justifyContent: "flex-end",
                flexDirection: "row",
                marginTop: 8,
                // marginBottom: 16,
              }}
            >
              <TouchableOpacity
                style={{
                  backgroundColor: "#F8D470",
                  paddingHorizontal: 12,
                  paddingVertical: 8,
                  // width: '20%',
                  justifyContent: "center",
                  alignItems: "center",
                  borderRadius: 20,
                  marginRight: 20,
                }}
                onPress={this.onPress}
              >
                <Text
                  style={{
                    color: "#A1683A",
                    fontWeight: "bold",
                    lineHeight: 20,
                    fontSize: 14,
                    fontFamily: "Quicksand-Bold",
                  }}
                >
                  {this.state.textValue}
                </Text>
              </TouchableOpacity>
            </View>
            <Spinner visible={this.state.isLoading} />
            <View
              style={{
                //   backgroundColor: 'red',
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Text
                style={{
                  // fontWeight: "bold",
                  marginBottom: 10,
                  fontFamily: "Quicksand-Bold",
                  lineHeight: 26,
                  color: "#060D0B",
                  // fontWeight: "bold",
                  fontSize: 20,
                }}
              >
                {strings.signup}
              </Text>
              <Text
                style={{
                  fontSize: 14,
                  lineHeight: 18,
                  fontFamily: "Quicksand-Bold",
                }}
              >
                {strings.signuptitle}
              </Text>
            </View>
            <View style={{ marginHorizontal: 16 }}>
              <View>
                {/* <Text
                  style={{
                    color: "red",
                    position: "absolute",
                    top: 20,
                    left: 95,
                  }}
                >
                  *
                </Text> */}
                <CustomInput
                  placeholder={strings.nameplaceholder}
                  heading={strings.name}
                  value={this.state.firstname}
                  placeholderTextColor="gray"
                  onChangeText={this.onFirstNameChange}
                />
              </View>

              <View style={{ width: "90%" }}>
                {this.state.ErrorFirstName && (
                  <Text style={styles.errortext}>
                    {this.state.ErrorFirstName}
                  </Text>
                )}
              </View>
              <View style={{ width: "90%" }}>
                {this.state.ErrorName && (
                  <Text style={styles.errortext}>{this.state.ErrorName}</Text>
                )}
              </View>
              <View>
                {/* <Text
                  style={{
                    color: "red",
                    position: "absolute",
                    top: 20,
                    right: 254,
                  }}
                >
                  *
                </Text> */}
                <CustomInput
                  placeholder={strings.eplaceholder}
                  heading={strings.email}
                  placeholderTextColor="gray"
                  value={this.state.username}
                  onChangeText={this.onUsernameChange}
                  keyboardType={"email-address"}
                />
              </View>

              <View style={{ width: "90%" }}>
                {this.state.ErrorUserName && (
                  <Text style={styles.errortext}>
                    {this.state.ErrorUserName}
                  </Text>
                )}
              </View>
              <View style={{ width: "90%" }}>
                {this.state.ErrorUserEmail && (
                  <Text style={styles.errortext}>
                    {this.state.ErrorUserEmail}
                  </Text>
                )}
              </View>
              <View>
                <CustomInput
                  placeholder={strings.passsetupplaceholder}
                  heading={strings.passsetup}
                  value={this.state.password}
                  placeholderTextColor="gray"
                  onChangeText={this.onPasswordChange}
                  secureTextEntry={this.state.hidePassword}
                />
                <TouchableOpacity
                  onPress={this.managePasswordVisibility}
                  style={{
                    position: "absolute",
                    right: 15,
                    top: 55,
                  }}
                >
                  <Feather
                    name={this.state.hidePassword ? "eye-off" : "eye"}
                    size={23}
                    color="gray"
                    // style={{ marginLeft: 80 }}
                  />
                </TouchableOpacity>
              </View>
              <View style={{ width: "90%" }}>
                {this.state.ErrorPassword && (
                  <Text style={styles.errortext}>
                    {this.state.ErrorPassword}
                  </Text>
                )}
              </View>
              <View style={{ width: "90%" }}>
                {this.state.ErrorPasswordLength && (
                  <Text style={styles.errortext}>
                    {this.state.ErrorPasswordLength}
                  </Text>
                )}
              </View>
              <View>
                {/* <Text
                  style={{
                    color: "red",
                    position: "absolute",
                    top: 20,
                    right: 222,
                  }}
                >
                  *
                </Text> */}
                <CustomInput
                  placeholder={strings.confirmpassplaceholder}
                  heading={strings.confirmpass}
                  value={this.state.cpassword}
                  placeholderTextColor="gray"
                  onChangeText={this.onCPasswordChange}
                  secureTextEntry={this.state.hideConfirmPassword}
                />
                <TouchableOpacity
                  style={{
                    position: "absolute",
                    right: 15,
                    top: 55,
                  }}
                  onPress={this.manageConfirmPasswordVisibility}
                >
                  <Feather
                    name={this.state.hideConfirmPassword ? "eye-off" : "eye"}
                    size={23}
                    color="gray"
                  />
                </TouchableOpacity>
              </View>
            </View>

            <View style={{ marginTop: 20 }}>
              <CustomButton
                title={strings.signup}
                redius={40}
                width={230}
                height={45}
                onPress={() => this.onPressRegister()}
              />
            </View>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Login")}
            >
              <View
                style={{
                  alignItems: "center",
                  marginTop: 15,
                }}
              >
                <Text
                  style={{
                    color: "#377867",
                    fontFamily: "Quicksand-Bold",
                    fontWeight: "bold",
                  }}
                >
                  {strings.login}
                </Text>
              </View>
            </TouchableOpacity>
            {this.state.msg && (
              <View>
                <Text>{this.state.msg}</Text>
              </View>
            )}
            <View style={{ position: "absolute", bottom: 10, width: "100%" }}>
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  width: "100%",
                }}
              >
                <TouchableOpacity onPress={() => this.contact()}>
                  <Text style={{ color: "#A1683B", fontWeight: "700" }}>
                    {strings.contact}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  errortext: {
    color: "red",
    marginTop: 5,
    marginLeft: "10%",
  },
});
