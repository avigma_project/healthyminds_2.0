import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Linking,
  Image,
} from "react-native";
import CustomHeader from "../CustomFolder/CustomHeader";
import CustomInput from "../CustomFolder/CustomInput";
import Spinner from "react-native-loading-spinner-overlay";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { userprofile } from "../Api/function";
import { updateprofile } from "../Api/function";
import strings from "../Language/Language";
import { Toast } from "native-base";
import Feather from "react-native-vector-icons/Feather";

export default class ChangePassowrd extends Component {
  constructor() {
    super();
    this.state = {
      checkpassword: null,
      password: null,
      newpassword: null,
      cpassword: null,
      npassword: null,
      access_token: "",
      clientid: 2,
      grant_type: "password",
      role: 2,
      isLoading: false,
      ErrorPassword: null,
      NErrorPassword: null,
      NErrorPasswordLength: null,
      ErrorPasswordLength: null,
      NCErrorPassword: null,
      msg: null,
      hideCurrentPassword: true,
      hidePassword: true,
      hideConfirmPassword: true,
    };
  }
  componentDidMount() {
    const { navigation } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      this.getToken();
    });

    // this.focusListener = navigation.addListener("didFocus", () => {
    //   // The screen is focused
    //   // Call any action
    //   this.getToken();
    // });
  }
  componentWillUnmount() {
    // this.backHandler.remove();
    // this.focusListener.remove();
    this._unsubscribe;
  }
  contact = () => {
    Linking.openURL("https://www.healthelivin.org/contact-us/");
  };
  onPasswordChange = (password) => {
    this.setState({ password });
  };
  onNewPasswordChange = (newpassword) => {
    this.setState({ newpassword });
  };
  onCPasswordChange = (cpassword) => {
    this.setState({ cpassword });
  };
  onNPasswordChange = (npassword) => {
    this.setState({ npassword });
  };

  manageCurrentPasswordVisibility = () => {
    this.setState({ hideCurrentPassword: !this.state.hideCurrentPassword });
  };

  managePasswordVisibility = () => {
    this.setState({ hidePassword: !this.state.hidePassword });
  };

  manageConfirmPasswordVisibility = () => {
    this.setState({ hideConfirmPassword: !this.state.hideConfirmPassword });
  };
  Validation = () => {
    this.setState({ isLoading: false });
    // debugger;
    const invalidFields = [];
    if (this.state.checkpassword != this.state.newpassword) {
      // alert(this.state.checkpassword);
      invalidFields.push("password");
      this.showMessage("Invalid Current Password");
    }
    if (!this.state.newpassword) {
      invalidFields.push("password");
      this.setState({ ErrorPassword: "Password is required" });
    } else {
      this.setState({ ErrorPassword: null });
    }
    if (!this.state.npassword) {
      invalidFields.push("password");
      this.setState({ NErrorPassword: "New Password is required" });
    } else {
      this.setState({ NErrorPassword: null });
    }
    if (this.state.password && this.state.password.length < 6) {
      invalidFields.push("passwordlength");
      this.setState({
        NErrorPasswordLength: "Password length is must greater then 6",
      });
    } else {
      this.setState({ NErrorPasswordLength: null });
    }
    if (this.state.npassword && this.state.npassword.length < 6) {
      invalidFields.push("passwordlength");
      console.log("iflengthpass new");
      this.setState({
        NErrorPasswordLength: "Password length is must greater then 6",
      });
    } else {
      console.log("elselengthpass new");

      this.setState({ NErrorPasswordLength: null });
    }
    if (this.state.npassword !== this.state.cpassword) {
      invalidFields.push("npassword");
      this.setState({
        NCErrorPassword: "Password and Confirm Password does not match",
      });
    } else {
      this.setState({ NCErrorPassword: null });
    }
    return invalidFields.length > 0;
  };
  getToken = async () => {
    let token;
    try {
      token = await AsyncStorage.getItem("token");
      if (token) {
        // await AsyncStorage.removeItem("token");
        this.getUserData(token);
        // console.log(token);
      } else {
        console.log("no token found");
      }
    } catch (e) {
      console.log(e);
    }
    // console.log("userTokannnnnn", token);
  };
  getUserData = async (token) => {
    var data = JSON.stringify({
      Type: 2,
    });
    try {
      const res = await userprofile(data, token);
      console.log("res: user profile", res[0][0]);
      this.setState({
        checkpassword: res[0][0].User_Password,
      });
    } catch (error) {
      console.log("hihihihihihih", { e: error.response.data.error });
      let message = "";
      if (error.response) {
        this.setState({ isLoading: false });
      } else {
        message = "";
      }
      console.log({ message });
    }
  };
  UpdatePassword = async () => {
    let token;
    this.setState({ isLoading: false });
    const validate = this.Validation();
    // alert(validate);
    if (!validate) {
      const { cpassword, userid } = this.state;
      this.setState({ isLoading: true });

      let data = {
        User_PkeyID: userid,
        User_Password: cpassword,
        Type: 5,
      };
      console.log("dataaaaa", data);

      try {
        token = await AsyncStorage.getItem("token");
        const res = await updateprofile(data, token);
        console.log("resssssssssssssssssss: ", res);
        this.setState({ isLoading: false });
        this.showMessage("Password updated successfully");
        this.props.navigation.navigate("Profile");
      } catch (error) {
        console.log({ e: error.response.data.error });
        let message = "";
        if (error.request) {
          this.setState({ isLoading: false });
          this.showMessage("Unable to Update ,error in request");
        }
        if (error.response) {
          const {
            data: { error_description },
          } = error.response;
          message = error_description;
          // if (error.response.data.error === "-99") {
          //   this.showMessage("User is already exist");
          // }
          this.setState({ isLoading: false });
        }
        if (error) {
          this.setState({ isLoading: false });
          this.showMessage("Invalid Current Password");
        }
        console.log({ message });
      }
    }
  };

  showMessage = (message) => {
    if (message !== "" && message !== null && message !== undefined) {
      Toast.show({
        text: message,
        // style: styles.toasttxt,
        duration: 3000,
      });
    }
  };
  render() {
    return (
      <SafeAreaView>
        <View
          style={{
            backgroundColor: "#fff",
            // backgroundColor: "#E5E5E5",
            height: "100%",
          }}
        >
          <Spinner visible={this.state.isLoading} />

          <CustomHeader
            title={strings.changepass}
            save={true}
            navigation={this.props.navigation}
            onPress={() => this.UpdatePassword()}
          />
          <View style={{ marginHorizontal: 16 }}>
            <CustomInput
              placeholder={strings.atleast}
              heading={strings.currpass}
              value={this.state.newpassword}
              placeholderTextColor="gray"
              onChangeText={this.onNewPasswordChange}
              secureTextEntry={this.state.hideCurrentPassword}
            />
            <TouchableOpacity
              onPress={this.manageCurrentPasswordVisibility}
              style={{
                position: "absolute",
                right: 15,
                top: 52,
              }}
            >
              <Feather
                name={this.state.hidePassword ? "eye-off" : "eye"}
                size={23}
                color="gray"
                // style={{ marginLeft: 80 }}
              />
            </TouchableOpacity>
          </View>

          <View style={{ width: "90%" }}>
            {this.state.ErrorPassword && (
              <Text style={styles.errortext}>{this.state.ErrorPassword}</Text>
            )}
          </View>
          {/* <View style={{ width: "90%" }}>
            {this.state.ErrorPasswordLength && (
              <Text style={styles.errortext}>
                {this.state.ErrorPasswordLength}
              </Text>
            )}
          </View> */}
          <View style={{ marginHorizontal: 16 }}>
            <CustomInput
              placeholder={strings.atleast}
              heading={strings.setupnewpass}
              value={this.state.npassword}
              placeholderTextColor="gray"
              onChangeText={this.onNPasswordChange}
              secureTextEntry={this.state.hidePassword}
            />
            <TouchableOpacity
              onPress={this.managePasswordVisibility}
              style={{
                position: "absolute",
                right: 15,
                top: 52,
              }}
            >
              <Feather
                name={this.state.hidePassword ? "eye-off" : "eye"}
                size={23}
                color="gray"
                // style={{ marginLeft: 80 }}
              />
            </TouchableOpacity>
          </View>

          <View style={{ width: "90%" }}>
            {this.state.NErrorPassword && (
              <Text style={styles.errortext}>{this.state.NErrorPassword}</Text>
            )}
          </View>
          <View style={{ width: "90%" }}>
            {this.state.NErrorPasswordLength && (
              <Text style={styles.errortext}>
                {this.state.NErrorPasswordLength}
              </Text>
            )}
          </View>
          <View style={{ width: "90%" }}>
            {this.state.NCErrorPassword && (
              <Text style={styles.errortext}>{this.state.NCErrorPassword}</Text>
            )}
          </View>
          <View style={{ marginHorizontal: 16 }}>
            <CustomInput
              placeholder={strings.confirmpassplaceholder}
              heading={strings.confrimnewpass}
              value={this.state.cpassword}
              placeholderTextColor="gray"
              onChangeText={this.onCPasswordChange}
              secureTextEntry={this.state.hideConfirmPassword}
            />
            <TouchableOpacity
              style={{
                position: "absolute",
                right: 15,
                top: 52,
              }}
              onPress={this.manageConfirmPasswordVisibility}
            >
              <Feather
                name={this.state.hideConfirmPassword ? "eye-off" : "eye"}
                size={23}
                color="gray"
              />
            </TouchableOpacity>
          </View>
          <View
            style={{ alignSelf: "center", position: "absolute", bottom: 50 }}
          >
            <View style={{}}>
              <TouchableOpacity onPress={() => this.contact()}>
                <Text style={{ color: "#A1683B", fontWeight: "700" }}>
                  {strings.contact}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          {/* <View style={{ position: "absolute", bottom: 0 }}>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-around",
                height: 49,
                borderTopColor: "lightgray",
                borderTopWidth: 0.5,
                alignItems: "center",
                width: "95%",
              }}
            >
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("Search", {
                    screen: "Welcome",
                  })
                }
              >
                <Image
                  source={require("../Images/GroupSearch.png")}
                  style={{
                    height: 20,
                    width: 20,
                    resizeMode: "stretch",
                    alignSelf: "center",
                  }}
                />
                <Text
                  style={{
                    color: "gray",
                    fontSize: 10,
                    // fontFamily: "Quicksand-Bold",
                  }}
                >
                  Search
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("GetStarted")}
              >
                <Image
                  source={require("../Images/GroupScreening.png")}
                  style={{
                    height: 20,
                    width: 20,
                    resizeMode: "stretch",
                    alignSelf: "center",
                  }}
                />
                <Text
                  style={{
                    color: "gray",
                    fontSize: 10,
                    // fontFamily: "Quicksand-Bold",
                  }}
                >
                  Screening
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("Resources", {
                    screen: "Default",
                  })
                }
              >
                <Image
                  source={require("../Images/GroupResources.png")}
                  style={{
                    height: 20,
                    width: 20,
                    resizeMode: "stretch",
                    alignSelf: "center",
                  }}
                />
                <Text
                  style={{
                    color: "gray",
                    fontSize: 10,
                    // fontFamily: "Quicksand-Bold",
                  }}
                >
                  Resources
                </Text>
              </TouchableOpacity>
            </View>
          </View>
         */}
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  errortext: {
    color: "red",
    marginTop: 5,
    marginLeft: "10%",
  },
});
