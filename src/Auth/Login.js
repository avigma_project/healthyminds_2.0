import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  Linking,
  Dimensions,
} from "react-native";
import Header from "../CustomFolder/Header";
import CustomInput from "../CustomFolder/CustomInput";
import Feather from "react-native-vector-icons/Feather";
import CustomButton from "../CustomFolder/CustomButton";
import Spinner from "react-native-loading-spinner-overlay";
import { login } from "../Api/function";
import qs from "qs";
import { Toast } from "native-base";
import strings from "../Language/Language";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
const deviceHeight = Dimensions.get("window").height;

export default class Login extends Component {
  constructor() {
    super();
    this.state = {
      username: "",
      password: "",
      firstname: null,
      ErrorUserName: null,
      ErrorPassword: null,
      ErrorUserEmail: null,
      clientid: 1,
      role: 2,
      isLoading: false,
      grant_type: "password",
      access_token: "",
      msg: null,
      textValue: "Spanish",
      hidePassword: true,
    };
  }
  onUsernameChange = (username) => {
    this.setState({ username });
  };

  onPasswordChange = (password) => {
    this.setState({ password });
  };
  managePasswordVisibility = () => {
    this.setState({ hidePassword: !this.state.hidePassword });
  };
  Validation = () => {
    this.setState({ isLoading: false });
    // debugger;
    const invalidFields = [];

    if (!this.state.username) {
      invalidFields.push("username");
      this.setState({ ErrorUserName: "Email address is required" });
    } else {
      console.log("else");
      this.setState({ ErrorUserName: null });
    }
    if (!this.state.password) {
      invalidFields.push("password");
      this.setState({ ErrorPassword: "Password is required" });
    } else {
      this.setState({ ErrorPassword: null });
    }
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(this.state.username) === false && this.state.username) {
      invalidFields.push("ErrorUserEmail");
      this.setState({ ErrorUserEmail: "Please enter valid email" });
    } else {
      this.setState({ ErrorUserEmail: null });
    }
    return invalidFields.length > 0;
  };
  onPressLogin = async () => {
    console.log("onPressLogin");
    this.setState({ isLoading: true });
    const validate = this.Validation();
    console.log("validate", validate);
    if (!validate) {
      const {
        username,
        password,
        firstname,
        clientid,
        role,
        grant_type,
      } = this.state;

      this.setState({ isLoading: true });
      let data = qs.stringify({
        grant_type: grant_type,
        username: username,
        password: password,
        ClientId: clientid,
        FirstName: "",
      });
      console.log(data);
      await login(data)
        .then((res) => {
          console.log("res: ", JSON.stringify(res));
          this.setState({ isLoading: false, access_token: res.access_token });
          const token = res.access_token;
          AsyncStorage.setItem("token", token);
          axios.defaults.headers.common["TOKEN"] = token;
          this.props.navigation.navigate("Search", {
            screen: "Welcome",
          });

          // console.log('res:123', res.access_token);
          // this.setState({isLoading: false, access_token: res.access_token});
          // const token = res.access_token;
          // AsyncStorage.setItem('token', token);
          // this.props.setToken(token);
          // this.props.registerMode(false);
        })
        .catch((error) => {
          if (error.response) {
            console.log("responce_error", error.response);
            if (error.response.data.error === "0") {
              // this.showMessage("The EmailID or password is incorrect.");
              this.setState({ isLoading: false });
            }
          } else if (error.request) {
            this.setState({ isLoading: false });
            console.log("request error", error.request);
          }
          // this.showMessage("Server Error");
          this.showMessage("The EmailID or password is incorrect.");
          this.setState({ isLoading: false });
        });
    }
  };

  showMessage = (message) => {
    if (message !== "" && message !== null && message !== undefined) {
      Toast.show({
        text: message,
        duration: 5000,
      });
    }
  };

  componentDidMount() {
    this.getLanguage();
  }

  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
      } else {
        strings.setLanguage("sp");
      }
    });
  }

  async setLanguage(languageCode) {
    console.log({ languageCode });
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        this.setState({ Ltype: 1 });
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
        this.setState({ Ltype: 2 });
      });
    }
  }

  onPress = () => {
    // alert("hiiii");
    if (this.state.textValue == "Spanish") {
      this.setState({
        textValue: "English",
      });
      this.setLanguage("sp");
    } else {
      this.setState({
        textValue: "Spanish",
      });
      this.setLanguage("en");
    }
  };

  contact = () => {
    Linking.openURL("https://www.healthelivin.org/contact-us/");
  };

  render() {
    const { isLoading } = this.state;

    return (
      <SafeAreaView>
        <View
          style={{
            height: "100%",
            backgroundColor: "#fff",
            //  backgroundColor: "#E5E5E5"
          }}
        >
          <ScrollView
            contentContainerStyle={{
              // backgroundColor: "pink",
              height: deviceHeight - 100,
            }}
            keyboardShouldPersistTaps="handled"
          >
            <View
              style={{
                width: "100%",
                // backgroundColor: "pink",
                justifyContent: "flex-end",
                flexDirection: "row",
                marginTop: 8,
                marginBottom: 16,
              }}
            >
              <TouchableOpacity
                style={{
                  backgroundColor: "#F8D470",
                  paddingHorizontal: 12,
                  paddingVertical: 8,
                  // width: '20%',
                  justifyContent: "center",
                  alignItems: "center",
                  borderRadius: 20,
                  marginRight: 20,
                }}
                onPress={this.onPress}
              >
                <Text
                  style={{
                    color: "#A1683A",
                    fontWeight: "bold",
                    lineHeight: 20,
                    fontSize: 14,
                    fontFamily: "Quicksand-Bold",
                  }}
                >
                  {this.state.textValue}
                </Text>
              </TouchableOpacity>
            </View>
            <Spinner visible={isLoading} />
            <View
              style={{
                //   backgroundColor: 'red',
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Text
                style={{
                  marginBottom: 8,
                  fontWeight: "bold",
                  lineHeight: 26,
                  color: "#060D0B",
                  fontWeight: "700",
                  fontSize: 20,
                  fontFamily: "Quicksand-Bold",
                }}
              >
                {strings.login}
              </Text>
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: "Quicksand-Bold",
                  fontWeight: "400",
                  lineHeight: 18,
                  color: "#060D0B",
                  // fontWeight: "700",
                }}
              >
                {strings.welcome}{" "}
              </Text>
            </View>
            <View style={{ marginHorizontal: 16 }}>
              <CustomInput
                placeholder={strings.eplaceholder}
                heading={strings.email}
                placeholderTextColor="gray"
                value={this.state.username}
                onChangeText={this.onUsernameChange}
                keyboardType={"email-address"}
              />
              <View style={{ width: "90%" }}>
                {this.state.ErrorUserName && (
                  <Text style={styles.errortext}>
                    {this.state.ErrorUserName}
                  </Text>
                )}
              </View>
              <View style={{ width: "90%" }}>
                {this.state.ErrorUserEmail && (
                  <Text style={styles.errortext}>
                    {this.state.ErrorUserEmail}
                  </Text>
                )}
              </View>
              <View>
                <CustomInput
                  placeholder={strings.pplaceholder}
                  heading={strings.password}
                  value={this.state.password}
                  placeholderTextColor="gray"
                  onChangeText={this.onPasswordChange}
                  secureTextEntry={this.state.hidePassword}
                />
                <TouchableOpacity
                  onPress={this.managePasswordVisibility}
                  style={{
                    position: "absolute",
                    right: 15,
                    top: 55,
                  }}
                >
                  <Feather
                    name={this.state.hidePassword ? "eye-off" : "eye"}
                    size={23}
                    color="gray"
                    // style={{ marginLeft: 80 }}
                  />
                </TouchableOpacity>
              </View>
              <View style={{ width: "90%" }}>
                {this.state.ErrorPassword && (
                  <Text style={styles.errortext}>
                    {this.state.ErrorPassword}
                  </Text>
                )}
              </View>
            </View>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("FindPassword")}
              style={{ marginTop: 16, marginHorizontal: 17 }}
            >
              <Text
                style={{
                  color: "#377867",
                  fontStyle: "italic",
                  lineHeight: 18,
                  fontWeight: "400",
                  // //fontFamily: "OpenSans-Regular",
                  fontSize: 14,
                }}
              >
                {strings.forgetpassword}
              </Text>
            </TouchableOpacity>
            <View style={{ marginTop: 24 }}>
              <CustomButton
                title={strings.login}
                redius={100}
                width="60%"
                height={45}
                onPress={() => this.onPressLogin()}
              />
            </View>
            {/* {this.state.msg && (
              <View>
                <Text>{this.state.msg}</Text>
              </View>
            )} */}

            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("SignUp")}
            >
              <View style={{ alignItems: "center", marginTop: 40 }}>
                <Text
                  style={{
                    color: "#377867",
                    fontWeight: "700",
                    fontFamily: "Quicksand-Bold",
                  }}
                >
                  {strings.signup}
                </Text>
              </View>
            </TouchableOpacity>
            <View style={{ position: "absolute", bottom: 0, width: "100%" }}>
              <View
                style={{
                  // backgroundColor: "pink",
                  width: "100%",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <TouchableOpacity onPress={() => this.contact()}>
                  <Text
                    style={{
                      color: "#A1683B",
                      fontWeight: "700",
                      lineHeight: 24,
                      fontFamily: "Quicksand-Bold",
                    }}
                  >
                    {strings.contact}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  errortext: {
    color: "red",
    marginTop: 5,
    marginLeft: "10%",
  },
});
