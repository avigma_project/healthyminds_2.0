import React from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Image,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Actions } from "react-native-router-flux";
import Icon from "react-native-vector-icons/FontAwesome";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import strings from "../Language/Language";
const screenWidth = Math.round(Dimensions.get("window").width);

export default class CustomTabBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      colorSearch: "#828282",
      colorScreening: "#828282",
      colorResources: "#828282",
      ImageSearch: require("../Images/GroupSearch.png"),
      ImageScreening: require("../Images/GroupScreening.png"),
      ImageResources: require("../Images/GroupResources.png"),
    };
  }

  selectedValue(value) {
    if (value == 1) {
      this.setState({
        ImageSearch: require("../Images/GroupSearchColor.png"),
        colorSearch: "#377867",
      });
      Actions.PatientDetails();
    } else {
      this.setState({
        ImageSearch: require("../Images/GroupSearch.png"),
        colorSearch: "#828282",
      });
      // this.setState({ ImageSearch: false });
    }
    if (value == 2) {
      this.setState({
        ImageScreening: require("../Images/GroupScreeningColor.png"),
        colorScreening: "#377867",
      });
      Actions.GetStarted();
    } else {
      this.setState({
        ImageScreening: require("../Images/GroupScreening.png"),
        colorScreening: "#828282",
      });
    }
    if (value == 3) {
      this.setState({
        ImageResources: require("../Images/GroupResourcesColor.png"),
        colorResources: "#377867",
      });
      Actions.Depression();
    } else {
      this.setState({
        ImageResources: require("../Images/GroupResources.png"),
        colorResources: "#828282",
      });
    }
  }
  componentDidMount() {
    this.getLanguage();
  }
  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      // console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
        this.setLanguage("en");
      } else {
        if (get_Data == "2") {
          strings.setLanguage("sp");
          this.setLanguage("sp");
        } else {
          strings.setLanguage("en");
          this.setLanguage("en");
        }
      }
    });
  }
  async setLanguage(languageCode) {
    // console.log(languageCode)
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        // console.log("refresh", get_Data);

        this.setState({ Ltype: 1 });
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
        this.setState({ Ltype: 2 });
      });
    }
    this.GetMasterDetails();
    Actions.refresh({ key: "tasks" });
    // getLanguage(languageCode)
  }

  handleLogout = async () => {
    console.log("logout press");
    let token;

    try {
      token = await AsyncStorage.getItem("token");
      console.log(tokennnnnnnnnn);
      const token2 = JSON.stringify(token);
      if (token2) {
        console.log("logououoout", token2);
        await AsyncStorage.removeItem("token");
        navigation.reset({
          index: 0,
          routes: [{ name: "Login" }],
        });
      } else {
        console.log("no token found");
      }
    } catch (e) {
      console.log(e);
    }
  };
  render() {
    const { state } = this.props.navigation;
    const activeTabIndex = state.index;
    // console.log("Routes: ", state.routes);
    return (
      <View style={styles.tabBar}>
        <View style={styles.tabView}>
          <TouchableOpacity onPress={() => this.selectedValue(1)}>
            <View
              style={{
                width: screenWidth / 3,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              {/* <Icon name="home" style={{ color: this.state.color }} size={25} /> */}

              <Image
                // style={{ color: this.state.color }}
                style={{ height: "62%", width: "24%" }}
                source={this.state.ImageSearch}
              />
              <Text
                style={{
                  color: this.state.colorSearch,
                  fontWeight: "500",
                  fontFamily: "Quicksand-Bold",
                  fontSize: 12,
                  lineHeight: 16,
                }}
              >
                {strings.Search}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.selectedValue(2)}>
            <View
              style={{
                width: screenWidth / 3,
                justifyContent: "center",
                alignItems: "center",
                alignSelf: "center",
              }}
            >
              {/* <Icon
                name="map-marker"
                style={{ color: this.state.changeColorCenters }}
                size={25}
              /> */}

              <Image
                // style={{ color: this.state.changeColorCenters }}
                style={{ height: "62%", width: "24%" }}
                source={this.state.ImageScreening}
              />
              <Text
                style={{
                  color: this.state.colorScreening,
                  fontWeight: "500",
                  fontFamily: "Quicksand-Bold",
                  fontSize: 12,
                  lineHeight: 16,
                }}
              >
                {strings.Screening}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.selectedValue(3)}>
            <View
              style={{
                width: screenWidth / 3,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Image
                // style={{ color: this.state.changeColorSurvey }}
                style={{ height: "62%", width: "24%" }}
                source={this.state.ImageResources}
              />
              <Text
                style={{
                  color: this.state.colorResources,
                  fontWeight: "500",
                  fontFamily: "Quicksand-Bold",
                  fontSize: 12,
                  lineHeight: 16,
                }}
              >
                {strings.Resources}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tabBar: {
    height: 60,
    borderTopColor: "darkgrey",
    borderTopWidth: 1,
    opacity: 0.98,
    justifyContent: "space-between",
    backgroundColor: "#ffffff",
  },
  tabView: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: 60,
  },
});
