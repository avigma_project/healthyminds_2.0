import Realm from "realm";

export const Patient_Base_Schema = "Patient_Base_Details_Master";
export const Patient_Master_Schema = "Patient_Master";
// export const User_Master_Schema = "User_Master";
export const Patient_Depression_Schema = "Patient_Depression_Master";
export const Patient_MoodDisorder_Schema = "Patient_MoodDisorder_Master";
export const Patient_MoodDisorder_Schema_2 = "Patient_MoodDisorder_2_Master";
export const Patient_MoodDisorder_Schema_3 = "Patient_MoodDisorder_3_Master";
export const Patient_AnxietyDisorder_Schema = "Patient_AnxietyDisorder_Master";
export const Patient_PTSD_Schema = "Patient_PTSD_Master";
export const Substance_Use_Schema = "Substance_Use_Master";
export const Substance_Option_Schema = "Substance_Option_Master";
export const Patient_Treated_Schema = "Patient_Treated_Master";
export const Final_Scores_Schema = "Final_Scores_Master";
export const Patient_Survey_Schema = "Patient_Survey_Master";
export const Patient_Base_Survey_Schema = "Patient_Base_Survey_Master";
export const Center_List_Schema = "Center_List_Master";
export const Techniques_List_Schema = "Techniques_List_Master";

export const Final_Scores_Master = {
  name: Final_Scores_Schema,
  primaryKey: "Id",
  properties: {
    Id: "int",
    Title: "string?",
    Score: "int?",
  },
};

export const Patient_Base_Details_Master = {
  name: Patient_Base_Schema,
  primaryKey: "Id",
  properties: {
    Id: "int",
    Name: "string?",
    age: "string?",
    zipcode: "string?",
    value_cuss: "int?",
    val_staf: "int?",
  },
};
export const Patient_Base_Survey_Master = {
  name: Patient_Base_Survey_Schema,
  properties: "Survey_Cust_PKeyID",
  properties: {
    Survey_Cust_PKeyID: "int",
    Survey_Cust_Name: "string?",
    Survey_Cust_Email: "string?",
    Survey_Cust_Mobile_Number: "string?",
  },
};
export const Patient_Master = {
  name: Patient_Master_Schema,
  primaryKey: "Id",
  properties: {
    Id: "int",
    Participant_number: "int?",
    Name: "string?",
    Address: "string?",
    Zip: "string?",
    age: "string?",
    Gender: "string?",
    partnership_Status: "string?",
    Ethnic_identity: "string?",
    Suicide: "bool?",
    Medication: "bool?",
  },
};

//Depression
export const Patient_Depression_Master = {
  name: Patient_Depression_Schema,
  primaryKey: "DST_PKeyID",
  properties: {
    DST_PKeyID: "int",
    DST_Option: "string?",
    Score: "int?",
    Staff_Score: "int?",
    Patid: "int?",
    DST_IsActive: "bool",
    Type: "int?",
    value: "int?",
  },
};
//MoodDisorder
export const Patient_MoodDisorder_Master = {
  name: Patient_MoodDisorder_Schema,
  primaryKey: "MDQ_PkeyId",
  properties: {
    MDQ_PkeyId: "int",
    MDQ_Option: "string?",
    Score: "int?",
    Staff_Score: "int?",
    Patid: "int?",
    MDQ_IsActive: "bool",
    Type: "int?",
    value: "bool?",
  },
};
export const Patient_MoodDisorder_2_Master = {
  name: Patient_MoodDisorder_Schema_2,
  primaryKey: "Id",
  properties: {
    Id: "int",
    value: "int?",
    MDQ_Option: "string?",
  },
};

export const Patient_MoodDisorder_3_Master = {
  name: Patient_MoodDisorder_Schema_3,
  primaryKey: "Id",
  properties: {
    Id: "int",
    value: "bool?",
  },
};

//AnxietyDisorder
export const Patient_AnxietyDisorder_Master = {
  name: Patient_AnxietyDisorder_Schema,
  primaryKey: "ADS_PKeyId",
  properties: {
    ADS_PKeyId: "int",
    ADS_Option: "string?",
    Score: "int?",
    Staff_Score: "int?",
    Patid: "int?",
    ADS_IsActive: "bool?",
    value: "bool?",
  },
};
// PTSD
export const Patient_PTSD_Master = {
  name: Patient_PTSD_Schema,
  primaryKey: "MS_PKeyId",
  properties: {
    MS_PKeyId: "int",
    MS_Option: "string?",
    Score: "int?",
    Staff_Score: "int?",
    Patid: "int?",
    MS_IsActive: "bool?",
    Type: "int?",
    value: "bool?",
  },
};
// Substance Use Test
export const Substance_Use_Master = {
  name: Substance_Use_Schema,
  primaryKey: "Sub_PkeyId",
  properties: {
    Sub_PkeyId: "int",
    Sub_Option: "string?",
    Sub_IsActive: "bool?",
    Acc_value: "int?",
    Tab_value: "int?",
    value: "int?",
  },
};
//substance checkbox
export const Substance_Option_Master = {
  name: Substance_Option_Schema,
  primaryKey: "SubsOpt_PkeyID",
  properties: {
    SubsOpt_PkeyID: "int",
    // SubsOpt_Subs_PkeyID: "int?",
    SubsOpt_Options: "string?",
    SubsOpt_IsActive: "bool?",
    // Type: "int?",
    value: "bool?",
    Sub_value: "int?",
    SubsOpt_Options_Value1: "string?",
    SubsOpt_Value: "int?",
  },
};

export const Patient_Treated_Master = {
  name: Patient_Treated_Schema,
  primaryKey: "Treated_Master_PKeyId",
  properties: {
    Treated_Master_PKeyId: "int",
    Treated_Master_Option: "string?",
    other_Val: "string?",
    Patid: "int?",
    Treated_Master_IsActive: "bool?",
    Type: "int?",
    value: "bool?",
  },
};
export const Patient_Survey_Master = {
  name: Patient_Survey_Schema,
  primaryKey: "SUR_PKeyID",
  properties: {
    SUR_PKeyID: "int",
    SUR_Heading: "string?",
    SUR_Question: "string?",
    SUR_Control_Type: "int?",
    SUR_IsActive: "bool?",
    SUR_IsDelete: "string?",
    Type: "int?",
    UserID: "int?",
    Sur_Value: "string?",
    Survey_Questions_Options_DTO: "string?",
    // Survey_Questions_Options_DTO:{type: 'linkingObjects',objectType:'Dto'}
  },
};

// const DTO_Schema ={
//   name:Dto,
//   properties:{
//   sur_opt_PKeyID:'int',
//   sur_Question_Pkey: 'int?',
//   sur_OPT_VAL: 'string?',
//   sur_IsActive: 'bool?',
//   sur_IsDelete: 'bool?',
//   Type: 'int?',
//   UserID: 'int',
//   sur_IsChecked: 'bool?'
// }
// }
export const Center_List_Master = {
  name: Center_List_Schema,
  primaryKey: "HC_Pkey",
  properties: {
    HC_Pkey: "int",
    HC_Name: "string?",
    HC_Address: "string?",
    HC_Contactnum: "string?",
    HC_ZipCode: "int?",
    HC_Lat: "string?",
    HC_Long: "string?",
    PatientDistance: "double?",
    HC_Web_URL: "string?",
    HC_Sep: "int?",
    MFlag: "bool?",
    // HC_IsActive:'bool?',
    // UserID:'string?',
    // Type:'int?'
  },
};
export const Techniques_List_Master = {
  name: Techniques_List_Schema,
  primaryKey: "TIP_Pkey",
  properties: {
    TIP_Pkey: "int",
    TIP_Name: "string?",
    TIP_Desc: "string?",
    TIP_Web_URL: "string?",
  },
};
const databaseOptions = {
  path: "Hmind25.realm",
  schema: [
    Patient_Base_Details_Master,
    Patient_Master,
    // User_Master_Data,
    Patient_Depression_Master,
    Patient_MoodDisorder_Master,
    Patient_AnxietyDisorder_Master,
    Patient_PTSD_Master,
    Substance_Use_Master,
    Substance_Option_Master,
    Patient_Treated_Master,
    Patient_MoodDisorder_2_Master,
    Patient_MoodDisorder_3_Master,
    Final_Scores_Master,
    Patient_Survey_Master,
    Patient_Base_Survey_Master,
    Center_List_Master,
    Techniques_List_Master,
  ],
  schemaVersion: 91,
};

// Insert  query
export const InsertBaseDetails = (data) =>
  new Promise((resolve, reject) => {
    //  console.log('Insert function callded',JSON.stringify(data));
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          realm.create(Patient_Base_Schema, data);
          resolve();
        });
      })
      .catch((error) => {
        // console.log('InsertError',error)
        reject(error);
        // console.log('Insert function callded error',error);
      });
  });

export const UpdatePatientBaseMaster_final = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Update function callded UpdatePatientMaster_final",
      JSON.stringify(data)
    );
    let datarray = [];
    let obj = data.map((value) => {
      console.log(`Checking aray ${JSON.stringify(value)}`);
      let ldata = {
        Id: 1,
        Name: null,
        age: null,
        zipcode: null,
        value_cuss: null,
        val_staf: null,
      };
      datarray.push(ldata);
    });

    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          datarray.forEach((obj) => {
            realm.create(Patient_Base_Schema, obj, "modified");
            resolve;
          });
        });
        resolve();
      })

      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });
export const InsertCenter_List_Master = (data) =>
  new Promise((resolve, reject) => {
    //  console.log('Insert function callded',JSON.stringify(data));
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          realm.create(Center_List_Master, data);
          resolve();
        });
      })
      .catch((error) => {
        // console.log('InsertError',error)
        reject(error);
        // console.log('Insert function callded error',error);
      });
  });

//insert Base survey details
export const InsertBaseSurveyDetails = (data) =>
  new Promise((resolve, reject) => {
    // console.log("Insert function callded", JSON.stringify(data));
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          realm.create(Patient_Base_Survey_Schema, data);
          resolve();
        });
      })
      .catch((error) => {
        // console.log('InsertError',error)
        reject(error);
        // console.log('Insert function callded error',error);
      });
  });
//update base survey Details final

export const UpdatePatientBaseSurveyMaster_final = (data) =>
  new Promise((resolve, reject) => {
    // console.log(
    //   "Update function callded UpdatePatientMaster_final",
    //   JSON.stringify(data)
    // );
    let datarray = [];
    let obj = data.map((value) => {
      // console.log(`Checking aray ${JSON.stringify(value)}`);
      let ldata = {
        Survey_Cust_PKeyID: 1,
        Survey_Cust_Name: null,
        Survey_Cust_Email: null,
        Survey_Cust_Mobile_Number: null,
      };
      datarray.push(ldata);
    });

    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          datarray.forEach((obj) => {
            realm.create(Patient_Base_Survey_Schema, obj, "modified");
            resolve;
          });
        });
        resolve();
      })

      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });

//insert final score
export const InsertFinalScores = (data) =>
  new Promise((resolve, reject) => {
    // console.log(`Insertion of final score: ${JSON.stringify(data)}`);
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          realm.create(Final_Scores_Schema, data, "modified");
          resolve(data);
        });
      })
      .catch((error) => {
        console.log(`Error in insertion of final score: ${error}`);
        reject(error);
      });
  });

//PatientMaster
export const InsertPatientMaster = (data) =>
  new Promise((resolve, reject) => {
    //debugger
    console.log("Insert function callded", JSON.stringify(data));
    let obj = data;
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          realm.create(Patient_Master_Schema, data);
          resolve();
          console.log("insert suuccess");
        });
      })
      // .then(realm => {
      //   realm.write(() => {
      //     obj.forEach(obj => {
      //         realm.create(Patient_Master_Schema, obj);
      //     });
      // });
      // resolve();})
      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });
//update
// export const UpdatePatientMaster = data =>
//   new Promise((resolve, reject) => {
//     console.log('Update function callded UpdatePatientMaster', JSON.stringify(data));
//     let obj = data
//     Realm.open(databaseOptions)
//       .then(realm => {
//         realm.write(() => {
//           obj.forEach(obj => {
//             realm.create(Patient_Master_Schema, obj, "modified");
//             resolve
//           });
//         });
//         resolve();
//       })

//       .catch(error => {
//         console.log('InsertError', error)
//         reject(error)
//       }
//       );
//   });

// Depression
export const InsertPatientDepressionMaster = (data) =>
  new Promise((resolve, reject) => {
    // console.log(
    //   "Insert function callded InsertPatientDepressionMaster",
    //   JSON.stringify(data)
    // );
    let obj = data;
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          obj.forEach((obj) => {
            realm.create(Patient_Depression_Schema, obj);
          });
        });
        resolve();
      })

      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });

//update base details default
export const UpdatePatientMaster_final = (data) =>
  new Promise((resolve, reject) => {
    // console.log(
    //   "Update function callded UpdatePatientMaster_final",
    //   JSON.stringify(data)
    // );
    let datarray = [];
    let obj = data.map((value) => {
      console.log(`Checking aray ${JSON.stringify(value)}`);
      let ldata = {
        Id: 1,
        Participant_number: null,
        Name: null,
        Address: null,
        Zip: null,
        age: null,
        Gender: null,
        partnership_Status: null,
        Ethnic_identity: null,
        Suicide: false,
        Medication: false,
      };
      datarray.push(ldata);
    });

    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          datarray.forEach((obj) => {
            realm.create(Patient_Master_Schema, obj, "modified");
            resolve;
          });
        });
        resolve();
      })

      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });

// update Depression
export const UpdatePatientDepressionMaster = (data) =>
  new Promise((resolve, reject) => {
    // console.log(
    //   "Update function callded UpdatePatientDepressionMaster",
    //   JSON.stringify(data)
    // );
    let obj = data;
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          obj.forEach((obj) => {
            realm.create(Patient_Depression_Schema, obj, "modified");
            resolve;
          });
        });
        resolve();
      })

      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });

//final update default value Depression

export const UpdatePatientDepressionMaster_final = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Update function callded UpdatePatientDepressionMaster_final",
      JSON.stringify(data)
    );
    let datarray = [];
    let obj = data.map((value) => {
      console.log(`Checking aray ${JSON.stringify(value)}`);
      let ldata = {
        DST_PKeyID: value.DST_PKeyID,
        DST_Option: value.DST_Option,
        Score: null,
        Staff_Score: null,
        Patid: value.Patid,
        DST_IsActive: value.DST_IsActive,
        Type: value.Type,
        value: 0,
      };
      datarray.push(ldata);
    });

    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          datarray.forEach((obj) => {
            realm.create(Patient_Depression_Schema, obj, "modified");
            resolve;
          });
          console.log("default null UpdatePatientDepressionMaster_final");
        });
        resolve();
      })

      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });

// Mood disorder
export const InsertPatientMoodDisorderMaster = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Insert function callded InsertPatientMoodDisorderMaster",
      JSON.stringify(data)
    );
    let obj = data;
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          obj.forEach((obj) => {
            realm.create(Patient_MoodDisorder_Schema, obj);
          });
        });
        resolve();
      })
      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });

export const InsertPatientMoodDisorderMaster_2 = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Insert function callded InsertPatientMoodDisorderMaster",
      JSON.stringify(data)
    );
    let obj = data;
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          obj.forEach((obj) => {
            realm.create(Patient_MoodDisorder_Schema_2, obj);
          });
        });
        resolve();
      })
      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });
// update default value

export const UpdatePatientMoodDisorderMaster_2_final = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Update function callded UpdatePatientMoodDisorderMaster_2_final",
      JSON.stringify(data)
    );
    let datarray = [];
    let obj = data.map((value) => {
      console.log(`Checking aray ${JSON.stringify(value)}`);
      let ldata = {
        Id: value.Id,
        value: null,
        MDQ_Option: value.MDQ_Option,
      };
      datarray.push(ldata);
    });

    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          datarray.forEach((obj) => {
            realm.create(Patient_Depression_Schema, obj, "modified");
            resolve;
          });
        });
        resolve();
      })

      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });

export const InsertPatientMoodDisorderMaster_3 = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Insert function callded InsertPatientMoodDisorderMaster",
      JSON.stringify(data)
    );
    let obj = data;
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          obj.forEach((obj) => {
            realm.create(Patient_MoodDisorder_Schema_3, obj);
          });
        });
        resolve();
      })
      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });

export const UpdatePatientMoodDisorderMaster_3_final = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Update function callded UpdatePatientMoodDisorderMaster_2_final",
      JSON.stringify(data)
    );
    let datarray = [];
    let obj = data.map((value) => {
      console.log(`Checking aray ${JSON.stringify(value)}`);
      let ldata = {
        Id: value.Id,
        value: false,
      };
      datarray.push(ldata);
    });

    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          datarray.forEach((obj) => {
            realm.create(Patient_Depression_Schema, obj, "modified");
            resolve;
          });
        });
        resolve();
      })

      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });

// update
export const UpdatePatientMoodDisorderMaster = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Update function callded UpdatePatientMoodDisorderMaster",
      JSON.stringify(data)
    );
    let obj = data;
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          obj.forEach((obj) => {
            realm.create(Patient_MoodDisorder_Schema, obj, "modified");
          });
        });
        resolve();
      })
      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });
// update default value mooddisorder
export const UpdatePatientMoodDisorderMaster_final = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Update function callded UpdatePatientMoodDisorderMaster_final",
      JSON.stringify(data)
    );
    let datarray = [];
    let obj = data.map((value) => {
      console.log(`Checking aray ${JSON.stringify(value)}`);
      let ldata = {
        MDQ_PkeyId: value.MDQ_PkeyId,
        MDQ_Option: value.MDQ_Option,
        Score: null,
        Staff_Score: null,
        Patid: value.Patid,
        MDQ_IsActive: value.MDQ_IsActive,
        Type: value.Type,
        value: false,
      };
      datarray.push(ldata);
    });
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          datarray.forEach((obj) => {
            realm.create(Patient_MoodDisorder_Schema, obj, "modified");
          });
        });
        resolve();
      })
      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });
// AnxietyDisorde
export const InsertPatientAnxietyDisorderMaster = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Insert function callded InsertPatientAnxietyDisorderMaster",
      JSON.stringify(data)
    );
    let obj = data;
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          obj.forEach((obj) => {
            realm.create(Patient_AnxietyDisorder_Schema, obj);
          });
        });
        resolve();
      })
      .catch((error) => {
        reject(error);
      });
  });
//update
export const UpdatePatient_AnxietyDisorder_Schema = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Update function callded UpdatePatient_AnxietyDisorder_Schema",
      JSON.stringify(data)
    );
    let obj = data;
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          obj.forEach((obj) => {
            realm.create(Patient_AnxietyDisorder_Schema, obj, "modified");
          });
        });
        resolve();
      })
      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });

//update default value AnxietyDisorde

export const UpdatePatient_AnxietyDisorder_Schema_final = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Update function callded UpdatePatient_AnxietyDisorder_Schema_final",
      JSON.stringify(data)
    );
    let datarray = [];
    let obj = data.map((value) => {
      console.log(`Checking aray ${JSON.stringify(value)}`);
      let ldata = {
        ADS_PKeyId: value.ADS_PKeyId,
        ADS_Option: value.ADS_Option,
        Score: null,
        Staff_Score: null,
        Patid: value.Patid,
        ADS_IsActive: value.ADS_IsActive,
        value: false,
      };
      datarray.push(ldata);
    });
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          datarray.forEach((obj) => {
            realm.create(Patient_AnxietyDisorder_Schema, obj, "modified");
          });
        });
        resolve();
      })
      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });

// PTSD
export const InsertPatientPTSDMaster = (data) =>
  new Promise((resolve, reject) => {
    // alert("InsertPatientPTSDMaster");
    console.log(
      "Insert function callded InsertPatientPTSDMaster",
      JSON.stringify(data)
    );
    let obj = data;
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          obj.forEach((obj) => {
            realm.create(Patient_PTSD_Schema, obj);
          });
        });
        resolve();
      })

      .catch((error) => {
        console.log("InsertPatientPTSDMaster", error);
        reject(error);
      });
  });
//update
export const UpdatePatient_PTSD_Schema = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Update function callded UpdatePatient_PTSD_Schema",
      JSON.stringify(data)
    );
    let obj = data;
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          obj.forEach((obj) => {
            realm.create(Patient_PTSD_Schema, obj, "modified");
          });
        });
        resolve();
      })
      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });

//update final default value UpdatePatient_PTSD_Schema_final

export const UpdatePatient_PTSD_Schema_final = (data) =>
  new Promise((resolve, reject) => {
    //debugger
    console.log(
      "Update function callded UpdatePatient_PTSD_Schema_final",
      JSON.stringify(data)
    );
    let datarray = [];
    let obj = data.map((value) => {
      console.log(`Checking aray ${JSON.stringify(value)}`);
      let ldata = {
        MS_PKeyId: value.MS_PKeyId,
        MS_Option: value.MS_Option,
        Score: 0,
        Staff_Score: 0,
        Patid: value.Patid,
        MS_IsActive: value.MS_IsActive,
        Type: value.Type,
        value: false,
        // value.value = false
      };
      datarray.push(ldata);
    });

    console.log(
      "default null UpdatePatient_PTSD_Schema_final ",
      JSON.stringify(data)
    );
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          datarray.forEach((obj) => {
            realm.create(Patient_PTSD_Schema, obj, "modified");
          });
        });
        resolve();
      })
      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });
//Treated
export const InsertPatientTreatedMaster = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Insert function callded InsertPatientTreatedMaster",
      JSON.stringify(data)
    );
    let obj = data;
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          obj.forEach((obj) => {
            realm.create(Patient_Treated_Schema, obj);
          });
        });
        resolve();
      })

      .catch((error) => {
        console.log("InsertPatientTreatedMaster", error);
        reject(error);
      });
  });
// update
export const UpdatePatientTreatedMaster = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Update function callded Patient_Treated_Schema",
      JSON.stringify(data)
    );
    let obj = data;
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          obj.forEach((obj) => {
            realm.create(Patient_Treated_Schema, obj, "modified");
          });
        });
        resolve();
      })
      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });

//update default value

export const UpdatePatientTreatedMaster_final = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Update function callded UpdatePatientTreatedMaster_final",
      JSON.stringify(data)
    );

    let datarray = [];
    let obj = data.map((value) => {
      console.log(`Checking aray ${JSON.stringify(value)}`);
      let ldata = {
        Treated_Master_PKeyId: value.Treated_Master_PKeyId,
        Treated_Master_Option: value.Treated_Master_Option,
        other_Val: value.other_Val,
        Patid: value.Patid,
        Treated_Master_IsActive: value.Treated_Master_IsActive,
        Type: value.Type,
        value: false,
      };
      datarray.push(ldata);
    });

    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          datarray.forEach((obj) => {
            realm.create(Patient_Treated_Schema, obj, "modified");
          });
        });
        resolve();
        console.log("UpdatePatientTreatedMaster_final");
      })
      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });
//insert query InsertPatientSurveyMaster

// Substance option
export const InsertSubstanceOptionMaster = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Insert function callded InsertSubstanceOptionMaster",
      JSON.stringify(data)
    );
    let obj = data;
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          obj.forEach((obj) => {
            realm.create(Substance_Option_Schema, obj);
          });
        });
        resolve();
      })
      .catch((error) => {
        reject(error);
      });
  });
//update Substance option
export const Update_SubstanceOption_Master = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Update function callded UpdateSubstance_Option_Schema",
      JSON.stringify(data)
    );
    let obj = data;
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          obj.forEach((obj) => {
            realm.create(Substance_Option_Schema, obj, "modified");
          });
        });
        resolve();
      })
      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });

//update default value Substance option
export const Update_SubstanceOption_Master_final = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Update function callded UpdateSubstance_Option_Schema_final",
      JSON.stringify(data)
    );

    let datarray = [];
    let obj = data.map((value) => {
      console.log(`Checking aray ${JSON.stringify(value)}`);
      let ldata = {
        SubsOpt_PkeyID: value.SubsOpt_PkeyID,
        SubsOpt_Subs_PkeyID: value.SubsOpt_Subs_PkeyID,
        SubsOpt_Options: value.SubsOpt_Options,
        SubsOpt_IsActive: value.SubsOpt_IsActive,
        SubsOpt_Options_Value1: null,
        SubsOpt_Value: null,
        Type: value.Type,
        value: false,
        Sub_value: null,
      };
      datarray.push(ldata);
    });

    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          datarray.forEach((obj) => {
            realm.create(Substance_Option_Schema, obj, "modified");
          });
        });
        resolve();
        console.log("Update_SubstanceOption_Master_final");
      })
      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });

//Substance Schema

export const InsertSubstanceUseMaster = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Insert function callded InsertSubstance_Use_SchemanMaster",
      JSON.stringify(data)
    );
    let obj = data;
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          obj.forEach((obj) => {
            realm.create(Substance_Use_Schema, obj);
          });
        });
        resolve();
      })
      .catch((error) => {
        reject(error);
      });
  });
//update Substance option
export const Update_Substance_Use_Master = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Update function callded UpdateSubstance_Use_Schema",
      JSON.stringify(data)
    );
    let obj = data;
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          obj.forEach((obj) => {
            realm.create(Substance_Use_Schema, obj, "modified");
          });
        });
        resolve();
      })
      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });

//update default value Substance option
export const Update_Substance_Use_Master_final = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Update function callded UpdateSubstance_Use_Schema_final",
      JSON.stringify(data)
    );

    let datarray = [];
    let obj = data.map((value) => {
      console.log(`Checking aray ${JSON.stringify(value)}`);
      let ldata = {
        Sub_PkeyId: value.Sub_PkeyId,
        Sub_Option: value.Sub_Option,
        Sub_IsActive: value.Sub_IsActive,
        Type: value.Type,
        value: 0,
        Acc_value: 0,
        Tab_value: 0,
      };
      datarray.push(ldata);
    });

    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          datarray.forEach((obj) => {
            realm.create(Substance_Use_Schema, obj, "modified");
          });
        });
        resolve();
        console.log("Update_Substance_Use_Schema_final");
      })
      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });

export const InsertPatientSurveyMaster = (data) =>
  new Promise((resolve, reject) => {
    //debugger
    console.log(
      "Insert function callded InsertPatientSurveyMaster",
      JSON.stringify(data)
    );
    let obj = data;
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          obj.forEach((obj) => {
            let arrr = {
              SUR_PKeyID: obj.SUR_PKeyID,
              SUR_Heading: obj.SUR_Heading,
              SUR_Question: obj.SUR_Question,
              SUR_Control_Type: obj.SUR_Control_Type,
              SUR_IsActive: obj.SUR_IsActive,
              SUR_IsDelete: obj.SUR_IsDelete,
              Type: obj.Type,
              UserID: obj.UserID,
              Sur_Value: obj.Sur_Value,
              Survey_Questions_Options_DTO: JSON.stringify(
                obj.Survey_Questions_Options_DTO
              ),
            };
            console.log("Survey data insert ", arrr);
            realm.create(Patient_Survey_Schema, arrr);
          });
        });
        resolve();
      })
      .catch((error) => {
        console.log("InsertPatientSurveyMaster", error);
        reject(error);
      });
  });
//update UpdatePatientSurveyMaster default value
export const UpdatePatientSurveyMaster = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Update function callded UpdatePatientSurveyMaster",
      JSON.stringify(data)
    );
    //debugger
    let datarray = [];
    //debugger
    let obj = data.map((value) => {
      console.log(`Checking aray ${JSON.stringify(value)}`);
      let ldata = {
        SUR_PKeyID: value.SUR_PKeyID,
        SUR_Heading: value.SUR_Heading,
        SUR_Question: value.SUR_Question,
        SUR_Control_Type: value.SUR_Control_Type,
        SUR_IsActive: value.SUR_IsActive,
        SUR_IsDelete: value.SUR_IsDelete,
        Type: value.Type,
        UserID: value.UserID,
        Sur_Value: null,
        Survey_Questions_Options_DTO: JSON.stringify(
          value.Survey_Questions_Options_DTO
        ),
      };
      datarray.push(ldata);
    });

    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          datarray.forEach((obj) => {
            realm.create(Patient_Survey_Schema, obj, "modified");
          });
          console.log("UpdatePatientSurveyMaster");
        });
        resolve();
      })
      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });
//updat  values
export const UpdatePatientSurveyMaster_final = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Update function callded UpdatePatientSurveyMaster_final",
      JSON.stringify(data)
    );
    let obj = data;
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          obj.forEach((obj) => {
            realm.create(Patient_Survey_Schema, obj, "modified");
          });
        });
        resolve();
      })
      .catch((error) => {
        console.log("InsertError", error);
        reject(error);
      });
  });
//Health center insert
export const InsertHealthCenterDetails = (data) =>
  new Promise((resolve, reject) => {
    // console.log(
    //   "Insert function callded InsertHealthCenterDetails",
    //   JSON.stringify(data)
    // );
    let obj = data;
    // console.log("lvalue of data", obj);
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          obj.forEach((obj) => {
            console.log("lvalue data", obj);
            realm.create(Center_List_Schema, obj);
          });
        });
        resolve();
      })
      .catch((error) => {
        console.log(" error InsertHealthCenterDetails", error);
        reject(error);
      });
  });

//update PatientDistance details
export const UpdatePatientCenter_List_SchemaMaster = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Update function callded UpdatePatientCenter_List_SchemaMaster",
      JSON.stringify(data)
    );
    let obj = data;
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          obj.forEach((obj) => {
            realm.create(Center_List_Schema, obj, "modified");
            resolve;
          });
        });
        resolve();
      })

      .catch((error) => {
        console.log("UpdatePatientCenter_List_SchemaMaster", error);
        reject(error);
      });
  });

//insert TIP data

export const InsertTechniques_List_SchemaDetails = (data) =>
  new Promise((resolve, reject) => {
    console.log(
      "Insert function callded Techniques_List_Schema",
      JSON.stringify(data)
    );
    let obj = data;
    console.log("lvalue of data", obj);
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          obj.forEach((obj) => {
            console.log("lvalue data", obj);
            realm.create(Techniques_List_Schema, obj);
          });
        });
        resolve();
      })
      .catch((error) => {
        console.log("error Techniques_List_Schema", error);
        reject(error);
      });
  });
// Insert query End

// Select query starts

export const queryBaseDetails = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        let Patient = realm.objects(Patient_Base_Schema);
        let allGoback = Patient; //.filtered('GB_work = true')

        resolve([JSON.parse(JSON.stringify(allGoback))]);
      })
      .catch((error) => {
        reject(error);
      });
  });
//Base survey query
export const queryBaseSurveyDetails = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        let Patient = realm.objects(Patient_Base_Survey_Schema);
        let allGoback = Patient; //.filtered('GB_work = true')

        resolve([JSON.parse(JSON.stringify(allGoback))]);
      })
      .catch((error) => {
        reject(error);
      });
  });
// heatlh center select query
export const queryHealthCenterDetails = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        let Patient = realm.objects(Center_List_Schema);
        let allGoback = Patient; //.filtered('GB_work = true')

        //.filtered('GB_work like % true% ')

        resolve([JSON.parse(JSON.stringify(allGoback))]);
      })
      .catch((error) => {
        reject(error);
      });
  });

// Select Zip Code
// heatlh center select query
export const queryHealthDetails = (zipcode) =>
  new Promise((resolve, reject) => {
    const zipsrting = zipcode.toString();
    Realm.open(databaseOptions)
      .then((realm) => {
        let Patient = realm.objects(Center_List_Schema);
        let allGoback = Patient.filtered(
          `HC_Address CONTAINS[c] "${zipsrting}"`
        ); //.filtered('GB_work = true')
        resolve([JSON.parse(JSON.stringify(allGoback))]);
      })
      .catch((error) => {
        reject(error);
      });
  });

//Tip select details
export const queryTechniques_List_SchemaDetails = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        let Patient = realm.objects(Techniques_List_Schema);
        let allGoback = Patient; //.filtered('GB_work = true')

        resolve([JSON.parse(JSON.stringify(allGoback))]);
      })
      .catch((error) => {
        reject(error);
      });
  });
//Final Score
export const queryAllFinalScore = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        let Score = realm.objects(Final_Scores_Schema);
        let allGoback = Score;
        let array = Array.from(Score);
        resolve([JSON.parse(JSON.stringify(array))]);
      })
      .catch((error) => {
        reject(error);
      });
  });

// Patient_Master
export const queryAllPatient_Master = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        let Patient = realm.objects(Patient_Master_Schema);
        let allGoback = Patient; //.filtered('GB_work = true')

        resolve([JSON.parse(JSON.stringify(allGoback))]);
      })
      .catch((error) => {
        reject(error);
      });
  });
// Depression
export const queryAllPatient_Depression = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        let Patient = realm.objects(Patient_Depression_Schema);
        let allGoback = Patient; //.filtered('GB_work = true')

        resolve([
          JSON.parse(JSON.stringify(allGoback)),
          // console.log("json ashok", JSON.stringify(allGoback)),
        ]);
      })
      .catch((error) => {
        reject(error);
      });
  });
// Mood disorder
export const queryAllPatient_MoodDisorder_Schema = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        let Patient = realm.objects(Patient_MoodDisorder_Schema);
        let allGoback = Patient; //.filtered('GB_work = true')

        resolve([JSON.parse(JSON.stringify(allGoback))]);
      })
      .catch((error) => {
        reject(error);
      });
  });
export const queryAllPatient_MoodDisorder_Schema_2 = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        let Patient = realm.objects(Patient_MoodDisorder_Schema_2);
        let allGoback = Patient; //.filtered('GB_work = true')

        resolve([JSON.parse(JSON.stringify(allGoback))]);
      })
      .catch((error) => {
        reject(error);
      });
  });
export const queryAllPatient_MoodDisorder_Schema_3 = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        let Patient = realm.objects(Patient_MoodDisorder_Schema_3);
        let allGoback = Patient; //.filtered('GB_work = true')

        resolve([JSON.parse(JSON.stringify(allGoback))]);
      })
      .catch((error) => {
        reject(error);
      });
  });
// Anxiety Disorder
export const queryAllPatient_AnxietyDisorder_Schema = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        let Patient = realm.objects(Patient_AnxietyDisorder_Schema);
        let allGoback = Patient; //.filtered('GB_work = true')

        resolve([JSON.parse(JSON.stringify(allGoback))]);
      })
      .catch((error) => {
        reject(error);
      });
  });
// PTSD
export const queryAllPatient_PTSD_Schema = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        let Patient = realm.objects(Patient_PTSD_Schema);
        let allGoback = Patient; //.filtered('GB_work = true')

        resolve([JSON.parse(JSON.stringify(allGoback))]);
      })
      .catch((error) => {
        reject(error);
      });
  });
// Substance Option
export const queryAllSubstance_Option_Schema = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        let Patient = realm.objects(Substance_Option_Schema);
        let allGoback = Patient; //.filtered('GB_work = true')

        resolve([JSON.parse(JSON.stringify(allGoback))]);
      })
      .catch((error) => {
        reject(error);
      });
  });
// Substance use Test
export const queryAllSubstance_Use_Schema = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        let Patient = realm.objects(Substance_Use_Schema);
        let allGoback = Patient; //.filtered('GB_work = true')

        resolve([JSON.parse(JSON.stringify(allGoback))]);
      })
      .catch((error) => {
        reject(error);
      });
  });

//Patient_Treated_Schema
export const queryAllPatient_Treated_Schema = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        let Patient = realm.objects(Patient_Treated_Schema);
        let allGoback = Patient; //.filtered('GB_work = true')

        resolve([JSON.parse(JSON.stringify(allGoback))]);
      })
      .catch((error) => {
        reject(error);
      });
  });

// select PatientSurveyMaster
export const queryAllPatientSurvey_Schema = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        let Patient = realm.objects(Patient_Survey_Schema);
        let allGoback = Patient; //.filtered('GB_work = true')

        resolve([JSON.parse(JSON.stringify(allGoback))]);
      })
      .catch((error) => {
        reject(error);
      });
  });

// Select query ends

// Delete query
export const deleteBaseDetails = (id) =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          let PatientsMaster = realm.objects(Patient_Base_Schema); //.filtered('Inv_Con_pkeyId = "' + id + '"');
          realm.delete(PatientsMaster);
        });
        resolve();
        console.log("deleteBaseDetails");
      })
      .catch((error) => reject(error));
  });
export const deletePatientMaster = (id) =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          let PatientsMaster = realm.objects(Patient_Master_Schema); //.filtered('Inv_Con_pkeyId = "' + id + '"');
          realm.delete(PatientsMaster);
        });
        resolve();
        console.log("deletePatientMaster");
      })
      .catch((error) => reject(error));
  });
export const deleteSubstanceUseMaster = (id) =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          let SubstanceUseMaster = realm.objects(Substance_Use_Schema); //.filtered('Inv_Con_pkeyId = "' + id + '"');
          realm.delete(SubstanceUseMaster);
        });
        resolve();
        console.log("deleteSubstanceUseMaster");
      })
      .catch((error) => reject(error));
  });
export const deleteSubstanceOptionMaster = (id) =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          let SubstanceOptionMasterr = realm.objects(Substance_Option_Schema); //.filtered('Inv_Con_pkeyId = "' + id + '"');
          realm.delete(SubstanceOptionMasterr);
        });
        resolve();
        console.log("deleteSubstanceOptionMasterr");
      })
      .catch((error) => reject(error));
  });

export const deletePatientDepression = (id) =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          let PatientsMaster = realm.objects(Patient_Depression_Schema); //.filtered('Inv_Con_pkeyId = "' + id + '"');
          realm.delete(PatientsMaster);
        });
        resolve();
      })
      .catch((error) => reject(error));
  });

export const deletePatientDepression2 = (id) =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          let PatientsMaster = realm.objects(Patient_MoodDisorder_Schema); //.filtered('Inv_Con_pkeyId = "' + id + '"');
          realm.delete(PatientsMaster);
        });
        resolve();
      })
      .catch((error) => reject(error));
  });
export const deletePatientDepression_2 = (id) =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          console.log("delete dep2");

          let PatientsMaster = realm.objects(Patient_MoodDisorder_Schema_2); //.filtered('Inv_Con_pkeyId = "' + id + '"');
          realm.delete(PatientsMaster);
        });
        resolve();
      })
      .catch((error) => reject(error));
    return "Success";
  });
export const deletePatientDepression_3 = (id) =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          console.log("delete dep3");
          let PatientsMaster = realm.objects(Patient_MoodDisorder_Schema_3); //.filtered('Inv_Con_pkeyId = "' + id + '"');
          realm.delete(PatientsMaster);
        });
        resolve();
      })
      .catch((error) => reject(error));
  });
export const deletePatientDepression3 = (id) =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          let PatientsMaster = realm.objects(Patient_AnxietyDisorder_Schema); //.filtered('Inv_Con_pkeyId = "' + id + '"');
          realm.delete(PatientsMaster);
        });
        resolve();
      })
      .catch((error) => reject(error));
  });
export const deletePatientDepression4 = (id) =>
  new Promise((resolve, reject) => {
    // alert("hiii");
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          let PatientsMaster = realm.objects(Patient_PTSD_Schema); //.filtered('Inv_Con_pkeyId = "' + id + '"');
          realm.delete(PatientsMaster);
          console.log("detele success full ptsd");
        });
        resolve();
      })
      .catch((error) => reject(error));
  });

//Patient_Treated_Schema
export const deletePatient_Treated_Schema = (id) =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          let PatientsMaster = realm.objects(Patient_Treated_Schema); //.filtered('Inv_Con_pkeyId = "' + id + '"');
          realm.delete(PatientsMaster);
        });
        resolve();
      })
      .catch((error) => reject(error));
  });
// delete PatientSurveyMaster
export const deletePatientSurveyMaster_Schema = (id) =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          let PatientsMaster = realm.objects(Patient_Survey_Schema); //.filtered('Inv_Con_pkeyId = "' + id + '"');
          realm.delete(PatientsMaster);
        });
        resolve();
        console.log("deletePatientSurveyMaster_Schema");
      })
      .catch((error) => reject(error));
  });
// delete patient base survey details
export const deletePatientSurveyBaseMaster_Schema = (id) =>
  new Promise((resolve, reject) => {
    //debugger
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          let PatientsMaster = realm.objects(Patient_Base_Survey_Schema); //.filtered('Inv_Con_pkeyId = "' + id + '"');
          realm.delete(PatientsMaster);
        });
        resolve();
        console.log("deletePatientSurveyBaseMaster_Schema");
      })
      .catch((error) => reject(error));
  });
//delete health center

export const deletePatientHealthCenters_Schema = (id) =>
  new Promise((resolve, reject) => {
    //debugger
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          let PatientsMaster = realm.objects(Center_List_Schema); //.filtered('Inv_Con_pkeyId = "' + id + '"');
          realm.delete(PatientsMaster);
        });
        resolve();
      })
      .catch((error) => reject(error));
  });

//Tip delete data

export const deletePatientTechniques_List_Schema_Schema = (id) =>
  new Promise((resolve, reject) => {
    //debugger
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          let PatientsMaster = realm.objects(Techniques_List_Schema); //.filtered('Inv_Con_pkeyId = "' + id + '"');
          realm.delete(PatientsMaster);
        });
        resolve();
      })
      .catch((error) => reject(error));
  });

export const deleteFinal_Scores_Schema = (id) =>
  new Promise((resolve, reject) => {
    //debugger
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          let PatientsMaster = realm.objects(Final_Scores_Schema); //.filtered('Inv_Con_pkeyId = "' + id + '"');
          realm.delete(PatientsMaster);
        });
        resolve();
      })
      .catch((error) => reject(error));
  });

//Delete query end

export default new Realm(databaseOptions);
