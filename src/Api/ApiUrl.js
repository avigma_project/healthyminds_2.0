// const BASE_URL = "http://hospapi.ikaart.org";

const BASE_URL = "https://api.healthelivin.org";
//hospapi.ikaart.org
//
export const API = {
  GET_ALL_DATA: BASE_URL + "/api/HealthyMindsApp/GetMasterDetails",
  LOGIN_API: BASE_URL + "/token",
  REGISTRATION_API: BASE_URL + "/token",
  FORGETPASSWORD_API: BASE_URL + "/api/HealthyMindsApp/ForGotPassword",
  GET_USER_DATA: BASE_URL + "/api/HealthyMindsApp/GetUserMasterData",
  UPDAT_USER_DATA: BASE_URL + "/api/HealthyMindsApp/AddUserMasterData",
  STORE_IMAGE_API: BASE_URL + "/api/HealthyMindsApp/AddUserMasterData",
  POST_DETAILS: BASE_URL + "/api/HealthyMindsApp/PostPatientDetails",
  GET_CENTERS: BASE_URL + "/api/HealthyMindsApp/GetHealthCenterDataWithDist",
  GET_BLOG_DATA: BASE_URL + "/api/HealthyMindsApp/GetBlogsMasterData",
  GET_BLOG_SEARCH_DATA: BASE_URL + "/api/HealthyMindsApp/SearchBlogsMasterData",
  GET_PATIENT_DETAILS: BASE_URL + "/api/HealthyMindsApp/GetPatientDetails",

  // GET_HEALTH_CENTER: "/api/HealthyMindsApp/GetHealthCenterDataWithDist",
};
