import axios from "axios";
import { API } from "./ApiUrl";

//base Function for all the api

export function fetchapi(data, access_token) {
  return axios(API.POST_DETAILS, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + access_token,
    },
    responseType: "json",
    data: data,
  });
}

export function fetchcenter(data) {
  console.log(data, "GET_CENTERS");
  return axios(API.GET_CENTERS, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    responseType: "json",
    data: data,
  });
}

export function getdata(data, access_token) {
  return axios(API.GET_ALL_DATA, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + access_token,
    },
    responseType: "json",
    data: data,
  });
}

const axiosTiming = (instance) => {
  instance.interceptors.request.use((request) => {
    request.ts = Date.now();
    // console.log("request send", request);
    return request;
  });
  //   NetInfo.fetch().then((state) => {
  //     if (state.isConnected) {
  //       console.log("Connection type", state.type);
  //       console.log("Is connected?", state.isConnected);
  //     } else {
  //       alert("plzz check internet connection");
  //     }
  //   });

  instance.interceptors.response.use((response) => {
    const timeInMs = `${Number(Date.now() - response.config.ts).toFixed()}ms`;
    response.latency = timeInMs;
    // console.log("response recived", response);

    return response;
  });
};
axiosTiming(axios);

export const register = async (data) => {
  // console.log("REGISTRATION_API", data);
  return axios(API.REGISTRATION_API, {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    data,
  })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

export const login = async (data) => {
  return axios(API.LOGIN_API, {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      Accept: "application/json",
    },
    data,
  })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

export const forgotpassword = async (data) => {
  console.log(data);
  return axios(API.FORGETPASSWORD_API, {
    method: "POST",
    data,
  })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};
export const userprofile = async (data, access_token) => {
  return axios(API.GET_USER_DATA, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + access_token,
    },
    data,
  })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

export const registerStoreImage = async (data, access_token) => {
  return axios(API.STORE_IMAGE_API, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + access_token,
    },
    data,
  })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

export const updateprofile = async (data, access_token) => {
  return axios(API.UPDAT_USER_DATA, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + access_token,
    },
    data,
  })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

export const getblogdata = async (data) => {
  return axios(API.GET_BLOG_DATA, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    data,
  })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

export const getblogsearchdata = async (data) => {
  return axios(API.GET_BLOG_SEARCH_DATA, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    data,
  })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};
export const getpatientdetails = async (data, access_token) => {
  console.log("access_token", access_token);
  return axios(API.GET_PATIENT_DETAILS, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + access_token,
    },
    data,
  })
    .then((response) => response.data)
    .catch((error) => {
      throw error;
    });
};

// export const gethealthcenter = async (data) => {
//   return axios(API.GET_HEALTH_CENTER, {
//     method: "POST",
//     headers: {
//       "Content-Type": "application/json",
//     },
//     data,
//   })
//     .then((response) => response.data)
//     .catch((error) => {
//       throw error;
//     });
// };
