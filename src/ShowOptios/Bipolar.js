import React from "react";
import {
  View,
  Text,
  Animated,
  Image,
  Dimensions,
  ToastAndroid,
  SafeAreaView,
  Button,
} from "react-native";
import { Actions } from "react-native-router-flux";
import Toast from "react-native-simple-toast";
import * as database from "../Database/allSchemas";
import strings from "../Language/Language";
import NativeBaseButton from "../components/NativeBaseButton";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

height = deviceHeight * 2;

export default class Bipolar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    // this.GetMasterDetails();
    // setTimeout(() => Actions.Welcome(), 5000);
  }

  render() {
    return (
      <SafeAreaView style={styles.con}>
        <View style={{ alignSelf: "center", width: "90%" }}>
          <Text style={{ fontSize: 20 }}>{strings.screen2_2}</Text>
        </View>
        <View style={{ alignSelf: "center", width: "90%", marginTop: "5%" }}>
          <Text style={{ fontSize: 20 }}>{strings.screen2}</Text>
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-around",
            marginTop: "5%",
            width: "100%",
          }}
        >
          {/* <View style={{ alignItems: 'center', marginTop: '5%', backgroundColor: "#4B96FB", width: '40%', alignSelf: 'center', padding: '3%' }} >
                            <Button
                                onPress={() => Actions.MDQ()}
                                title={strings.continue}
                                color="#ffffff"
                                style={styles.button}
                            />
                        </View> */}
          <NativeBaseButton
            onPress={() => Actions.MDQ()}
            label={strings.continue}
          />
          {/* <View style={{ alignItems: 'center', marginTop: '5%', backgroundColor: "#4B96FB", width: '40%', alignSelf: 'center', padding: '3%' }} >
                            <Button
                                onPress={() => Actions.Anxiety()}
                                title={strings.skip}
                                color="#ffffff"
                                style={styles.button}
                            />
                        </View> */}
          <NativeBaseButton
            onPress={() => Actions.Anxiety()}
            label={strings.skip}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const styles = {
  con: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
  },
  circle: {
    width: height,
    height,
    // backgroundColor: '#4B96FB',
    borderRedius: deviceHeight,
    position: "absolute",
    zIndex: -1,
  },
  icon: {
    width: "90%",
    height: "50%",
  },
  button: {
    width: (deviceWidth * 30) / 100,
    height: (deviceHeight * 8) / 100,
    backgroundColor: "#4B96FB",
    borderRadius: 5,
    marginVertical: 12,
    paddingVertical: 12,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
  },
  _button: {
    width: (deviceWidth * 20) / 100,
    height: (deviceHeight * 4) / 100,
    backgroundColor: "#4B96FB",
    borderRadius: 5,
    marginVertical: 7,
    paddingVertical: 7,
  },
  button_Text: {
    fontSize: 12,
    color: "#ffffff",
    textAlign: "center",
  },
  buttonText: {
    fontSize: 16,
    color: "#ffffff",
    textAlign: "center",
  },
};
