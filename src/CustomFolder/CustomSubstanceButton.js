import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Keyboard,
  Dimensions,
} from "react-native";
const deviceWidth = Dimensions.get("window").width;
export default class CustomSubstanceButton extends Component {
  constructor() {
    super();
    this.state = {};
  }
  render() {
    return (
      <View
        style={{
          justifyContent: "center",
        }}
      >
        <TouchableOpacity
          // onPress={Keyboard.dismiss()}
          onPress={this.props.onPress}
          style={{
            backgroundColor: this.props.backgroundColor,
            width: this.props.width,
            height: this.props.height,
            justifyContent: "center",
            alignItems: "center",
            borderRadius: this.props.redius,
            borderColor: "#A1683A",
            borderWidth: 1,
          }}
        >
          <View
            style={{
              width: (deviceWidth * 85.5) / 100,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Text
              style={{
                color: this.props.color,
                fontSize: 15,
                fontWeight: "700",
                lineHeight: 22,
                fontFamily: "Quicksand-Bold",
              }}
            >
              {this.props.title}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
