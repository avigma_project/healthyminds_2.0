import React, { Component } from "react";
import { Text, View, TouchableOpacity } from "react-native";

export default class CustomDisButton extends Component {
  render() {
    return (
      <View
        style={{
          // backgroundColor: 'red',
          alignItems: "center",
        }}
      >
        <TouchableOpacity
          onPress={this.props.onPress}
          style={{
            backgroundColor: "#BDBDBD",
            width: this.props.width,
            height: this.props.height,
            justifyContent: "center",
            alignItems: "center",
            borderRadius: this.props.redius,
          }}
        >
          <Text
            style={{
              color: "#F2F2F2",
              fontSize: 16,
              fontWeight: "700",
              lineHeight: 22,
              fontFamily: "Quicksand-Bold",
            }}
          >
            {this.props.title}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
