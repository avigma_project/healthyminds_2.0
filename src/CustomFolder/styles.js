import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  sliderContainer: {
    paddingVertical: 1,
  },
  titleContainer: {
    alignItems: "center",
    justifyContent: "center",
  },
  thumb: {
    height: 40,
    width: 40,
    borderRadius: 40 / 2,
    borderWidth: 2,
    borderColor: "#A1683A",
  },
  track: {
    height: 8,
  },
  caption: {
    fontSize: 14,
    lineHeight: 20,
    color: "#060D0B",
    fontFamily: "Quicksand-Bold",
    fontWeight: "700",
  },
  caption2: {
    fontSize: 14,
    lineHeight: 20,
    color: "#A1683A",
    fontFamily: "Quicksand-Bold",
    fontWeight: "700",
  },
});

const borderWidth = 4;
export const trackMarkStyles = StyleSheet.create({
  activeMark: {
    borderColor: "#F8D470",
    borderWidth,
    left: -borderWidth / 2,
  },
  inactiveMark: {
    borderColor: "#F8D470",
    borderWidth,
    left: -borderWidth / 2,
  },
});
