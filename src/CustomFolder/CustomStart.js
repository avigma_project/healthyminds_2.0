import React, { Component } from "react";
import { Text, View, TouchableOpacity } from "react-native";
import strings from "../Language/Language";

export default class CustomStart extends Component {
  render() {
    return (
      <View
        style={{
          // borderWidth: 1,
          justifyContent: "center",
          // alignItems: "center",
          right: 10,
        }}
      >
        <TouchableOpacity
          onPress={this.props.onPress}
          style={
            {
              // justifyContent: "center",
              // alignItems: "center",
            }
          }
        >
          <Text
            style={{
              color: "#377867",
              fontSize: 14,
              fontWeight: "700",
              lineHeight: 20,
              fontFamily: "Quicksand-Bold",
            }}
          >
            {strings.start}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
