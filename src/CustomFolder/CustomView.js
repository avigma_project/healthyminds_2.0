import React, { Component } from "react";
import { Text, View, TouchableOpacity } from "react-native";

export default class CustomView extends Component {
  render() {
    return (
      <View style={{ marginTop: 32, marginHorizontal: 17 }}>
        <View>
          <Text
            style={{
              fontSize: 14,
              fontWeight: "400",
              lineHeight: 18,
              // //fontFamily: "OpenSans-Regular",
            }}
          >
            {this.props.title}
          </Text>
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            marginTop: 16,
          }}
        >
          <View>
            <Text
              style={{
                fontSize: 16,
                fontWeight: "400",
                lineHeight: 22,
                // //fontFamily: "OpenSans-Regular",
              }}
            >
              {this.props.name ? this.props.name : "N/A"}
            </Text>
          </View>
          {/* <TouchableOpacity onPress={this.props.onPress}>
            <Text
              style={{
                color: "#377867",
                fontSize: 16,
                fontWeight: "700",
                lineHeight: 22,
               //fontFamily: "Quicksand-Bold",
              }}
            >
              Edit
            </Text>
          </TouchableOpacity> */}
        </View>
      </View>
    );
  }
}
