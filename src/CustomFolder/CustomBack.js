import React, { Component } from "react";
import { View, TouchableOpacity, Text } from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import strings from "../Language/Language";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default class CustomBack extends Component {
  state = {
    langugageCode: "",
  };
  componentDidMount() {
    const { navigation } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      this.getlanguage();
    });
  }

  async getlanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      this.setState({ langugageCode: get_Data });
    });
  }

  render() {
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          marginVertical: 16,
          // backgroundColor: "pink",
          alignItems: "center",
        }}
      >
        <View>
          {this.props.back && (
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <AntDesign name="left" size={25} color="#377867" />
            </TouchableOpacity>
          )}
          {this.props.back2 && (
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("Search", {
                  screen: "Welcome",
                })
              }
            >
              <AntDesign name="left" size={25} color="#377867" />
            </TouchableOpacity>
          )}
        </View>
        <TouchableOpacity
          onPress={this.props.onPress}
          style={{
            width: this.state.langugageCode != 1 ? "50%" : "40%",
            // backgroundColor: "orange",
            justifyContent: "flex-end",
            flexDirection: "row",
            width: "50%",
          }}
        >
          <Text
            style={{
              fontSize: 16,
              color: "#377867",
              fontWeight: "700",
              fontFamily: "Quicksand-Bold",
            }}
          >
            {strings.login}/{strings.signup}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
