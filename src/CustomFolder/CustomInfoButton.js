import React, { Component } from "react";
import { Text, View, TouchableOpacity, Keyboard } from "react-native";

export default class CustomInfoButton extends Component {
  constructor() {
    super();
    this.state = {
      // buttonColor: "#F2F2F2",
      // textColor: "#A1683A",
    };
  }
  // selectedValue(value) {
  //   if (value == 1) {
  //     this.setState({
  //       buttonColor: "#A1683A",
  //       textColor: "#F2F2F2",
  //     });
  //   } else {
  //     this.setState({
  //       buttonColor: "#F2F2F2",
  //       textColor: "#A1683A",
  //     });
  //   }
  // }
  render() {
    return (
      <View
        style={{
          justifyContent: "center",
        }}
      >
        <TouchableOpacity
          // onPress={Keyboard.dismiss()}
          onPress={this.props.onPress}
          style={{
            backgroundColor: this.props.backgroundColor,
            width: this.props.width,
            height: this.props.height,
            justifyContent: "center",
            alignItems: "center",
            borderRadius: this.props.redius,
            borderColor: "#A1683A",
            borderWidth: 1,
          }}
        >
          <Text
            style={{
              color: this.props.color,
              fontSize: 16,
              fontWeight: "700",
              lineHeight: 22,
              fontFamily: "Quicksand-Bold",
            }}
          >
            {this.props.title}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
