import React, { Component } from "react";
import SnapSlider from "react-native-snap-slider";

import { StyleSheet, Text, View, Dimensions } from "react-native";
const deviceWidth = Dimensions.get("window").width;

export default class SliderTest extends Component {
  sliderOptions = [
    { value: 0, label: "None or little of the time" },
    { value: 1, label: "Some of the time" },
    { value: 2, label: "Most of the time" },
    { value: 3, label: "All the time" },
  ];
  constructor(props) {
    super(props);
    this.state = this.getInitialState();
    this.slidingComplete = this.slidingComplete.bind(this);
  }

  getInitialState() {
    return {
      itemVal: 0,
    };
  }

  slidingComplete(itemSelected) {
    itemSelected = this.sliderOptions[this.refs.slider.state.item].value;
    // this.setState({ itemVal: itemSelected });
    this.state.itemVal = itemSelected;

    alert(this.state.itemVal);
  }

  render() {
    return (
      <View style={styles.container}>
        <SnapSlider
          ref="slider"
          containerStyle={styles.snapsliderContainer}
          style={styles.snapslider}
          itemWrapperStyle={styles.snapsliderItemWrapper}
          itemStyle={styles.snapsliderItem}
          items={this.sliderOptions}
          labelPosition="top"
          defaultItem={0}
          onSlidingComplete={this.slidingComplete}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  snapsliderContainer: {
    borderWidth: 0,
    backgroundColor: "transparent",
  },
  snapslider: {
    borderWidth: 0,
    width: (deviceWidth * 90) / 100,
  },
  snapsliderItemWrapper: {
    borderWidth: 0,
  },
  snapsliderItem: {
    borderWidth: 0,
    width: "20%",
    fontSize: 14,
    lineHeight: 20,
    color: "#060D0B",
    fontFamily: "Quicksand-Bold",
    fontWeight: "700",
  },
});
