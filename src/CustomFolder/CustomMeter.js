import React, { Component } from "react";
import { View, TouchableOpacity, Text } from "react-native";
import RNSpeedometer from "react-native-speedometer";

export default class CustomMeter extends Component {
  render() {
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          marginTop: "5%",
        }}
      >
        <RNSpeedometer
          value={(this.props.value * 100) / 30}
          size={200}
          // needleImage={require("../../Images/Arrow.png")}
          // imageWrapperStyle={{}}
          needleImage={false}
          labels={[
            {
              labelColor: "#377867",
              activeBarColor: "#377867",
            },
            {
              labelColor: "#F8D470",
              activeBarColor: "#F8D470",
            },
            {
              labelColor: "#EB5757",
              activeBarColor: "#EB5757",
            },
          ]}
          labelStyle={{
            color: "#060D0B",
            marginTop: "-25%",
            fontSize: 25,
            lineHeight: 32,
          }}
        />
      </View>
    );
  }
}
