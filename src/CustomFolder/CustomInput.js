import React, { Component } from "react";
import { Text, View, TextInput } from "react-native";

export default class CustomInput extends Component {
  render() {
    const {
      placeholder,
      heading,
      placeholderTextColor,
      value,
      onChangeText,
      keyboardType,
      onEndEditing,
      secureTextEntry,
      defaultValue,
      onSubmitEditing,
      searchtext,
    } = this.props;
    return (
      <View>
        {heading && (
          <View
            style={{
              marginBottom: 8,
              marginTop: searchtext ? 0 : 15,
            }}
          >
            <Text
              style={{
                fontSize: 16,
                lineHeight: 22,
                color: "#060D0B",
                // fontFamily: "Quicksand-Bold",
                fontWeight: "400",
              }}
            >
              {heading}
            </Text>
          </View>
        )}

        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            // marginHorizontal: 17,
          }}
        >
          <View
            style={{
              height: 40,
              justifyContent: "center",
              borderWidth: 1,
              borderColor: "gray",
              width: "100%",
              paddingHorizontal: "4%",
              borderRadius: 3,
            }}
          >
            <TextInput
              onSubmitEditing={onSubmitEditing}
              placeholder={placeholder}
              value={value}
              defaultValue={defaultValue}
              keyboardType={keyboardType}
              placeholderTextColor={placeholderTextColor}
              onChangeText={onChangeText}
              onEndEditing={onEndEditing}
              secureTextEntry={secureTextEntry}
              placeholderStyle={
                {
                  // color: "black",
                  // //fontFamily: "OpenSans-Regular",
                }
              }
              style={{ color: "#000000" }}
            />
          </View>
        </View>
      </View>
    );
  }
}
