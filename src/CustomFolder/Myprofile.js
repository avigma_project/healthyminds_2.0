import React, { Component } from "react";
import { View, TouchableOpacity, Text } from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import strings from "../Language/Language";

export default class Myprofile extends Component {
  render() {
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          marginVertical: 16,
          // backgroundColor: "red",
          alignItems: "center",
        }}
      >
        <View>
          {this.props.back && (
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <AntDesign name="left" size={25} color="#377867" />
            </TouchableOpacity>
          )}
          {this.props.back2 && (
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("Search", {
                  screen: "Welcome",
                })
              }
            >
              <AntDesign name="left" size={25} color="#377867" />
            </TouchableOpacity>
          )}
        </View>
        <TouchableOpacity
          onPress={this.props.onPress}
          style={{
            width: "40%",
            // backgroundColor: "orange",
            justifyContent: "flex-end",
            flexDirection: "row",
          }}
        >
          <Text
            style={{
              fontSize: 16,
              color: "#377867",
              fontWeight: "700",
              fontFamily: "Quicksand-Bold",
            }}
          >
            {strings.myprofile}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
