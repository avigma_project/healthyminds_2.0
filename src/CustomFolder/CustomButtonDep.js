import React, { Component } from "react";
import { Text, View, TouchableOpacity, Keyboard } from "react-native";

export default class CustomButtonDep extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (
      <View style={{}}>
        <TouchableOpacity
          // onPress={Keyboard.dismiss()}
          onPress={this.props.onPress}
          style={{
            backgroundColor: this.props.backgroundColor,
            width: this.props.width,
            height: this.props.height,
            justifyContent: "center",
            alignItems: "center",
            borderRadius: this.props.redius,
            borderColor: "#A1683A",
            borderWidth: 1,
          }}
        >
          <Text
            style={{
              color: this.props.color,
              fontSize: 16,
              fontWeight: "700",
              // lineHeight: 16,
              width: "75%",

              fontFamily: "Quicksand-Bold",
            }}
          >
            {this.props.title}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
