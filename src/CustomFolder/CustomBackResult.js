import React, { Component } from "react";
import { View, TouchableOpacity, Text, Platform } from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import strings from "../Language/Language";

export default class CustomBackResult extends Component {
  render() {
    return (
      <View
        style={{
          marginTop: 20,
          // backgroundColor: "pink",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <TouchableOpacity style={{}} onPress={this.props.navigate}>
          <AntDesign name="left" size={25} color="#377867" />
        </TouchableOpacity>
        <Text
          style={{
            fontSize: 18,
            lineHeight: 24,
            fontWeight: "700",
            fontFamily: "Quicksand-Bold",
          }}
        >
          {strings.result}
        </Text>
        <View
          style={{
            paddingRight: 10,
          }}
        >
          <TouchableOpacity onPress={this.props.onPress} style={{}}>
            <Text
              style={{
                fontSize: 16,
                color: "#377867",
                fontWeight: "700",
                width: "100%",
                fontFamily: "Quicksand-Bold",
              }}
            >
              {strings.login}/{strings.signup}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
