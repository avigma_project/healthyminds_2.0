import React, { Component } from "react";
import { Text, View, TouchableOpacity } from "react-native";

export default class Header extends Component {
  render() {
    return (
      <View
        style={{
          width: "100%",
          // backgroundColor: 'red',
          justifyContent: "flex-end",
          flexDirection: "row",
          marginTop: 8,
          marginBottom: 16,
        }}
      >
        <TouchableOpacity
          style={{
            backgroundColor: "#F8D470",
            paddingHorizontal: 12,
            paddingVertical: 8,
            // width: '20%',
            justifyContent: "center",
            alignItems: "center",
            borderRadius: 20,
            marginRight: 20,
          }}
          onPress={this.props.onPress}
        >
          <Text
            style={{
              color: "#A1683A",
              fontWeight: "bold",
              lineHeight: 20,
              fontSize: 14,
              fontFamily: "Quicksand-Bold",
            }}
          >
            Spanish
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
