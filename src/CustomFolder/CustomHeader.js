import React, { Component } from "react";
import { Text, View, TouchableOpacity } from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import strings from "../Language/Language";

export default class CustomHeader extends Component {
  render() {
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
          height: 50,
          // zIndex: 1111,
          // backgroundColor: "pink",
        }}
      >
        <TouchableOpacity
          onPress={() => this.props.navigation.goBack()}
          style={{
            width: "5%",
            // left: 20,
            // backgroundColor: "red",
          }}
        >
          <AntDesign name="left" size={30} style={{ color: "#377867" }} />
        </TouchableOpacity>
        <View
          style={{
            width: "70%",
            alignItems: "center",
            left: 10,
            // backgroundColor: "green",
          }}
        >
          <Text
            style={{
              fontFamily: "Quicksand-Bold",
              lineHeight: 26,
              color: "#060D0B",
              fontWeight: "bold",
              fontSize: 18,
            }}
          >
            {this.props.title}
          </Text>
        </View>
        <TouchableOpacity
          onPress={this.props.onPress}
          style={{
            width: "20%",
            flexDirection: "row",
            justifyContent: "space-evenly",
            // right: 20,
            paddingRight: 15,
          }}
        >
          {this.props.save && (
            <Text
              style={{
                fontFamily: "Quicksand-Bold",
                color: "#377867",
                fontWeight: "bold",
                lineHeight: 20,
                fontWeight: "700",
                fontSize: 16,
              }}
            >
              {strings.save}
            </Text>
          )}
        </TouchableOpacity>
      </View>
    );
  }
}
