import * as React from "react";
import { SafeAreaView, ScrollView, Text, View, Dimensions } from "react-native";
import { Slider } from "@miblanchard/react-native-slider";
const deviceWidth = Dimensions.get("window").width;
import { styles, trackMarkStyles } from "./styles";

const DEFAULT_VALUE = 0.2;

const SliderContainer = (props: {
  caption: string,
  children: React.Node,
  sliderValue?: Array<number>,
  trackMarks?: Array<number>,
  OnChange: () => number,
}) => {
  const { caption, sliderValue, trackMarks, OnChange } = props;

  const [value, setValue] = React.useState(
    sliderValue ? sliderValue : DEFAULT_VALUE
  );

  React.useEffect(() => {
    let mounted = true;
    if (mounted) {
      OnChange(value);
    }
    return () => {
      mounted = false;
    };
  }, [value]);

  let renderTrackMarkComponent;
  if (trackMarks?.length) {
    renderTrackMarkComponent = (index: number) => {
      const currentMarkValue = trackMarks[index];
      const style =
        currentMarkValue > Math.max(value)
          ? trackMarkStyles.activeMark
          : trackMarkStyles.inactiveMark;
      return <View style={style} />;
    };
  }

  const renderChildren = () => {
    return React.Children.map(props.children, (child) => {
      if (!!child && child.type === Slider) {
        return React.cloneElement(child, {
          onValueChange: setValue,
          renderTrackMarkComponent,
          trackMarks,
          value,
        });
      }
      return child;
    });
  };
  // console.log(value);
  return (
    <View style={{ paddingVertical: 1 }}>
      <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
        <Text style={value == 3 ? styles.caption2 : styles.caption}>
          None or{"\n"}little of{"\n"}the time
        </Text>
        <Text style={value == 8 ? styles.caption2 : styles.caption}>
          Some of{"\n"}the time
        </Text>
        <Text style={value == 15 ? styles.caption2 : styles.caption}>
          Most of{"\n"}the time
        </Text>
        <Text style={value == 20 ? styles.caption2 : styles.caption}>
          {"    "}All{"\n"}the time
        </Text>

        {/* <Text>{Array.isArray(value) ? value.join(" - ") : value}</Text> */}
      </View>
      {renderChildren()}
    </View>
  );
};

const CustomSlider = (props) => {
  return (
    <SafeAreaView>
      <ScrollView>
        <View>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginTop: 5,
            }}
          >
            <SliderContainer
              sliderValue={[1]}
              trackMarks={[3, 8, 15, 20]}
              OnChange={(value) => props.OnChange(value)}

              // OnChange={() => OnChange}
            >
              <Slider
                containerStyle={{
                  width: (deviceWidth * 90) / 100,
                }}
                maximumValue={20}
                minimumValue={1}
                step={1}
                thumbTintColor="#F8D470"
                minimumTrackTintColor="#A1683A"
                maximumTrackTintColor="#A1683A"
                thumbStyle={styles.thumb}
                trackStyle={styles.track}
                onSlidingComplete={props.onSlidingComplete}
                value={props.value}
                trackClickable={true}
              />
            </SliderContainer>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default CustomSlider;
