import React from "react";
import {
  View,
  Text,
  Dimensions,
  FlatList,
  BackHandler,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Platform,
  Linking,
} from "react-native";
import Myprofile from "../../CustomFolder/Myprofile";
import AsyncStorage from "@react-native-async-storage/async-storage";
import CustomBack from "../../CustomFolder/CustomBack";
import * as database from "../../Database/allSchemas";
import strings from "../../Language/Language";
import CustomBackResult from "../../CustomFolder/CustomBackResult";
import CustomButton from "../../CustomFolder/CustomButton";
import Ionicons from "react-native-vector-icons/Ionicons";
import CustomMeter from "../../CustomFolder/CustomMeter";
import RNSpeedometer from "react-native-speedometer";
import { userprofile, getpatientdetails, fetchapi } from "../../Api/function";
import Spinner from "react-native-loading-spinner-overlay";
import AntDesign from "react-native-vector-icons/AntDesign";
const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;
const lvalue = [];

export default class ExistResult extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      token: "",
      firstname: "",
      patient_base: "",
      patient_master: "",
      result: [],
      mood_Disorder_1: "",
      mood_Disorder_2: "",
      subtitle: [],
      previousdate: null,
      call: false,
    };
  }
  // componentWillMount() {
  //   //debugger
  //   BackHandler.addEventListener(
  //     "hardwareBackPress",
  //     this.backHandler.bind(this)
  //   );
  // }

  // database
  // .queryAllFinalScore()
  // .then((res) => {
  //   Score = Object.values(res[0]);

  // componentWillUnmount() {
  //   //debugger
  //   BackHandler.addEventListener(
  //     "hardwareBackPress",
  //     this.backHandler.bind(this)
  //   );
  // }
  componentDidMount() {
    //debugger
    const { navigation } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      this.getToken();
      this.getScore();
    });
  }
  componentWillUnmount() {
    this._unsubscribe;
  }
  getUserData = async (token) => {
    // this.setState({
    //   isLoading: true,
    // });
    var data = JSON.stringify({
      Type: 2,
    });
    try {
      const res = await userprofile(data, token);
      console.log(res, "resssss");
      this.setState({
        firstname: res[0][0].User_Name,
        // isLoading: false,
      });
      console.log(this.state.firstname, "namemmemmeme");
    } catch (error) {
      console.log("hihihihihihih", { e: error.response.data.error });
      let message = "";
      if (error.response) {
        // this.setState({ isLoading: false });
      } else {
        message = "";
      }
      console.log({ message });
    }
  };
  GetPatientDetails = async (token) => {
    this.setState({ isLoading: true });
    var data = {
      Type: 3,
    };
    try {
      const res = await getpatientdetails(data, token);

      if (res[0].length > 0 && this.state.previousdate === null) {
        this.setState({ isLoading: false });
        this.setState({ previousdate: res[0][0].HMP_CreatedOn });
      } else {
        this.setState({ previousdate: null });
      }
      this.setState({ isLoading: false });
    } catch (error) {
      console.log(error);
    }
  };

  getToken = async () => {
    let token;
    try {
      token = await AsyncStorage.getItem("token");
      if (token) {
        this.GetPatientDetails(token);
        this.getUserData(token);
        this.setState({ token: token });
      } else {
        this.setState({ token: "" });
        console.log("no token found");
      }
    } catch (e) {
      console.log(e);
    }
    console.log("userTokannnnnn", token);
  };

  async storeData() {
    await AsyncStorage.getItem("PatientDetials", (err, ldata) => {
      // console.log("AsyncStorage profile", ldata);
    });
    await AsyncStorage.setItem("PatientDetials", "2");
  }

  getScore = () => {
    //debugger
    // console.log("resp", lvalue);
    database
      .queryAllFinalScore()
      .then((result) => {
        console.log(`Getting all the score value: ${JSON.stringify(result)}`);
        this.setState({ result: result });
      })
      .catch((error) => {
        console.log(`Error in getting score value: ${error}`);
      });
  };

  contact = () => {
    Linking.openURL("https://www.healthelivin.org/contact-us/");
  };

  getDescription = (title, score) => {
    // console.log("Title: ", title, "Score: ", score);

    if (title === "Depression") {
      if (score <= 8) {
        // return 'A complete evaluation is not recommended';
        return (
          <View style={{ marginLeft: "10%" }}>
            <View
              style={{
                width: (deviceWidth * 40) / 100,
              }}
            >
              <Text style={styles.complete}>{strings.depressive}</Text>
            </View>
          </View>
        );
      } else if (score <= 16 && score >= 9) {
        // return 'A complete evalutaion is recommended';
        return (
          <View style={{ marginLeft: "10%" }}>
            <View style={{ width: (deviceWidth * 40) / 100 }}>
              <Text style={styles.complete}>
                {strings.depressive1}
                {/* {strings.Evaluationrecommended} */}
              </Text>
            </View>
            <TouchableOpacity
              style={styles.button}
              onPress={() =>
                this.props.navigation.navigate("Search", { screen: "Welcome" })
              }
            >
              <Text style={styles.buttonText}>{strings.clickhereforeval}</Text>
            </TouchableOpacity>
          </View>
        );
      } else if (score <= 40 && score >= 17) {
        // return 'A complete evalutaion is strongly recommended';
        return (
          <View style={{ marginLeft: "10%" }}>
            <View style={{ width: (deviceWidth * 40) / 100 }}>
              <Text style={styles.complete}>
                {strings.depressive2}
                {/* {strings.Evaluation} */}
              </Text>
            </View>
            <TouchableOpacity
              style={styles.button}
              onPress={() =>
                this.props.navigation.navigate("Search", { screen: "Welcome" })
              }
            >
              <Text style={styles.buttonText}>{strings.clickhereforeval}</Text>
            </TouchableOpacity>
          </View>
        );
      }
    }
    if (title === "Bipolar") {
      const ldata = lvalue;
      if (
        // this.state.mood_Disorder_1 != undefined ||
        // this.state.mood_Disorder_2 != undefined ||
        score >= 0
      ) {
        return (
          <View style={{ marginTop: "-10%" }}>
            <View style={{ flexDirection: "row", marginTop: "3%" }}>
              <Ionicons
                name={
                  score >= 7 && score <= 13
                    ? "checkbox-outline"
                    : "square-outline"
                }
                size={30}
                style={{
                  color: score >= 7 && score <= 13 ? "#EB5757" : "#060D0B",
                }}
              />
              <View style={{ marginLeft: "5%" }}>
                <Text
                  style={{
                    color: score >= 7 && score <= 13 ? "#EB5757" : "#060D0B",
                    fontFamily: "verdana",
                    fontSize: 16,
                    lineHeight: 22,
                    fontWeight: "400",
                    width: (deviceWidth * 75) / 100,
                  }}
                >
                  {strings.bipolor1}
                </Text>
              </View>
            </View>

            <View style={{ flexDirection: "row", marginTop: "1%" }}>
              <Ionicons
                name={
                  this.state.mood_Disorder_2 != undefined &&
                  this.state.mood_Disorder_2.value == true
                    ? "checkbox-outline"
                    : "square-outline"
                }
                size={30}
                style={{
                  color:
                    this.state.mood_Disorder_2 != undefined &&
                    this.state.mood_Disorder_2.value == true
                      ? "#EB5757"
                      : "#060D0B",
                }}
              />
              <View style={{ marginLeft: "5%" }}>
                <Text
                  style={{
                    color:
                      this.state.mood_Disorder_2 != undefined &&
                      this.state.mood_Disorder_2.value == true
                        ? "#EB5757"
                        : "#060D0B",
                    fontFamily: "verdana",
                    fontSize: 16,
                    lineHeight: 22,
                    fontWeight: "400",
                    width: (deviceWidth * 75) / 100,
                  }}
                >
                  {strings.bipolor2}
                </Text>
              </View>
            </View>
            <View style={{ flexDirection: "row", marginTop: "3%" }}>
              <View>
                <Ionicons
                  name={
                    this.state.mood_Disorder_1 != undefined &&
                    this.state.mood_Disorder_1.value >= 3
                      ? "checkbox-outline"
                      : "square-outline"
                  }
                  size={30}
                  style={{
                    color:
                      this.state.mood_Disorder_1 != undefined &&
                      this.state.mood_Disorder_1.value >= 3
                        ? "#EB5757"
                        : "#060D0B",
                  }}
                />
              </View>
              <View style={{ marginLeft: "5%" }}>
                <Text
                  style={{
                    color:
                      this.state.mood_Disorder_1 != undefined &&
                      this.state.mood_Disorder_1.value >= 3
                        ? "#EB5757"
                        : "#060D0B",
                    fontFamily: "verdana",
                    fontSize: 16,
                    lineHeight: 22,
                    fontWeight: "400",
                    width: (deviceWidth * 75) / 100,
                  }}
                >
                  {strings.bipolor3}
                </Text>
              </View>
            </View>
          </View>
        );
      }
    }
    if (title === "Anxiety") {
      if (score <= 5) {
        // return 'A complete evaluation is not recommended';
        return (
          <View style={{ marginLeft: "10%" }}>
            <View style={{ width: (deviceWidth * 40) / 100 }}>
              <Text style={styles.complete}>{strings.Anxity}</Text>
            </View>
          </View>
        );
      } else {
        // return 'A complete evalutaion is recommended';
        return (
          <View style={{ marginLeft: "10%" }}>
            <View style={{ width: (deviceWidth * 40) / 100 }}>
              <Text style={styles.complete}>{strings.Anxity1}</Text>
            </View>
            <TouchableOpacity
              style={styles.button}
              onPress={() =>
                this.props.navigation.navigate("Search", { screen: "Welcome" })
              }
            >
              <Text style={styles.buttonText}>{strings.clickhereforeval}</Text>
            </TouchableOpacity>
          </View>
        );
      }
    }
    if (title === "Post Traumatic Stress Disorder") {
      if (score <= 1) {
        // return 'A complete evaluation is not recommended';
        return (
          <View style={{ marginLeft: "10%" }}>
            <View style={{ width: (deviceWidth * 40) / 100 }}>
              <Text>
                {strings.SymptomsPTSD1}
                {/* {strings.notRecommended} */}
              </Text>
            </View>
          </View>
        );
      } else if (score <= 3 && score >= 2) {
        return (
          <View style={{ marginLeft: "10%" }}>
            <View style={{ width: (deviceWidth * 40) / 100 }}>
              <Text style={styles.complete}>
                {strings.SymptomsPTSD23}
                {/* {strings.reco} */}
              </Text>
            </View>
            <TouchableOpacity
              style={styles.button}
              onPress={() =>
                this.props.navigation.navigate("Search", { screen: "Welcome" })
              }
            >
              <Text style={styles.buttonText}>{strings.clickhereforeval}</Text>
            </TouchableOpacity>
          </View>
        );
      } else if (score == 4) {
        // return 'A complete evalutaion is strongly recommended';
        return (
          <View style={{ marginLeft: "10%" }}>
            <View style={{ width: (deviceWidth * 40) / 100 }}>
              <Text style={styles.complete}>
                {strings.SymptomsPTSD4}
                {/* {strings.Evaluation} */}
              </Text>
              {/* <Text style={{ fontWeight: "bold" }}>
                {strings.Evaluation}
              </Text> */}
            </View>
            <TouchableOpacity
              style={styles.button}
              onPress={() =>
                this.props.navigation.navigate("Search", { screen: "Welcome" })
              }
            >
              <Text style={styles.buttonText}>{strings.clickhereforeval}</Text>
            </TouchableOpacity>
          </View>
        );
      }
    }
    if (title === "For tobacco and other substances:") {
      if (score <= 7) {
        // return 'A complete evaluation is not recommended';
        return (
          <View style={{ marginLeft: "10%" }}>
            <View style={{ width: (deviceWidth * 40) / 100 }}>
              <Text style={styles.complete}>
                {strings.tobacco1}
                {/* {strings.notRecommended} */}
              </Text>
            </View>
          </View>
        );
      } else if (score < 23 && score >= 8) {
        return (
          <View style={{ marginLeft: "10%" }}>
            <View style={{ width: (deviceWidth * 40) / 100 }}>
              <Text style={styles.complete}>
                {strings.tobacco2}
                {/* {strings.reco} */}
              </Text>
            </View>
            <TouchableOpacity
              style={styles.button}
              onPress={() =>
                this.props.navigation.navigate("Search", { screen: "Welcome" })
              }
            >
              <Text style={styles.buttonText}>{strings.clickhereforeval}</Text>
            </TouchableOpacity>
          </View>
        );
      } else if (score >= 23) {
        // return 'A complete evalutaion is strongly recommended';
        return (
          <View style={{ marginLeft: "10%" }}>
            <View style={{ width: (deviceWidth * 40) / 100 }}>
              <Text style={styles.evaluation}>
                {strings.tobacco3}
                {/* {strings.Evaluation} */}
              </Text>
              {/* <Text style={{ fontWeight: "bold" }}>
                {strings.Evaluation}
              </Text> */}
            </View>
            <TouchableOpacity
              style={styles.button}
              onPress={() =>
                this.props.navigation.navigate("Search", { screen: "Welcome" })
              }
            >
              <Text style={styles.buttonText}>{strings.clickhereforeval}</Text>
            </TouchableOpacity>
          </View>
        );
      }
    }
    if (title === "For alcohol use:") {
      if (score <= 7) {
        // return 'A complete evaluation is not recommended';
        return (
          <View style={{ marginLeft: "10%" }}>
            <View style={{ width: (deviceWidth * 40) / 100 }}>
              <Text style={styles.complete}>
                {strings.tobacco1}
                {/* {strings.notRecommended} */}
              </Text>
            </View>
          </View>
        );
      } else if (score < 23 && score >= 8) {
        return (
          <View style={{ marginLeft: "10%" }}>
            <View style={{ width: (deviceWidth * 40) / 100 }}>
              <Text style={styles.complete}>
                {strings.tobacco2}
                {/* {strings.reco} */}
              </Text>
            </View>
            <TouchableOpacity
              style={styles.button}
              onPress={() =>
                this.props.navigation.navigate("Search", { screen: "Welcome" })
              }
            >
              <Text style={styles.buttonText}>{strings.clickhereforeval}</Text>
            </TouchableOpacity>
          </View>
        );
      } else if (score >= 23) {
        // return 'A complete evalutaion is strongly recommended';
        return (
          <View style={{ marginLeft: "10%" }}>
            <View style={{ width: (deviceWidth * 40) / 100 }}>
              <Text style={styles.evaluation}>
                {strings.tobacco3}
                {/* {strings.Evaluation} */}
              </Text>
              {/* <Text style={{ fontWeight: "bold" }}>
                {strings.Evaluation}
              </Text> */}
            </View>
            <TouchableOpacity
              style={styles.button}
              onPress={() =>
                this.props.navigation.navigate("Search", { screen: "Welcome" })
              }
            >
              <Text style={styles.buttonText}>{strings.clickhereforeval}</Text>
            </TouchableOpacity>
          </View>
        );
      }
    }
  };

  getDescription1 = (title, score) => {
    // console.log("Title: ", title, "Score: ", score);
    if (title === "Depression") {
      if (score <= 8) {
        // return 'A complete evaluation is not recommended';

        return (
          <View>
            <View
              style={{
                width: (deviceWidth * 90) / 100,
              }}
            >
              <Text style={styles.evaluation}>{strings.notRecommended}</Text>
            </View>
          </View>
        );
      } else if (score <= 16 && score >= 9) {
        // return 'A complete evalutaion is recommended';
        return (
          <View>
            <View style={{ width: (deviceWidth * 90) / 100 }}>
              <Text style={styles.evaluation}>
                {/* {strings.depressive1} */}
                {strings.Evaluationrecommended}
              </Text>
              <Text
                style={{
                  width: (deviceWidth * 90) / 100,
                  fontFamily: "verdana",
                  fontSize: 16,
                  lineHeight: 22,
                  fontWeight: "400",
                  color: "#060D0B",
                }}
              >
                {strings.DepEva}
              </Text>
            </View>
          </View>
        );
      } else if (score <= 40 && score >= 17) {
        // return 'A complete evalutaion is strongly recommended';
        if (!this.state.subtitle.includes("Depression")) {
          this.state.subtitle.push(title);
        }

        return (
          <View>
            <View style={{ width: (deviceWidth * 90) / 100 }}>
              <Text style={styles.evaluation}>
                {/* {strings.depressive2} */}
                {strings.Evaluation}
              </Text>
              <Text
                style={{
                  width: (deviceWidth * 90) / 100,
                  fontFamily: "verdana",
                  fontSize: 16,
                  lineHeight: 22,
                  fontWeight: "400",
                  color: "#060D0B",
                }}
              >
                {strings.DepEva1}
              </Text>
            </View>
          </View>
        );
      }
    }
    if (title === "Bipolar") {
      const ldata = lvalue;
      if (
        (this.state.mood_Disorder_1 != undefined &&
          this.state.mood_Disorder_2 != undefined &&
          ((score >= 7 &&
            score <= 13 &&
            this.state.mood_Disorder_1.value >= 3 &&
            this.state.mood_Disorder_2.value == true) ||
            (this.state.mood_Disorder_1.value >= 3 &&
              this.state.mood_Disorder_2.value == true))) ||
        (this.state.mood_Disorder_1 != undefined &&
          score >= 7 &&
          score <= 13 &&
          this.state.mood_Disorder_1.value >= 3) ||
        (this.state.mood_Disorder_2 != undefined &&
          score >= 7 &&
          score <= 13 &&
          this.state.mood_Disorder_2.value == true)
      ) {
        if (!this.state.subtitle.includes("Bipolar")) {
          this.state.subtitle.push(title);
        }

        return (
          <View>
            <View style={{ width: (deviceWidth * 90) / 100 }}>
              <Text style={styles.evaluation}>{strings.bipopos}</Text>
            </View>
            <TouchableOpacity
              // style={styles.button}
              onPress={() =>
                this.props.navigation.navigate("Search", { screen: "Welcome" })
              }
            >
              <Text style={styles.bipotext}>{strings.clickhereforeval}</Text>
            </TouchableOpacity>
          </View>
        );
      } else {
        return (
          <View style={{ width: (deviceWidth * 90) / 100 }}>
            <Text style={styles.evaluation}>{strings.biponeg}</Text>
          </View>
        );
      }
    }
    if (title === "Anxiety") {
      if (score <= 5) {
        // return 'A complete evaluation is not recommended';
        return (
          <View style={{ width: (deviceWidth * 90) / 100 }}>
            <Text style={styles.evaluation}>{strings.notRecommended}</Text>
          </View>
        );
      } else {
        // return 'A complete evalutaion is recommended';
        if (!this.state.subtitle.includes("Anxiety")) {
          this.state.subtitle.push(title);
          // console.log(this.state.subtitle, "Anxiety");
        }
        return (
          <View>
            <View style={{ width: (deviceWidth * 90) / 100 }}>
              <Text style={styles.evaluation}>
                {strings.Evaluationrecommended}
              </Text>
            </View>
          </View>
        );
      }
    }
    if (title === "Post Traumatic Stress Disorder") {
      if (score <= 1) {
        // return 'A complete evaluation is not recommended';
        return (
          <View style={{ width: (deviceWidth * 90) / 100 }}>
            <Text style={styles.evaluation}>{strings.notRecommended}</Text>
          </View>
        );
      } else if (score <= 3 && score >= 2) {
        return (
          <View>
            <View style={{ width: (deviceWidth * 90) / 100 }}>
              <Text style={styles.evaluation}>
                {/* {strings.SymptomsPTSD23} */}
                {strings.recommended}
              </Text>
            </View>
          </View>
        );
      } else if (score == 4) {
        if (!this.state.subtitle.includes("Post Traumatic Stress Disorder")) {
          this.state.subtitle.push(title);
        }
        // return 'A complete evalutaion is strongly recommended';
        return (
          <View>
            <View style={{ width: (deviceWidth * 90) / 100 }}>
              <Text style={styles.evaluation}>{strings.Evaluation}</Text>
            </View>
          </View>
        );
      }
    }
    if (title === "For alcohol use:") {
      if (score <= 7) {
        // return 'A complete evaluation is not recommended';
        return (
          <View style={{ width: (deviceWidth * 90) / 100 }}>
            <Text style={styles.evaluation}>
              {strings.tobalc1}
              {/* {strings.notRecommended} */}
            </Text>
          </View>
        );
      } else if (score < 23 && score >= 8) {
        return (
          <View>
            <View style={{ width: (deviceWidth * 90) / 100 }}>
              <Text style={styles.evaluation}>
                {strings.tobalc1}
                {/* {strings.reco} */}
              </Text>
            </View>
          </View>
        );
      } else if (score >= 23) {
        if (!this.state.subtitle.includes("Substance Use")) {
          this.state.subtitle.push("Substance Use");
        }
        // return 'A complete evalutaion is strongly recommended';
        return (
          <View>
            <View style={{ width: (deviceWidth * 90) / 100 }}>
              <Text style={styles.evaluation}>
                {strings.tobalc2}
                {/* {strings.Evaluation} */}
              </Text>
              {/* <Text style={{ fontWeight: "bold" }}>
                {strings.Evaluation}
              </Text> */}
            </View>
          </View>
        );
      }
    }

    this.subttext(this.state.subtitle);
  };

  renderRow = ({ item }) => {
    // console.log(`Getting all the score item: ${JSON.stringify(item)}`);
    // console.log(item.Title, "mobo");
    return (
      <View
        style={{
          flex: 1,
          marginTop: "10%",
          marginLeft: "5%",
        }}
      >
        <View>
          {item.Title == "For tobacco and other substances:" ? (
            <View style={{ marginBottom: "7%" }}>
              <Text
                style={{
                  fontFamily: "Quicksand-Bold",
                  fontSize: 18,
                  lineHeight: 24,
                  fontWeight: "700",
                  color: "#060D0B",
                }}
              >
                Substance Use
              </Text>
            </View>
          ) : null}
          <Text
            style={{
              fontFamily: "Quicksand-Bold",
              fontSize: 18,
              lineHeight: 24,
              fontWeight: "700",
              color: "#060D0B",
            }}
          >
            {item.Title}
          </Text>
        </View>
        <View
          style={{
            marginTop: "7.5%",
            // marginRight: "45%",
            flexDirection: "row",
          }}
        >
          {item.Title == "Bipolar" ? (
            <View />
          ) : item.Title == "For alcohol use:" ||
            item.Title == "For tobacco and other substances:" ? (
            <View>
              <View
                style={{
                  position: "absolute",
                  top: 95,
                }}
              >
                <Text>0</Text>
              </View>
              <View
                style={{
                  position: "absolute",
                  top: 1,
                  left: 20,
                }}
              >
                <Text>10</Text>
              </View>
              <View
                style={{
                  position: "absolute",
                  top: -8,
                  left: 40,
                }}
              >
                <Text>11</Text>
              </View>
              <View
                style={{
                  position: "absolute",
                  top: -10,
                  right: 40,
                }}
              >
                <Text>19</Text>
              </View>
              <View
                style={{
                  position: "absolute",
                  top: 0,
                  right: 17,
                }}
              >
                <Text>20</Text>
              </View>
              <RNSpeedometer
                value={item.Score}
                minValue={0}
                maxValue={30}
                size={160}
                labels={[
                  {
                    name: "Low",
                    labelColor: "#060D0B",
                    activeBarColor: "#377867",
                  },
                  {
                    name: "Moderate",
                    labelColor: "#060D0B",
                    activeBarColor: "#F8D470",
                  },
                  {
                    name: "High",
                    labelColor: "#060D0B",
                    activeBarColor: "#EB5757",
                  },
                ]}
                labelStyle={{
                  display: "none",
                }}
                innerCircleStyle={{
                  height:
                    Platform.OS === "ios"
                      ? (deviceHeight * 7) / 100
                      : (deviceHeight * 8) / 100,
                  width:
                    Platform.OS === "ios"
                      ? (deviceWidth * 30) / 100
                      : (deviceWidth * 32) / 100,
                }}
              />
            </View>
          ) : item.Title == "Anxiety" ? (
            <View>
              <View
                style={{
                  position: "absolute",
                  top: 95,
                }}
              >
                <Text>0</Text>
              </View>
              <View
                style={{
                  position: "absolute",
                  top: -20,
                  right: 85,
                }}
              >
                <Text>5</Text>
              </View>
              <View
                style={{
                  position: "absolute",
                  top: 15,
                  right: 5,
                }}
              >
                <Text>+6</Text>
              </View>
              <RNSpeedometer
                easeDuration={500}
                minValue={0}
                maxValue={10}
                value={item.Score}
                size={160}
                labels={[
                  {
                    labelColor: "#377867",
                    activeBarColor: "#377867",
                  },
                  {
                    labelColor: "#EB5757",
                    activeBarColor: "#EB5757",
                  },
                ]}
                labelStyle={{
                  color: "#060D0B",
                  fontSize: 25,
                  lineHeight: 32,
                }}
                innerCircleStyle={{
                  height:
                    Platform.OS === "ios"
                      ? (deviceHeight * 7) / 100
                      : (deviceHeight * 8) / 100,
                  width:
                    Platform.OS === "ios"
                      ? (deviceWidth * 30) / 100
                      : (deviceWidth * 32) / 100,
                }}
              />
            </View>
          ) : item.Title == "Post Traumatic Stress Disorder" ? (
            <View>
              <View
                style={{
                  position: "absolute",
                  top: 95,
                }}
              >
                <Text>0</Text>
              </View>
              <View
                style={{
                  position: "absolute",
                  left: 25,
                }}
              >
                <Text>1</Text>
              </View>
              <View
                style={{
                  position: "absolute",
                  left: 40,
                  top: -10,
                }}
              >
                <Text>2</Text>
              </View>
              <View
                style={{
                  position: "absolute",
                  right: 30,
                  top: -5,
                }}
              >
                <Text>3</Text>
              </View>
              <View
                style={{
                  position: "absolute",
                  top: 95,
                  right: 0,
                }}
              >
                <Text>4</Text>
              </View>
              <RNSpeedometer
                minValue={0}
                maxValue={4}
                value={item.Score}
                size={160}
                labels={[
                  {
                    labelColor: "#377867",
                    activeBarColor: "#377867",
                  },
                  {
                    labelColor: "#F8D470",
                    activeBarColor: "#F8D470",
                  },
                  {
                    labelColor: "#EB5757",
                    activeBarColor: "#EB5757",
                  },
                ]}
                innerCircleStyle={{
                  height:
                    Platform.OS === "ios"
                      ? (deviceHeight * 7) / 100
                      : (deviceHeight * 8) / 100,
                  width:
                    Platform.OS === "ios"
                      ? (deviceWidth * 30) / 100
                      : (deviceWidth * 32) / 100,
                }}
                labelStyle={{
                  color: "#060D0B",
                  // marginTop: "-25%",
                  fontSize: 25,
                  lineHeight: 32,
                }}
              />
            </View>
          ) : (
            <View>
              <View
                style={{
                  position: "absolute",
                  top: 90,
                }}
              >
                <Text>0</Text>
              </View>
              <View
                style={{
                  position: "absolute",
                  top: 1,
                  left: 25,
                }}
              >
                <Text>8</Text>
              </View>
              <View
                style={{
                  position: "absolute",
                  top: -8,
                  left: 40,
                }}
              >
                <Text>9</Text>
              </View>
              <View
                style={{
                  position: "absolute",
                  top: -10,
                  right: 40,
                }}
              >
                <Text>16</Text>
              </View>
              <View
                style={{
                  position: "absolute",
                  top: 0,
                  right: 17,
                }}
              >
                <Text>17</Text>
              </View>
              <View
                style={{
                  position: "absolute",
                  top: 90,
                  right: -5,
                }}
              >
                <Text>30</Text>
              </View>
              <RNSpeedometer
                minValue={0}
                maxValue={30}
                value={item.Score}
                size={160}
                labels={[
                  {
                    labelColor: "#377867",
                    activeBarColor: "#377867",
                  },
                  {
                    labelColor: "#F8D470",
                    activeBarColor: "#F8D470",
                  },
                  {
                    labelColor: "#EB5757",
                    activeBarColor: "#EB5757",
                  },
                ]}
                innerCircleStyle={{
                  height:
                    Platform.OS === "ios"
                      ? (deviceHeight * 7) / 100
                      : (deviceHeight * 8) / 100,
                  width:
                    Platform.OS === "ios"
                      ? (deviceWidth * 30) / 100
                      : (deviceWidth * 32) / 100,
                }}
                labelStyle={{
                  color: "#060D0B",
                  // marginTop: "-25%",
                  fontSize: 25,
                  lineHeight: 32,
                }}
              />
            </View>
          )}

          <View style={{ flex: 1 }}>
            <View style={{ marginTop: "10%" }}>
              {this.getDescription(item.Title, item.Score)}
            </View>
          </View>
        </View>
        <View
          style={{ textAlign: "center", marginTop: "15%", marginRight: "5%" }}
        >
          {this.getDescription1(item.Title, item.Score)}
          <View
            style={{
              top: 20,
              borderTopWidth: 2,
              borderColor: "#E0E0E0",
              width: "100%",
            }}
          />
        </View>
      </View>
    );
  };

  subttext() {
    const val = "";
    if (this.state.subtitle.length > 0) {
      for (var i = 0; i >= this.state.subtitle.length; i++) {
        this.state.subtitle.forEach(subttext);
      }
      // this.state.subtitle.splice(this.state.subtitle.length - 1, 3, "and");
      const data = this.state.subtitle.toString();
      return (
        <View style={{ width: (deviceWidth * 90) / 100 }}>
          <Text style={styles.complete}>{strings.constint}</Text>
          <Text style={styles.evaluation}>{data} .</Text>
          <Text style={styles.complete}>{strings.neededstr}.</Text>
        </View>
      );
    } else {
      return (
        <View style={{ width: (deviceWidth * 90) / 100 }}>
          <Text style={styles.complete}>{strings.youlikely}.</Text>
        </View>
      );
    }
  }

  retake() {
    database.deletePatientMaster().catch((error) => {
      console.log(error);
    });
    database.deleteFinal_Scores_Schema().catch((Error) => {
      console.log(Error);
    });
    database.deletePatientMaster().catch((error) => {
      console.log(error);
    });

    database.deletePatientDepression_2().catch((error) => {
      console.log(error);
    });

    // database.deletePatientDepression_3().catch((Error) => {
    //   console.log(Error);
    // });
    database.deletePatientDepression4().catch((Error) => {
      console.log(Error);
    });
    let substanceoption = [];
    // database.deletePatientDepressionValue().catch((error) => {
    //   console.log(error);
    // });
    this.props.navigation.navigate("Screening", {
      screen: "SelectOptions",
      retake: true,
    });
    // this.props.navigation.navigate("SelectOptions", { retake: true });
  }
  Navigate = (page) => {
    // this.props.navigation.reset({
    //   index: 0,
    //   routes: [{ name: page }],
    // });
    this.props.navigation.navigate("Search", {
      screen: page,
    });
  };
  render() {
    const { result } = this.state;
    return (
      <View style={{ backgroundColor: "#fff" }}>
        <Spinner visible={this.state.isLoading} />
        <ScrollView showsVerticalScrollIndicator={false}>
          {this.state.token ? (
            <View>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  // backgroundColor: "pink",
                  alignItems: "center",
                  marginTop: 12,
                }}
              >
                <TouchableOpacity
                  style={{ paddingLeft: 3 }}
                  onPress={() =>
                    this.props.navigation.navigate("SelectOptions")
                  }
                >
                  <AntDesign name="left" size={25} color="#377867" />
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ paddingRight: 16 }}
                  onPress={() =>
                    this.props.navigation.navigate("Search", {
                      screen: "Profile",
                    })
                  }
                >
                  <Text
                    style={{
                      fontSize: 16,
                      color: "#377867",
                      fontWeight: "700",
                      fontFamily: "Quicksand-Bold",
                    }}
                  >
                    {strings.myprofile}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          ) : (
            <View style={{}}>
              <CustomBackResult
                navigate={() => {
                  // this.retake();
                  this.props.navigation.navigate("Screening", {
                    screen: "SelectOptions",
                    retake: true,
                  });
                  // }
                  // this.props.navigation.navigate("SelectOptions");
                }}
                onPress={() =>
                  this.props.navigation.navigate("Search", {
                    screen: "Login",
                  })
                }
              />
            </View>
          )}

          {this.state.token ? (
            <View style={{ marginTop: "5%", marginLeft: "5%" }}>
              <Text
                style={{
                  fontFamily: "Quicksand-Bold",
                  fontSize: 20,
                  lineHeight: 26,
                  fontWeight: "700",
                  color: "#060D0B",
                }}
              >
                {strings.hi}, {this.state.firstname}
              </Text>
            </View>
          ) : (
            <View style={{ marginTop: "5%", marginLeft: "5%" }}>
              <Text
                style={{
                  fontFamily: "Quicksand-Bold",
                  fontSize: 20,
                  lineHeight: 26,
                  fontWeight: "700",
                  color: "#060D0B",
                }}
              >
                {strings.thankyou}
              </Text>
            </View>
          )}

          <View
            style={{
              marginTop: "5%",
              marginLeft: "5%",
            }}
          >
            {this.state.previousdate && (
              <View style={{ marginBottom: 20 }}>
                <Text style={styles.complete}>
                  Your last screening was <Text>{this.state.previousdate}</Text>
                  . Below are your past results:
                </Text>
              </View>
            )}
            {this.subttext()}
          </View>
          <View style={{ marginTop: "5%", marginLeft: "5%" }}>
            <Text
              style={{
                fontFamily: "verdana",
                fontSize: 16,
                lineHeight: 22,
                fontWeight: "400",
                color: "#060D0B",
              }}
            >
              {strings.details}
            </Text>
          </View>
          <FlatList
            data={result[0]}
            renderItem={this.renderRow}
            keyExtractor={(item, index) => index.toString()}
          />
          <View
            style={{
              marginTop: "5%",
              marginLeft: "5%",
              width: (deviceWidth * 90) / 100,
            }}
          >
            <Text style={styles.complete}>{strings.family}</Text>
          </View>
          {this.state.token ? (
            <View style={{ marginTop: 40 }}>
              <CustomButton
                title={strings.retake}
                redius={40}
                width={230}
                height={45}
                onPress={() => this.retake()}
              />
            </View>
          ) : (
            <View>
              <View
                style={{
                  marginTop: "5%",
                  marginLeft: "5%",
                  width: (deviceWidth * 90) / 100,
                }}
              >
                <Text style={styles.evaluation}>{strings.botsign}</Text>
              </View>
              <View style={{ marginTop: 40 }}>
                <CustomButton
                  title={strings.signup}
                  redius={40}
                  width={230}
                  height={45}
                  onPress={() => this.Navigate("SignUp")}
                />
              </View>
              <TouchableOpacity onPress={() => this.Navigate("Login")}>
                <View style={{ alignItems: "center", marginTop: 25 }}>
                  <Text
                    style={{
                      color: "#377867",
                      fontFamily: "Quicksand-Bold",
                      fontWeight: "bold",
                    }}
                  >
                    {strings.login}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          )}

          <View
            style={{ alignItems: "center", marginTop: 35, marginBottom: 15 }}
          >
            <TouchableOpacity onPress={() => this.contact()}>
              <Text
                style={{
                  color: "#A1683B",
                  fontFamily: "Quicksand-Bold",
                  fontWeight: "bold",
                  lineHeight: 20,
                  fontWeight: "700",
                  fontSize: 14,
                }}
              >
                {strings.contact}
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  complete: {
    fontFamily: "verdana",
    fontSize: 16,
    lineHeight: 22,
    fontWeight: "400",
    color: "#060D0B",
  },
  evaluation: {
    fontFamily: "verdana",
    fontSize: 16,
    lineHeight: 22,
    fontWeight: "700",
    color: "#060D0B",
  },
  bipolortext: {
    fontFamily: "verdana",
    fontSize: 16,
    lineHeight: 22,
    fontWeight: "400",
  },
  button: {
    top: 10,
    backgroundColor: "#EFD2CB",
    width: (deviceWidth * 40) / 100,
    height: (deviceHeight * 6) / 100,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 8,
  },
  buttonText: {
    color: "#A1683A",
    fontFamily: "Quicksand-Bold",
    fontWeight: "bold",
    fontSize: 12,
    width: "85%",
    // lineHeight: 22,
  },
  bipotext: {
    color: "#A1683A",
    // fontFamily: Quicksand,
    fontSize: 16,
    lineHeight: 22,
    fontWeight: "700",
    width: "85%",
  },
});
