import React, { Component } from "react";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  FlatList,
} from "react-native";
import Toast from "react-native-simple-toast";
import CustomBack from "../../CustomFolder/CustomBack";
import Myprofile from "../../CustomFolder/Myprofile";
import CustomDisButton from "../../CustomFolder/CustomDisButton";
import CustomButton from "../../CustomFolder/CustomButton";
import CustomInfoButton from "../../CustomFolder/CustomInfoButton";
import * as database from "../../Database/allSchemas";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { connect } from "react-redux";
import { emptyScore } from "../../actions/score";
import strings from "../../Language/Language";
let flipValue = [];
let finalPicker = [];
const deviceWidth = Dimensions.get("window").width;
export let selectedMDQ = [];
class BipolarScreening extends Component {
  constructor() {
    super();
    this.state = {
      token: "",
      value: false,
      depression: [],
      final: 0,
      confirmation: false,
      MDQScore: 0,
    };
  }
  componentDidMount() {
    const { navigation } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      this.getToken();
    });
    this.getData();
    this.getData2();
    this.getLanguage();
  }
  componentWillUnmount() {
    this._unsubscribe;
  }
  getToken = async () => {
    let token;
    token = await AsyncStorage.getItem("token");
    this.setState({
      token: token,
    });
    // console.log("this.state.token", this.state.token);
  };

  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      console.log("refresh", get_Data);

      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
        this.setLanguage("en");
      } else {
        strings.setLanguage("sp");
        this.setLanguage("sp");
      }
    });
  }
  async setLanguage(languageCode) {
    console.log(languageCode);
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
      });
    }

    // getLanguage(languageCode)
  }

  getData() {
    //debugger
    database
      .queryAllPatient_MoodDisorder_Schema()
      .then((res) => {
        const result = Object.values(res[0]);
        this.setState({ depression: result });
      })
      .catch((error) => console.log(error));
  }
  getData2() {
    database
      .queryAllPatient_MoodDisorder_Schema_2()
      .then((res) => {
        console.log(res[0], "res");
        var allEmpty = Object.keys(res[0]).every(function (key) {
          return res[0][key].length === 0;
        });
        if (allEmpty == true) {
          this.setState({ final: 0 });
        } else {
          const result = Object.values(res[0]);
          console.log("resykltttt", result);
          const data = result[0].value;
          console.log(data, "datatatat");
          this.setState({ final: data });
        }
      })
      .catch((error) => console.log(error));
    database
      .queryAllPatient_MoodDisorder_Schema_3()
      .then((res) => {
        var allEmpty1 = Object.keys(res[0]).every(function (key) {
          return res[0][key].length === 0;
        });
        if (allEmpty1 == true) {
          this.setState({ confirmation: false });
        } else {
          const result = Object.values(res[0]);
          const ldata = result[0].value;
          this.setState({ confirmation: ldata });
        }
      })
      .catch((error) => console.log(error));
  }

  insertIntoDatabase() {
    //debugger
    try {
      database.deletePatientDepression_2().catch((error) => console.log(error));
      // database.deletePatientDepression_3().catch((error) => console.log(error));

      database
        .InsertPatientMoodDisorderMaster_2(finalPicker)
        .catch((error) => console.log(error));
      database
        .InsertPatientMoodDisorderMaster_3(flipValue)
        .catch((error) => console.log(error));
    } catch (error) {
      console.log(error);
    } finally {
      this.props.remove();
      this.props.navigation.navigate("SelectOptions");

      // Actions.Anxiety();
    }
  }

  validateScore(value) {
    var lvalue = 0;
    switch (value) {
      case true: {
        lvalue = 1;
        break;
      }
      case false: {
        lvalue = 0;
        break;
      }
    }
    return lvalue;
  }
  updateval(value) {
    //debugger
    flipValue = [];
    console.log(flipValue, "flipValue");
    this.setState({ confirmation: value });
    var value = value;
    var Id = 1;
    var ldata = { Id, value };
    flipValue.push(ldata);
    // this.getData2();
  }
  updatevaluepicker(value) {
    finalPicker = [];
    console.log(finalPicker, "finalPicker");
    this.setState({ final: value });
    var value = value;
    var Id = 1;
    var ldata = { Id, value };
    finalPicker.push(ldata);
    // this.getData2();
  }
  updatevalue(key, value) {
    selectedMDQ = [];

    this.setState({ value });
    var Staff_Score = this.validateScore(value);
    var Score = Staff_Score;
    var subval = 0;
    var value = value;
    var MDQ_PkeyId = key;
    var ldata = { value, MDQ_PkeyId, Staff_Score, Score };
    selectedMDQ.push(ldata);

    database
      .UpdatePatientMoodDisorderMaster(selectedMDQ)
      .catch((error) => console.log(error));
    this.getData();
    database.queryAllPatient_MoodDisorder_Schema().then((res) => {
      const patient_mooddisorder = Object.values(res[0]);
      for (var i = 0; i < patient_mooddisorder.length; i++) {
        subval = subval + patient_mooddisorder[i].Score;
      }
      console.log("sub vallllll ", subval);
      // this.props.score(subval);
      this.setState({ MDQScore: subval }, () => {
        console.log("MDQ Total Score: ", subval);
        let data = {
          Id: 2,
          Title: "Bipolar",
          Score: this.state.MDQScore,
        };
        database
          .InsertFinalScores(data)
          .then((data) =>
            console.log(`Depression screening: ${JSON.stringify(data)}`)
          )
          .catch((error) =>
            console.log(`Error in depression screening: ${error}`)
          );
      });
    });
  }

  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#ffffff",
          //  backgroundColor: "#E5E5E5"
        }}
      >
        <ScrollView keyboardShouldPersistTaps="handled">
          <View>
            {this.state.token ? (
              <View style={{ marginHorizontal: 16 }}>
                <Myprofile
                  back={true}
                  navigation={this.props.navigation}
                  onPress={() =>
                    this.props.navigation.navigate("Search", {
                      screen: "Profile",
                    })
                  }
                />
              </View>
            ) : (
              <View style={{ marginHorizontal: 16 }}>
                <CustomBack
                  back={true}
                  navigation={this.props.navigation}
                  onPress={() =>
                    this.props.navigation.navigate("Search", {
                      screen: "Login",
                    })
                  }
                />
              </View>
            )}
          </View>
          <View style={{ paddingHorizontal: "4%", marginTop: "5%" }}>
            <Text
              style={{
                fontFamily: "Quicksand-Bold",
                fontSize: 18,
                fontWeight: "700",
                lineHeight: 24,
              }}
            >
              {strings.biposcreen}
            </Text>
          </View>
          <View style={{ paddingHorizontal: "4%", marginTop: "2%" }}>
            <Text
              style={{
                fontFamily: "verdana",
                fontSize: 16,
                // fontWeight: "600",
                lineHeight: 22,
              }}
            >
              {strings.bipodesc}...
            </Text>
          </View>
          <View style={{ paddingHorizontal: "4%", marginTop: "-4%" }}>
            <FlatList
              data={this.state.depression}
              style={{ marginTop: "5%" }}
              renderItem={({ item }) => (
                <View style={{ flexDirection: "column", marginTop: "5%" }}>
                  {/* <View style={{ flexDirection: "row", width: "90%" }}> */}
                  <View style={{ width: "90%", flexDirection: "row" }}>
                    <View>
                      <Text
                        style={{
                          fontSize: 16,
                          lineHeight: 22,
                          color: "#060D0B",
                          fontFamily: "Quicksand-Bold",
                          fontWeight: "400",
                        }}
                      >
                        {"\u2981"}{" "}
                      </Text>
                    </View>
                    <Text
                      style={{
                        fontSize: 16,
                        lineHeight: 22,
                        color: "#060D0B",
                        fontFamily: "verdana",
                        fontWeight: "400",
                      }}
                    >
                      {item.MDQ_Option}
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: "column",
                      justifyContent: "space-around",
                    }}
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                        marginTop: 10,
                        paddingHorizontal: "1%",
                      }}
                    >
                      <View>
                        <CustomInfoButton
                          onPress={() => {
                            {
                              this.updatevalue(item.MDQ_PkeyId, true);
                            }
                          }}
                          color={item.Staff_Score == 1 ? "#F2F2F2" : "#A1683A"}
                          backgroundColor={
                            item.Staff_Score == 1 ? "#A1683A" : "#F2F2F2"
                          }
                          title="Yes"
                          redius={10}
                          width={(deviceWidth * 43) / 100}
                          height={45}
                        />
                      </View>
                      <View
                        style={{
                          paddingHorizontal: "4%",
                        }}
                      >
                        <CustomInfoButton
                          onPress={() => {
                            {
                              this.updatevalue(item.MDQ_PkeyId, false);
                            }
                          }}
                          color={item.Staff_Score == 0 ? "#F2F2F2" : "#A1683A"}
                          backgroundColor={
                            item.Staff_Score == 0 ? "#A1683A" : "#F2F2F2"
                          }
                          title="No"
                          redius={10}
                          width={(deviceWidth * 43) / 100}
                          height={45}
                        />
                      </View>
                    </View>
                  </View>
                </View>
              )}
            />
          </View>
          {this.state.MDQScore >= 2 ? (
            <View>
              <View style={{ paddingHorizontal: "4%", marginTop: "4%" }}>
                <Text
                  style={{
                    fontSize: 14,
                    lineHeight: 22,
                    color: "#060D0B",
                    fontFamily: "Quicksand-Bold",
                    fontWeight: "400",
                  }}
                >
                  {strings.ifyes}?
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    marginTop: 10,
                    paddingHorizontal: "1%",
                  }}
                >
                  <View>
                    <CustomInfoButton
                      onPress={() => {
                        {
                          this.updateval(true);
                        }
                      }}
                      color={
                        this.state.confirmation == true ? "#F2F2F2" : "#A1683A"
                      }
                      backgroundColor={
                        this.state.confirmation == true ? "#A1683A" : "#F2F2F2"
                      }
                      title="Yes"
                      redius={10}
                      width={(deviceWidth * 43) / 100}
                      height={45}
                    />
                  </View>
                  <View
                    style={{
                      paddingHorizontal: "4%",
                    }}
                  >
                    <CustomInfoButton
                      onPress={() => {
                        {
                          this.updateval(false);
                        }
                      }}
                      color={
                        this.state.confirmation == false ? "#F2F2F2" : "#A1683A"
                      }
                      backgroundColor={
                        this.state.confirmation == false ? "#A1683A" : "#F2F2F2"
                      }
                      title="No"
                      redius={10}
                      width={(deviceWidth * 43) / 100}
                      height={45}
                    />
                  </View>
                </View>
              </View>
              <View style={{ paddingHorizontal: "4%", marginTop: "4%" }}>
                <Text
                  style={{
                    fontSize: 14,
                    lineHeight: 22,
                    color: "#060D0B",
                    fontFamily: "Quicksand-Bold",
                    fontWeight: "400",
                  }}
                >
                  {strings.howmuch}?
                </Text>
                <View
                  style={{
                    marginTop: 15,
                    paddingHorizontal: "1%",
                  }}
                >
                  <CustomInfoButton
                    onPress={() => this.updatevaluepicker(1)}
                    color={this.state.final == 1 ? "#F2F2F2" : "#A1683A"}
                    backgroundColor={
                      this.state.final == 1 ? "#A1683A" : "#F2F2F2"
                    }
                    title={strings.problem}
                    redius={10}
                    width={(deviceWidth * 90) / 100}
                    height={45}
                  />
                </View>
                <View
                  style={{
                    marginTop: 15,
                    paddingHorizontal: "1%",
                  }}
                >
                  <CustomInfoButton
                    onPress={() => this.updatevaluepicker(2)}
                    color={this.state.final == 2 ? "#F2F2F2" : "#A1683A"}
                    backgroundColor={
                      this.state.final == 2 ? "#A1683A" : "#F2F2F2"
                    }
                    title={strings.problem1}
                    redius={10}
                    width={(deviceWidth * 90) / 100}
                    height={45}
                  />
                </View>
                <View
                  style={{
                    marginTop: 15,
                    paddingHorizontal: "1%",
                  }}
                >
                  <CustomInfoButton
                    onPress={() => this.updatevaluepicker(3)}
                    color={this.state.final == 3 ? "#F2F2F2" : "#A1683A"}
                    backgroundColor={
                      this.state.final == 3 ? "#A1683A" : "#F2F2F2"
                    }
                    title={strings.problem2}
                    redius={10}
                    width={(deviceWidth * 90) / 100}
                    height={45}
                  />
                </View>
                <View
                  style={{
                    marginTop: 15,
                    paddingHorizontal: "1%",
                  }}
                >
                  <CustomInfoButton
                    onPress={() => this.updatevaluepicker(4)}
                    color={this.state.final == 4 ? "#F2F2F2" : "#A1683A"}
                    backgroundColor={
                      this.state.final == 4 ? "#A1683A" : "#F2F2F2"
                    }
                    title={strings.problem3}
                    redius={10}
                    width={(deviceWidth * 90) / 100}
                    height={45}
                  />
                </View>
              </View>
              <View style={{ marginTop: 30, marginBottom: 30 }}>
                <CustomButton
                  title={strings.finish}
                  redius={40}
                  width={230}
                  height={45}
                  onPress={() => this.insertIntoDatabase()}
                />
              </View>
            </View>
          ) : null}
          {this.state.MDQScore <= 1 ? (
            selectedMDQ != "" && selectedMDQ != [] ? (
              <View style={{ marginTop: 30, marginBottom: 30 }}>
                <CustomButton
                  title={strings.finish}
                  redius={40}
                  width={230}
                  height={45}
                  onPress={() => this.insertIntoDatabase()}
                />
              </View>
            ) : (
              <View style={{ marginTop: 30, marginBottom: 30 }}>
                <CustomDisButton
                  title={strings.finish}
                  redius={40}
                  width={230}
                  height={45}
                  onPress={() => Toast.show(strings.toast1)}
                />
              </View>
            )
          ) : null}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  headtext: {
    fontSize: 14,
    lineHeight: 22,
    color: "#060D0B",
    fontFamily: "Quicksand-Bold",
    fontWeight: "400",
  },
});

// const mapStateToProps = (state) => {
//   console.log("MDQ state: ", state);
//   return {
//     score: state.scoreReducer.score,
//   };
// };

const mapDispatchToProps = (dispatch) => {
  return {
    remove: () => dispatch(emptyScore()),
  };
};

export default connect(null, mapDispatchToProps)(BipolarScreening);
