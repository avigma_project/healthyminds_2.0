import React, { Component } from "react";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  TextInput,
  ToastAndroid,
  Alert,
  Keyboard,
  TouchableWithoutFeedback,
  FlatList,
} from "react-native";
import CustomButton from "../../CustomFolder/CustomButton";
import CustomBack from "../../CustomFolder/CustomBack";
import CustomInput from "../../CustomFolder/CustomInput";
import Myprofile from "../../CustomFolder/Myprofile";
import CustomInfoButton from "../../CustomFolder/CustomInfoButton";
import CustomDisButton from "../../CustomFolder/CustomDisButton";
import Spinner from "react-native-loading-spinner-overlay";
import * as database from "../../Database/allSchemas";
import Toast from "react-native-simple-toast";
import AsyncStorage from "@react-native-async-storage/async-storage";
import strings from "../../Language/Language";
import { userprofile, getdata, getpatientdetails } from "../../Api/function";

export let selectedtreatment = [];
const deviceWidth = Dimensions.get("window").width;
export default class BasicInformation extends Component {
  constructor() {
    super();
    this.state = {
      value_other: "",
      Ltype: 1,
      questation: [],
      firstname: "",
      token: "",
      err: null,
      show: false,
      otherVal: "",
      other_val_show: false,
      isNever: false,
      txtcolor: "#A1683A",
      btnColor: "#F2F2F2",
      value: false,
      isLoading: false,
      ErrorOther: "",
      userData: {
        name: "",
        age: null,
        identity: "",
        partnership: "",
        ethnic: "",
        suicide: false,
        medication: false,
        userid: null,
      },
      treatment1: [],
      treatment2: [],
      istreatment: false,
      // GenderIdentity
      isSelectedGender: false,
      backgroundColorFe: "#F2F2F2",
      colorFe: "#A1683A",
      backgroundColorMa: "#F2F2F2",
      colorMa: "#A1683A",
      backgroundColorTra: "#F2F2F2",
      colorTra: "#A1683A",
      backgroundColorOth: "#F2F2F2",
      colorOth: "#A1683A",
      //PartnerShipStatus
      isSelectedPartnershipStatus: false,
      backgroundColorMarried: "#F2F2F2",
      colorMarried: "#A1683A",
      backgroundColorPartner: "#F2F2F2",
      colorPartner: "#A1683A",
      backgroundColorSepartly: "#F2F2F2",
      colorSepartly: "#A1683A",
      backgroundColorDivorced: "#F2F2F2",
      colorDivorced: "#A1683A",
      backgroundColorSingle: "#F2F2F2",
      colorSingle: "#A1683A",
      backgroundColorWidowed: "#F2F2F2",
      colorWidowed: "#A1683A",
      //racial/ethnic identity
      isSelectedRacialIdentity: false,
      backgroundColorLatino: "#F2F2F2",
      colorLatino: "#A1683A",
      backgroundColorBlack: "#F2F2F2",
      colorBlack: "#A1683A",
      backgroundColorAmerici: "#F2F2F2",
      colorAmerici: "#A1683A",
      backgroundColorNative: "#F2F2F2",
      colorNative: "#A1683A",
      backgroundColorAsian: "#F2F2F2",
      colorAsian: "#A1683A",
      backgroundColorWhite: "#F2F2F2",
      colorWhite: "#A1683A",
      backgroundColorMixed: "#F2F2F2",
      colorMixed: "#A1683A",
      backgroundColorOther: "#F2F2F2",
      colorOther: "#A1683A",

      //Suiced Attempt
      colorYes: "#A1683A",
      backgroundYes: "#F2F2F2",
      colorNo: "#A1683A",
      backgroundNo: "#F2F2F2",
      isSelectedAttempt: false,
      //medicatio
      colorIncludeYes: "#A1683A",
      backgroundIncludeYes: "#F2F2F2",
      colorIncludeNo: "#A1683A",
      backgroundIncludeNo: "#F2F2F2",
      isSelectedAttempt: false,
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      this.getToken();
    });
    this.getLanguage();
    this.getData();
  }

  componentWillUnmount() {
    this._unsubscribe;
  }

  GetPatientDetails = async (token) => {
    console.log("++++****", token);
    this.setState({ isLoading: true });
    var data = {
      Type: 3,
    };
    console.log(data, token);

    try {
      const res = await getpatientdetails(data, token);
      console.log(res[0], "******GetPatientDetails1111*******");
      if (res[0].length > 0) {
        this.setState({
          userData: {
            name: res[0][0].HMP_Name,
            age: res[0][0].HMP_Age.toString(),
          },
        });
        this.selectedValueForGender(res[0][0].HMP_Gender);
        this.selectedValueForPartnerShipStatus(
          res[0][0].HMP_Partnership_Status
        );
        this.selectedValueForRacialidentity(res[0][0].HMP_Racial_or_Ethnic);
        this.setState({ isLoading: false });
      } else {
        this.getUserData(token);
        database.deletePatientMaster().catch((error) => console.log(error));
      }
    } catch (error) {
      console.log(error);
    }
  };

  getUserData = async (token) => {
    var data = JSON.stringify({
      Type: 2,
    });
    try {
      const res = await userprofile(data, token);
      console.log("resres", res);
      this.setState({
        userData: {
          name: res[0][0].User_Name,
        },
        isLoading: false,
      });
    } catch (error) {
      console.log("hihihihihihih", { e: error.response.data.error });
      let message = "";
      if (error.response) {
        this.setState({ isLoading: false });
      } else {
        message = "";
      }
      console.log({ message });
    }
  };

  getDetaiulss = async (token) => {
    //debugger

    console.log("ltype data", this.state.Ltype);

    this.setState({ isLoading: true });
    var data = JSON.stringify({
      Ltype: 1,
    });
    //debugger;
    try {
      //  getdata(data, token).then((response) => {
      //     console.log("response ---namee---------kc", response);
      const res = await getdata(data, token);
      const result = res.data[10];
      console.log(result[0], "ressssssssss");
      this.setState({
        userData: {
          name: result[0].HMP_Name,
          age: result[0].HMP_Age.toString(),
          identity: result[0].HMP_Gender,
          partnership: result[0].HMP_Partnership_Status,
          ethnic: result[0].HMP_Racial_or_Ethnic,

          userid: result[0].User_PkeyID,
        },
      });

      this.selectedValueForGender(this.state.userData.identity);
      this.selectedValueForPartnerShipStatus(this.state.userData.partnership);
      this.selectedValueForRacialidentity(this.state.userData.ethnic);
      this.setState({ isLoading: false });
      // database.deletePatientMaster().catch((error) => console.log(error));

      // });
    } catch (error) {
      console.log(error);
    }
  };

  getToken = async () => {
    let token;
    try {
      token = await AsyncStorage.getItem("token");
      if (token) {
        this.GetPatientDetails(token);
        // this.getDetaiulss(token);
        this.setState({ token: token });
      } else {
        console.log("no token found");
      }
    } catch (e) {
      console.log(e);
    }
  };
  selectedValueForGender(value) {
    Keyboard.dismiss();
    if (value == "Female") {
      this.setState({
        backgroundColorFe: "#A1683A",
        colorFe: "#F2F2F2",
        isSelectedGender: true,
        userData: {
          ...this.state.userData,
          identity: "Female",
        },
      });
    } else {
      this.setState({
        backgroundColorFe: "#F2F2F2",
        colorFe: "#A1683A",
        isSelectedGender: false,
      });
    }
    if (value == "Male") {
      this.setState({
        backgroundColorMa: "#A1683A",
        colorMa: "#F2F2F2",
        isSelectedGender: true,
        userData: {
          ...this.state.userData,
          identity: "Male",
        },
      });
    } else {
      this.setState({
        backgroundColorMa: "#F2F2F2",
        colorMa: "#A1683A",
        isSelectedGender: false,
      });
    }
    if (value == "Transgender") {
      this.setState({
        backgroundColorTra: "#A1683A",
        colorTra: "#F2F2F2",
        isSelectedGender: true,
        userData: {
          ...this.state.userData,
          identity: "Transgender",
        },
      });
    } else {
      this.setState({
        backgroundColorTra: "#F2F2F2",
        colorTra: "#A1683A",
        isSelectedGender: false,
      });
    }
    if (value == "Other") {
      this.setState({
        backgroundColorOth: "#A1683A",
        colorOth: "#F2F2F2",
        isSelectedGender: true,
        userData: {
          ...this.state.userData,
          identity: "Other",
        },
      });
    } else {
      this.setState({
        backgroundColorOth: "#F2F2F2",
        colorOth: "#A1683A",
        isSelectedGender: false,
      });
    }
  }
  selectedValueForPartnerShipStatus(value) {
    Keyboard.dismiss();
    if (value == "Married or registered domestic partnership") {
      this.setState({
        backgroundColorMarried: "#A1683A",
        colorMarried: "#F2F2F2",
        isSelectedPartnershipStatus: true,
        userData: {
          ...this.state.userData,
          partnership: "Married or registered domestic partnership",
        },
      });
    } else {
      this.setState({
        backgroundColorMarried: "#F2F2F2",
        colorMarried: "#A1683A",
        isSelectedPartnershipStatus: false,
      });
    }
    if (value == "Living with my partner") {
      this.setState({
        backgroundColorPartner: "#A1683A",
        colorPartner: "#F2F2F2",
        isSelectedPartnershipStatus: true,
        userData: {
          ...this.state.userData,
          partnership: "Living with my partner",
        },
      });
    } else {
      this.setState({
        backgroundColorPartner: "#F2F2F2",
        colorPartner: "#A1683A",
        isSelectedPartnershipStatus: false,
      });
    }
    if (value == "I am partnered, living separately") {
      this.setState({
        backgroundColorSepartly: "#A1683A",
        colorSepartly: "#F2F2F2",
        isSelectedPartnershipStatus: true,
        userData: {
          ...this.state.userData,
          partnership: "I am partnered, living separately",
        },
      });
    } else {
      this.setState({
        backgroundColorSepartly: "#F2F2F2",
        colorSepartly: "#A1683A",
        isSelectedPartnershipStatus: false,
      });
    }
    if (value == "I am divorced or separated") {
      this.setState({
        backgroundColorDivorced: "#A1683A",
        colorDivorced: "#F2F2F2",
        isSelectedPartnershipStatus: true,
        userData: {
          ...this.state.userData,
          partnership: "I am divorced or separated",
        },
      });
    } else {
      this.setState({
        backgroundColorDivorced: "#F2F2F2",
        colorDivorced: "#A1683A",
        isSelectedPartnershipStatus: false,
      });
    }
    if (value == "I am single") {
      this.setState({
        backgroundColorSingle: "#A1683A",
        colorSingle: "#F2F2F2",
        isSelectedPartnershipStatus: true,
        userData: {
          ...this.state.userData,
          partnership: "I am single",
        },
      });
    } else {
      this.setState({
        backgroundColorSingle: "#F2F2F2",
        colorSingle: "#A1683A",
        isSelectedPartnershipStatus: false,
      });
    }
    if (value == "I am widowed") {
      this.setState({
        backgroundColorWidowed: "#A1683A",
        colorWidowed: "#F2F2F2",
        isSelectedPartnershipStatus: true,
        userData: {
          ...this.state.userData,
          partnership: "I am widowed",
        },
      });
    } else {
      this.setState({
        backgroundColorWidowed: "#F2F2F2",
        colorWidowed: "#A1683A",
        isSelectedPartnershipStatus: false,
      });
    }
  }
  selectedValueForRacialidentity(value) {
    Keyboard.dismiss();
    if (value == "Hispanic/Latino") {
      this.setState({
        backgroundColorLatino: "#A1683A",
        colorLatino: "#F2F2F2",
        isSelectedRacialIdentity: true,
        userData: {
          ...this.state.userData,
          ethnic: "Hispanic/Latino",
        },
      });
    } else {
      this.setState({
        backgroundColorLatino: "#F2F2F2",
        colorLatino: "#A1683A",
        isSelectedRacialIdentity: false,
      });
    }
    if (value == "Black or African American") {
      this.setState({
        backgroundColorBlack: "#A1683A",
        colorBlack: "#F2F2F2",
        isSelectedRacialIdentity: true,
        userData: {
          ...this.state.userData,
          ethnic: "Black or African American",
        },
      });
    } else {
      this.setState({
        backgroundColorBlack: "#F2F2F2",
        colorBlack: "#A1683A",
        isSelectedRacialIdentity: false,
      });
    }
    if (value == "American Indian/Alaska Native") {
      this.setState({
        backgroundColorAmerici: "#A1683A",
        colorAmerici: "#F2F2F2",
        isSelectedRacialIdentity: true,
        userData: {
          ...this.state.userData,
          ethnic: "American Indian/Alaska Native",
        },
      });
    } else {
      this.setState({
        backgroundColorAmerici: "#F2F2F2",
        colorAmerici: "#A1683A",
        isSelectedRacialIdentity: false,
      });
    }
    if (value == "Native Hawaiian/Pacific Islander") {
      this.setState({
        backgroundColorNative: "#A1683A",
        colorNative: "#F2F2F2",
        isSelectedRacialIdentity: true,
        userData: {
          ...this.state.userData,
          ethnic: "Native Hawaiian/Pacific Islander",
        },
      });
    } else {
      this.setState({
        backgroundColorNative: "#F2F2F2",
        colorNative: "#A1683A",
        isSelectedRacialIdentity: false,
      });
    }
    if (value == "Asian") {
      this.setState({
        backgroundColorAsian: "#A1683A",
        colorAsian: "#F2F2F2",
        isSelectedRacialIdentity: true,
        userData: {
          ...this.state.userData,
          ethnic: "Asian",
        },
      });
    } else {
      this.setState({
        backgroundColorAsian: "#F2F2F2",
        colorAsian: "#A1683A",
        isSelectedRacialIdentity: false,
      });
    }
    if (value == "White") {
      this.setState({
        backgroundColorWhite: "#A1683A",
        colorWhite: "#F2F2F2",
        isSelectedRacialIdentity: true,
        userData: {
          ...this.state.userData,
          ethnic: "White",
        },
      });
    } else {
      this.setState({
        backgroundColorWhite: "#F2F2F2",
        colorWhite: "#A1683A",
        isSelectedRacialIdentity: false,
      });
    }
    if (value == "Mixed Race") {
      this.setState({
        backgroundColorMixed: "#A1683A",
        colorMixed: "#F2F2F2",
        isSelectedRacialIdentity: true,
        userData: {
          ...this.state.userData,
          ethnic: "Mixed Race",
        },
      });
    } else {
      this.setState({
        backgroundColorMixed: "#F2F2F2",
        colorMixed: "#A1683A",
        isSelectedRacialIdentity: false,
      });
    }
    if (value == "Other") {
      this.setState({
        backgroundColorOther: "#A1683A",
        colorOther: "#F2F2F2",
        isSelectedRacialIdentity: true,
        userData: {
          ...this.state.userData,
          ethnic: "Other",
        },
      });
    } else {
      this.setState({
        backgroundColorOther: "#F2F2F2",
        colorOther: "#A1683A",
        isSelectedRacialIdentity: false,
      });
    }
  }

  selectedValueForSuicide(value) {
    Keyboard.dismiss();
    if (value == 1) {
      this.setState({
        backgroundYes: "#A1683A",
        colorYes: "#F2F2F2",
        isSelectedAttempt: true,
        userData: {
          ...this.state.userData,
          suicide: true,
        },
      });
      Alert.alert("", strings.emergency, [{ text: "RETURN TO SCREENING" }], {
        cancelable: false,
      });
    } else {
      this.setState({
        backgroundYes: "#F2F2F2",
        colorYes: "#A1683A",
        isSelectedAttempt: false,
      });
    }
    if (value == 2) {
      this.setState({
        backgroundNo: "#A1683A",
        colorNo: "#F2F2F2",
        isSelectedAttempt: true,
        userData: {
          ...this.state.userData,
          suicide: false,
        },
      });
    } else {
      this.setState({
        backgroundNo: "#F2F2F2",
        colorNo: "#A1683A",
        isSelectedAttempt: false,
      });
    }
  }
  selectedValueForMedication(value) {
    Keyboard.dismiss();
    if (value == 1) {
      this.setState({
        backgroundIncludeYes: "#A1683A",
        colorIncludeYes: "#F2F2F2",
        isSelectedAttempt: true,
        userData: {
          ...this.state.userData,
          medication: true,
        },
      });
    } else {
      this.setState({
        backgroundIncludeYes: "#F2F2F2",
        colorIncludeYes: "#A1683A",
        isSelectedAttempt: false,
      });
    }
    if (value == 2) {
      this.setState({
        backgroundIncludeNo: "#A1683A",
        colorIncludeNo: "#F2F2F2",
        isSelectedAttempt: true,
        userData: {
          ...this.state.userData,
          medication: false,
        },
      });
    } else {
      this.setState({
        backgroundIncludeNo: "#F2F2F2",
        colorIncludeNo: "#A1683A",
        isSelectedAttempt: false,
      });
    }
  }
  onNameChange = (name) => {
    this.setState({
      userData: {
        ...this.state.userData,
        name,
      },
    });
  };
  onAgeChange = (age) => {
    this.setState({
      userData: {
        ...this.state.userData,
        age,
      },
    });
  };

  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
        this.setLanguage("en");
      } else {
        strings.setLanguage("sp");
        this.setLanguage("sp");
      }
    });
  }
  async setLanguage(languageCode) {
    console.log(languageCode);
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
      });
    }
    this.getData();

    // getLanguage(languageCode)
  }
  validateScore(value) {
    var lvalue = 0;
    switch (value) {
      case true: {
        lvalue = 1;
        break;
      }
      case false: {
        lvalue = 0;
        break;
      }
    }
    return lvalue;
  }

  NeverBtn() {
    Keyboard.dismiss();
    if (this.state.isNever == false) {
      this.setState({
        txtcolor: "#F2F2F2",
        btnColor: "#A1683A",
        isNever: true,
        istreatment: false,
      });
    } else if (this.state.isNever == true) {
      this.setState({
        txtcolor: "#A1683A",
        btnColor: "#F2F2F2",
        isNever: false,
        istreatment: true,
      });
    }
  }
  ontreatmentupdate(key, value) {
    this.setState({ istreatment: value });
    this.setState({ isNever: false });
    selectedtreatment = [];
    var Treated_Master_PKeyId = key;
    var value = value;
    var Type = this.validateScore(value);
    var ldata = { Treated_Master_PKeyId, value, Type };
    selectedtreatment.push(ldata);
    database
      .UpdatePatientTreatedMaster(selectedtreatment)
      .catch((error) => console.log(error));
    this.getData();
  }
  ontreatmentupdate1(key, value) {
    console.log(key, value);
    // this.setState({ istreatment: value });
    // if(key == 12 && value)
    selectedtreatment = [];
    var Treated_Master_PKeyId = key;
    var value = value;
    var Type = this.validateScore(value);
    var ldata = { Treated_Master_PKeyId, value, Type };
    selectedtreatment.push(ldata);
    database
      .UpdatePatientTreatedMaster(selectedtreatment)
      .catch((error) => console.log(error));
    this.getData();
  }

  getData() {
    //debugger
    try {
      database
        .queryAllPatient_Treated_Schema()
        .then((res) => {
          const result = Object.values(res[0]);

          const option1 = result.slice(0, 4);
          const option2 = result.slice(4, 12);
          const other = result[12];
          this.setState({ otherVal: other });
          this.setState({ treatment1: option1 });
          this.setState({ treatment2: option2 });
        })
        .catch((error) => console.log(error));
    } catch (error) {
      console.log(error);
    }
  }

  inputView() {
    //debugger
    if (this.state.show)
      return (
        <View>
          <View
            style={{
              marginTop: "5%",
              height: 100,
              // justifyContent: "center",
              borderWidth: 1,
              borderColor: "#BDBDBD",
              width: "95%",
              // paddingHorizontal: 1,
              borderRadius: 3,
            }}
          >
            <TextInput
              placeholder={strings.othTreat}
              placeholderTextColor="#828282"
              onChangeText={(value_other) => {
                this.setState({ value_other });
              }}
              placeholderStyle={
                {
                  //fontFamily: "OpenSans-Regular",
                }
              }
            />
          </View>
          {this.state.err && (
            <View>
              <Text style={{ color: "red" }}>{this.state.err}</Text>
            </View>
          )}
        </View>
      );
  }

  othervalupdate() {
    if (this.state.other_val_show == true && this.state.value_other != "") {
      let ldata = {
        Treated_Master_PKeyId: 13,
        value: true,
        other_Val: this.state.value_other,
      };
      selectedtreatment.push(ldata);
      database
        .UpdatePatientTreatedMaster(selectedtreatment)
        .catch((error) => console.log(error));
    } else if (this.state.value_other == "") {
      this.setState({ err: "Please fill required field" });
      // alert("heyyyyyyy");
    } else {
    }
    this.getData();
  }

  validateUpdate() {
    this.setState({ isLoading: true });
    this.othervalupdate();
    let data = {
      Id: 1,
      Name: this.state.userData.name,
      age: this.state.userData.age,
      Gender: this.state.userData.identity,
      partnership_Status: this.state.userData.partnership,
      Ethnic_identity: this.state.userData.ethnic,
      Suicide: this.state.userData.suicide,
      Medication: this.state.userData.medication,
    };

    console.log("dataaaaaaa", JSON.stringify(data));

    database.deletePatientMaster().catch((error) => console.log(error));

    database.InsertPatientMaster(data).catch((error) => console.log(error));
    if (
      (this.state.other_val_show == true && this.state.value_other != "") ||
      this.state.other_val_show == false
    ) {
      this.props.navigation.navigate("SelectOptions");

      Toast.show("Information added successfully!");
    }
    this.setState({ isLoading: false });
  }

  // isShow() {
  //   this.setState({ isNever: false });
  // }

  render() {
    return (
      <SafeAreaView
        style={{
          backgroundColor: "#fff",
          //  backgroundColor: "#E5E5E5"
        }}
      >
        <ScrollView
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{ marginHorizontal: 16 }}
        >
          <Spinner visible={this.state.isLoading} />
          {this.state.token ? (
            <View>
              <Myprofile
                back={true}
                navigation={this.props.navigation}
                onPress={() =>
                  this.props.navigation.navigate("Search", {
                    screen: "Profile",
                  })
                }
              />
            </View>
          ) : (
            <View>
              <CustomBack
                back={true}
                navigation={this.props.navigation}
                onPress={() =>
                  this.props.navigation.navigate("Search", {
                    screen: "Login",
                  })
                }
              />
            </View>
          )}
          <View style={{ marginTop: "5%" }}>
            <Text
              style={{
                fontSize: 18,
                fontWeight: "700",
                fontFamily: "Quicksand-Bold",
                color: "#060D0B",
              }}
            >
              {strings.basic}
            </Text>
          </View>
          <View>
            <CustomInput
              // onPress={this.props}
              placeholder=""
              heading={strings.wecall}
              value={this.state.userData.name}
              placeholderTextColor="gray"
              onChangeText={this.onNameChange}
              onSubmitEditing={Keyboard.dismiss}
            />
          </View>
          <View>
            <CustomInput
              placeholder=""
              heading={strings.isage}
              value={this.state.userData.age}
              placeholderTextColor="gray"
              onChangeText={this.onAgeChange}
              keyboardType="numeric"
              onSubmitEditing={Keyboard.dismiss}
            />
          </View>
          <View style={{ marginBottom: 8, marginTop: 20 }}>
            <Text
              style={{
                fontSize: 16,
                lineHeight: 22,
                color: "#060D0B",
                // fontFamily: "Quicksand-Bold",
                fontWeight: "400",
              }}
            >
              {strings.isgender}
            </Text>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginTop: 10,
                paddingHorizontal: "1%",
              }}
            >
              <View>
                <CustomInfoButton
                  onPress={() => this.selectedValueForGender("Female")}
                  title={strings.Female1}
                  color={this.state.colorFe}
                  backgroundColor={this.state.backgroundColorFe}
                  redius={10}
                  width={(deviceWidth * 43) / 100}
                  height={45}
                />
              </View>
              <View
                style={{
                  paddingHorizontal: "4%",
                }}
              >
                <CustomInfoButton
                  onPress={() => this.selectedValueForGender("Male")}
                  title={strings.Male1}
                  color={this.state.colorMa}
                  backgroundColor={this.state.backgroundColorMa}
                  redius={10}
                  width={(deviceWidth * 43) / 100}
                  height={45}
                />
              </View>
            </View>
            <View
              style={{
                marginTop: "5%",
                flexDirection: "row",
                justifyContent: "space-between",
                paddingHorizontal: "1%",
              }}
            >
              <View>
                <CustomInfoButton
                  onPress={() => this.selectedValueForGender("Transgender")}
                  color={this.state.colorTra}
                  backgroundColor={this.state.backgroundColorTra}
                  title={strings.Transgender1}
                  redius={10}
                  width={(deviceWidth * 43) / 100}
                  height={45}
                />
              </View>
              <View
                style={{
                  paddingHorizontal: "4%",
                }}
              >
                <CustomInfoButton
                  onPress={() => this.selectedValueForGender("Other")}
                  color={this.state.colorOth}
                  backgroundColor={this.state.backgroundColorOth}
                  title={strings.other}
                  redius={10}
                  width={(deviceWidth * 43) / 100}
                  height={45}
                />
              </View>
            </View>
          </View>
          <View style={{ marginBottom: 8, marginTop: 20 }}>
            <Text
              style={{
                fontSize: 16,
                lineHeight: 22,
                color: "#060D0B",
                fontFamily: "verdana",
                fontWeight: "400",
              }}
            >
              {strings.isstatus}
            </Text>
            <View
              style={{
                marginTop: 15,
                paddingHorizontal: "1%",
              }}
            >
              <CustomInfoButton
                onPress={() =>
                  this.selectedValueForPartnerShipStatus(
                    "Married or registered domestic partnership"
                  )
                }
                title={strings.married}
                backgroundColor={this.state.backgroundColorMarried}
                color={this.state.colorMarried}
                redius={10}
                width={(deviceWidth * 90) / 100}
                height={60}
              />
            </View>
            <View
              style={{
                marginTop: 15,
                paddingHorizontal: "1%",
              }}
            >
              <CustomInfoButton
                onPress={() =>
                  this.selectedValueForPartnerShipStatus(
                    "Living with my partner"
                  )
                }
                title={strings.partner}
                backgroundColor={this.state.backgroundColorPartner}
                color={this.state.colorPartner}
                redius={10}
                width={(deviceWidth * 90) / 100}
                height={45}
              />
            </View>
            <View
              style={{
                marginTop: 15,
                paddingHorizontal: "1%",
              }}
            >
              <CustomInfoButton
                onPress={() =>
                  this.selectedValueForPartnerShipStatus(
                    "I am partnered, living separately"
                  )
                }
                title={strings.living}
                backgroundColor={this.state.backgroundColorSepartly}
                color={this.state.colorSepartly}
                redius={10}
                width={(deviceWidth * 90) / 100}
                height={45}
              />
            </View>
            <View
              style={{
                marginTop: 15,
                paddingHorizontal: "1%",
              }}
            >
              <CustomInfoButton
                onPress={() =>
                  this.selectedValueForPartnerShipStatus(
                    "I am divorced or separated"
                  )
                }
                title={strings.separated}
                backgroundColor={this.state.backgroundColorDivorced}
                color={this.state.colorDivorced}
                redius={10}
                width={(deviceWidth * 90) / 100}
                height={45}
              />
            </View>
            <View
              style={{
                marginTop: 15,
                paddingHorizontal: "1%",
              }}
            >
              <CustomInfoButton
                onPress={() =>
                  this.selectedValueForPartnerShipStatus("I am single")
                }
                title={strings.single}
                backgroundColor={this.state.backgroundColorSingle}
                color={this.state.colorSingle}
                redius={10}
                width={(deviceWidth * 90) / 100}
                height={45}
              />
            </View>
            <View
              style={{
                marginTop: 15,
                paddingHorizontal: "1%",
              }}
            >
              <CustomInfoButton
                onPress={() =>
                  this.selectedValueForPartnerShipStatus("I am widowed")
                }
                title={strings.widowed}
                backgroundColor={this.state.backgroundColorWidowed}
                color={this.state.colorWidowed}
                redius={10}
                width={(deviceWidth * 90) / 100}
                height={45}
              />
            </View>
          </View>
          <View style={{ marginBottom: 8, marginTop: 20 }}>
            <Text
              style={{
                fontSize: 16,
                lineHeight: 22,
                color: "#060D0B",
                fontFamily: "verdana",
                fontWeight: "400",
              }}
            >
              {strings.isracial}
            </Text>
            <View
              style={{
                marginTop: 15,
                paddingHorizontal: "1%",
              }}
            >
              <CustomInfoButton
                onPress={() =>
                  this.selectedValueForRacialidentity("Hispanic/Latino")
                }
                title={strings.black}
                backgroundColor={this.state.backgroundColorLatino}
                color={this.state.colorLatino}
                redius={10}
                width={(deviceWidth * 90) / 100}
                height={45}
              />
            </View>
            <View
              style={{
                marginTop: 15,
                paddingHorizontal: "1%",
              }}
            >
              <CustomInfoButton
                onPress={() =>
                  this.selectedValueForRacialidentity(
                    "Black or African American"
                  )
                }
                title={strings.African}
                backgroundColor={this.state.backgroundColorBlack}
                color={this.state.colorBlack}
                redius={10}
                width={(deviceWidth * 90) / 100}
                height={45}
              />
            </View>
            <View
              style={{
                marginTop: 15,
                paddingHorizontal: "1%",
              }}
            >
              <CustomInfoButton
                onPress={() =>
                  this.selectedValueForRacialidentity(
                    "American Indian/Alaska Native"
                  )
                }
                title={strings.Alaska}
                backgroundColor={this.state.backgroundColorAmerici}
                color={this.state.colorAmerici}
                redius={10}
                width={(deviceWidth * 90) / 100}
                height={45}
              />
            </View>
            <View
              style={{
                marginTop: 15,
                paddingHorizontal: "1%",
              }}
            >
              <CustomInfoButton
                onPress={() =>
                  this.selectedValueForRacialidentity(
                    "Native Hawaiian/Pacific Islander"
                  )
                }
                title={strings.Hawaiian}
                backgroundColor={this.state.backgroundColorNative}
                color={this.state.colorNative}
                redius={10}
                width={(deviceWidth * 90) / 100}
                height={45}
              />
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginTop: 12,
                paddingHorizontal: "1%",
              }}
            >
              <View>
                <CustomInfoButton
                  onPress={() => this.selectedValueForRacialidentity("Asian")}
                  title={strings.asian}
                  backgroundColor={this.state.backgroundColorAsian}
                  color={this.state.colorAsian}
                  redius={10}
                  width={(deviceWidth * 43) / 100}
                  height={45}
                />
              </View>
              <View
                style={{
                  paddingHorizontal: "4%",
                }}
              >
                <CustomInfoButton
                  onPress={() => this.selectedValueForRacialidentity("White")}
                  title={strings.white}
                  backgroundColor={this.state.backgroundColorWhite}
                  color={this.state.colorWhite}
                  redius={10}
                  width={(deviceWidth * 43) / 100}
                  height={45}
                />
              </View>
            </View>
            <View
              style={{
                marginTop: "3%",
                flexDirection: "row",
                justifyContent: "space-between",
                paddingHorizontal: "1%",
              }}
            >
              <View>
                <CustomInfoButton
                  onPress={() =>
                    this.selectedValueForRacialidentity("Mixed Race")
                  }
                  title={strings.mixed}
                  backgroundColor={this.state.backgroundColorMixed}
                  color={this.state.colorMixed}
                  redius={10}
                  width={(deviceWidth * 43) / 100}
                  height={45}
                />
              </View>
              <View
                style={{
                  paddingHorizontal: "4%",
                }}
              >
                <CustomInfoButton
                  onPress={() => this.selectedValueForRacialidentity("Other")}
                  title={strings.other}
                  backgroundColor={this.state.backgroundColorOther}
                  color={this.state.colorOther}
                  redius={10}
                  width={(deviceWidth * 43) / 100}
                  height={45}
                />
              </View>
            </View>
          </View>
          <View style={{ marginBottom: 8, marginTop: 20 }}>
            <Text
              style={{
                fontSize: 16,
                lineHeight: 22,
                color: "#060D0B",
                fontFamily: "verdana",
                fontWeight: "400",
              }}
            >
              6. {strings.treatment}
            </Text>
            <FlatList
              data={this.state.treatment1}
              renderItem={({ item }) => (
                <View
                  style={{
                    marginTop: 15,
                    paddingHorizontal: "1%",
                  }}
                >
                  <CustomInfoButton
                    onPress={() =>
                      this.ontreatmentupdate(
                        item.Treated_Master_PKeyId,
                        item.value ? this.state.value : true
                      )
                    }
                    title={item.Treated_Master_Option}
                    color={
                      this.state.isNever == false && item.value == true
                        ? "#F2F2F2"
                        : "#A1683A"
                    }
                    backgroundColor={
                      this.state.isNever == false && item.value == true
                        ? "#A1683A"
                        : "#F2F2F2"
                    }
                    redius={10}
                    width={(deviceWidth * 90) / 100}
                    height={45}
                  />
                </View>
              )}
            />
            <View
              style={{
                marginTop: 15,
                paddingHorizontal: "1%",
                marginBottom: "5%",
              }}
            >
              <CustomInfoButton
                onPress={() => {
                  this.NeverBtn();
                }}
                color={this.state.txtcolor}
                backgroundColor={this.state.btnColor}
                title={strings.m3never}
                redius={10}
                width={(deviceWidth * 90) / 100}
                height={45}
              />
            </View>
          </View>
          {this.state.isNever == true ? (
            <View>
              <View style={{ marginBottom: 8, marginTop: 10 }}>
                <Text
                  style={{
                    fontSize: 16,
                    lineHeight: 22,
                    color: "#060D0B",
                    fontFamily: "verdana",
                    fontWeight: "400",
                  }}
                >
                  7. {strings.treatment}
                </Text>
                <FlatList
                  data={this.state.treatment2}
                  renderItem={({ item }) => (
                    <View
                      style={{
                        marginTop: 15,
                        paddingHorizontal: "1%",
                      }}
                    >
                      <CustomInfoButton
                        onPress={() =>
                          this.ontreatmentupdate1(
                            item.Treated_Master_PKeyId,
                            item.value ? this.state.value : true
                          )
                        }
                        title={item.Treated_Master_Option}
                        color={item.value == true ? "#F2F2F2" : "#A1683A"}
                        backgroundColor={
                          item.value == true ? "#A1683A" : "#F2F2F2"
                        }
                        redius={10}
                        width={(deviceWidth * 90) / 100}
                        height={50}
                      />
                    </View>
                  )}
                />
                <View
                  style={{
                    marginTop: 15,
                    paddingHorizontal: "1%",
                  }}
                >
                  <CustomInfoButton
                    onPress={(other_val_show) =>
                      this.setState({
                        other_val_show: !this.state.other_val_show,
                        show: !this.state.other_val_show,
                      })
                    }
                    title={strings.other}
                    color={this.state.show == true ? "#F2F2F2" : "#A1683A"}
                    backgroundColor={
                      this.state.show == true ? "#A1683A" : "#F2F2F2"
                    }
                    redius={10}
                    width={(deviceWidth * 90) / 100}
                    height={45}
                  />
                  {this.state.show == true ? this.inputView() : null}
                </View>
              </View>
              <View style={{ marginBottom: 8, marginTop: 20 }}>
                <Text
                  style={{
                    fontSize: 16,
                    lineHeight: 22,
                    color: "#060D0B",
                    fontFamily: "verdana",
                    fontWeight: "400",
                  }}
                >
                  {strings.issuicide}
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    marginTop: 10,
                    paddingHorizontal: "1%",
                  }}
                >
                  <View>
                    <CustomInfoButton
                      onPress={() => this.selectedValueForSuicide(1)}
                      title="Yes"
                      color={this.state.colorYes}
                      backgroundColor={this.state.backgroundYes}
                      redius={10}
                      width={(deviceWidth * 43) / 100}
                      height={45}
                    />
                  </View>
                  <View
                    style={{
                      paddingHorizontal: "4%",
                    }}
                  >
                    <CustomInfoButton
                      onPress={() => this.selectedValueForSuicide(2)}
                      title="No"
                      color={this.state.colorNo}
                      backgroundColor={this.state.backgroundNo}
                      redius={10}
                      width={(deviceWidth * 43) / 100}
                      height={45}
                    />
                  </View>
                </View>
              </View>
              {(this.state.userData.name &&
                this.state.userData.age &&
                this.state.userData.identity &&
                this.state.userData.partnership &&
                this.state.userData.ethnic) != "" &&
              this.state.userData.name &&
              this.state.userData.age &&
              this.state.userData.identity &&
              this.state.userData.partnership &&
              this.state.userData.ethnic ? (
                <View style={{ marginTop: 30, marginBottom: 30 }}>
                  <CustomButton
                    title="Finish"
                    redius={40}
                    width={230}
                    height={45}
                    onPress={() => this.validateUpdate()}
                  />
                </View>
              ) : (
                <View style={{ marginTop: 30, marginBottom: 30 }}>
                  <CustomDisButton
                    title={strings.finish}
                    redius={40}
                    width={230}
                    height={45}
                    onPress={() => Toast.show(strings.toast1)}
                  />
                </View>
              )}
            </View>
          ) : null}
          {this.state.istreatment == true ? (
            <View>
              <View style={{ marginBottom: 8, marginTop: 10 }}>
                <Text
                  style={{
                    fontSize: 16,
                    lineHeight: 22,
                    color: "#060D0B",
                    fontFamily: "verdana",
                    fontWeight: "400",
                  }}
                >
                  {strings.ismedication}
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    marginTop: 10,
                    paddingHorizontal: "1%",
                  }}
                >
                  <View>
                    <CustomInfoButton
                      onPress={() => this.selectedValueForMedication(1)}
                      title="Yes"
                      color={this.state.colorIncludeYes}
                      backgroundColor={this.state.backgroundIncludeYes}
                      redius={10}
                      width={(deviceWidth * 43) / 100}
                      height={45}
                    />
                  </View>
                  <View
                    style={{
                      paddingHorizontal: "4%",
                    }}
                  >
                    <CustomInfoButton
                      onPress={() => this.selectedValueForMedication(2)}
                      title="No"
                      color={this.state.colorIncludeNo}
                      backgroundColor={this.state.backgroundIncludeNo}
                      redius={10}
                      width={(deviceWidth * 43) / 100}
                      height={45}
                    />
                  </View>
                </View>
              </View>
              <View>
                <View style={{ marginBottom: 8, marginTop: 10 }}>
                  <Text
                    style={{
                      fontSize: 16,
                      lineHeight: 22,
                      color: "#060D0B",
                      fontFamily: "verdana",
                      fontWeight: "400",
                    }}
                  >
                    8. {strings.treatment}
                  </Text>
                  <FlatList
                    data={this.state.treatment2}
                    renderItem={({ item }) => (
                      <View
                        style={{
                          marginTop: 15,
                          paddingHorizontal: "1%",
                        }}
                      >
                        <CustomInfoButton
                          onPress={() =>
                            this.ontreatmentupdate1(
                              item.Treated_Master_PKeyId,
                              item.value ? this.state.value : true
                            )
                          }
                          title={item.Treated_Master_Option}
                          color={item.value == true ? "#F2F2F2" : "#A1683A"}
                          backgroundColor={
                            item.value == true ? "#A1683A" : "#F2F2F2"
                          }
                          redius={10}
                          width={(deviceWidth * 90) / 100}
                          height={45}
                        />
                      </View>
                    )}
                  />
                  <View
                    style={{
                      marginTop: 15,
                      paddingHorizontal: "1%",
                    }}
                  >
                    <CustomInfoButton
                      onPress={(other_val_show) =>
                        this.setState({
                          other_val_show: !this.state.other_val_show,
                          show: !this.state.other_val_show,
                        })
                      }
                      title={strings.other}
                      color={this.state.show == true ? "#F2F2F2" : "#A1683A"}
                      backgroundColor={
                        this.state.show == true ? "#A1683A" : "#F2F2F2"
                      }
                      redius={10}
                      width={(deviceWidth * 90) / 100}
                      height={45}
                    />
                    {this.state.show == true ? this.inputView() : null}
                  </View>
                </View>
                <View style={{ marginBottom: 8, marginTop: 20 }}>
                  <Text
                    style={{
                      fontSize: 16,
                      lineHeight: 22,
                      color: "#060D0B",
                      fontFamily: "verdana",
                      fontWeight: "400",
                    }}
                  >
                    {strings.isuicide1}
                  </Text>
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      marginTop: 10,
                      paddingHorizontal: "1%",
                    }}
                  >
                    <View>
                      <CustomInfoButton
                        onPress={() => this.selectedValueForSuicide(1)}
                        title="Yes"
                        color={this.state.colorYes}
                        backgroundColor={this.state.backgroundYes}
                        redius={10}
                        width={(deviceWidth * 43) / 100}
                        height={45}
                      />
                    </View>
                    <View
                      style={{
                        paddingHorizontal: "4%",
                      }}
                    >
                      <CustomInfoButton
                        onPress={() => this.selectedValueForSuicide(2)}
                        title="No"
                        color={this.state.colorNo}
                        backgroundColor={this.state.backgroundNo}
                        redius={10}
                        width={(deviceWidth * 43) / 100}
                        height={45}
                      />
                    </View>
                  </View>
                </View>
                {(this.state.userData.name &&
                  this.state.userData.age &&
                  this.state.userData.identity &&
                  this.state.userData.partnership &&
                  this.state.userData.ethnic) != "" &&
                (this.state.userData.name &&
                  this.state.userData.age &&
                  this.state.userData.identity &&
                  this.state.userData.partnership &&
                  this.state.userData.ethnic) != undefined ? (
                  <View style={{ marginTop: 30, marginBottom: 30 }}>
                    <CustomButton
                      title="Finish"
                      redius={40}
                      width={230}
                      height={45}
                      onPress={() => this.validateUpdate()}
                    />
                  </View>
                ) : (
                  <View style={{ marginTop: 30, marginBottom: 30 }}>
                    <CustomDisButton
                      title="Finish"
                      redius={40}
                      width={230}
                      height={45}
                      onPress={() =>
                        Toast.show("Please fill in all the details")
                      }
                    />
                  </View>
                )}
              </View>
            </View>
          ) : null}
        </ScrollView>
      </SafeAreaView>
    );
  }
}
