import React, { Component } from "react";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  Linking,
  Image,
} from "react-native";
import Spinner from "react-native-loading-spinner-overlay";
import CustomButton from "../../CustomFolder/CustomButton";
import CustomBack from "../../CustomFolder/CustomBack";
import CustomStart from "../../CustomFolder/CustomStart";
import Myprofile from "../../CustomFolder/Myprofile";
import Icon from "react-native-vector-icons/FontAwesome";
import AsyncStorage from "@react-native-async-storage/async-storage";
import strings from "../../Language/Language";
import { getpatientdetails } from "../../Api/function";

import * as database from "../../Database/allSchemas";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
export default class SelectOptions extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: false,
      token: "",
      basicInformation: "",
      selectedHDST: "",
      selectedMDQ: "",
      selectedADS: "",
      selectedPTSD: "",
      selectedSubstance: "",
      getName: "",
      textColor1: "#828282",
      iconColor1: "#d3d3d3",
      textColor2: "#828282",
      iconColor2: "#d3d3d3",
      textColor3: "#828282",
      iconColor3: "#d3d3d3",
      textColor4: "#828282",
      iconColor4: "#d3d3d3",
      textColor5: "#828282",
      iconColor5: "#d3d3d3",
      textColor6: "#828282",
      iconColor6: "#d3d3d3",
      isSelectedInformation: false,
      isSelectedDepression: false,
      isSelectedBipolar: false,
      isSelectedAnxiety: false,
      isSelectedPTSD: false,
      isSelectedSubstance: false,
      basicText: null,
      call: false,
    };
  }

  GetPatientDetails = async (token) => {
    // database.deletePatientMaster().catch((error) => console.log(error));
    this.setState({ isLoading: true });
    var data = {
      Type: 3,
    };
    console.log(data, this.state.token);

    try {
      const res = await getpatientdetails(data, token);
      console.log("******GetPatientDetails1111*******", res[0]);

      if (res[0].length > 0) {
        // database.deletePatientMaster().catch((error) => console.log(error));
        this.selectedValue(1);
        this.setState({ isLoading: false });
      } else {
        this.selectedValue(1);
        // database.deletePatientMaster().catch((error) => console.log(error));
      }
      this.setState({ isLoading: false });
    } catch (error) {
      console.log(error);
    }
  };

  selectedValue(value) {
    if (value == 1) {
      this.setState({
        textColor1: "#060D0B",
        iconColor1: "#377867",
        isSelectedInformation: true,
      });
    }
    // } else {
    //   this.setState({
    //     textColor1: "#828282",
    //     iconColor1: "#d3d3d3",
    //     isSelectedInformation: false,
    //   });
    // }
    else if (value == 2) {
      this.setState({
        textColor2: "#060D0B",
        iconColor2: "#377867",
        // isSelectedDepression: true,
      });
    }
    // else {
    //   this.setState({
    //     textColor2: "#828282",
    //     iconColor2: "#d3d3d3",

    //     // isSelectedDepression: false,
    //   });
    // }
    else if (value == 3) {
      this.setState({
        textColor3: "#060D0B",
        iconColor3: "#377867",
        isSelectedBipolar: true,
      });
    }
    // else {
    // this.setState({
    //   textColor3: "#828282",
    //   iconColor3: "#d3d3d3",
    //   isSelectedBipolar: false,
    // });
    // }
    else if (value == 4) {
      this.setState({
        textColor4: "#060D0B",
        iconColor4: "#377867",
        isSelectedAnxiety: true,
      });
    }
    //  else {
    //   this.setState({
    //     textColor4: "#828282",
    //     iconColor4: "#d3d3d3",
    //     isSelectedAnxiety: false,
    //   });
    // }
    else if (value == 5) {
      this.setState({
        textColor5: "#060D0B",
        iconColor5: "#377867",
        isSelectedPTSD: true,
      });
    }
    // else {
    //   this.setState({
    //     textColor5: "#828282",
    //     iconColor5: "#d3d3d3",
    //     isSelectedPTSD: false,
    //   });
    // }
    else if (value == 6) {
      this.setState({
        textColor6: "#060D0B",
        iconColor6: "#377867",
        isSelectedSubstance: true,
      });
    }
    // else {
    //   this.setState({
    //     textColor6: "#828282",
    //     iconColor6: "#d3d3d3",
    //     isSelectedSubstance: false,
    //   });
    // }
  }
  // onNextClick() {
  //   if (this.state.isSelectedInformation === true) {
  //     this.props.navigation.navigate("BasicInformation");
  //   } else if (this.state.isSelectedDepression === true) {
  //     this.props.navigation.navigate("DepressionScreening");
  //   }
  // }
  // onStartClick() {
  //   if (this.state.isSelectedBipolar === true) {
  //     this.props.navigation.navigate("BipolarScreening");
  //   } else if (this.state.isSelectedAnxiety === true) {
  //     this.props.navigation.navigate("AnxietyScreening");
  //   } else if (this.state.isSelectedPTSD === true) {
  //     this.props.navigation.navigate("PTSDScreening");
  //   } else if (this.state.isSelectedSubstance === true) {
  //     this.props.navigation.navigate("SubstanceUseTest");
  //   }
  // }

  componentDidMount() {
    const { navigation } = this.props;
    this.getLanguage();
    this.selectedValue(1);
    this._unsubscribe = navigation.addListener("focus", () => {
      this.setState({
        textColor3: "#828282",
        iconColor3: "#d3d3d3",
        isSelectedBipolar: false,
        textColor2: "#828282",
        iconColor2: "#d3d3d3",
        textColor6: "#828282",
        iconColor6: "#d3d3d3",
        isSelectedSubstance: false,
        textColor5: "#828282",
        iconColor5: "#d3d3d3",
        isSelectedPTSD: false,
        textColor4: "#828282",
        iconColor4: "#d3d3d3",
        isSelectedAnxiety: false,
        call: true,
      });
      if (this.props.route.params) {
        const { retake } = this.props.route.params;
        if (retake) {
          this.setState({
            textColor3: "#828282",
            iconColor3: "#d3d3d3",
            isSelectedBipolar: false,
            textColor2: "#828282",
            iconColor2: "#d3d3d3",
            textColor6: "#828282",
            iconColor6: "#d3d3d3",
            isSelectedSubstance: false,
            textColor5: "#828282",
            iconColor5: "#d3d3d3",
            isSelectedPTSD: false,
            textColor4: "#828282",
            iconColor4: "#d3d3d3",
            isSelectedAnxiety: false,
            call: true,
          });
        }
      }
      this.getToken();
      this.getData();
      this.getDepScore();
      this.getId();
    });
  }
  componentWillUnmount() {
    this._unsubscribe;
  }
  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
        this.setLanguage("en");
      } else {
        strings.setLanguage("sp");
        this.setLanguage("sp");
      }
    });
  }
  async setLanguage(languageCode) {
    console.log(languageCode);
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
      });
    }
  }

  getToken = async () => {
    let token;
    token = await AsyncStorage.getItem("token");
    if (token) {
      this.GetPatientDetails(token);
      this.setState({
        token: token,
      });
    }
  };
  getData() {
    database.queryAllPatient_Master().then((res) => {
      var allEmpty = Object.keys(res[0]).every(function (key) {
        return res[0][key].length === 0;
      });
      if (allEmpty != true) {
        const result = Object.values(res[0]);
        const basicInformation = result[0].Name;
        this.setState({ basicInformation: basicInformation });
        this.SubtitleText(1);
        this.selectedValue(2);
        console.log("first");
      } else {
        console.log("two");
        this.selectedValue(1);
        this.setState({
          basicInformation: "",
          basicText: null,
          selectedMDQ: [],
          selectedADS: [],
          selectedPTSD: [],
          selectedSubstance: [],
        });
      }
    });
  }

  getDepScore() {
    database.queryAllFinalScore().then((res) => {
      var allEmpty = Object.keys(res[0]).every(function (key) {
        return res[0][key].length === 0;
      });
      if (allEmpty != true) {
        const result = Object.values(res[0]);
        this.setState({ selectedHDST: result });
        this.SubtitleText(2);
        if (
          this.state.basicInformation &&
          this.state.selectedHDST != "" &&
          this.state.selectedHDST != []
        ) {
          // alert("hiii");

          this.selectedValue(3);
          this.selectedValue(4);
          this.selectedValue(5);
          this.selectedValue(6);
        }
      } else {
        this.setState({ selectedHDST: [] });
      }
    });
  }
  getId() {
    database.queryAllSubstance_Option_Schema().then((res) => {
      if (res[0] != null && res[0] != undefined) {
        let subs = Object.values(res[0][10]);
        let val = Object.values(res[0][11]);
        let final = subs[6] + val[6];
        this.setState({ selectedSubstance: final });
      }
    });
    database.queryAllFinalScore().then((res) => {
      const result = Object.values(res[0]);
      for (var i = 0; i < result.length; i++) {
        var Id = result[i]["Id"];
        if (Id != undefined && Id == 2) {
          this.setState({ selectedMDQ: result });
          this.SubtitleText(3);
        }
        if (Id != undefined && Id == 3) {
          this.setState({ selectedADS: result });
          this.SubtitleText(4);
        }
        if (Id != undefined && Id == 4) {
          this.setState({ selectedPTSD: result });
          this.SubtitleText(5);
        }
        if (Id != undefined && (Id == 5 || Id == 6)) {
          this.setState({ selectedSubstance: result });
          this.SubtitleText(6);
        }
      }
    });
  }

  SubtitleText(value) {
    if (value == 1 && this.state.basicInformation) {
      this.setState({
        basicText: strings.nextwe,
      });
    }
    if (
      value == 2 &&
      this.state.selectedHDST != "" &&
      this.state.selectedHDST != []
    ) {
      this.setState({
        basicText: strings.depcomplete,
      });
    }
    if (
      value == 3 &&
      this.state.selectedMDQ != "" &&
      this.state.selectedMDQ != []
    ) {
      this.setState({
        basicText: strings.bipcomplete,
      });
    }
    if (
      value == 4 &&
      this.state.selectedADS != "" &&
      this.state.selectedADS != []
    ) {
      this.setState({
        basicText: strings.anxcomplete,
      });
    }
    if (
      value == 5 &&
      this.state.selectedPTSD != "" &&
      this.state.selectedPTSD != []
    ) {
      this.setState({
        basicText: strings.ptsdcomplete,
      });
    }
    if (
      value == 6 &&
      this.state.selectedSubstance != "" &&
      this.state.selectedSubstance != []
    ) {
      this.setState({
        basicText: strings.allcomplete,
      });
    }
    // console.log(this.state.basicText, "getAnxScore");
  }

  Onselect() {
    if (
      this.state.basicInformation == [] ||
      this.state.basicInformation == undefined
    ) {
      this.props.navigation.navigate("BasicInformation");
    } else {
      this.props.navigation.navigate("DepressionScreening");
    }
  }

  contact = () => {
    Linking.openURL("https://www.healthelivin.org/contact-us/");
  };

  render() {
    return (
      <SafeAreaView
        style={{
          backgroundColor: "#fff",
          // backgroundColor: "#E5E5E5",
          height: (deviceHeight * 100) / 100,
        }}
      >
        <ScrollView
          contentContainerStyle={{
            marginHorizontal: 16,
            paddingBottom: 100,
          }}
        >
          {this.state.token ? (
            <View>
              <Myprofile
                back2={true}
                navigation={this.props.navigation}
                onPress={() =>
                  this.props.navigation.navigate("Search", {
                    screen: "Profile",
                  })
                }
              />
            </View>
          ) : (
            <View>
              <CustomBack
                back2={true}
                navigation={this.props.navigation}
                onPress={() =>
                  this.props.navigation.navigate("Search", {
                    screen: "Login",
                  })
                }
              />
            </View>
          )}
          {!this.state.basicInformation ? (
            <View style={{}}>
              {this.state.token ? (
                <Text
                  style={{
                    fontSize: 20,
                    fontWeight: "700",
                    fontFamily: "Quicksand-Bold",
                    color: "#060D0B",
                    lineHeight: 26,
                  }}
                >
                  {strings.optiontitle}
                </Text>
              ) : (
                <Text
                  style={{
                    fontSize: 20,
                    fontWeight: "700",
                    fontFamily: "Quicksand-Bold",
                    color: "#060D0B",
                    lineHeight: 26,
                  }}
                >
                  {strings.opttitle2}
                </Text>
              )}
            </View>
          ) : (
            <View style={{}}>
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: "700",
                  fontFamily: "Quicksand-Bold",
                  color: "#060D0B",
                  lineHeight: 26,
                }}
              >
                {strings.thank}, {this.state.basicInformation} !
              </Text>
            </View>
          )}
          {this.state.basicText && (
            <View
              style={{
                width: "85%",
                marginTop: 20,
              }}
            >
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: "400",
                  fontFamily: "verdana",
                  color: "#060D0B",
                  lineHeight: 22,
                }}
              >
                {this.state.basicText}
              </Text>
            </View>
          )}

          <View style={{ marginTop: "1%" }}>
            {!this.state.basicInformation ? (
              <View
                style={{
                  justifyContent: "center",
                  // alignItems: "center",
                }}
              >
                <TouchableOpacity
                  onPress={() => this.selectedValue(1)}
                  style={{ flexDirection: "row" }}
                >
                  <Icon
                    name="circle"
                    size={24}
                    color={this.state.iconColor1}
                    style={styles.icon}
                  />

                  <Text style={[{ color: this.state.textColor1 }, styles.text]}>
                    {strings.basinform}
                  </Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View style={{}}>
                <TouchableOpacity
                  onPress={() => this.selectedValue(1)}
                  style={{ flexDirection: "row", alignItems: "center" }}
                >
                  <Image
                    resizeMode="stretch"
                    source={require("../../../assets/icon.png")}
                    style={{
                      width: 25,
                      height: 25,
                      marginRight: "4%",
                      // backgroundColor: "pink",
                    }}
                  />
                  {/* <Icon
                    name="check-circle"
                    size={24}
                    color="#377867"
                    style={styles.icon}
                  /> */}

                  <Text style={[{ color: "#377867" }, styles.text]}>
                    {strings.basinform}
                  </Text>
                </TouchableOpacity>
              </View>
            )}
            {this.state.selectedHDST != "" && this.state.selectedHDST != [] ? (
              <View>
                <TouchableOpacity
                  onPress={() => this.selectedValue(2)}
                  style={{ flexDirection: "row", alignItems: "center" }}
                >
                  <Image
                    resizeMode="stretch"
                    source={require("../../../assets/icon.png")}
                    style={{
                      width: 25,
                      height: 25,
                      marginRight: "4%",
                      // backgroundColor: "pink",
                    }}
                  />
                  {/* <Icon
                    name="check-circle"
                    size={24}
                    color="#377867"
                    style={styles.icon}
                  /> */}
                  <Text style={[{ color: "#377867" }, styles.text]}>
                    {strings.dpsceen}
                  </Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View>
                <TouchableOpacity
                  disabled={!this.state.basicInformation}
                  onPress={() => this.selectedValue(2)}
                  style={{ flexDirection: "row" }}
                >
                  <Icon
                    name="circle"
                    size={24}
                    color={this.state.iconColor2}
                    style={styles.icon}
                  />
                  <Text style={[{ color: this.state.textColor2 }, styles.text]}>
                    {strings.dpsceen}
                  </Text>
                </TouchableOpacity>
              </View>
            )}

            {this.state.selectedMDQ != "" && this.state.selectedMDQ != [] ? (
              <View>
                <TouchableOpacity
                  onPress={() => this.selectedValue(3)}
                  style={{ flexDirection: "row", alignItems: "center" }}
                >
                  <Image
                    resizeMode="stretch"
                    source={require("../../../assets/icon.png")}
                    style={{
                      width: 25,
                      height: 25,
                      marginRight: "4%",
                      // backgroundColor: "pink",
                    }}
                  />
                  {/* <Icon
                    name="check-circle"
                    size={24}
                    color="#377867"
                    style={styles.icon}
                  /> */}
                  <Text style={[{ color: "#377867" }, styles.text]}>
                    {strings.bipsceen}
                  </Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View style={{ flexDirection: "row" }}>
                <TouchableOpacity
                  disabled={
                    !(
                      this.state.basicInformation &&
                      this.state.selectedHDST != "" &&
                      this.state.selectedHDST != []
                    )
                  }
                  onPress={() => this.selectedValue(3)}
                  style={{ flexDirection: "row" }}
                >
                  <Icon
                    name="circle"
                    size={24}
                    color={this.state.iconColor3}
                    style={styles.icon}
                  />
                  <Text style={[{ color: this.state.textColor3 }, styles.text]}>
                    {strings.bipsceen}
                  </Text>
                </TouchableOpacity>
                {this.state.basicInformation &&
                this.state.selectedHDST != "" &&
                this.state.selectedHDST != [] ? (
                  <CustomStart
                    onPress={() => {
                      this.props.navigation.navigate("BipolarScreening");
                    }}
                  />
                ) : null}
              </View>
            )}
            {this.state.selectedADS != "" && this.state.selectedADS != [] ? (
              <View>
                <TouchableOpacity
                  onPress={() => this.selectedValue(4)}
                  style={{ flexDirection: "row", alignItems: "center" }}
                >
                  <Image
                    resizeMode="stretch"
                    source={require("../../../assets/icon.png")}
                    style={{
                      width: 25,
                      height: 25,
                      marginRight: "4%",
                      // backgroundColor: "pink",
                    }}
                  />
                  {/* <Icon
                    name="check-circle"
                    size={24}
                    color="#377867"
                    style={styles.icon}
                  /> */}
                  <Text style={[{ color: "#377867" }, styles.text]}>
                    {strings.axnsceen}
                  </Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View style={{ flexDirection: "row" }}>
                <TouchableOpacity
                  disabled={
                    !(
                      this.state.basicInformation &&
                      this.state.selectedHDST != "" &&
                      this.state.selectedHDST != []
                    )
                  }
                  onPress={() => this.selectedValue(4)}
                  style={{ flexDirection: "row" }}
                >
                  <Icon
                    name="circle"
                    size={24}
                    color={this.state.iconColor4}
                    style={styles.icon}
                  />
                  <Text style={[{ color: this.state.textColor4 }, styles.text]}>
                    {strings.axnsceen}
                  </Text>
                </TouchableOpacity>
                {this.state.basicInformation &&
                this.state.selectedHDST != "" &&
                this.state.selectedHDST != [] ? (
                  <CustomStart
                    onPress={() => {
                      this.props.navigation.navigate("AnxietyScreening");
                    }}
                  />
                ) : null}
              </View>
            )}
            {this.state.selectedPTSD != "" && this.state.selectedPTSD != [] ? (
              <View>
                <TouchableOpacity
                  onPress={() => this.selectedValue(5)}
                  style={{ flexDirection: "row", alignItems: "center" }}
                >
                  <Image
                    resizeMode="stretch"
                    source={require("../../../assets/icon.png")}
                    style={{
                      width: 25,
                      height: 25,
                      marginRight: "4%",
                      // backgroundColor: "pink",
                    }}
                  />
                  {/* <Icon
                    name="check-circle"
                    size={24}
                    color="#377867"
                    style={styles.icon}
                  /> */}
                  <Text style={[{ color: "#377867" }, styles.text]}>
                    {strings.ptsdsceen}
                  </Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View style={{ flexDirection: "row" }}>
                <TouchableOpacity
                  disabled={
                    !(
                      this.state.basicInformation &&
                      this.state.selectedHDST != "" &&
                      this.state.selectedHDST != []
                    )
                  }
                  onPress={() => this.selectedValue(5)}
                  style={{ flexDirection: "row" }}
                >
                  <Icon
                    name="circle"
                    size={24}
                    color={this.state.iconColor5}
                    style={styles.icon}
                  />
                  <Text style={[{ color: this.state.textColor5 }, styles.text]}>
                    {strings.ptsdsceen}
                  </Text>
                </TouchableOpacity>
                {this.state.basicInformation &&
                this.state.selectedHDST != "" &&
                this.state.selectedHDST != [] ? (
                  <CustomStart
                    onPress={() => {
                      this.props.navigation.navigate("PTSDScreening");
                    }}
                  />
                ) : null}
              </View>
            )}
            {this.state.selectedSubstance != "" &&
            this.state.selectedSubstance != [] ? (
              <View>
                <TouchableOpacity
                  onPress={() => this.selectedValue(6)}
                  style={{ flexDirection: "row", alignItems: "center" }}
                >
                  <Image
                    resizeMode="stretch"
                    source={require("../../../assets/icon.png")}
                    style={{
                      width: 25,
                      height: 25,
                      marginRight: "4%",
                      // backgroundColor: "pink",
                    }}
                  />
                  {/* <Icon
                    name="check-circle"
                    size={24}
                    color="#377867"
                    style={styles.icon}
                  /> */}
                  <Text style={[{ color: "#377867" }, styles.text]}>
                    {strings.subsuse}
                  </Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View style={{ flexDirection: "row" }}>
                <TouchableOpacity
                  disabled={
                    !(
                      this.state.basicInformation &&
                      this.state.selectedHDST != "" &&
                      this.state.selectedHDST != []
                    )
                  }
                  onPress={() => this.selectedValue(6)}
                  style={{ flexDirection: "row" }}
                >
                  <Icon
                    name="circle"
                    size={24}
                    color={this.state.iconColor6}
                    style={styles.icon}
                  />
                  <Text style={[{ color: this.state.textColor6 }, styles.text]}>
                    {strings.subsuse}
                  </Text>
                </TouchableOpacity>
                {this.state.basicInformation &&
                this.state.selectedHDST != "" &&
                this.state.selectedHDST != [] ? (
                  <CustomStart
                    onPress={() => {
                      this.props.navigation.navigate("SubstanceUseTest");
                    }}
                  />
                ) : null}
              </View>
            )}
          </View>
          {this.state.basicInformation &&
          this.state.selectedHDST != "" &&
          this.state.selectedHDST != [] ? (
            <View style={{ marginTop: "5%", marginBottom: "15%" }}>
              <CustomButton
                title={strings.ctr}
                redius={40}
                width={230}
                height={45}
                onPress={() =>
                  this.props.navigation.navigate("Result", {
                    call: this.state.call,
                  })
                }
              />
            </View>
          ) : (
            <View style={{ marginTop: "5%", marginBottom: "15%" }}>
              <CustomButton
                title={strings.Next}
                redius={40}
                width={230}
                height={45}
                onPress={() => this.Onselect()}
              />
            </View>
          )}
          <View
            style={{
              alignSelf: "center",
              marginBottom: 100,
            }}
          >
            <TouchableOpacity style={{}} onPress={() => this.contact()}>
              <Text
                style={{
                  color: "#A1683B",
                  fontWeight: "700",
                  lineHeight: 24,
                  fontFamily: "Quicksand-Bold",
                }}
              >
                {strings.contact}
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  text: {
    lineHeight: 24,
    fontSize: 18,
    fontWeight: "700",
    fontFamily: "Quicksand-Bold",
    padding: "4%",
    paddingHorizontal: "1%",
    width: (deviceWidth * 70) / 100,
  },
  icon: {
    paddingVertical: "4%",
    paddingRight: "4%",
    borderColor: "#F8D470",
  },
});
