import React, { Component } from "react";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  FlatList,
  Platform,
} from "react-native";
import CustomBack from "../../CustomFolder/CustomBack";
import CustomButton from "../../CustomFolder/CustomButton";
import Myprofile from "../../CustomFolder/Myprofile";
import CustomInfoButton from "../../CustomFolder/CustomInfoButton";
import CustomSubstanceButton from "../../CustomFolder/CustomSubstanceButton";
import CustomSliderForTestPage from "../../CustomFolder/CustomSliderForTestPage";
import * as database from "../../Database/allSchemas";
import AsyncStorage from "@react-native-async-storage/async-storage";
import strings from "../../Language/Language";
const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;
export let selectedSubstance = [];
export let selectedSubstanceOption = [];
let selectedHistory = [];
export default class SubstanceUseTest extends Component {
  constructor() {
    super();
    this.state = {
      id: null,
      history: "",
      token: "",
      questation: [],
      questation1: [],
      questation2: [],
      questation3: [],
      ques1: "",
      ques2: "",
      sliderValue: 0,
      value: 0,
      val: false,
      txtcolor: "#A1683A",
      btnColor: "#F2F2F2",
      isNever: false,
      slectedVal: false,
      TobaccoScore: 0,
      AlcoholScore: 0,
      OptionScore: 0,
      sub: 0,
      HistoryQues: [],
    };
  }

  NeverBtn() {
    if (this.state.isNever == false) {
      this.setState({
        txtcolor: "#F2F2F2",
        btnColor: "#A1683A",
        isNever: true,
        slectedVal: false,
      });
    } else if (this.state.isNever == true) {
      this.setState({
        txtcolor: "#A1683A",
        btnColor: "#F2F2F2",
        isNever: false,
        slectedVal: true,
      });
    }
  }

  componentDidMount() {
    const { navigation } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      this.getToken();
    });
    this.getLanguage();
    this.getData();
    this.getData1();
    // this.getHistoryData();
  }
  componentWillUnmount() {
    this._unsubscribe;
  }
  getToken = async () => {
    let token;
    token = await AsyncStorage.getItem("token");
    this.setState({
      token: token,
    });
    // console.log("this.state.token", this.state.token);
  };
  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
        this.setLanguage("en");
      } else {
        strings.setLanguage("sp");
        this.setLanguage("sp");
      }
    });
  }
  async setLanguage(languageCode) {
    console.log(languageCode);
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
      });
    }
  }

  getData() {
    //debugger;
    database.queryAllSubstance_Option_Schema().then((res) => {
      const result = Object.values(res[0]);
      const final = result.slice(0, 10);

      this.setState({ questation: final });
      const history = result.slice(10, 12);
      this.setState({ HistoryQues: history });
      const Option = result[0].SubsOpt_Options;
      this.setState({ ques1: Option });
      const Option1 = result[1].SubsOpt_Options;
      this.setState({ ques2: Option1 });
    });
  }
  getData1() {
    database.queryAllSubstance_Use_Schema().then((res) => {
      const result = Object.values(res[0]);
      const Option1 = [result[6], result[7]];
      this.setState({ questation1: Option1 });
      const Option2 = result.slice(0, 4);
      this.setState({ questation2: Option2 });
      const Option3 = result.slice(4, 6);
      this.setState({ questation3: Option3 });
    });
  }

  // getHistoryData() {
  //   database
  //     .queryAllSubstance_History_Schema()
  //     .then((res) => {
  //       const result = Object.values(res[0]);
  //       const ldata = result[0].value;
  //       if (ldata != "") {
  //         this.setState({ selectedHistory: ldata });
  //       } else {
  //         console.log();
  //         this.setState({ selectedHistory: false });
  //       }
  //     })
  //     .catch((error) => console.log(error));
  // }
  validateupdatescore(value) {
    var lvalue = "";
    switch (value) {
      case "I’m currently being treated": {
        lvalue = 0;
        break;
      }
      case "I have received treatment in the past": {
        lvalue = 1;
        break;
      }
      case "I have never been treated": {
        lvalue = 2;
        break;
      }
    }
    return lvalue;
  }

  validateUpdate(id, value) {
    let data = {
      SubsOpt_PkeyID: id,
      SubsOpt_Options_Value1: value,
      SubsOpt_Value: this.validateupdatescore(value),
    };

    selectedHistory.push(data);
    console.log("dataaaaaaa", JSON.stringify(selectedHistory));
    database
      .Update_SubstanceOption_Master(selectedHistory)
      .catch((error) => console.log(error));
    this.getData();

    // database.queryAllSubstance_Option_Schema().then((res) => {
    //   const result = Object.values(res[0]);
    //   console.log("resultresult", result);
    //   var subval = 0;
    //   for (var i = 0; i < result.length; i++) {
    //     subval = subval + result[i].SubsOpt_Value;
    //   }
    //   console.log("sub val ", subval);
    //   this.setState({ OptionScore: subval }, () => {
    //     console.log("Set OptionScore", this.state.OptionScore);
    //     // this.props.score(subval);
    //     let data = {
    //       Id: 7,
    //       Title: "OptionScore",
    //       Score: this.state.OptionScore,
    //     };
    //     database
    //       .InsertFinalScores(data)
    //       .then((data) =>
    //         console.log(`Substance Alcohol screening: ${JSON.stringify(data)}`)
    //       )
    //       .catch((error) =>
    //         console.log(`Error in Substance Alcohol screening: ${error}`)
    //       );
    //   });
    // });
  }

  // historymaster() {
  //   database
  //     .InsertSubstanceHistoryMaster(selectedHistory)
  //     .catch((error) => console.log(error));
  // }

  validateScore(value) {
    var lvalue = 0;
    switch (value) {
      case true: {
        lvalue = 1;
        break;
      }
      case false: {
        lvalue = 0;
        break;
      }
    }
    return lvalue;
  }
  validateSliderScore(sliderValue) {
    var lvalue = 0;
    switch (sliderValue) {
      case 3: {
        lvalue = 0;
        break;
      }
      case 8: {
        lvalue = 1;
        break;
      }
      case 15: {
        lvalue = 2;
        break;
      }
      case 20: {
        lvalue = 3;
        break;
      }
      case 25: {
        lvalue = 4;
        break;
      }
    }
    return lvalue;
  }

  // updatevalueforslideralc = (key, value) => {
  //   selectedSubstance = [];
  //   // this.setState({ value: value });
  //   // var Sub_value = this.validateSliderScore(Number(value));
  //   var value = value;
  //   var Sub_PkeyId = key;
  //   var ldata = { Sub_PkeyId, value };
  //   selectedSubstance.push(ldata);

  //   database
  //     .Update_Substance_Use_Master(selectedSubstance)
  //     .catch((error) => console.log(error));
  //   this.getData1();
  // };
  updatevalueforslidertob = (key, sliderValue) => {
    selectedSubstance = [];
    var Tab_value = sliderValue;
    var Sub_PkeyId = key;
    var subval = 0;
    var ldata = { Sub_PkeyId, Tab_value };
    selectedSubstance.push(ldata);
    database
      .Update_Substance_Use_Master(selectedSubstance)
      .catch((error) => console.log(error));
    this.getData1();

    database.queryAllSubstance_Use_Schema().then((res) => {
      const result = Object.values(res[0]);
      console.log("resultresult", result);

      for (var i = 0; i < result.length; i++) {
        subval = subval + result[i].Tab_value;
      }
      console.log("sub val ", subval);
      this.setState({ TobaccoScore: subval }, () => {
        console.log("Set Tobacco Score", this.state.TobaccoScore);
        // this.props.score(subval);
        let data = {
          Id: 5,
          Title: "For tobacco and other substances:",
          Score: this.state.TobaccoScore,
        };
        database
          .InsertFinalScores(data)
          .then((data) =>
            console.log(`Tobaco Substance screening: ${JSON.stringify(data)}`)
          )
          .catch((error) =>
            console.log(`Error in Tobaco Substance screening: ${error}`)
          );
      });
    });
  };
  updatevalueforslideralc = (key, sliderValue) => {
    selectedSubstanceOption = [];
    var Acc_value = sliderValue;
    var Sub_PkeyId = key;
    var ldata = { Sub_PkeyId, Acc_value };
    var subval = 0;
    selectedSubstance.push(ldata);
    database
      .Update_Substance_Use_Master(selectedSubstance)
      .catch((error) => console.log(error));
    this.getData1();

    database.queryAllSubstance_Use_Schema().then((res) => {
      const result = Object.values(res[0]);
      console.log("resultresult", result);

      for (var i = 0; i < result.length; i++) {
        subval = subval + result[i].Acc_value;
      }
      console.log("sub val ", subval);
      this.setState({ AlcoholScore: subval }, () => {
        console.log("Set Alcohol Score", this.state.AlcoholScore);
        // this.props.score(subval);
        let data = {
          Id: 6,
          Title: "For alcohol use:",
          Score: this.state.AlcoholScore,
        };
        database
          .InsertFinalScores(data)
          .then((data) =>
            console.log(`Substance Alcohol screening: ${JSON.stringify(data)}`)
          )
          .catch((error) =>
            console.log(`Error in Substance Alcohol screening: ${error}`)
          );
      });
    });
  };
  updatevalueforbuttontob = (key, btnValue) => {
    selectedSubstance = [];
    var Tab_value = btnValue;
    var Sub_PkeyId = key;
    var subval = 0;
    var ldata = { Sub_PkeyId, Tab_value };
    selectedSubstance.push(ldata);
    database
      .Update_Substance_Use_Master(selectedSubstance)
      .catch((error) => console.log(error));
    this.getData1();
    database.queryAllSubstance_Use_Schema().then((res) => {
      const result = Object.values(res[0]);
      console.log("resultresult", result);

      for (var i = 0; i < result.length; i++) {
        subval = subval + result[i].Tab_value;
      }
      console.log("sub val ", subval);
      this.setState({ TobaccoScore: subval }, () => {
        console.log("Set Tobacco Score", this.state.TobaccoScore);
        // this.props.score(subval);
        let data = {
          Id: 5,
          Title: "For tobacco and other substances:",
          Score: this.state.TobaccoScore,
        };
        database
          .InsertFinalScores(data)
          .then((data) =>
            console.log(`Tobaco Substance screening: ${JSON.stringify(data)}`)
          )
          .catch((error) =>
            console.log(`Error in Tobaco Substance screening: ${error}`)
          );
      });
    });
  };
  updatevalueforbuttonalc = (key, btnValue) => {
    selectedSubstance = [];
    var Acc_value = btnValue;
    var Sub_PkeyId = key;
    var subval = 0;
    var ldata = { Sub_PkeyId, Acc_value };
    selectedSubstance.push(ldata);
    database
      .Update_Substance_Use_Master(selectedSubstance)
      .catch((error) => console.log(error));
    this.getData1();

    database.queryAllSubstance_Use_Schema().then((res) => {
      const result = Object.values(res[0]);
      console.log("resultresult", result);

      for (var i = 0; i < result.length; i++) {
        subval = subval + result[i].Acc_value;
      }
      console.log("sub val ", subval);
      this.setState({ AlcoholScore: subval }, () => {
        console.log("Set Alcohol Score", this.state.AlcoholScore);
        // this.props.score(subval);
        let data = {
          Id: 6,
          Title: "For alcohol use:",
          Score: this.state.AlcoholScore,
        };
        database
          .InsertFinalScores(data)
          .then((data) =>
            console.log(`Substance Alcohol screening: ${JSON.stringify(data)}`)
          )
          .catch((error) =>
            console.log(`Error in Substance Alcohol screening: ${error}`)
          );
      });
    });
  };

  updatecheckvalue = (key, value) => {
    this.setState({ slectedVal: value });
    this.setState({ isNever: false });
    selectedSubstanceOption = [];
    var Sub_value = this.validateScore(value);
    var value = value;
    var SubsOpt_PkeyID = key;

    var ldata = { SubsOpt_PkeyID, value, Sub_value };
    selectedSubstanceOption.push(ldata);

    database
      .Update_SubstanceOption_Master(selectedSubstanceOption)
      .catch((error) => console.log(error));
    this.getData();
  };

  bold() {
    return (
      <Text
        style={{
          fontSize: 16,
          fontWeight: "700",
          lineHeight: 22,
          textDecorationLine: "underline",
        }}
      >
        {strings.ever}{" "}
      </Text>
    );
  }
  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#ffffff",
          // backgroundColor: "#E5E5E5"
        }}
      >
        <ScrollView keyboardShouldPersistTaps="handled">
          <View>
            {this.state.token ? (
              <View style={{ marginHorizontal: 16 }}>
                <Myprofile
                  back={true}
                  navigation={this.props.navigation}
                  onPress={() =>
                    this.props.navigation.navigate("Search", {
                      screen: "Profile",
                    })
                  }
                />
              </View>
            ) : (
              <View style={{ marginHorizontal: 16 }}>
                <CustomBack
                  back={true}
                  navigation={this.props.navigation}
                  onPress={() =>
                    this.props.navigation.navigate("Search", {
                      screen: "Login",
                    })
                  }
                />
              </View>
            )}
          </View>
          <View style={{ paddingHorizontal: "4%", marginTop: "5%" }}>
            <Text
              style={{
                fontFamily: "Quicksand-Bold",
                fontSize: 18,
                fontWeight: "700",
                lineHeight: 24,
              }}
            >
              {strings.sut}
            </Text>
          </View>
          <View style={{ paddingHorizontal: "4%", marginTop: "2%" }}>
            <Text
              style={{
                //fontFamily: "Open Sans",
                fontSize: 16,
                // fontWeight: "600",
                lineHeight: 22,
              }}
            >
              {strings.inurlife} {this.bold()}
              {strings.used}.
            </Text>
          </View>
          <View style={{ paddingHorizontal: 16, marginTop: "-4%" }}>
            <FlatList
              data={this.state.questation}
              style={{ marginTop: "5%" }}
              renderItem={({ item }) => (
                <View style={{ flexDirection: "column", marginTop: "5%" }}>
                  <CustomSubstanceButton
                    onPress={() => {
                      {
                        this.updatecheckvalue(
                          item.SubsOpt_PkeyID,
                          item.value ? this.state.val : true
                        );
                      }
                    }}
                    color={item.Sub_value == true ? "#F2F2F2" : "#A1683A"}
                    backgroundColor={
                      item.Sub_value == true ? "#A1683A" : "#F2F2F2"
                    }
                    title={item.SubsOpt_Options}
                    redius={10}
                    width={"100%"}
                    height={
                      Platform.OS === "ios"
                        ? (deviceHeight * 8) / 100
                        : (deviceHeight * 12) / 100
                    }
                  />
                </View>
              )}
            />
            <View style={{ marginTop: "5%", marginBottom: "5%" }}>
              <CustomSubstanceButton
                onPress={() => {
                  this.NeverBtn();
                }}
                color={this.state.txtcolor}
                backgroundColor={this.state.btnColor}
                title={strings.nosubst}
                redius={10}
                width={(deviceWidth * 90) / 100}
                height={60}
              />
            </View>
          </View>
          {this.state.isNever == true && this.state.slectedVal == false ? (
            <View style={{ paddingHorizontal: "5%" }}>
              <FlatList
                data={this.state.HistoryQues}
                style={{ marginTop: "2%" }}
                renderItem={({ item }) => (
                  <View style={{ flexDirection: "column", marginTop: "5%" }}>
                    <View style={{ width: "100%" }}>
                      <Text
                        style={{
                          fontSize: 16,
                          lineHeight: 22,
                          color: "#060D0B",
                          fontFamily: "Quicksand-Bold",
                          fontWeight: "400",
                        }}
                      >
                        {item.SubsOpt_Options}.
                      </Text>
                    </View>
                    <View style={{ marginTop: 10 }}>
                      <CustomSubstanceButton
                        onPress={() => {
                          this.validateUpdate(
                            item.SubsOpt_PkeyID,
                            "I’m currently being treated"
                          );
                        }}
                        color={
                          item.SubsOpt_Options_Value1 ==
                          "I’m currently being treated"
                            ? "#F2F2F2"
                            : "#A1683A"
                        }
                        backgroundColor={
                          item.SubsOpt_Options_Value1 ==
                          "I’m currently being treated"
                            ? "#A1683A"
                            : "#F2F2F2"
                        }
                        title={strings.current}
                        redius={10}
                        width={(deviceWidth * 90) / 100}
                        height={50}
                      />
                    </View>
                    <View
                      style={{
                        marginTop: 10,
                      }}
                    >
                      <CustomSubstanceButton
                        onPress={() => {
                          this.validateUpdate(
                            item.SubsOpt_PkeyID,
                            "I have received treatment in the past"
                          );
                        }}
                        color={
                          item.SubsOpt_Options_Value1 ==
                          "I have received treatment in the past"
                            ? "#F2F2F2"
                            : "#A1683A"
                        }
                        backgroundColor={
                          item.SubsOpt_Options_Value1 ==
                          "I have received treatment in the past"
                            ? "#A1683A"
                            : "#F2F2F2"
                        }
                        title={strings.past}
                        redius={10}
                        width={(deviceWidth * 90) / 100}
                        height={50}
                      />
                    </View>
                    <View
                      style={{
                        marginTop: 10,
                      }}
                    >
                      <CustomSubstanceButton
                        onPress={() => {
                          this.validateUpdate(
                            item.SubsOpt_PkeyID,
                            "I have never been treated"
                          );
                        }}
                        color={
                          item.SubsOpt_Options_Value1 ==
                          "I have never been treated"
                            ? "#F2F2F2"
                            : "#A1683A"
                        }
                        backgroundColor={
                          item.SubsOpt_Options_Value1 ==
                          "I have never been treated"
                            ? "#A1683A"
                            : "#F2F2F2"
                        }
                        title={strings.never}
                        redius={10}
                        width={(deviceWidth * 90) / 100}
                        height={50}
                      />
                    </View>
                  </View>
                )}
              />
              <View style={{ marginTop: 30, marginBottom: 30 }}>
                <CustomButton
                  title={strings.finish}
                  redius={40}
                  width={230}
                  height={45}
                  onPress={() =>
                    this.props.navigation.navigate("SelectOptions")
                  }
                />
              </View>
            </View>
          ) : null}
          {this.state.slectedVal == true && this.state.isNever == false ? (
            <View>
              <View style={{ paddingHorizontal: "4%", marginTop: "2%" }}>
                <Text
                  style={{
                    //fontFamily: "Open Sans",
                    fontSize: 16,
                    fontWeight: "600",
                    lineHeight: 22,
                  }}
                >
                  {strings.tellus}.
                </Text>
                <View>
                  <FlatList
                    data={this.state.questation2}
                    style={{}}
                    renderItem={({ item }) => (
                      <View
                        style={{ flexDirection: "column", marginTop: "5%" }}
                      >
                        <View style={{ width: "90%", flexDirection: "row" }}>
                          {/* <Text>{JSON.stringify(item.value)}</Text> */}
                          <View>
                            <Text
                              style={{
                                fontSize: 16,
                                lineHeight: 22,
                                color: "#060D0B",
                                fontFamily: "verdana",
                                fontWeight: "400",
                              }}
                            >
                              {"\u066D"}{" "}
                            </Text>
                          </View>
                          <Text
                            style={{
                              fontSize: 16,
                              lineHeight: 22,
                              color: "#060D0B",
                              fontFamily: "verdana",
                              fontWeight: "400",
                            }}
                          >
                            {item.Sub_Option} ?
                          </Text>
                        </View>
                        {/* <FlatList
                          data={this.state.ques1}
                          style={{}}
                          renderItem={({ item }) => ( */}
                        <View
                          style={{
                            flexDirection: "column",
                            marginTop: "5%",
                          }}
                        >
                          <View style={{ width: "90%", flexDirection: "row" }}>
                            {/* <Text>{JSON.stringify(item.value)}</Text> */}
                            <View style={{ marginTop: "0.5%" }} />
                            <Text
                              style={{
                                fontSize: 16,
                                lineHeight: 22,
                                color: "#060D0B",
                                fontFamily: "verdana",
                                fontWeight: "400",
                              }}
                            >
                              {"\u2981"}
                              {this.state.ques1}
                            </Text>
                          </View>
                          {/* <View
                            style={{
                              marginTop: "5%",
                            }}
                          >
                            <CustomSliderForTestPage
                              value={this.state.sliderValue}
                              onSlidingComplete={(value) =>
                                this.updatevalueforslidertob(
                                  item.Sub_PkeyId,
                                  value
                                )
                              }
                              OnChangeVal={(value) =>
                                this.setState({ sliderValue: value })
                              }
                            />
                          </View> */}
                          <View
                            style={{
                              flexDirection: "row",
                              justifyContent: "space-between",
                              paddingHorizontal: "1%",
                              marginTop: 5,
                            }}
                          >
                            <View style={{ marginBottom: 10 }}>
                              <CustomInfoButton
                                onPress={() =>
                                  this.updatevalueforslidertob(
                                    item.Sub_PkeyId,
                                    1
                                  )
                                }
                                title={strings.Never}
                                backgroundColor={
                                  item.Tab_value == 1 ? "#A1683A" : "#F2F2F2"
                                }
                                color={
                                  item.Tab_value == 1 ? "#F2F2F2" : "#A1683A"
                                }
                                redius={10}
                                width={(deviceWidth * 44) / 100}
                                height={45}
                              />
                            </View>
                            <View style={{}}>
                              <CustomInfoButton
                                onPress={() =>
                                  this.updatevalueforslidertob(
                                    item.Sub_PkeyId,
                                    2
                                  )
                                }
                                title={strings.Once}
                                backgroundColor={
                                  item.Tab_value == 2 ? "#A1683A" : "#F2F2F2"
                                }
                                color={
                                  item.Tab_value == 2 ? "#F2F2F2" : "#A1683A"
                                }
                                redius={10}
                                width={(deviceWidth * 44) / 100}
                                height={45}
                              />
                            </View>
                          </View>
                          <View
                            style={{
                              flexDirection: "row",
                              justifyContent: "space-between",
                              marginTop: 5,
                              paddingHorizontal: "1%",
                            }}
                          >
                            <View style={{}}>
                              <CustomInfoButton
                                onPress={() =>
                                  this.updatevalueforslidertob(
                                    item.Sub_PkeyId,
                                    3
                                  )
                                }
                                title={strings.Monthly}
                                backgroundColor={
                                  item.Tab_value == 3 ? "#A1683A" : "#F2F2F2"
                                }
                                color={
                                  item.Tab_value == 3 ? "#F2F2F2" : "#A1683A"
                                }
                                redius={10}
                                width={(deviceWidth * 44) / 100}
                                height={45}
                              />
                            </View>
                            <View style={{}}>
                              <CustomInfoButton
                                onPress={() =>
                                  this.updatevalueforslidertob(
                                    item.Sub_PkeyId,
                                    4
                                  )
                                }
                                title={strings.Weekly}
                                backgroundColor={
                                  item.Tab_value == 4 ? "#A1683A" : "#F2F2F2"
                                }
                                color={
                                  item.Tab_value == 4 ? "#F2F2F2" : "#A1683A"
                                }
                                redius={10}
                                width={(deviceWidth * 44) / 100}
                                height={45}
                              />
                            </View>
                          </View>
                          <View
                            style={{ marginTop: 10, paddingHorizontal: "1%" }}
                          >
                            <CustomInfoButton
                              onPress={() =>
                                this.updatevalueforslidertob(item.Sub_PkeyId, 5)
                              }
                              title={strings.Daily}
                              backgroundColor={
                                item.Tab_value == 5 ? "#A1683A" : "#F2F2F2"
                              }
                              color={
                                item.Tab_value == 5 ? "#F2F2F2" : "#A1683A"
                              }
                              redius={10}
                              width={(deviceWidth * 90) / 100}
                              height={45}
                            />
                            {/* </View> */}
                          </View>
                        </View>
                        <View
                          style={{
                            flexDirection: "column",
                            marginTop: "5%",
                          }}
                        >
                          <View style={{ width: "90%", flexDirection: "row" }}>
                            {/* <Text>{JSON.stringify(item.value)}</Text> */}
                            <View style={{ marginTop: "0.5%" }} />
                            <Text
                              style={{
                                fontSize: 16,
                                lineHeight: 22,
                                color: "#060D0B",
                                fontFamily: "verdana",
                                fontWeight: "400",
                              }}
                            >
                              {"\u2981"}
                              {this.state.ques2}
                            </Text>
                          </View>
                          {/* <View
                            style={{
                              marginTop: "5%",
                            }}
                          >
                            <CustomSliderForTestPage
                              value={this.state.sliderValue}
                              onSlidingComplete={(value) =>
                                this.updatevalueforslideralc(
                                  item.Sub_PkeyId,
                                  value
                                )
                              }
                              OnChangeVal={(value) =>
                                this.setState({ sliderValue: value })
                              }
                            />
                          </View> */}
                          <View
                            style={{
                              flexDirection: "row",
                              justifyContent: "space-between",
                              marginTop: 5,
                              paddingHorizontal: "1%",
                            }}
                          >
                            <View style={{ marginBottom: 10 }}>
                              <CustomInfoButton
                                onPress={() =>
                                  this.updatevalueforslideralc(
                                    item.Sub_PkeyId,
                                    1
                                  )
                                }
                                title={strings.Never}
                                backgroundColor={
                                  item.Acc_value == 1 ? "#A1683A" : "#F2F2F2"
                                }
                                color={
                                  item.Acc_value == 1 ? "#F2F2F2" : "#A1683A"
                                }
                                redius={10}
                                width={(deviceWidth * 44) / 100}
                                height={45}
                              />
                            </View>

                            <View style={{ marginBottom: 10 }}>
                              <CustomInfoButton
                                onPress={() =>
                                  this.updatevalueforslideralc(
                                    item.Sub_PkeyId,
                                    2
                                  )
                                }
                                title={strings.Once}
                                backgroundColor={
                                  item.Acc_value == 2 ? "#A1683A" : "#F2F2F2"
                                }
                                color={
                                  item.Acc_value == 2 ? "#F2F2F2" : "#A1683A"
                                }
                                redius={10}
                                width={(deviceWidth * 44) / 100}
                                height={45}
                              />
                            </View>
                          </View>
                          <View
                            style={{
                              flexDirection: "row",
                              justifyContent: "space-between",
                              marginTop: 5,
                              paddingHorizontal: "1%",
                            }}
                          >
                            <View style={{ marginBottom: 10 }}>
                              <CustomInfoButton
                                onPress={() =>
                                  this.updatevalueforslideralc(
                                    item.Sub_PkeyId,
                                    3
                                  )
                                }
                                title={strings.Monthly}
                                backgroundColor={
                                  item.Acc_value == 3 ? "#A1683A" : "#F2F2F2"
                                }
                                color={
                                  item.Acc_value == 3 ? "#F2F2F2" : "#A1683A"
                                }
                                redius={10}
                                width={(deviceWidth * 44) / 100}
                                height={45}
                              />
                            </View>
                            <View style={{ marginBottom: 10 }}>
                              <CustomInfoButton
                                onPress={() =>
                                  this.updatevalueforslideralc(
                                    item.Sub_PkeyId,
                                    4
                                  )
                                }
                                title={strings.Weekly}
                                backgroundColor={
                                  item.Acc_value == 4 ? "#A1683A" : "#F2F2F2"
                                }
                                color={
                                  item.Acc_value == 4 ? "#F2F2F2" : "#A1683A"
                                }
                                redius={10}
                                width={(deviceWidth * 44) / 100}
                                height={45}
                              />
                            </View>
                          </View>
                          <View
                            style={{
                              marginBottom: 10,
                              paddingHorizontal: "1%",
                            }}
                          >
                            <CustomInfoButton
                              onPress={() =>
                                this.updatevalueforslideralc(item.Sub_PkeyId, 5)
                              }
                              title={strings.Daily}
                              backgroundColor={
                                item.Acc_value == 5 ? "#A1683A" : "#F2F2F2"
                              }
                              color={
                                item.Acc_value == 5 ? "#F2F2F2" : "#A1683A"
                              }
                              redius={10}
                              width={(deviceWidth * 90) / 100}
                              height={45}
                            />
                          </View>
                        </View>
                      </View>
                      // </View>
                    )}
                  />
                </View>
                <FlatList
                  data={this.state.questation3}
                  style={{}}
                  renderItem={({ item }) => (
                    <View style={{ flexDirection: "column", marginTop: "5%" }}>
                      <View style={{ width: "90%", flexDirection: "row" }}>
                        {/* <Text>{JSON.stringify(item.value)}</Text> */}
                        <View style={{ marginTop: "0.5%" }}>
                          <Text
                            style={{
                              fontSize: 16,
                              lineHeight: 22,
                              color: "#060D0B",
                              fontFamily: "verdana",
                              fontWeight: "400",
                            }}
                          >
                            {"\u2981"}{" "}
                          </Text>
                        </View>
                        <Text
                          style={{
                            fontSize: 16,
                            lineHeight: 22,
                            color: "#060D0B",
                            fontFamily: "verdana",
                            fontWeight: "400",
                          }}
                        >
                          {item.Sub_Option} ?
                        </Text>
                      </View>
                      {/* <FlatList
                        data={this.state.ques1}
                        style={{}}
                        renderItem={({ item }) => ( */}
                      <View
                        style={{
                          flexDirection: "column",
                          marginTop: "5%",
                        }}
                      >
                        <View style={{ width: "90%", flexDirection: "row" }}>
                          {/* <Text>{JSON.stringify(item.value)}</Text> */}
                          <View style={{ marginTop: "0.5%" }} />
                          <Text
                            style={{
                              fontSize: 16,
                              lineHeight: 22,
                              color: "#060D0B",
                              fontFamily: "verdana",
                              fontWeight: "400",
                            }}
                          >
                            {"\u2981"}
                            {this.state.ques1}
                          </Text>
                        </View>
                        <View style={{ marginTop: 10 }}>
                          <CustomSubstanceButton
                            onPress={() => {
                              this.updatevalueforbuttontob(item.Sub_PkeyId, 3);
                            }}
                            color={item.Tab_value == 3 ? "#F2F2F2" : "#A1683A"}
                            backgroundColor={
                              item.Tab_value == 3 ? "#A1683A" : "#F2F2F2"
                            }
                            title={strings.n3months}
                            redius={10}
                            width={(deviceWidth * 90) / 100}
                            height={50}
                          />
                        </View>
                        <View style={{ marginTop: 10 }}>
                          <CustomSubstanceButton
                            onPress={() => {
                              this.updatevalueforbuttontob(item.Sub_PkeyId, 2);
                            }}
                            color={item.Tab_value == 2 ? "#F2F2F2" : "#A1683A"}
                            backgroundColor={
                              item.Tab_value == 2 ? "#A1683A" : "#F2F2F2"
                            }
                            title={strings.y3months}
                            redius={10}
                            width={(deviceWidth * 90) / 100}
                            height={50}
                          />
                        </View>
                        <View style={{ marginTop: 10 }}>
                          <CustomSubstanceButton
                            onPress={() => {
                              this.updatevalueforbuttontob(item.Sub_PkeyId, 1);
                            }}
                            color={item.Tab_value == 1 ? "#F2F2F2" : "#A1683A"}
                            backgroundColor={
                              item.Tab_value == 1 ? "#A1683A" : "#F2F2F2"
                            }
                            title={strings.m3never}
                            redius={10}
                            width={(deviceWidth * 90) / 100}
                            height={50}
                          />
                        </View>
                      </View>
                      <View
                        style={{
                          flexDirection: "column",
                          marginTop: "5%",
                        }}
                      >
                        <View style={{ width: "100%", flexDirection: "row" }}>
                          {/* <Text>{JSON.stringify(item.value)}</Text> */}
                          <View style={{ marginTop: "0.5%" }} />
                          <Text
                            style={{
                              fontSize: 16,
                              lineHeight: 22,
                              color: "#060D0B",
                              fontFamily: "verdana",
                              fontWeight: "400",
                            }}
                          >
                            {"\u2981"}
                            {this.state.ques2}
                          </Text>
                        </View>
                        <View style={{ marginTop: 10 }}>
                          <CustomSubstanceButton
                            onPress={() => {
                              this.updatevalueforbuttonalc(item.Sub_PkeyId, 3);
                            }}
                            color={item.Acc_value == 3 ? "#F2F2F2" : "#A1683A"}
                            backgroundColor={
                              item.Acc_value == 3 ? "#A1683A" : "#F2F2F2"
                            }
                            title={strings.n3months}
                            redius={10}
                            width={(deviceWidth * 90) / 100}
                            height={50}
                          />
                        </View>
                        <View style={{ marginTop: 10 }}>
                          <CustomSubstanceButton
                            onPress={() => {
                              this.updatevalueforbuttonalc(item.Sub_PkeyId, 2);
                            }}
                            color={item.Acc_value == 2 ? "#F2F2F2" : "#A1683A"}
                            backgroundColor={
                              item.Acc_value == 2 ? "#A1683A" : "#F2F2F2"
                            }
                            title={strings.y3months}
                            redius={10}
                            width={(deviceWidth * 90) / 100}
                            height={50}
                          />
                        </View>
                        <View style={{ marginTop: 10 }}>
                          <CustomSubstanceButton
                            onPress={() => {
                              this.updatevalueforbuttonalc(item.Sub_PkeyId, 1);
                            }}
                            color={item.Acc_value == 1 ? "#F2F2F2" : "#A1683A"}
                            backgroundColor={
                              item.Acc_value == 1 ? "#A1683A" : "#F2F2F2"
                            }
                            title={strings.m3never}
                            redius={10}
                            width={(deviceWidth * 90) / 100}
                            height={50}
                          />
                        </View>
                      </View>
                    </View>
                  )}
                />
              </View>
              <View style={{ paddingHorizontal: "5%" }}>
                <FlatList
                  data={this.state.HistoryQues}
                  style={{ marginTop: "2%" }}
                  renderItem={({ item }) => (
                    <View style={{ flexDirection: "column", marginTop: "5%" }}>
                      <View style={{ width: "100%" }}>
                        <Text
                          style={{
                            fontSize: 16,
                            lineHeight: 22,
                            color: "#060D0B",
                            fontFamily: "Quicksand-Bold",
                            fontWeight: "400",
                          }}
                        >
                          {item.SubsOpt_Options}.
                        </Text>
                      </View>
                      <View style={{ marginTop: 10 }}>
                        <CustomSubstanceButton
                          onPress={() => {
                            this.validateUpdate(
                              item.SubsOpt_PkeyID,
                              "I’m currently being treated"
                            );
                          }}
                          color={
                            item.SubsOpt_Options_Value1 ==
                            "I’m currently being treated"
                              ? "#F2F2F2"
                              : "#A1683A"
                          }
                          backgroundColor={
                            item.SubsOpt_Options_Value1 ==
                            "I’m currently being treated"
                              ? "#A1683A"
                              : "#F2F2F2"
                          }
                          title={strings.current}
                          redius={10}
                          width={(deviceWidth * 90) / 100}
                          height={50}
                        />
                      </View>
                      <View
                        style={{
                          marginTop: 10,
                        }}
                      >
                        <CustomSubstanceButton
                          onPress={() => {
                            this.validateUpdate(
                              item.SubsOpt_PkeyID,
                              "I have received treatment in the past"
                            );
                          }}
                          color={
                            item.SubsOpt_Options_Value1 ==
                            "I have received treatment in the past"
                              ? "#F2F2F2"
                              : "#A1683A"
                          }
                          backgroundColor={
                            item.SubsOpt_Options_Value1 ==
                            "I have received treatment in the past"
                              ? "#A1683A"
                              : "#F2F2F2"
                          }
                          title={strings.past}
                          redius={10}
                          width={(deviceWidth * 90) / 100}
                          height={50}
                        />
                      </View>
                      <View
                        style={{
                          marginTop: 10,
                        }}
                      >
                        <CustomSubstanceButton
                          onPress={() => {
                            this.validateUpdate(
                              item.SubsOpt_PkeyID,
                              "I have never been treated"
                            );
                          }}
                          color={
                            item.SubsOpt_Options_Value1 ==
                            "I have never been treated"
                              ? "#F2F2F2"
                              : "#A1683A"
                          }
                          backgroundColor={
                            item.SubsOpt_Options_Value1 ==
                            "I have never been treated"
                              ? "#A1683A"
                              : "#F2F2F2"
                          }
                          title={strings.never}
                          redius={10}
                          width={(deviceWidth * 90) / 100}
                          height={50}
                        />
                      </View>
                    </View>
                  )}
                />
                <View style={{ marginTop: 30, marginBottom: 30 }}>
                  <CustomButton
                    title={strings.finish}
                    redius={40}
                    width={230}
                    height={45}
                    onPress={() =>
                      this.props.navigation.navigate("SelectOptions")
                    }
                  />
                </View>
              </View>
            </View>
          ) : null}
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  headtext: {
    fontSize: 14,
    lineHeight: 22,
    color: "#060D0B",
    fontFamily: "Quicksand-Bold",
    fontWeight: "400",
  },
});
