import React, { Component } from "react";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  FlatList,
  Alert,
} from "react-native";
import Toast from "react-native-simple-toast";
import CustomBack from "../../CustomFolder/CustomBack";
import CustomSlider from "../../CustomFolder/CustomSlider";
import CustomButton from "../../CustomFolder/CustomButton";
import CustomDisButton from "../../CustomFolder/CustomDisButton";
import CustomInfoButton from "../../CustomFolder/CustomInfoButton";
import CustomButtonDep from "../../CustomFolder/CustomButtonDep";
import Myprofile from "../../CustomFolder/Myprofile";
import * as database from "../../Database/allSchemas";
import { SliderContainer } from "../../CustomFolder/CustomSlider";
import AsyncStorage from "@react-native-async-storage/async-storage";
import strings from "../../Language/Language";
export var selectedHDST = [];
const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;
import { getpatientdetails } from "../../Api/function";

export default class DepressionScreening extends Component {
  constructor() {
    super();
    this.state = {
      token: "",
      isLoading: false,
      value: 0,
      questation: [],
      TotalScore: 0,
      selectedPK: 0,
      status: true,
      langugageCode: null,
      showbutton: false,
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      this.getToken();
    });
    this.getLanguage();
    this.getData();
  }
  componentWillUnmount() {
    this._unsubscribe;
  }
  getToken = async () => {
    let token;
    token = await AsyncStorage.getItem("token");
    this.setState({
      token: token,
    });
    // console.log("this.state.token", this.state.token);
  };
  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      this.setState({ langugageCode: get_Data });
      console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
        this.setLanguage("en");
      } else {
        strings.setLanguage("sp");
        this.setLanguage("sp");
      }
    });
  }
  async setLanguage(languageCode) {
    console.log(languageCode);
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
      });
    }
    // getLanguage(languageCode)
  }

  getData() {
    //debugger
    database.queryAllPatient_Depression().then((res) => {
      const result = Object.values(res[0]);
      this.setState({ questation: result });
      // console.log("result", this.state.questation);
    });
  }

  updatevalue(key, value) {
    selectedHDST = [];
    this.setState({ value: value });
    var value = value;
    var Score = value;
    var Staff_Score = value;
    var DST_PKeyID = key;
    var ldata = { value, DST_PKeyID, Score, Staff_Score };
    var subval = 0;
    selectedHDST.push(ldata);

    database
      .UpdatePatientDepressionMaster(selectedHDST)
      .catch((error) => console.error(error));
    this.getData();
    database.queryAllPatient_Depression().then((res) => {
      const result = Object.values(res[0]);
      console.log("resultresult", result);

      for (var i = 0; i < result.length; i++) {
        subval = subval + result[i].Score;
      }
      console.log("sub val ", subval);
      this.setState({ TotalScore: subval }, () => {
        console.log("Set Total Score", this.state.TotalScore);
        // this.props.score(subval);
        let data = {
          Id: 1,
          Title: "Depression",
          Score: this.state.TotalScore,
        };
        database
          .InsertFinalScores(data)
          .then((data) =>
            console.log(`Depression screening: ${JSON.stringify(data)}`)
          )
          .catch((error) =>
            console.log(`Error in depression screening: ${error}`)
          );
      });
      if (key === 8 && value !== 0) {
        console.log(result[7].Score, "result[7].Score ");
        Alert.alert("", strings.emergency, [{ text: "RETURN TO SCREENING" }], {
          cancelable: false,
        });
      }
    });
  }
  bold() {
    return (
      <Text
        style={{
          //fontFamily: "Open Sans",
          fontSize: 16,
          fontWeight: "700",
          lineHeight: 22,
          textDecorationLine: "underline",
        }}
      >
        {strings.ovprw}?
      </Text>
    );
  }

  //   sliderTrackValue(value, pkId) {
  //   if (value <= 3 && value >= 0) {
  //     this.setState(
  //       {
  //         value: 3,
  //       },
  //       () => this.updatevalue(pkId, value)
  //     );
  //   } else if (value > 3 && value <= 5) {
  //     this.setState(
  //       {
  //         value: 5,
  //       },
  //       () => this.updatevalue(pkId, value)
  //     );
  //   }
  // }

  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#fff",
          // backgroundColor: "#E5E5E5"
        }}
      >
        <ScrollView keyboardShouldPersistTaps="handled">
          <View>
            {this.state.token ? (
              <View style={{ marginHorizontal: 16 }}>
                <Myprofile
                  back={true}
                  navigation={this.props.navigation}
                  onPress={() =>
                    this.props.navigation.navigate("Search", {
                      screen: "Profile",
                    })
                  }
                />
              </View>
            ) : (
              <View style={{ marginHorizontal: 16 }}>
                <CustomBack
                  back={true}
                  navigation={this.props.navigation}
                  onPress={() =>
                    this.props.navigation.navigate("Search", {
                      screen: "Login",
                    })
                  }
                />
              </View>
            )}
          </View>
          <View style={{ paddingHorizontal: "4%", marginTop: "5%" }}>
            <Text
              style={{
                fontFamily: "Quicksand-Bold",
                fontSize: 18,
                fontWeight: "700",
                lineHeight: 24,
              }}
            >
              {strings.dpsceen}
            </Text>
          </View>
          <View
            style={{
              paddingHorizontal: "4%",
              marginTop: "2%",
            }}
          >
            <Text
              style={{
                //fontFamily: "Open Sans",
                fontSize: 16,
                // fontWeight: "600",
                lineHeight: 22,
              }}
            >
              {strings.biodesc} {this.bold()}
            </Text>
          </View>
          <View style={{ paddingHorizontal: "4%", marginTop: "-4%" }}>
            <FlatList
              data={this.state.questation}
              style={{ marginTop: "5%" }}
              renderItem={({ item }) => (
                <View style={{ flexDirection: "column", marginTop: "2%" }}>
                  <View style={{ width: "90%", flexDirection: "row" }}>
                    <View style={{ marginTop: "0%" }}>
                      <Text
                        style={{
                          fontSize: 16,
                          lineHeight: 22,
                          color: "#060D0B",
                          fontFamily: "Quicksand-Bold",
                          fontWeight: "400",
                        }}
                      >
                        {"\u2981"}{" "}
                      </Text>
                    </View>
                    <Text
                      style={{
                        fontSize: 16,
                        lineHeight: 22,
                        color: "#060D0B",
                        fontFamily: "verdana",
                        fontWeight: "400",
                      }}
                    >
                      {item.DST_Option}
                    </Text>
                  </View>

                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      marginTop: 5,
                      paddingHorizontal: "1%",
                    }}
                  >
                    <View style={{ marginBottom: 10 }}>
                      {/* <CustomSlider
                          value={this.state.value}
                          onSlidingComplete={(value) =>
                            this.updatevalue(item.DST_PKeyID, value)
                          }
                          // onSlidingComplete={() => console.log("heloooo")}
                          OnChange={(value) => this.setState({ value: value })}
                        /> */}
                      <TouchableOpacity
                        // onPress={Keyboard.dismiss()}
                        onPress={() => this.updatevalue(item.DST_PKeyID, 0)}
                        style={{
                          backgroundColor:
                            item.Score == 0 ? "#A1683A" : "#F2F2F2",

                          width: (deviceWidth * 44) / 100,
                          height:
                            Platform.OS === "ios"
                              ? (deviceHeight * 8) / 100
                              : (deviceHeight * 11) / 100,

                          justifyContent: "center",
                          alignItems: "center",
                          borderRadius: 10,
                          borderColor: "#A1683A",
                          borderWidth: 1,
                        }}
                      >
                        <Text
                          style={{
                            color: item.Score == 0 ? "#F2F2F2" : "#A1683A",
                            fontSize: 16,
                            fontWeight: "700",
                            // lineHeight: 16,

                            width:
                              this.state.langugageCode != 1 ? "100%" : "75%",
                            left: this.state.langugageCode != 1 ? 10 : 0,

                            fontFamily: "Quicksand-Bold",
                          }}
                        >
                          {strings.little}
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View style={{ marginBottom: 10 }}>
                      <CustomInfoButton
                        onPress={() => {
                          this.updatevalue(item.DST_PKeyID, 1);
                        }}
                        title={strings.Some}
                        backgroundColor={
                          item.Score == 1 ? "#A1683A" : "#F2F2F2"
                        }
                        color={item.Score == 1 ? "#F2F2F2" : "#A1683A"}
                        redius={10}
                        width={(deviceWidth * 44) / 100}
                        height={
                          Platform.OS === "ios"
                            ? (deviceHeight * 8) / 100
                            : (deviceHeight * 11) / 100
                        }
                      />
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      paddingHorizontal: "1%",
                    }}
                  >
                    <View style={{ marginBottom: 10 }}>
                      <TouchableOpacity
                        // onPress={Keyboard.dismiss()}
                        onPress={() => this.updatevalue(item.DST_PKeyID, 2)}
                        style={{
                          backgroundColor:
                            item.Score == 2 ? "#A1683A" : "#F2F2F2",

                          width: (deviceWidth * 44) / 100,
                          height:
                            Platform.OS === "ios"
                              ? (deviceHeight * 8) / 100
                              : (deviceHeight * 11) / 100,

                          justifyContent: "center",
                          alignItems: "center",
                          borderRadius: 10,
                          borderColor: "#A1683A",
                          borderWidth: 1,
                        }}
                      >
                        <Text
                          style={{
                            color: item.Score == 2 ? "#F2F2F2" : "#A1683A",
                            fontSize: 16,
                            fontWeight: "700",
                            // lineHeight: 16,

                            width:
                              this.state.langugageCode != 1 ? "100%" : "75%",
                            left: this.state.langugageCode != 1 ? 10 : 0,

                            fontFamily: "Quicksand-Bold",
                          }}
                        >
                          {strings.Most}
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View style={{ marginBottom: 10 }}>
                      <CustomInfoButton
                        onPress={() => this.updatevalue(item.DST_PKeyID, 3)}
                        title={strings.All}
                        backgroundColor={
                          item.Score == 3 ? "#A1683A" : "#F2F2F2"
                        }
                        color={item.Score == 3 ? "#F2F2F2" : "#A1683A"}
                        redius={10}
                        width={(deviceWidth * 44) / 100}
                        height={
                          Platform.OS === "ios"
                            ? (deviceHeight * 8) / 100
                            : (deviceHeight * 11) / 100
                        }
                      />
                    </View>
                  </View>
                </View>
              )}
            />
          </View>
          {selectedHDST != "" && selectedHDST != [] ? (
            <View style={{ marginTop: 30, marginBottom: 30 }}>
              <CustomButton
                title={strings.finish}
                redius={40}
                width={230}
                height={45}
                onPress={() => this.props.navigation.navigate("SelectOptions")}
              />
            </View>
          ) : (
            <View style={{ marginTop: 30, marginBottom: 30 }}>
              <CustomDisButton
                title={strings.finish}
                redius={40}
                width={230}
                height={45}
                onPress={() => Toast.show("Please fill all details")}
              />
            </View>
          )}
        </ScrollView>
      </SafeAreaView>
    );
  }
}
