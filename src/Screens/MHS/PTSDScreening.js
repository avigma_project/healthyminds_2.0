import React, { Component } from "react";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  FlatList,
} from "react-native";
import CustomDisButton from "../../CustomFolder/CustomDisButton";
import Toast from "react-native-simple-toast";
import CustomBack from "../../CustomFolder/CustomBack";
import CustomButton from "../../CustomFolder/CustomButton";
import Myprofile from "../../CustomFolder/Myprofile";
import CustomInfoButton from "../../CustomFolder/CustomInfoButton";
const deviceWidth = Dimensions.get("window").width;
import * as database from "../../Database/allSchemas";
import AsyncStorage from "@react-native-async-storage/async-storage";
import strings from "../../Language/Language";
export let selectedPTSD = [];
export default class PTSDScreening extends Component {
  constructor() {
    super();
    this.state = {
      token: "",
      questation: [],
      value: false,
      PTSDScore: 0,
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      this.getToken();
    });
    this.getLanguage();
    this.getData();
  }
  componentWillUnmount() {
    this._unsubscribe;
  }
  getToken = async () => {
    let token;
    token = await AsyncStorage.getItem("token");
    this.setState({
      token: token,
    });
    // console.log("this.state.token", this.state.token);
  };
  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
        this.setLanguage("en");
      } else {
        strings.setLanguage("sp");
        this.setLanguage("sp");
      }
    });
  }
  async setLanguage(languageCode) {
    console.log(languageCode);
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
      });
    }

    // getLanguage(languageCode)
  }

  getData() {
    // alert("getDataptsd");
    //debugger;
    database.queryAllPatient_PTSD_Schema().then((res) => {
      const result = Object.values(res[0]);
      console.log("hiii", result);
      this.setState({ questation: result });
    });
  }

  validateScore(value) {
    var lvalue = 0;
    switch (value) {
      case true: {
        lvalue = 1;
        break;
      }
      case false: {
        lvalue = 0;
        break;
      }
    }
    return lvalue;
  }

  updatevalue = (key, value) => {
    selectedPTSD = [];
    this.setState({ value });
    var Staff_Score = this.validateScore(value);
    var Score = Staff_Score;
    var subval = 0;
    var value = value;
    var MS_PKeyId = key;
    var ldata = { value, MS_PKeyId, Staff_Score, Score };
    selectedPTSD.push(ldata);

    database
      .UpdatePatient_PTSD_Schema(selectedPTSD)
      .catch((error) => console.log(error));
    this.getData();

    database.queryAllPatient_PTSD_Schema().then((res) => {
      const patient_ptsd = Object.values(res[0]);
      for (var i = 0; i < patient_ptsd.length; i++) {
        subval = subval + patient_ptsd[i].Score;
      }
      this.setState({ PTSDScore: subval }, () => {
        // this.props.score(subval);
        console.log("PTSD Total Score: ", subval);
        let data = {
          Id: 4,
          Title: "Post Traumatic Stress Disorder",
          Score: this.state.PTSDScore,
        };
        database
          .InsertFinalScores(data)
          .then((data) =>
            console.log(`Depression screening: ${JSON.stringify(data)}`)
          )
          .catch((error) =>
            console.log(`Error in depression screening: ${error}`)
          );
      });
    });
  };

  bold() {
    return (
      <Text
        style={{
          //fontFamily: "Open Sans",
          fontSize: 16,
          fontWeight: "700",
          lineHeight: 22,
          textDecorationLine: "underline",
          fontFamily: "Quicksand-Bold",
        }}
      >
        {strings.pw}.
      </Text>
    );
  }

  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#ffffff",
          //  backgroundColor: "#E5E5E5"
        }}
      >
        <ScrollView keyboardShouldPersistTaps="handled">
          <View>
            {this.state.token ? (
              <View style={{ marginHorizontal: 16 }}>
                <Myprofile
                  back={true}
                  navigation={this.props.navigation}
                  onPress={() => this.props.navigation.navigate("Profile")}
                />
              </View>
            ) : (
              <View style={{ marginHorizontal: 16 }}>
                <CustomBack
                  back={true}
                  navigation={this.props.navigation}
                  onPress={() => this.props.navigation.navigate("Login")}
                />
              </View>
            )}
          </View>
          <View style={{ paddingHorizontal: "4%", marginTop: "5%" }}>
            <Text
              style={{
                fontFamily: "Quicksand-Bold",
                fontSize: 18,
                fontWeight: "700",
                lineHeight: 24,
              }}
            >
              {strings.ptsdscreeen}
            </Text>
          </View>
          <View style={{ paddingHorizontal: "4%", marginTop: "2%" }}>
            <Text
              style={{
                //fontFamily: "Open Sans",
                fontSize: 16,
                // fontWeight: "600",
                lineHeight: 22,
              }}
            >
              {strings.ptsddesc}:
            </Text>
          </View>
          <View style={{ paddingHorizontal: "4%", marginTop: "2%" }}>
            <Text
              style={{
                // fontFamily: "Quicksand-Bold",
                fontSize: 16,
                // fontWeight: "600",
                lineHeight: 22,
              }}
            >
              {strings.pleasetell} {this.bold()}
            </Text>
          </View>

          <View style={{ paddingHorizontal: "4%", marginTop: "-4%" }}>
            <FlatList
              data={this.state.questation}
              style={{ marginTop: "5%" }}
              renderItem={({ item, index }) => (
                <View style={{ flexDirection: "column", marginTop: "5%" }}>
                  {/* <View style={{ flexDirection: "row", width: "90%" }}> */}
                  <View style={{ width: "90%", flexDirection: "row" }}>
                    {/* <Text>{JSON.stringify(item.value)}</Text> */}
                    <View>
                      <Text
                        style={{
                          fontSize: 16,
                          lineHeight: 22,
                          color: "#060D0B",
                          // fontFamily: "Quicksand-Bold",
                          // fontWeight: "400",
                        }}
                      >
                        {index + 1}
                        {". "}
                        {/* {"\u2981"}{" "} */}
                      </Text>
                    </View>
                    <Text
                      style={{
                        fontSize: 16,
                        lineHeight: 22,
                        color: "#060D0B",
                        // fontFamily: "Quicksand-Bold",
                        fontWeight: "400",
                      }}
                    >
                      {item.MS_Option}
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: "column",
                      justifyContent: "space-around",
                    }}
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                        marginTop: 10,
                        paddingHorizontal: "1%",
                      }}
                    >
                      <View>
                        <CustomInfoButton
                          onPress={() => {
                            {
                              this.updatevalue(item.MS_PKeyId, true);
                            }
                          }}
                          color={item.Score == 1 ? "#F2F2F2" : "#A1683A"}
                          backgroundColor={
                            item.Score == 1 ? "#A1683A" : "#F2F2F2"
                          }
                          title="Yes"
                          redius={10}
                          width={(deviceWidth * 43) / 100}
                          height={45}
                        />
                      </View>
                      <View
                        style={{
                          paddingHorizontal: "4%",
                        }}
                      >
                        <CustomInfoButton
                          onPress={() => {
                            {
                              this.updatevalue(item.MS_PKeyId, false);
                            }
                          }}
                          color={item.Score == 0 ? "#F2F2F2" : "#A1683A"}
                          backgroundColor={
                            item.Score == 0 ? "#A1683A" : "#F2F2F2"
                          }
                          title="No"
                          redius={10}
                          width={(deviceWidth * 43) / 100}
                          height={45}
                        />
                      </View>
                    </View>
                  </View>
                  {/* </View> */}
                </View>
              )}
            />
          </View>
          {selectedPTSD != "" && selectedPTSD != [] ? (
            <View style={{ marginTop: 30, marginBottom: 30 }}>
              <CustomButton
                title={strings.finish}
                redius={40}
                width={230}
                height={45}
                onPress={() => this.props.navigation.navigate("SelectOptions")}
              />
            </View>
          ) : (
            <View style={{ marginTop: 30, marginBottom: 30 }}>
              <CustomDisButton
                title={strings.finish}
                redius={40}
                width={230}
                height={45}
                onPress={() => Toast.show(strings.toast1)}
              />
            </View>
          )}
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  headtext: {
    fontSize: 14,
    lineHeight: 22,
    color: "#060D0B",
    fontFamily: "Quicksand-Bold",
    fontWeight: "400",
  },
});
