import React, { Component } from "react";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  Linking,
} from "react-native";
import CustomButton from "../../CustomFolder/CustomButton";
import CustomBack from "../../CustomFolder/CustomBack";
import Myprofile from "../../CustomFolder/Myprofile";
import AsyncStorage from "@react-native-async-storage/async-storage";
import strings from "../../Language/Language";
const deviceHeight = Dimensions.get("window").height;
import * as database from "../../Database/allSchemas";
import { getpatientdetails } from "../../Api/function";
import Spinner from "react-native-loading-spinner-overlay";
import { getdata } from "../../Api/function";

export default class GetStarted extends Component {
  state = {
    token: "",
    showbutton: false,
    isLoading: false,
    call: true,
    langugageCode: null,
    Ltype: 1,
    textValue: "Spanish",
  };
  componentDidMount() {
    const { navigation } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      this.getToken();
      this.getLanguage();
      this.GetMasterDetails();
    });
  }

  async GetMasterDetails() {
    console.log("ltype data", this.state.Ltype);
    this.setState({ isLoading: true });
    var data = JSON.stringify({
      Ltype: this.state.Ltype,
    });
    //debugger;
    try {
      getdata(data).then((response) => {
        console.log("response ---dfiffff", response);
        this.setState({ isLoading: false });
        database.deletePatientMaster().catch((error) => console.log(error));
        database.deleteFinal_Scores_Schema().catch((Error) => {
          console.log(Error);
        });
        database.deletePatientDepression().catch((error) => console.log(error));
        database
          .deletePatientDepression2()
          .catch((error) => console.log(error));
        database
          .deletePatientDepression3()
          .catch((error) => console.log(error));
        database
          .deletePatientDepression4()
          .catch((error) => console.log(error));
        database
          .deleteSubstanceUseMaster()
          .catch((error) => console.log(error));
        database
          .deleteSubstanceOptionMaster()
          .catch((error) => console.log(error));

        // database
        //   .deleteSubstanceHistoryMaster()
        //   .catch((error) => console.log(error));
        database
          .deletePatient_Treated_Schema()
          .catch((error) => console.log(error));
        database.deletePatientSurveyMaster_Schema().catch((error) => {
          console.log(error);
        });
        database.deletePatientHealthCenters_Schema().catch((error) => {
          console.log(error);
        });
        database.deletePatientTechniques_List_Schema_Schema().catch((error) => {
          console.log(error);
        });

        this.setState({ isLoading: false });

        if (response.data != "") {
          if (response.data[0] != "") {
            database
              .InsertPatientDepressionMaster(response.data[0])
              .catch((error) => console.log(error));

            // ToastAndroid.showWithGravity(
            //     "InsertPatientDepressionMaster",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
            console.log(response, "Spanish");
          }
          if (response.data[1] != "") {
            database
              .InsertPatientMoodDisorderMaster(response.data[1])
              .catch((error) => console.log(error));
            // ToastAndroid.showWithGravity(
            //     "InsertPatientMoodDisorderMaster",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
          }
          if (response.data[2] != "") {
            database
              .InsertPatientAnxietyDisorderMaster(response.data[2])
              .catch((error) => console.log(error));
            // ToastAndroid.showWithGravity(
            //     "InsertPatientAnxietyDisorderMaster",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
          }
          if (response.data[3] != "") {
            database
              .InsertPatientPTSDMaster(response.data[3])
              .catch((error) => console.log(error));
            // ToastAndroid.showWithGravity(
            //     "InsertPatientPTSDMaster",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
          }
          if (response.data[4] != "") {
            // console.log('repson 3', response.data[4]);
            database
              .InsertPatientTreatedMaster(response.data[4])
              .catch((error) => console.log(error));
            // ToastAndroid.showWithGravity(
            //     "InsertPatientTreatedMaster",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
          }
          if (response.data[5][0] != "") {
            // console.log('repson 3', response.data[5][0]);
            database
              .InsertPatientSurveyMaster(response.data[5][0])
              .catch((error) => console.log(error));
            // ToastAndroid.showWithGravity(
            //     "InsertPatientTreatedMaster",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
            // console.log("Ashok final response", response.data[5][0])
          }
          if (response.data[6][0] != "") {
            // console.log('repson 3', response.data[6][0]);
            database
              .InsertHealthCenterDetails(response.data[6][0])
              .catch((error) => console.log(error));
            // ToastAndroid.showWithGravity(
            //     "InsertHealthCenterDetails",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
            // console.log("Ashok  InsertHealthCenterDetails", response.data[6][0])
          }

          if (response.data[7][0] != "") {
            // console.log('repson 3', response.data[7][0]);
            database
              .InsertTechniques_List_SchemaDetails(response.data[7][0])
              .catch((error) => console.log(error));
            // ToastAndroid.showWithGravity(
            //     "InsertTechniques_List_SchemaDetails",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
            // console.log("Ashok  InsertTechniques_List_SchemaDetails", response.data[7][0])
          }
          if (response.data[8] != "") {
            database
              .InsertSubstanceOptionMaster(response.data[8])
              .catch((error) => console.log(error));
          }
          if (response.data[9] != "") {
            database
              .InsertSubstanceUseMaster(response.data[9])
              .catch((error) => console.log(error));
          }
          // if (response.data[10] != "") {
          //   // console.log(
          //   //   response.data[10],
          //   //   "response.data[10]auhashdiuhduihudhuihuihauidh"
          //   // );
          //   database
          //     .InsertPatientMaster(response.data[10])
          //     .catch((error) => console.log(error));
          // }
        }
      });
    } catch (error) {
      console.log(error);
    }
  }
  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      // console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
        this.setLanguage("en");
      } else {
        if (get_Data == "2") {
          strings.setLanguage("sp");
          this.setLanguage("sp");
        } else {
          strings.setLanguage("en");
          this.setLanguage("en");
        }
      }
    });
  }

  async setLanguage(languageCode) {
    // console.log(languageCode)
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        // console.log("refresh", get_Data);
        this.setState({
          Ltype: 1,
          language: false,
        });
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
        this.setState({
          Ltype: 2,
          language: false,
        });
      });
    }
    this.GetMasterDetails();
    // getLanguage(languageCode)
  }

  async componentDidUpdate() {
    // this.getLanguage()
    if (this.state.update === true) {
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log(get_Data, "Change language");
        if (get_Data === 1) {
          this.setState({ update: false });
          this.GetMasterDetails();
        } else {
          if (get_Data === 2) {
            this.setState({ update: false });
            this.GetMasterDetails();
          }
        }
      });
    }
  }

  GetPatientDetails = async (token) => {
    this.setState({ isLoading: true });
    var data = {
      Type: 3,
    };
    console.log("datata", data, token);
    try {
      const res = await getpatientdetails(data, token);
      console.log("******GetPatientDetails*******", res);
      if (res[0].length > 0) {
        this.setState({ isLoading: false });
        database.deleteFinal_Scores_Schema().catch((Error) => {
          console.log(Error);
        });
        database.deletePatientMaster().catch((error) => console.log(error));
        let data1 = {
          Id: 1,
          Name: res[0][0].HMP_Name,
          age: res[0][0].HMP_Age.toString(),
          Gender: res[0][0].HMP_Gender,
          partnership_Status: res[0][0].HMP_Partnership_Status,
          Ethnic_identity: res[0][0].HMP_Racial_or_Ethnic,
          Suicide: res[0][0].HMP_AttemptedSuicide,
          Medication: res[0][0].HMP_Medication,
        };
        console.log("******getpatientdetails******", data1);
        database
          .InsertPatientMaster(data1)
          .catch((error) => console.log("minalerror", error));
        if (res[0][0].DST_Score !== 0) {
          let Depression = {
            Id: 1,
            Title: "Depression",
            Score: res[0][0].DST_Score,
          };
          database
            .InsertFinalScores(Depression)
            .then((data) =>
              console.log(`Depression screening: ${JSON.stringify(data)}`)
            )
            .catch((error) =>
              console.log(`Error in depression screening: ${error}`)
            );
        }

        if (res[0][0].ADS_Score !== 0) {
          let Bipolar = {
            Id: 2,
            Title: "Bipolar",
            Score: res[0][0].ADS_Score,
          };
          database
            .InsertFinalScores(Bipolar)
            .then((data) =>
              console.log(`Depression screening: ${JSON.stringify(data)}`)
            )
            .catch((error) =>
              console.log(`Error in depression screening: ${error}`)
            );
        }
        if (res[0][0].ADS_Score != 0) {
          let Anxiety = {
            Id: 3,
            Title: "Anxiety",
            Score: res[0][0].ADS_Score,
          };
          database
            .InsertFinalScores(Anxiety)
            .then((data) =>
              console.log(`Depression screening: ${JSON.stringify(data)}`)
            )
            .catch((error) =>
              console.log(`Error in depression screening: ${error}`)
            );
        }
        if (res[0][0].MS_Score !== 0) {
          let PTSDScore = {
            Id: 4,
            Title: "Post Traumatic Stress Disorder",
            Score: res[0][0].MS_Score,
          };
          database
            .InsertFinalScores(PTSDScore)
            .then((data) =>
              console.log(`Depression screening: ${JSON.stringify(data)}`)
            )
            .catch((error) =>
              console.log(`Error in depression screening: ${error}`)
            );
        }
        if (res[0][0].HMP_Score != 0) {
          let alcohol = {
            Id: 6,
            Title: "For alcohol use:",
            Score: res[0][0].HMP_Score,
          };
          database
            .InsertFinalScores(alcohol)
            .then((data) =>
              console.log(
                `Substance Alcohol screening: ${JSON.stringify(data)}`
              )
            )
            .catch((error) =>
              console.log(`Error in Substance Alcohol screening: ${error}`)
            );
        }
        this.setState({ showbutton: true, call: false, isLoading: false }, () =>
          this.props.navigation.navigate("ExistResult")
        );
      } else {
        this.setState({ isLoading: false, showbutton: false });
        database.deletePatientMaster().catch((error) => console.log(error));
      }
    } catch (error) {
      console.log(error);
    }
  };
  componentWillUnmount() {
    this._unsubscribe;
  }
  NextPage = () => {
    database
      .deleteFinal_Scores_Schema()
      .then(() => {
        database
          .deletePatientMaster()
          .then(() => {
            this.props.navigation.navigate("SelectOptions");
          })
          .catch((error) => console.log(error));
      })
      .catch((Error) => {
        console.log(Error);
      });
  };
  getToken = async () => {
    let token;
    token = await AsyncStorage.getItem("token");
    if (token) {
      if (this.state.call) {
        this.setState({ token });
        this.GetPatientDetails(token);
      } else {
        this.setState({ token: "" });
        this.setState({ showbutton: false });
      }
    }
    // else {
    //   database.deletePatientMaster().catch((error) => console.log(error));
    // }
    this.setState({
      token: token,
    });
  };

  contact = () => {
    Linking.openURL("https://www.healthelivin.org/contact-us/");
  };

  render() {
    return (
      <SafeAreaView
        style={{
          backgroundColor: "#fff",
          // backgroundColor: "#E5E5E5",
          height: deviceHeight - 10,
        }}
      >
        <Spinner visible={this.state.isLoading} />
        {this.state.token ? (
          <View style={{ marginHorizontal: 16 }}>
            <Myprofile
              navigation={this.props.navigation}
              onPress={() => this.props.navigation.navigate("Profile")}
            />
          </View>
        ) : (
          <View style={{ marginHorizontal: 16 }}>
            <CustomBack
              navigation={this.props.navigation}
              onPress={() =>
                this.props.navigation.navigate("Search", {
                  screen: "Login",
                })
              }
            />
          </View>
        )}

        <Text
          style={{
            fontSize: 25,
            fontWeight: "bold",
            fontFamily: "Quicksand-Bold",
            color: "#060D0B",
            paddingVertical: "8%",
            paddingHorizontal: "4%",
          }}
        >
          {strings.hithere},
        </Text>
        <View style={{ marginTop: "-7%" }}>
          <Text
            style={{
              fontSize: 16,
              fontWeight: "400",
              //fontFamily: "Open Sans",
              color: "#060D0B",
              paddingVertical: "4%",
              paddingHorizontal: "4%",
              lineHeight: 22,
            }}
          >
            {strings.des1}.
          </Text>
        </View>
        <View style={{ marginTop: "-5%" }}>
          <Text
            style={{
              fontSize: 16,
              fontWeight: "400",
              //fontFamily: "Open Sans",
              color: "#060D0B",
              paddingVertical: "4%",
              paddingHorizontal: "4%",
              lineHeight: 22,
            }}
          >
            {strings.des2}.
          </Text>
        </View>
        <View style={{ marginTop: 10 }}>
          <CustomButton
            title={strings.getstart}
            redius={40}
            width={230}
            height={45}
            onPress={() => this.NextPage()}
          />
        </View>
        {/* <View style={{ marginTop: 10 }}>
          {this.state.showbutton && (
            <CustomButton
              title={strings.ctr}
              redius={40}
              width={230}
              height={45}
              onPress={() => this.ResultPage()}
            />
          )}
        </View> */}
        <View
          style={{ alignSelf: "center", position: "absolute", bottom: 130 }}
        >
          <View style={{}}>
            <TouchableOpacity onPress={() => this.contact()}>
              <Text style={{ color: "#A1683B", fontWeight: "700" }}>
                {strings.contact}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
