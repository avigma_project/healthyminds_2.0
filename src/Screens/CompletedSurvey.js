import React from "react";
import {
  View,
  Text,
  Image,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
} from "react-native";
import * as database from "../Database/allSchemas";
import { fetchapi } from "../Api/function";
import { Actions } from "react-native-router-flux";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default class CompletedSurvey extends React.Component {
  componentDidMount() {
    this.getData();
  }
  getData() {
    //debugger;
    var arrData = [];
    var CustData, SurveryData;

    database
      .queryBaseSurveyDetails()
      .then((res) => {
        CustData = Object.values(res[0]);
        // CustData = JSON.stringify(patient_base_val);
        // console.log('patient_Base', baseSurvey);

        database
          .queryAllPatientSurvey_Schema()
          .then((res) => {
            SurveryData = Object.values(res[0]);
            for (var i = 0; i < SurveryData.length; i++) {
              console.log("data i", SurveryData[i].SUR_Control_Type);
              // var control_Type = SurveryData[i].SUR_Control_Type;
              let ldata = {
                SUR_Control_Type: SurveryData[i].SUR_Control_Type,
                SUR_Heading: SurveryData[i].SUR_Heading,
                SUR_IsActive: SurveryData[i].SUR_IsActive,
                SUR_IsDelete: SurveryData[i].SUR_IsDelete,
                SUR_PKeyID: SurveryData[i].SUR_PKeyID,
                SUR_Question: SurveryData[i].SUR_Question,
                Sur_Value: SurveryData[i].Sur_Value,
                Survey_Questions_Options_DTO: [],
                Type: SurveryData[i].Type,
                UserID: SurveryData[i].UserID,
              };
              arrData.push(ldata);
            }
            // console.log('patient_Base', SurveyDetails);
            console.log("SurveryData arrData", JSON.stringify(arrData));
            console.log("base value", JSON.stringify(CustData));
            const ldata = JSON.stringify(SurveryData);
            console.log("survey details", JSON.parse(ldata));

            fetchapi({
              url: "HealthyMindsApp/PostSurveryResponse",

              CustData: JSON.stringify(CustData),
              SurveryData: JSON.stringify(arrData),
            }).then((response) => {
              console.log("response survey", response);
            });
            // console.log("CustData",CustData)
            // console.log("SurveryData",SurveryData)
            // this.updateDefaultValue(CustData, SurveryData);
          })
          .catch((error) => console.log(error));
      })
      .catch((error) => console.log(error));
  }
  updateDefaultValue(baseSurvey, SurveyDetails) {
    //debugger;
    database.deletePatientSurveyBaseMaster_Schema(baseSurvey).catch((error) => {
      console.log(error);
    });

    database.UpdatePatientSurveyMaster(SurveyDetails).catch((Error) => {
      console.log(Error);
    });
  }
  render() {
    return (
      <View>
        <View
          style={{
            alignItmes: "center",
            alignItems: "center",
            justifyContent: "center",
            marginTop: "10%",
          }}
        >
          <Image
            style={{
              width: (deviceWidth * 35) / 100,
              height: (deviceHeight * 20) / 100,
              marginRight: 10,
              borderRadius: 250 / 2,
            }}
            source={require("../Images/logo.png")}
          />
        </View>
        <View
          style={{
            alignItmes: "center",
            alignItems: "center",
            justifyContent: "center",
            marginTop: "10%",
          }}
        >
          <Text style={{ fontWeight: "bold", fontSize: 18 }}>
            Thank you for Completing Survey{" "}
          </Text>
        </View>
        <View style={{ alignItems: "center", marginTop: 5 }}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => Actions.Welcome()}
          >
            <Text style={styles.buttonText}>Continue</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  button: {
    width: (deviceWidth * 60) / 100,
    height: (deviceHeight * 7) / 100,
    backgroundColor: "#4B96FB",
    borderRadius: 5,
    marginVertical: 12,
    paddingVertical: 12,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
  },
  buttonStyle: {
    color: "red",
    marginTop: 20,
    padding: 20,
    backgroundColor: "green",
  },
  buttonText: {
    fontSize: 18,
    color: "#ffffff",
    textAlign: "center",
  },
});
