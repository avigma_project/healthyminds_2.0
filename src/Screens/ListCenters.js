// import React from 'react';
// import { View, Text, StyleSheet, Dimensions, FlatList, ScrollView } from 'react-native';
// import { Card } from 'react-native-elements';
// const deviceweight = Dimensions.get("window").width;
// const deviceheight = Dimensions.get("window").height;
// const ldata = [
//     { id: 1, Hname: 'WES Health System, 1315 Windrim Avenue,', Hlocation: ' Philadelphia, PA 19141,', distance: '', contact: '215-455-3900' },
//     { id: 2, Hname: 'WES Health System, 2514 N. Broad Street,', Hlocation: 'Philadelphia, PA 19132,', distance: '', contact: '215-455-3900' },
//     { id: 3, Hname: 'Dunbar Community Counseling Services, 5238 Chester Avenue,', Hlocation: 'Philadelphia, PA 19143', distance: '', contact: '' },
//     { id: 4, Hname: 'Community Council Health Systems, 4900 Wyalusing Avenue,', Hlocation: ' Philadelphia, PA, 19131,', distance: '', contact: '215-473-7033' },
//     { id: 5, Hname: 'Merakey, 27 E. Mt. Airy Avenue,', Hlocation: ' Philadelphia, PA 19119,', distance: '', contact: '215-248-6700' },
//     { id: 6, Hname: 'Consortium, Inc, 5429 Chestnut Street,', Hlocation: 'Philadelphia, PA 19139,', distance: '', contact: '215-474-1280' }
// ]

// export default class ListCenters extends React.Component {

//     // constructor(props) {
//     //     super(props);
//     //     this.state = {
//     //         result: ''
//     //     }
//     // }
//     // componentDidMount() {

//     // }
//     render(item) {
//         //debugger
//         console.log(item)
//         return (
//             <View>
//                 <Text>{item.Hname}</Text>
//             </View>
//         )
//     }

//     render() {
//         return (
//             <ScrollView>
//                 <View style={styles.root}>
//                     <FlatList
//                         data={ldata}
//                         renderItem={(item)=>this.render(item)}
//                         keyExtractor={item => item.id.toString()}
//                     />
//                 </View>
//             </ScrollView>
//         )
//     }
// }
// const styles = StyleSheet.create({
//     root: {
//         flex: 1,
//         alignSelf: 'center',

//     },
//     container: {
//         marginTop: '5%'
//     },
//     txtStyle: {

//     }

// })

// THE HANDS® DEPRESSION SCREENING TOOL
import React from "react";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  PixelRatio,
  FlatList,
  TextInput,
  ScrollView,
  CheckBox,
  ToastAndroid,
  Linking,
  SafeAreaView,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Actions } from "react-native-router-flux";
// import Picker, { selectedHDST } from './PickerView';
import * as database from "../Database/allSchemas";
// import Datafile from '../reducers/Datafile';
// import {connect} from 'react-redux';
// import { emptyScore } from "../actions/score"
import { Card } from "react-native-elements";
import Loader from "../ShowOptios/Loader";
// import selectedHDST from './Picker';
import ScoreView from "./ScoreView";
import { fetchapi } from "../Api/function";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default class HDSTool extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      result: [],
      isLoading: false,
    };
  }

  componentDidMount() {
    this.getData();
  }
  getData() {
    //debugger
    database.queryBaseDetails().then((res) => {
      const result = Object.values(res[0]);
      if (result.length > 0) {
        this.patientDetailsApi(result[0].zipcode);
      } else {
        database.queryHealthCenterDetails().then((res) => {
          const result = Object.values(res[0]);
          console.log("result123", result);
          this.setState({ result: result });
        });
      }
    });
  }
  patientDetailsApi(zipCode) {
    this.setState({ isLoading: true });
    try {
      fetchapi({
        url: "HealthyMindsApp/GetHealthCenterDataWithDist",
        Type: 3,
        Zipcode: zipCode,
      }).then((response) => {
        // console.log({ response: JSON.stringify(response.data) });
        if (response != [] && response != null && response != "") {
          this.setState({
            result: response.data[0],
          });
          // console.log("ashok check log", response.data[0]);
          database
            .deletePatientHealthCenters_Schema()
            .catch((error) => console.log(error));
          database
            .InsertHealthCenterDetails(response.data[0])
            .catch((error) => console.log(error));
        }

        this.setState({ isLoading: false });
      });
    } catch (error) {
      this.setState({ isLoading: false });
      // console.log(error);
    }
  }
  openContact(ldata) {
    console.log("ldata", ldata);
    if (ldata != "") {
      Linking.openURL(`tel:${ldata}`);
    }
  }
  sendTOWeb() {
    return (
      <WebView
        source={{
          uri: "https://github.com/facebook/react-native",
        }}
      />
    );
  }

  async AsynStorageData(value) {
    if (value != null) {
      await AsyncStorage.setItem("webUrl", "0");
      await AsyncStorage.setItem("AsynstorageData", value);

      Actions.WebViewList();
    }
  }

  render_Item(item, index) {
    // console.log(item.HC_Web_URL);
    let distance = null;
    if (item.PatientDistance != null) {
      distance = parseFloat(item.PatientDistance);

      distance = distance.toFixed(2);
    }
    return (
      <Card>
        <TouchableOpacity
          style={styles.bottomView}
          onPress={() => this.AsynStorageData(item.HC_Web_URL)}
        >
          <View style={styles.container}>
            <Text style={{ color: "#1D4A99", fontWeight: "bold" }}>
              {item.HC_Name}
            </Text>
            <Text style={{ marginTop: "3%", color: "gray" }}>
              {item.HC_Address}
            </Text>
            <TouchableOpacity
              onPress={() => this.openContact(item.HC_Contactnum)}
            >
              <Text style={{ marginTop: "3%" }}>{item.HC_Contactnum}</Text>
            </TouchableOpacity>
          </View>
          {distance != null ? (
            <View style={{ marginTop: "5%" }}>
              <Text>{distance} miles</Text>
            </View>
          ) : (
            // null
            <View style={{ marginTop: "5%" }}>
              <Text>0.00 miles</Text>
            </View>
          )}
        </TouchableOpacity>
      </Card>
    );
  }
  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        {/* <View style={styles.container}> */}
        <View style={styles.titleView}>
          {/* <View style={styles.rowObject}> */}
          <Text style={styles.title}>HealthCare Centers</Text>
          {/* </View> */}
        </View>
        <Loader loading={this.state.isLoading} />
        <FlatList
          data={this.state.result}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => this.render_Item(item, index)} //(this.treatedChkbox(item),console.log('depression',item))}
        />
        {/* </View> */}
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
  },
  titleView: {
    width: "100%",
    height: 60,
    // justifyContent: 'center',
    // padding: 50,
    backgroundColor: "#359f06",
    borderBottomWidth: 1,
    borderBottomColor: "#CDD0D4EB",
    alignSelf: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 20,
    color: "#000",
    fontWeight: "bold",
    alignSelf: "center",
  },

  textStyles: {
    fontSize: 20,
    color: "#1D4A99",
  },
  container: {
    marginTop: "5%",
  },
});

// const mapStateToProps = (state) => {
//     console.log("HSDTool state: ", state);
//     return {
//         score: state.scoreReducer.score
//     }
// }

// const mapDispatchToProps = (dispatch) => {
//   return {
//     remove: () => dispatch(emptyScore())
//   }
// }

// export default connect(mapStateToProps, mapDispatchToProps)(HDSTool);
