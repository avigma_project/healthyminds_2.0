import React from "react";
import { View, Text } from "react-native";
import * as database from "../Database/allSchemas";
import { surveyList } from "./SurveyPicker";
import FlipToggle from "react-native-flip-toggle-button";

class SurveyFlip extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
    };
  }
  updatevalue(value) {
    //debugger;
    this.setState({ value: value });
    var Sur_Value = value.toString();
    var SUR_PKeyID = this.props.selectedQuest;
    let ldata = { SUR_PKeyID, Sur_Value };

    surveyList.push(ldata);

    database.UpdatePatientSurveyMaster_final(surveyList).catch((error) => {
      console.log(error);
    });
  }
  render() {
    return (
      <View>
        <FlipToggle
          value={this.state.value}
          buttonWidth={90}
          buttonHeight={30}
          buttonRadius={50}
          buttonOffColor={"gray"}
          sliderOffColor={"#ffffff"}
          buttonOnColor={"#4B96FB"}
          sliderOnColor={"#fff"}
          labelStyle={{ color: "#fff", fontSize: 10 }}
          onLabel={"Yes"}
          offLabel={"No"}
          onToggle={(value) => {
            this.updatevalue(value);
            // this.setState({ value: value });
          }}
        />
      </View>
    );
  }
}
export default SurveyFlip;
