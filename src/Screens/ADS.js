// CARROLL-DAVIDSON GENERALIZED ANXIETY DISORDER SCREEN©

import React from "react";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  PixelRatio,
  FlatList,
  TextInput,
  ScrollView,
  CheckBox,
  ToastAndroid,
  Picker,
  Linking,
  Button,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Actions } from "react-native-router-flux";
import FlipToggle from "react-native-flip-toggle-button";
import * as database from "../Database/allSchemas";
import FlipView, { selectedADS } from "./FlipView";
import ScoreView from "./ScoreView";
import { connect } from "react-redux";
import { emptyScore } from "../actions/score";
import strings from "../Language/Language";
import NativeBaseButton from "../components/NativeBaseButton";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const currentPage = "ValidateADS";

class ADS extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      questation: [],
    };
  }
  componentDidMount() {
    this.getLanguage();
    this.getData();
  }
  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
        this.setLanguage("en");
      } else {
        strings.setLanguage("sp");
        this.setLanguage("sp");
      }
    });
  }
  async setLanguage(languageCode) {
    console.log(languageCode);
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
      });
    }

    Actions.refresh({ key: "tasks" });
    // getLanguage(languageCode)
  }
  getData() {
    //debugger
    database.queryAllPatient_AnxietyDisorder_Schema().then((res) => {
      const result = Object.values(res[0]);
      this.setState({ questation: result });
    });
  }
  insertIntoDatabase() {
    console.log(selectedADS);
    // database
    //     .UpdatePatient_AnxietyDisorder_Schema(selectedADS)
    //     .catch(error => console.log(error))
    this.props.remove();
    // Actions.PTSD()
    Actions.Stress();
  }
  render() {
    return (
      <ScrollView style={{ flex: 1 }}>
        <View style={styles.container}>
          <View style={{ marginLeft: "5%" }}>
            <View style={{ marginTop: "3%" }}>
              <Text style={styles.questionStyles}>{strings.ADS}</Text>
            </View>
            <FlatList
              data={this.state.questation}
              style={{ marginTop: "5%" }}
              renderItem={({ item }) => (
                <View style={{ flexDirection: "column", marginTop: "5%" }}>
                  <View style={{ flexDirection: "row", width: "90%" }}>
                    <View style={{ width: "70%" }}>
                      <Text style={styles.textStyles}>{item.ADS_Option}</Text>
                    </View>

                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "space-around",
                      }}
                    >
                      <FlipView
                        selected_value={item.ADS_PKeyId}
                        page={currentPage}
                        defaultSelected={item.value}
                      />
                    </View>
                  </View>
                  {/* <TextInput
                            style={{ marginLeft: 10, fontSize: 16, width: '90%' }}
                            onChangeText={(suicide_Val) => { this.setState({ suicide_Val }, this.detailsInput(suicide_Val, 9)) }}
                            underlineColorAndroid="black"
                        /> */}
                </View>
              )}
            />
          </View>
          {/* <View style={{ alignItems: 'flex-end', flexDirection: 'row', justifyContent: 'space-between', marginRight: '4%', marginLeft: '4%' }}>
                        <TouchableOpacity style={styles.button} onPress={() => Actions.MDQ()}>
                            <Text style={styles.buttonText}>Previous</Text>
                        </TouchableOpacity>
                        <View style={{ width: deviceWidth * 20 / 100, height: deviceHeight * 10 / 100, alignItems: 'center', }}>
                            <ScoreView data={this.props.score} />
                        </View>
                        <TouchableOpacity style={styles.button} onPress={() => this.insertIntoDatabase()}>
                            <Text style={styles.buttonText}>Next</Text>
                        </TouchableOpacity>

                    </View> */}

          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-around",
              marginTop: "5%",
              width: "100%",
            }}
          >
            {/* <View
              style={{
                alignItems: "center",
                marginTop: "5%",
                backgroundColor: "#4B96FB",
                width: "30%",
                alignSelf: "center",
                padding: "3%",
              }}
            >
              <Button
                onPress={() => Actions.Anxiety()}
                title={strings.previous}
                color="#ffffff"
                style={styles.button}
              />
            </View> */}
            <NativeBaseButton
              onPress={() => Actions.Anxiety()}
              label={strings.previous}
            />
            <View
              style={{
                width: (deviceWidth * 20) / 100,
                height: (deviceHeight * 10) / 100,
                alignItems: "center",
              }}
            >
              <ScoreView data={this.props.score} />
            </View>
            {/* <View
              style={{
                alignItems: "center",
                marginTop: "5%",
                backgroundColor: "#4B96FB",
                width: "30%",
                alignSelf: "center",
                padding: "3%",
              }}
            >
              <Button
                onPress={() => this.insertIntoDatabase()}
                title={strings.Next}
                color="#ffffff"
                style={styles.button}
              />
            </View> */}
            <NativeBaseButton
              onPress={() => this.insertIntoDatabase()}
              label={strings.Next}
            />
          </View>

          <View style={{ width: "100%" }}>
            <TouchableOpacity
              style={styles.bottomView}
              onPress={() =>
                Linking.openURL(
                  "https://screening.mentalhealthscreening.org/health-e-livin"
                )
              }
            >
              <Text style={styles.bottonText} numberOfLines={2}>
                Source: Health E Livin
              </Text>
            </TouchableOpacity>
          </View>

          {/* {this.props.score > 0 ?  
                    (*/}
          <View>
            {/* <TouchableOpacity style={styles.skipbutton} onPress={() => Actions.Completed()}>
                            <Text style={styles.buttonText}>Skip</Text>
                        </TouchableOpacity> */}
          </View>
          {/*)
                     :null} */}
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
  },
  inputBox: {
    width: (deviceWidth * 85) / 100,
    height: (deviceHeight * 7) / 100,
    borderRadius: 5,
    // backgroundColor: '#EDF0F7',
    paddingVertical: 15,
  },
  skipbutton: {
    width: (deviceWidth * 30) / 100,
    height: (deviceHeight * 6) / 100,
    backgroundColor: "#4B96FB",
    borderRadius: 5,
    marginVertical: 9,
    paddingVertical: 9,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
  },
  textStyles: {
    fontSize: 18,
    color: "#1D4A99",
  },
  questionStyles: {
    fontSize: 25,
    color: "#1D4A99",
  },
  button: {
    width: (deviceWidth * 30) / 100,
    height: (deviceHeight * 6) / 100,
    backgroundColor: "#4B96FB",
    borderRadius: 5,
    marginVertical: 9,
    paddingVertical: 9,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
  },

  _button: {
    width: (deviceWidth * 20) / 100,
    height: (deviceHeight * 4) / 100,
    backgroundColor: "#4B96FB",
    borderRadius: 5,
    marginVertical: 7,
    paddingVertical: 7,
  },
  button_Text: {
    fontSize: 12,
    color: "#ffffff",
    textAlign: "center",
  },
  buttonText: {
    fontSize: 16,
    color: "#ffffff",
    textAlign: "center",
  },
  bottonText: {
    fontSize: 16,
    color: "#1D4A99",
    fontWeight: "bold",
  },
  bottomView: {
    width: (deviceWidth * 90) / 100,
    height: (deviceHeight * 6) / 100,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
  },
  avatarContainer: {
    borderColor: "#9B9B9B",
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },
  avatar: {
    borderRadius: 75,
    width: 120,
    height: 120,
  },
});

const mapStateToProps = (state) => {
  console.log("ADS state: ", state);
  return {
    score: state.scoreReducer.score,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    remove: () => dispatch(emptyScore()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ADS);
