// THE HANDS® DEPRESSION SCREENING TOOL
import React from "react";
import {
  View,
  Text,
  Platform,
  Dimensions,
  Alert,
  TouchableOpacity,
  Button,
  StyleSheet,
  PixelRatio,
  FlatList,
  TextInput,
  ScrollView,
  CheckBox,
  ToastAndroid,
  Linking,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Actions } from "react-native-router-flux";
import Picker, { selectedHDST } from "./PickerView";
import * as database from "../Database/allSchemas";
import Datafile from "../reducers/Datafile";
import { connect } from "react-redux";
import { emptyScore } from "../actions/score";
import strings from "../Language/Language";
// import selectedHDST from './Picker';
import ScoreView from "./ScoreView";
// export let showAlert =false
import { showAlert } from "./PickerView";

import NativeBaseButton from "../components/NativeBaseButton";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
class HDSTool extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      energy: "",
      energy_Val: "",
      blaming: "",
      blaming_Val: "",
      appetite: "",
      appetite_Val: "",
      asleep: "",
      asleep_Val: "",
      hopeless: "",
      hopeless_Val: "",
      blue: "",
      blue_Val: "",
      interest: "",
      interest_Val: "",
      worthlessness: "",
      worthlessness_Val: "",
      suicide: "",
      suicide_Val: "",
      concentrating: "",
      concentrating_Val: "",
      questation: [],
      iVal: [],
      showAlertValueData: false,
      isProcessing: false,
    };
  }

  componentDidMount() {
    this.getLanguage();
    this.getData();
  }

  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
        this.setLanguage("en");
      } else {
        strings.setLanguage("sp");
        this.setLanguage("sp");
      }
    });
  }
  async setLanguage(languageCode) {
    console.log(languageCode);
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
      });
    }

    Actions.refresh({ key: "tasks" });
    // getLanguage(languageCode)
  }

  getData() {
    //debugger
    database.queryAllPatient_Depression().then((res) => {
      const result = Object.values(res[0]);
      console.log("result", result);
      this.setState({ questation: result });
    });
  }
  previousSelect() {
    // setTimeout(() => { Actions.refresh({}) }, 1);
    // this.storeData()
    Actions.Depression();
  }
  async storeData() {
    await AsyncStorage.getItem("PatientDetials", (err, ldata) => {
      console.log("AsyncStorage profile", ldata);
    });
    await AsyncStorage.setItem("PatientDetials", "1");
  }

  insertIntoDatabase() {
    console.log("insert data", selectedHDST);
    // database
    //     .UpdatePatientDepressionMaster(selectedHDST)
    //     .catch(error => console.error(error));

    this.props.remove();
    Actions.Bipolar();
  }

  // setSelectedValue(data,index){
  //     //debugger;
  //     console.log(selectedHDST,data,index)
  // }
  render_Item(item, index) {
    // let index_val =item.DST_PKeyID + 10
    // console.log('appen',item.DST_PKeyID,index_val);
    // if (item.DST_PKeyID === 8) {
    //     if (item.Staff_Score === 1) {
    //         Alert.alert("", strings.emergency, [{ text: "RETURN TO SCREENING" }], {
    //             cancelable: false,
    //         }, 3000000);
    //     }
    //     if (item.Staff_Score === 2) {
    //         Alert.alert("", strings.emergency, [{ text: "RETURN TO SCREENING" }], {
    //             cancelable: false,
    //         }, 3000000);
    //     }
    //     if (item.Staff_Score === 3) {
    //         Alert.alert("", strings.emergency, [{ text: "RETURN TO SCREENING" }], {
    //             cancelable: false,
    //         }, 3000000);
    //     }
    //     console.log(item, "valued")
    // this.showAlertData()
    return (
      <View style={{ flexDirection: "column", marginTop: "5%" }}>
        <View style={{ flexDirection: "row", width: "95%" }}>
          <View
            style={{
              flexDirection: "column",
              width: "90%",
              backgroundColor: "",
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <View style={{ backgroundColor: "" }}>
                <Text style={styles.textStyles}>{item.DST_Option} </Text>
              </View>
            </View>
            <View>
              {/* <TouchableOpacity  onPress ={()=>this.setSelectedValue(item,index)}> */}
              <Picker
                itemData={item}
                selected_value={item.DST_PKeyID}
                defaultSelectedValue={item.value}
                valueUpdate={this.valueupdate}
              />
              {/* </TouchableOpacity> */}
            </View>
          </View>
        </View>
      </View>
    );
    // }
    // else {
    //     return (
    //         <View style={{ flexDirection: "column", marginTop: '5%', }}>
    //             <View style={{ flexDirection: 'row', width: '95%', }}>
    //                 <View style={{ flexDirection: 'column', width: '90%', backgroundColor: '' }}>
    //                     <View style={{ flexDirection: 'row' }}>
    //                         <View style={{ backgroundColor: '' }}>
    //                             <Text style={styles.textStyles}>{item.DST_Option} </Text>
    //                         </View>

    //                     </View>
    //                     <View>
    //                         {/* <TouchableOpacity  onPress ={()=>this.setSelectedValue(item,index)}> */}
    //                         <Picker itemData={item} selected_value={item.DST_PKeyID} defaultSelectedValue={item.value} />
    //                         {/* </TouchableOpacity> */}
    //                     </View>
    //                 </View>

    //             </View>

    //         </View>
    //     )
    // }
  }

  valueupdate = async (value) => {
    // alert(value)
    if (value != 0) {
      this.setState({ isProcessing: false }, () => {
        setTimeout(() => {
          Alert.alert(
            "",
            strings.emergency,
            [{ text: "RETURN TO SCREENING" }],
            { cancelable: false }
          );
        }, 500);
      });
    }
    Actions.refresh({ key: "tasks" });

    // console.log(value, "+++++++++++++++++++++++-------------------")
    // if (showAlert == true) {
    //     await AsyncStorage.getItem("alert", (err, get_Data) => {
    //         // console.log(get_Data , "Change language")
    //         // alert(get_Data, "Change language")
    //         if (get_Data == "2") {
    //             Alert.alert("", strings.emergency, [{ text: "RETURN TO SCREENING" }], {
    //                 cancelable: false,
    //             });
    //         }

    //     });

    // }
  };
  async disableValue() {
    await AsyncStorage.setItem("alert", "2");
  }
  // async componentDidUpdate() {
  //     // console.log("Checking API hit", "componentDidUpdate method is called");
  //     // this.setState({isLoading:true})
  //     if (this.state.update) {
  //         console.log(this.state.update, "+++++++++++++++++++++++-------------------")
  //         await AsyncStorage.getItem("alert", (err, get_Data) => {
  //             // console.log(get_Data , "Change language")
  //             if (get_Data == "1") {
  //                 Alert.alert("", strings.emergency, [{ text: "RETURN TO SCREENING" }], {
  //                     cancelable: false,
  //                 });
  //             }

  //         });
  //         await AsyncStorage.setItem('alert', "2");
  //     }

  // }
  // showAlertData() {
  //     if (showAlert == true) {
  //         Alert.alert("", strings.emergency, [{ text: "RETURN TO SCREENING" }], {
  //             cancelable: false,
  //         });
  //     }
  // }
  // componentDidUpdate() {
  //     if (showAlert == true) {
  //         Alert.alert("", strings.emergency, [{ text: "RETURN TO SCREENING" }], {
  //             cancelable: false,
  //         }, () => {
  //             showAlert = false
  //         });
  //     }
  // }
  render() {
    return (
      <ScrollView style={{ flex: 1 }}>
        <View style={styles.container}>
          <View style={{ marginLeft: "5%" }}>
            <View style={{ marginTop: "3%" }}>
              {/* <Text style={styles.questionStyles}>Over the past two weeks, how often have you?</Text> */}
              <Text style={styles.questionStyles}>{strings.Depression}</Text>
            </View>
            <FlatList
              data={this.state.questation}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item, index }) => this.render_Item(item, index)} //(this.treatedChkbox(item),console.log('depression',item))}
            />
          </View>

          {/* <View style={{ alignItems: 'flex-end', flexDirection: 'row', justifyContent: 'space-between', marginRight: '4%', marginLeft: '4%', width: deviceWidth * 90 / 100, height: deviceHeight * 10 / 100 }}>
                        <TouchableOpacity style={styles.button} onPress={() => this.previousSelect()}>
                            <Text style={styles.buttonText}>Previous</Text>
                        </TouchableOpacity>
                        <View style={{ width: deviceWidth * 20 / 100, height: deviceHeight * 10 / 100, alignItems: 'center', }}> */}
          {/* <Text style={styles.textStylesScore}> score </Text> */}

          {/* <Score /> */}

          {/* <ScoreView data={this.props.score} />

                        </View>
                        <TouchableOpacity style={styles.button} onPress={() => this.insertIntoDatabase()}>
                            <Text style={styles.buttonText}>Next</Text>
                        </TouchableOpacity>
                    </View> */}
          {/* <View style={{flexDirection:'row',justifyContent:'space-around',marginTop:'5%',width:'100%'}}>
                        <View style={styles.button}>
                        <Button
                          onPress={() => this.previousSelect()}
                            title="Previous"
                            color="#ffffff"
                            style={styles.buttonText}
                        />
                        </View>
                        <ScoreView data={this.props.score} />
                        <View style={styles.button}>
                        <Button
                           onPress={() => this.insertIntoDatabase()}
                            title="Next"
                            color="#ffffff"
                            style={styles.buttonText}
                        />
                        </View>
                    </View> */}
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-around",
              marginTop: "5%",
              width: "100%",
            }}
          >
            {/* <View
              style={{
                alignItems: "center",
                marginTop: "5%",
                backgroundColor: "#4B96FB",
                width: "30%",
                alignSelf: "center",
                padding: "3%",
              }}
            >
              <Button
                onPress={() => this.previousSelect()}
                title={strings.previous}
                color="#ffffff"
                style={styles.button}
              />
            </View> */}
            <NativeBaseButton
              onPress={() => this.previousSelect()}
              label={strings.previous}
            />
            <ScoreView data={this.props.score} />
            {/* <View
              style={{
                alignItems: "center",
                marginTop: "5%",
                backgroundColor: "#4B96FB",
                width: "30%",
                alignSelf: "center",
                padding: "3%",
              }}
            >
              <Button
                onPress={() => this.insertIntoDatabase()}
                title={strings.Next}
                color="#ffffff"
                style={styles.button}
              />
            </View> */}
            <NativeBaseButton
              onPress={() => this.insertIntoDatabase()}
              label={strings.Next}
            />
          </View>

          <View style={{ width: "100%" }}>
            <TouchableOpacity
              style={styles.bottomView}
              onPress={() =>
                Linking.openURL(
                  "https://screening.mentalhealthscreening.org/health-e-livin"
                )
              }
            >
              <Text style={styles.bottonText} numberOfLines={2}>
                Source: Health E Livin
              </Text>
            </TouchableOpacity>
          </View>
          {/* <TouchableOpacity style={styles.button,{marginTop:'5%'}} onPress={() => Actions.Completed()}>
                            <Text style={styles.buttonText}>Skip</Text>
                        </TouchableOpacity> */}

          {/* <View>
                    <TouchableOpacity style={styles.skipbutton} onPress={() => Actions.Completed()}>
                            <Text style={styles.buttonText}>Skip</Text>
                        </TouchableOpacity>
                    </View> */}
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
  },
  textStyles: {
    fontSize: 20,
    color: "#1D4A99",
  },
  questionStyles: {
    fontSize: 25,
    color: "#1D4A99",
  },
  textStylesScore: {
    fontSize: 20,
    color: "#1D4A99",
    margin: "15%",
  },
  inputBox: {
    width: (deviceWidth * 85) / 100,
    height: (deviceHeight * 7) / 100,
    borderRadius: 5,
    // backgroundColor: '#EDF0F7',
    paddingVertical: 15,
  },
  pickerStyle: {
    justifyContent: "center",
    fontSize: 12,
  },
  // textStyles: {
  //     fontSize: 18,
  //     color: 'black',
  //     fontWeight: 'bold'
  // },
  skipbutton: {
    width: (deviceWidth * 30) / 100,
    height: (deviceHeight * 6) / 100,
    backgroundColor: "#4B96FB",
    borderRadius: 5,
    marginVertical: 9,
    paddingVertical: 9,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
  },
  button: {
    width: (deviceWidth * 30) / 100,
    height: (deviceHeight * 6) / 100,
    backgroundColor: "#4B96FB",
    borderRadius: 5,
    marginVertical: 9,
    paddingVertical: 9,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
  },
  _button: {
    width: (deviceWidth * 20) / 100,
    height: (deviceHeight * 4) / 100,
    backgroundColor: "#4B96FB",
    borderRadius: 5,
    marginVertical: 7,
    paddingVertical: 7,
  },
  button_Text: {
    fontSize: 12,
    color: "#ffffff",
    textAlign: "center",
  },
  buttonText: {
    fontSize: 16,
    color: "#ffffff",
    textAlign: "center",
  },
  avatarContainer: {
    borderColor: "#9B9B9B",
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },
  avatar: {
    borderRadius: 75,
    width: 120,
    height: 120,
  },
  bottonText: {
    fontSize: 16,
    color: "#1D4A99",
    fontWeight: "bold",
  },
  bottomView: {
    width: (deviceWidth * 90) / 100,
    height: (deviceHeight * 6) / 100,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
  },
});

const mapStateToProps = (state) => {
  console.log("HSDTool state: ", state);
  return {
    score: state.scoreReducer.score,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    remove: () => dispatch(emptyScore()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HDSTool);
