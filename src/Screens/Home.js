import React, { PureComponent } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  Dimensions,
  StyleSheet,
  PixelRatio,
  Button,
  Image,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  Platform,
} from "react-native";
import { Button as NativeBaseButton } from "native-base";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Actions } from "react-native-router-flux";
import Toast from "react-native-simple-toast";
// import { Title } from 'react-native-paper'
import { connect } from "react-redux";
// import Database from '../Database/allSchemas';
import * as database from "../Database/allSchemas";
import { fetchapi } from "../Api/function";

import Loader from "../ShowOptios/Loader";
import strings from "../Language/Language";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

// export default class Home extends React.Component {
export default class Home extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      patname: "",
      age: "",
      zipcode: "",
      update: false,
      reload: false,
      isLoading: false,
      update: true,
      count: 0,
    };
  }

  componentDidMount() {
    //debugger
    // console.log("called did mount");
    // this.getLanguage()
  }
  // UNSAFE_componentWillUpdate(){
  //     // if(this.state.update == true){
  //         this.getLanguage()
  //     // }

  // }

  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      // console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
        this.setLanguage("en");
      } else {
        strings.setLanguage("sp");
        this.setLanguage("sp");
      }
    });
  }

  async setLanguage(languageCode) {
    // console.log(languageCode);
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      this.setState({ update: false });
      await AsyncStorage.setItem("refresh", "1");
    } else {
      this.setState({ update: false });
      await AsyncStorage.setItem("refresh", "2");
    }
    this.setState({ reload: true });
    Actions.refresh({ key: "tasks" });
    // getLanguage(languageCode)
  }
  patientDetailsApi() {
    this.setState({ isLoading: true });
    try {
      fetchapi({
        url: "HealthyMindsApp/GetHealthCenterDataWithDist",
        Type: 3,
        Zipcode: this.state.zipcode,
      }).then((response) => {
        // console.log("ashok check log", response.data[0]);
        database
          .deletePatientHealthCenters_Schema()
          .catch((error) => console.log(error));
        database
          .InsertHealthCenterDetails(response.data[0])
          .catch((error) => console.log(error));
        this.setState({ isLoading: false });
      });
    } catch (error) {
      this.setState({ isLoading: false });
      // console.log(error);
    }
  }

  getData() {
    //debugger
    database
      .queryBaseDetails()
      .then((res) => {
        const result = Object.values(res[0]);
        this.setState({
          patname: result[0].Name,
          age: result[0].age,
          zipcode: result[0].zipcode,
          update: true,
        });
      })
      .catch((error) => console.log(error));
  }
  sendNextpage() {
    //debugger
    this.setState({ isLoading: true });
    // this.patientDetailsApi()

    database.deleteBaseDetails().catch((error) => console.log("delete", error));
    if (
      (this.state.patname != "", this.state.age != "", this.state.zipcode != "")
    ) {
      // Actions.PatientDetails()
      let data = {
        Id: 1,
        Name: this.state.patname,
        age: this.state.age,
        zipcode: this.state.zipcode,
      };
      // console.log(data)
      database
        .InsertBaseDetails(data)
        .catch((error) => console.log("insert", error));

      // this.textInput.clear()
      // this.setState({
      //     patname:'',age:'',zipcode:''
      // })
      this.setState({ isLoading: false });
      Actions.PatientDetails();
      // Actions.PatientDetails({ zipcode: this.state.zipcode, age: this.state.age, patname: this.state.patname })
    } else {
      this.setState({ isLoading: false });
      Toast.show("Please fill all the details");
    }
  }

  async componentDidUpdate() {
    // console.log("Checking API hit", "componentDidUpdate method is called");
    // this.setState({isLoading:true})
    if (this.state.update) {
      console.log(
        this.state.update,
        "+++++++++++++++++++++++-------------------"
      );
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        // console.log(get_Data , "Change language")
        if (get_Data === 1) {
          this.setState({ update: false });
          this.setLanguage("en");
          Actions.refresh({ key: "tasks" });
        } else {
          if (get_Data === 2) {
            this.setLanguage("sp");
            this.setState({ update: false });
            Actions.refresh({ key: "tasks" });
          }
        }
      });
    }
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: "#ffffff" }}>
        <View style={styles.titleView}>
          {/* <View style={styles.rowObject}> */}
          <Text style={styles.title}>Mental Health Check-Up</Text>
          {/* </View> */}
        </View>
        <ScrollView keyboardShouldPersistTaps="handled" style={{ flex: 1 }}>
          <View
            style={{
              alignItmes: "center",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Image
              style={{
                width: (deviceWidth * 35) / 100,
                height: (deviceHeight * 20) / 100,
                marginRight: 10,
                borderRadius: 250 / 2,
              }}
              source={require("../Images/logo.png")}
            />
          </View>
          <Text style={styles.textStyles}>{strings.HomeName}</Text>
          <TextInput
            value={this.state.patname}
            style={{
              marginLeft: 10,
              fontSize: 16,
              width: "90%",
              borderBottomWidth: Platform.OS === "ios" ? 1 : 0,
              padding: "3%",
            }}
            onChangeText={(patname) => {
              this.setState({ patname });
            }}
            // ref={input => { this.textInput = input }}
            underlineColorAndroid="black"
          />
          <Text style={styles.textStyles}>{strings.HomeAge}</Text>
          <TextInput
            value={this.state.age}
            keyboardType="numeric"
            style={{
              marginLeft: 10,
              fontSize: 16,
              width: "90%",
              borderBottomWidth: Platform.OS === "ios" ? 1 : 0,
              padding: "3%",
            }}
            onChangeText={(age) => {
              this.setState({ age });
            }}
            underlineColorAndroid="black"
            // ref={input => { this.textInput = input }}

            maxLength={3}
          />
          <Text style={styles.textStyles}>{strings.HomeZipcode}</Text>

          <TextInput
            value={this.state.zipcode}
            keyboardType="numeric"
            style={{
              marginLeft: 10,
              fontSize: 16,
              color: "black",
              width: "90%",
              borderBottomWidth: Platform.OS === "ios" ? 1 : 0,
              padding: "3%",
            }}
            onChangeText={(zipcode) => {
              this.setState({ zipcode });
            }}
            underlineColorAndroid="black"
            // ref={input => { this.textInput = input }}
            maxLength={6}
          />
          {/* <View
            style={{
              alignItems: "center",
              marginTop: "5%",
              backgroundColor: "#4B96FB",
              width: "50%",
              alignSelf: "center",
              padding: "3%",
            }}
          >
            <Button
              onPress={() => this.sendNextpage()}
              title={strings.Next}
              // color="#ffffff"
              style={styles.button}
            />
          </View> */}
          <View
            style={{
              marginVertical: 30,
              paddingHorizontal: 50,
              flex: 0,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <NativeBaseButton
              style={{
                backgroundColor: "#4B96FB",
                height: 50,
                paddingHorizontal: 75,
              }}
              onPress={() => this.sendNextpage()}
            >
              <Text style={{ textTransform: "uppercase", color: "#ffffff" }}>
                {strings.Next}
              </Text>
            </NativeBaseButton>
          </View>
          <Loader loading={this.state.isLoading} />
          <View style={{ marginBottom: "60%" }} />
          {/* <View style={{marginBottom:"30%"}}/> */}
          {/* <View style={{marginBottom:"30%"}}/>
                    <View style={{marginBottom:"30%"}}/> */}
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  inputBox: {
    width: (deviceWidth * 85) / 100,
    height: (deviceHeight * 7) / 100,
    borderRadius: 5,
    // backgroundColor: '#EDF0F7',
    paddingVertical: 15,
  },
  textStyles: {
    fontSize: 25,
    marginTop: "5%",
    color: "#1D4A99",
    marginLeft: "3%",
  },
  button: {
    width: (deviceWidth * 60) / 100,
    height: (deviceHeight * 7) / 100,
    backgroundColor: "#4B96FB",
    borderRadius: 5,
    marginVertical: 12,
    paddingVertical: 12,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
  },
  buttonStyle: {
    color: "red",
    marginTop: 20,
    padding: 20,
    backgroundColor: "green",
  },
  buttonText: {
    fontSize: 18,
    color: "#ffffff",
    textAlign: "center",
  },
  avatarContainer: {
    borderColor: "#9B9B9B",
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },
  avatar: {
    borderRadius: 75,
    width: 120,
    height: 120,
  },
  titleView: {
    width: "100%",
    height: 60,
    // justifyContent: 'center',
    // padding: 50,
    backgroundColor: "#359f06",
    borderBottomWidth: 1,
    borderBottomColor: "#CDD0D4EB",
    alignSelf: "center",
    justifyContent: "center",
  },
  rowObject: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  title: {
    fontSize: 20,
    color: "#000",
    fontWeight: "bold",
    alignSelf: "center",
  },
});
const mapStateToProps = (state) => {
  return {
    ...state,
  };
};

module.exports = connect(mapStateToProps)(Home);
