import React from "react";
import { View, Text, TextInput } from "react-native";
import * as database from "../Database/allSchemas";
import { surveyList } from "./SurveyPicker";

class SurveyTextInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
    };
  }
  updatevalue(value) {
    //debugger;
    this.setState({ value: value });
    var Sur_Value = value.toString();
    var SUR_PKeyID = this.props.selectedQuest;
    let ldata = { SUR_PKeyID, Sur_Value };

    surveyList.push(ldata);

    database.UpdatePatientSurveyMaster_final(surveyList).catch((error) => {
      console.log(error);
    });
  }
  render() {
    return (
      <View>
        <TextInput
          value={this.state.value}
          style={{
            marginLeft: 10,
            fontSize: 16,
            width: "90%",
            borderBottomWidth: 1,
            padding: "2%",
          }}
          onChangeText={(value) => {
            this.setState({ value }), this.updatevalue(value);
          }}
          underlineColorAndroid="black"
        />
      </View>
    );
  }
}
export default SurveyTextInput;
