import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import FlipToggle from "react-native-flip-toggle-button";
import * as database from "../Database/allSchemas";
import { connect } from "react-redux";
import { addScore } from "../actions/score";

export let selectedMDQ = [];
export let selectedADS = [];
export let selectedPTSD = [];

// export let selectedHDST =''

class FlipView extends Component {
  state = {
    value: false,
    MDQScore: 0,
    ADSScore: 0,
    PTSDScore: 0,
  };

  validateScore(value) {
    var lvalue = 0;
    switch (value) {
      case true: {
        lvalue = 1;
        break;
      }
      case false: {
        lvalue = 0;
        break;
      }
    }
    return lvalue;
  }

  updatevalue = (value) => {
    // var Staff_Score = this.validateScore(value)
    // var Score = Staff_Score

    //debugger;
    if (this.props.page == "ValidateMDQ") {
      selectedMDQ = [];

      this.setState({ value: value });
      var Staff_Score = this.validateScore(value);
      var Score = Staff_Score;
      var subval = 0;
      var value = value;
      var MDQ_PkeyId = this.props.selected_value;
      var ldata = { value, MDQ_PkeyId, Staff_Score, Score };
      selectedMDQ.push(ldata);

      database
        .UpdatePatientMoodDisorderMaster(selectedMDQ)
        .catch((error) => console.log(error));

      database.queryAllPatient_MoodDisorder_Schema().then((res) => {
        const patient_mooddisorder = Object.values(res[0]);
        for (i = 0; i < patient_mooddisorder.length; i++) {
          subval = subval + patient_mooddisorder[i].Score;
        }
        console.log("sub vallllll ", subval);
        this.props.score(subval);
        this.setState({ MDQScore: subval }, () => {
          console.log("MDQ Total Score: ", subval);
          let data = {
            Id: 2,
            Title: "Mood Disorder",
            Score: this.state.MDQScore,
          };
          database
            .InsertFinalScores(data)
            .then((data) =>
              console.log(`Depression screening: ${JSON.stringify(data)}`)
            )
            .catch((error) =>
              console.log(`Error in depression screening: ${error}`)
            );
        });
      });

      // var datval = selectedMDQ.filter(item => item.MDQ_PkeyId === MDQ_PkeyId)
      // if (datval.length > 0) {
      //     console.log('datdatdatdatda', JSON.stringify(datval));
      //     console.log('index', datval.index);
      //     selectedMDQ.splice(datval.index, 1);
      // }

      // console.log('datdatdatdatdat', JSON.stringify(selectedMDQ));
      // selectedMDQ.push(ldata);

      // for (i = 0; i < selectedMDQ.length; i++) {
      //     subval = subval + selectedMDQ[i].Score;
      // }
      // console.log('sub vallllll ', subval)

      // console.log(JSON.stringify(selectedMDQ))
    }
    if (this.props.page == "ValidateADS") {
      selectedADS = [];

      this.setState({ value: value });
      var Staff_Score = this.validateScore(value);
      var Score = Staff_Score;
      var subval = 0;
      var value = value;
      var ADS_PKeyId = this.props.selected_value;
      var ldata = { value, ADS_PKeyId, Score, Staff_Score };
      selectedADS.push(ldata);

      database
        .UpdatePatient_AnxietyDisorder_Schema(selectedADS)
        .catch((error) => console.log(error));

      database.queryAllPatient_AnxietyDisorder_Schema().then((res) => {
        const patient_anxiety = Object.values(res[0]);
        for (i = 0; i < patient_anxiety.length; i++) {
          subval = subval + patient_anxiety[i].Score;
        }
        this.setState({ ADSScore: subval }, () => {
          console.log("ADS Total Score: ", subval);
          this.props.score(subval);
          let data = {
            Id: 3,
            Title: "Generalized Anxiety Disorder",
            Score: this.state.ADSScore,
          };
          database
            .InsertFinalScores(data)
            .then((data) =>
              console.log(`Depression screening: ${JSON.stringify(data)}`)
            )
            .catch((error) =>
              console.log(`Error in depression screening: ${error}`)
            );
        });
      });

      // selectedHDST = ldata;
      // console.log(selectedADS)
      // return selectedHDST;
      // var datval = selectedADS.filter(item => item.ADS_PKeyId === ADS_PKeyId)
      // if (datval.length > 0) {
      //     selectedADS.splice(datval.index, 1);
      // }

      // console.log('datdatdatdatdat', JSON.stringify(selectedADS));
      // selectedADS.push(ldata);

      // for (i = 0; i < selectedADS.length; i++) {
      //     subval = subval + selectedADS[i].Score;
      // }
      // console.log('sub vallllll ', subval)

      // console.log(JSON.stringify(selectedADS))
    }
    if (this.props.page == "ValidatePTSD") {
      selectedPTSD = [];

      this.setState({ value: value });
      var Staff_Score = this.validateScore(value);
      var Score = Staff_Score;
      var subval = 0;
      var value = value;
      var MS_PKeyId = this.props.selected_value;
      var ldata = { value, MS_PKeyId, Staff_Score, Score };
      selectedPTSD.push(ldata);
      database
        .UpdatePatient_PTSD_Schema(selectedPTSD)
        .catch((error) => console.log(error));

      database.queryAllPatient_PTSD_Schema().then((res) => {
        const patient_ptsd = Object.values(res[0]);
        for (i = 0; i < patient_ptsd.length; i++) {
          subval = subval + patient_ptsd[i].Score;
        }
        this.setState({ PTSDScore: subval }, () => {
          this.props.score(subval);
          console.log("PTSD Total Score: ", subval);
          let data = {
            Id: 4,
            Title: "P.T.S.D.",
            Score: this.state.PTSDScore,
          };
          database
            .InsertFinalScores(data)
            .then((data) =>
              console.log(`Depression screening: ${JSON.stringify(data)}`)
            )
            .catch((error) =>
              console.log(`Error in depression screening: ${error}`)
            );
        });
      });

      // // selectedHDST = ldata;
      // // console.log(selectedPTSD)
      // // return selectedHDST;
      // var datval = selectedPTSD.filter(item => item.MS_PKeyId === MS_PKeyId)
      // if (datval.length > 0) {
      //     selectedPTSD.splice(datval.index, 1);
      // }

      // console.log('datdatdatdatdat', JSON.stringify(selectedPTSD));
      // selectedPTSD.push(ldata);

      // for (i = 0; i < selectedPTSD.length; i++) {
      //     subval = subval + selectedPTSD[i].Score;
      // }
      // console.log('sub vallllll ', subval)

      // console.log(JSON.stringify(selectedPTSD))
    }
  };

  render() {
    return (
      <View style={{ justifyContent: "center", width: "50%" }}>
        <FlipToggle
          value={this.props.defaultSelected ? true : this.state.value}
          buttonWidth={90}
          buttonHeight={30}
          buttonRadius={50}
          buttonOffColor={"gray"}
          sliderOffColor={"#ffffff"}
          buttonOnColor={"#4B96FB"}
          sliderOnColor={"#fff"}
          labelStyle={{ color: "#fff", fontSize: 10 }}
          onLabel={"Yes"}
          offLabel={"No"}
          onToggle={(value) => {
            this.updatevalue(value);
            this.setState({ value: value });
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    alignSelf: "center",
    color: "red",
  },
  pickerStyle: {
    justifyContent: "center",
    fontSize: 12,
  },
});

const mapStateToProps = (state) => {
  console.log("HSDTool state: ", state);
  return {
    score: state.scoreReducer.score,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    score: (score) => dispatch(addScore(score)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FlipView);
