// THE HANDS® DEPRESSION SCREENING TOOL
import React from "react";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  PixelRatio,
  Button,
  FlatList,
  TextInput,
  ScrollView,
  CheckBox,
  ToastAndroid,
  Linking,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Picker } from "native-base";
import { Actions } from "react-native-router-flux";
import FlipToggle from "react-native-flip-toggle-button";
import * as database from "../Database/allSchemas";
import FlipView, { selectedMDQ } from "./FlipView";
import ChkView, { chkboxDetails } from "./ChkView";
import ScoreView from "./ScoreView";
import { connect } from "react-redux";
import { emptyScore } from "../actions/score";
import strings from "../Language/Language";
import NativeBaseButton from "../components/NativeBaseButton";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const currentPage = "ValidateMDQ";
const flipValue = [];
const finalPicker = [];

class MDQ extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      energy: false,
      energy_Val: "",
      blaming: false,
      blaming_Val: "",
      appetite: false,
      appetite_Val: "",
      asleep: false,
      asleep_Val: "",
      hopeless: false,
      hopeless_Val: "",
      blue: false,
      blue_Val: "",
      interest: false,
      interest_Val: "",
      worthlessness: false,
      worthlessness_Val: "",
      suicide: false,
      suicide_Val: "",
      concentrating: false,
      concentrating_Val: "",
      depression: [],
      confirmation: false,
      final: 0,
    };
  }
  componentDidMount() {
    this.getLanguage();
    this.getData();
  }
  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
        this.setLanguage("en");
      } else {
        strings.setLanguage("sp");
        this.setLanguage("sp");
      }
    });
  }
  async setLanguage(languageCode) {
    console.log(languageCode);
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
      });
    }

    Actions.refresh({ key: "tasks" });
    // getLanguage(languageCode)
  }

  getData() {
    //debugger
    database
      .queryAllPatient_MoodDisorder_Schema()
      .then((res) => {
        const result = Object.values(res[0]);
        this.setState({ depression: result });
      })
      .catch((error) => console.log(error));

    database
      .queryAllPatient_MoodDisorder_Schema_2()
      .then((res) => {
        const result = Object.values(res[0]);
        console.log("resykltttt", result);
        const data = result[0].value;
        if (data != "") {
          this.setState({ final: data });
        } else {
          this.setState({ final: 0 });
        }
      })
      .catch((error) => console.log(error));
    database
      .queryAllPatient_MoodDisorder_Schema_3()
      .then((res) => {
        const result = Object.values(res[0]);
        const ldata = result[0].value;
        if (data != "") {
          this.setState({ confirmation: ldata });
        } else {
          console.log();
          this.setState({ confirmation: false });
        }
      })
      .catch((error) => console.log(error));
  }
  insertIntoDatabase() {
    //debugger
    console.log(selectedMDQ);
    try {
      database.deletePatientDepression_2().catch((error) => console.log(error));
      // database.deletePatientDepression_3().catch((error) => console.log(error));

      database
        .InsertPatientMoodDisorderMaster_2(finalPicker)
        .catch((error) => console.log(error));
      database
        .InsertPatientMoodDisorderMaster_3(flipValue)
        .catch((error) => console.log(error));
    } catch (error) {
      console.log(error);
    } finally {
      this.props.remove();
      // Actions.ADS()
      Actions.Anxiety();
    }
  }
  skip() {
    database.deletePatientDepression_2().catch((error) => console.log(error));
    database.deletePatientDepression_3().catch((error) => console.log(error));

    database
      .InsertPatientMoodDisorderMaster_2(finalPicker)
      .catch((error) => console.log(error));
    database
      .InsertPatientMoodDisorderMaster_3(flipValue)
      .catch((error) => console.log(error));

    Actions.Completed();
  }
  treatedChkbox(item) {
    return (
      <View style={{ flexDirection: "column", marginTop: "5%" }}>
        <View style={{ flexDirection: "row", width: "100%" }}>
          <View style={{ width: "70%" }}>
            <Text style={styles.textStyles}>{item.MDQ_Option}</Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              width: "20%",
              justifyContent: "space-around",
            }}
          >
            <FlipView
              selected_value={item.MDQ_PkeyId}
              page={currentPage}
              defaultSelected={item.value}
            />
          </View>
        </View>
      </View>
    );
  }
  updatevalue(value) {
    //debugger
    this.setState({ confirmation: value });
    var value = value;
    var Id = 1;
    var ldata = { Id, value };
    flipValue.push(ldata);
  }
  updatevaluepicker(value) {
    this.setState({ final: value });
    var value = value;
    var Id = 1;
    var ldata = { Id, value };
    finalPicker.push(ldata);
  }
  render() {
    return (
      <ScrollView style={{ flex: 1 }}>
        <View style={styles.container}>
          <View style={{ marginLeft: "5%" }}>
            <View style={{ marginTop: "3%" }}>
              <Text style={styles.questionStyles}>{strings.MQD}</Text>
            </View>

            <FlatList
              data={this.state.depression}
              renderItem={({ item }) => this.treatedChkbox(item)}
              extraData={(item, index) => index.toString()}
            />
          </View>
          <View
            style={{
              flexDirection: "row",
              width: "100%",
              justifyContent: "center",
              marginTop: "3%",
              alignContent: "center",
              width: "100%",
            }}
          >
            <View style={{ width: "70%", alignItems: "center" }}>
              <Text style={styles.textStyles}>
                {" "}
                If you checked YES to more than one of the above, have several
                of these ever happened during the same period of time?
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                width: "25%",
                justifyContent: "flex-end",
                marginRight: "2%",
              }}
            >
              <FlipToggle
                value={this.state.confirmation}
                buttonWidth={90}
                buttonHeight={30}
                buttonRadius={50}
                buttonOffColor={"gray"}
                sliderOffColor={"#ffffff"}
                buttonOnColor={"#4B96FB"}
                sliderOnColor={"#fff"}
                labelStyle={{ color: "#fff", fontSize: 10 }}
                onLabel={"Yes"}
                offLabel={"No"}
                onToggle={(value) => {
                  this.updatevalue(value);
                  this.setState({ confirmation: value });
                }}
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: "column",
              width: "100%",
              justifyContent: "center",
              margin: "3%",
              alignContent: "center",
            }}
          >
            <View style={{ width: "90%" }}>
              <Text style={styles.textStyles}>{strings.MQD1}</Text>
            </View>
            <View style={{ width: "100%" }}>
              <Picker
                textStyle={{ fontSize: 15 }}
                style={styles.pickerStyle}
                placeholder={strings.selectOptions}
                placeholderStyle={{ fontSize: 15 }}
                selectedValue={this.state.final}
                onValueChange={(value) => this.updatevaluepicker(value)}
              >
                {/* <Picker.Item label='Please select an option...' value={0} /> */}
                <Picker.Item label={strings.problem} value={1} />
                <Picker.Item label={strings.problem1} value={2} />
                <Picker.Item label={strings.problem2} value={3} />
                <Picker.Item label={strings.problem3} value={4} />
              </Picker>
            </View>
          </View>
          {/* <View style={{ alignItems: 'flex-end', flexDirection: 'row', justifyContent: 'space-between', marginRight: '4%', marginLeft: '4%', width: deviceWidth * 90 / 100, height: deviceHeight * 10 / 100 }}>
                        <TouchableOpacity style={styles.button} onPress={() => Actions.HDSTool()}>
                            <Text style={styles.buttonText}>Previous</Text>
                        </TouchableOpacity>
                        <View style={{ width: deviceWidth * 20 / 100, height: deviceHeight * 10 / 100, alignItems: 'center', }}>
                            <ScoreView data={this.props.score} />
                        </View>
                        <TouchableOpacity style={styles.button} onPress={() => this.insertIntoDatabase()}>
                            <Text style={styles.buttonText}>Next</Text>
                        </TouchableOpacity>

                    </View> */}
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-around",
              marginTop: "5%",
              width: "100%",
            }}
          >
            {/* <View
              style={{
                alignItems: "center",
                marginTop: "5%",
                backgroundColor: "#4B96FB",
                width: "30%",
                alignSelf: "center",
                padding: "3%",
              }}
            >
              <Button
                //   onPress={() => Actions.HDSTool()}
                onPress={() => Actions.Bipolar()}
                title={strings.previous}
                color="#ffffff"
                style={styles.button}
              />
            </View> */}
            <NativeBaseButton
              onPress={() => Actions.Bipolar()}
              label={strings.previous}
            />
            <View
              style={{
                width: (deviceWidth * 20) / 100,
                height: (deviceHeight * 10) / 100,
                alignItems: "center",
              }}
            >
              <ScoreView data={this.props.score} />
            </View>
            <NativeBaseButton
              onPress={() => this.insertIntoDatabase()}
              label={strings.Next}
            />
            {/* <View
              style={{
                alignItems: "center",
                marginTop: "5%",
                backgroundColor: "#4B96FB",
                width: "30%",
                alignSelf: "center",
                padding: "3%",
              }}
            >
              <Button
                onPress={() => this.insertIntoDatabase()}
                title={strings.Next}
                color="#ffffff"
                style={styles.button}
              />
            </View> */}
          </View>
          <View style={{ width: "100%" }}>
            <TouchableOpacity
              style={styles.bottomView}
              onPress={() =>
                Linking.openURL(
                  "https://screening.mentalhealthscreening.org/health-e-livin"
                )
              }
            >
              <Text style={styles.bottonText} numberOfLines={2}>
                Source: Health E Livin
              </Text>
            </TouchableOpacity>
          </View>
          {/* {this.props.score > 0 ? 
                    ( */}
          <View>
            {/* <TouchableOpacity style={styles.skipbutton} onPress={() => this.skip()}>
                            <Text style={styles.buttonText}>Skip</Text>
                        </TouchableOpacity> */}
          </View>
          {/*)
                    :null} */}
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
  },
  questionStyles: {
    fontSize: 25,
    color: "#1D4A99",
  },
  skipbutton: {
    width: (deviceWidth * 30) / 100,
    height: (deviceHeight * 6) / 100,
    backgroundColor: "#4B96FB",
    borderRadius: 5,
    marginVertical: 9,
    paddingVertical: 9,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
  },
  inputBox: {
    width: (deviceWidth * 85) / 100,
    height: (deviceHeight * 7) / 100,
    borderRadius: 5,
    // backgroundColor: '#EDF0F7',
    paddingVertical: 15,
  },
  textStyles: {
    fontSize: 18,
    color: "#1D4A99",
  },
  button: {
    width: (deviceWidth * 30) / 100,
    height: (deviceHeight * 6) / 100,
    backgroundColor: "#4B96FB",
    borderRadius: 5,
    marginVertical: 9,
    paddingVertical: 9,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
  },
  _button: {
    width: (deviceWidth * 20) / 100,
    height: (deviceHeight * 4) / 100,
    backgroundColor: "#4B96FB",
    borderRadius: 5,
    marginVertical: 7,
    paddingVertical: 7,
  },
  button_Text: {
    fontSize: 12,
    color: "#ffffff",
    textAlign: "center",
  },
  buttonText: {
    fontSize: 16,
    color: "#ffffff",
    textAlign: "center",
  },
  avatarContainer: {
    borderColor: "#9B9B9B",
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },
  avatar: {
    borderRadius: 75,
    width: 120,
    height: 120,
  },
  bottonText: {
    fontSize: 16,
    color: "#1D4A99",
    fontWeight: "bold",
  },
  bottomView: {
    width: (deviceWidth * 90) / 100,
    height: (deviceHeight * 6) / 100,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
  },
});

const mapStateToProps = (state) => {
  console.log("MDQ state: ", state);
  return {
    score: state.scoreReducer.score,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    remove: () => dispatch(emptyScore()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MDQ);
