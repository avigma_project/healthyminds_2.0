import React, { Component } from "react";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  Image,
  FlatList,
  Keyboard,
  Linking,
  TouchableWithoutFeedback,
  ToastAndroid,
  Platform,
  AlertIOS,
} from "react-native";
import CustomBack from "../../CustomFolder/CustomBack";
import Myprofile from "../../CustomFolder/Myprofile";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Spinner from "react-native-loading-spinner-overlay";
import strings from "../../Language/Language";
import CustomInput from "../../CustomFolder/CustomInput";
import CustomInputMap from "./CustomFolder/CustomInputMap";
import CustomButton from "./CustomFolder/CustomButton";
import ActionsButton from "./CustomFolder/ActionsButton";
import AntDesign from "react-native-vector-icons/AntDesign";
import { Card } from "react-native-elements";
import { fetchcenter } from "../../Api/function";
const flagBrownImg = require("../../Images/Marker.png");
const flagYellowImg = require("../../Images/YellowMarker.png");
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps";
import * as database from "../../Database/allSchemas";
import styles from "./styles";
import { ScrollView } from "react-native-gesture-handler";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const { width, height } = Dimensions.get("window");

const ASPECT_RATIO = width / height;
const LATITUDE = 39.9692921;
const LONGITUDE = -75.2190992;
const LATITUDE_DELTA = 0.0922;
let patinetdistance = null;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;
export let healthdata = [];
export default class FindHealthProvider extends Component {
  state = {
    langugageCode: null,
    token: "",
    result: [],
    isSelected: false,
    zipcode: "",
    centers: 0,
    marker1: false,
    value1: false,
    value2: false,
    value3: false,
    value4: false,
    value5: false,
    show: false,
    modalVisible: false,
    // data: [],
    expertise1: "",
    expertise2: "",
    expertise3: "",
    currentLatitude: "",
    currentLongitude: "",
    filterResult: [],
    filterCenter: [],
    isFilter: false,
    isLoading: false,
    flag: false,
    id: 0,
  };

  componentDidMount() {
    const { navigation } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      this.getToken();

      this.setState({ zipcode: this.props.route.params.zipcode }, () =>
        this.gethealthcenterdata()
      );
      // this.setState({ isLoader: true }, () => this.getLatLong());
    });
    // this.getLanguage();
  }

  componentWillUnmount() {
    this._unsubscribe;
  }

  // getLatLong = async () => {
  //   const lt = await AsyncStorage.getItem("latitude");
  //   const lg = await AsyncStorage.getItem("longitude");
  //   this.setState({
  //     currentLatitude: parseFloat(lt),
  //     currentLongitude: parseFloat(lg),
  //   });
  // };

  getToken = async () => {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      this.setState({ langugageCode: get_Data });
    });
    let token;
    token = await AsyncStorage.getItem("token");
    this.setState({
      token: token,
    });
  };

  gethealthcenterdata = async () => {
    Keyboard.dismiss();
    this.setState({ isSearch: true, isLoading: true });
    var data = JSON.stringify({
      Type: 3,
      Zipcode: this.state.zipcode,
    });
    console.log(data);
    try {
      const res = await fetchcenter(data);
      console.log("gethealthcenterdata", res.data[0]);
      this.setState({
        result: res.data[0],
        centers: res.data[0].length,
        isLoading: false,
      });
      healthdata = result;
    } catch (error) {
      this.setState({ isLoading: false });
    }
  };

  // getZipcode() {
  //   Keyboard.dismiss();
  //   database.queryHealthDetails(this.state.zipcode).then((data) => {
  //     const result = Object.values(data[0]);
  //     if (result.length != 0) {
  //       this.setState({ result: result });
  //       this.setState({ centers: result.length });
  //       healthdata = result;
  //     } else {
  //       alert("No Health Center Found For Current City or Zipcode");
  //     }

  //     for (var i = 0; i <= result.length; i++) {
  //       // console.log(result[i]["HC_Sep"]);
  //       if (result[i]["HC_Sep"] !== undefined && result[i]["HC_Sep"] !== "") {
  //         if (result[i]["HC_Sep"] == 1) {
  //           this.setState({ expertise1: "Depression" });
  //         }
  //         if (result[i]["HC_Sep"] == 2) {
  //           this.setState({ expertise2: "Anxiety Disorder" });
  //         }
  //         if (result[i]["HC_Sep"] == 3) {
  //           this.setState({ expertise3: "BiPolar" });
  //         }
  //       }
  //     }
  //   });
  // }

  getFilterData1(data) {
    const filterdata = this.state.result.filter((c) => c.HC_Sep == data);
    console.log(filterdata);
    if (filterdata.length === 0) {
      // alert(filterdata.length);
      this.setState({
        result: this.state.result,
        centers: this.state.result.length,
        filterResult: [],
        filterCenter: [],
        value1: false,
        value2: false,
        value3: false,
        value4: false,
        value5: false,
      });
      alert("No Health Center Found For Current Filter");
    } else {
      this.setState({
        isFilter: true,
        filterResult: filterdata,
        filterCenter: filterdata[0],
      });
    }
  }

  getFilterData(data) {
    const filterdata = this.state.result.filter((c) => c.HC_Sep == data);
    console.log("minallll", filterdata);
    if (filterdata.length === 0) {
      // alert(filterdata.length);
      this.setState({
        result: this.state.result,
        centers: filterdata.length,
        filterResult: [],
        filterCenter: [],
        value1: false,
        value2: false,
        value3: false,
        value4: false,
        value5: false,
        isFilter: false,
      });
      alert("No Health Center Found For Current Filter");
    } else {
      this.setState({
        isFilter: true,
        filterResult: filterdata,
        filterCenter: filterdata[0],
        centers: filterdata.length,
      });

      if (!this.state.value1 === false && data === 1) {
        this.setState({
          filterResult: this.state.result,
          centers: this.state.result.length,
        });
      }
      if (!this.state.value2 === false && data === 2) {
        this.setState({
          filterResult: this.state.result,
          centers: this.state.result.length,
        });
      }
      if (!this.state.value3 === false && data === 3) {
        this.setState({
          filterResult: this.state.result,
          centers: this.state.result.length,
        });
      }
      if (!this.state.value4 === false && data === 4) {
        this.setState({
          filterResult: this.state.result,
          centers: this.state.result.length,
        });
      }
      if (!this.state.value5 === false && data === 5) {
        this.setState({
          filterResult: this.state.result,
          centers: this.state.result.length,
        });
      }
    }
  }

  openContact(ldata) {
    console.log("ldata", ldata);
    if (ldata != "") {
      Linking.openURL(`tel:${ldata}`);
    }
  }
  async AsynStorageData(value) {
    if (value != null) {
      await AsyncStorage.setItem("webUrl", "0");
      await AsyncStorage.setItem("AsynstorageData", value);

      this.props.navigation.navigate("WebViewList");
    }
  }

  map(data) {
    let scheme = Platform.select({ ios: "maps:0,0?q=", android: "geo:0,0?q=" });
    let latLng = `${data.HC_Lat},${data.HC_Long}`;
    let label = data.HC_Name;
    let url = Platform.select({
      ios: `${scheme}${label}@${latLng}`,
      android: `${scheme}${latLng}(${label})`,
    });
    Linking.openURL(url);
  }

  render_Item = (item, index) => {
    let distance = null;
    if (item.PatientDistance != null) {
      distance = parseFloat(item.PatientDistance);
      distance = distance.toFixed(2);
    }
    return (
      <Card>
        <View
          style={{
            flexDirection: "row",
            // width: "100%",
            // backgroundColor: "pink",
            justifyContent: "center",
            alignItems: "center",
            zIndex: 1,
          }}
        >
          <View
            style={{
              width: "90%",
            }}
          >
            <Text
              style={{
                color: "#060D0B",
                fontWeight: "700",
                fontSize: 18,
                lineHeight: 24,
                width: (deviceWidth * 60) / 100,
                fontFamily: "Quicksand-Bold",
              }}
            >
              {item.HC_Name}
            </Text>
            <Text
              style={{
                // marginTop: "3%",
                color: "#060D0B",
                fontWeight: "400",
                fontSize: 14,
                lineHeight: 18,
                width: (deviceWidth * 55) / 100,
              }}
            >
              {item.HC_Address}
            </Text>
            <View
              style={{
                flexDirection: "row",
                width: "85%",
                // backgroundColor: "pink",
                justifyContent: "space-between",
              }}
            >
              <View>
                <TouchableOpacity
                  onPress={() => this.openContact(item.HC_Contactnum)}
                >
                  <Text
                    style={{
                      marginTop: "3%",
                      marginBottom: "3%",
                      color: "#060D0B",
                      fontWeight: "400",
                      fontSize: 12,
                      lineHeight: 16.34,
                    }}
                  >
                    {item.HC_Contactnum}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.AsynStorageData(item.HC_Web_URL)}
                >
                  <Text style={styles.direction}>{strings.visitwebsite}</Text>
                </TouchableOpacity>
              </View>
              <View>
                {distance != null ? (
                  <View
                    style={{
                      left: Platform.OS === "ios" ? 5 : -35,
                      justifyContent: "center",
                      alignItems: "center",
                      alignSelf: "center",
                    }}
                  >
                    <Text style={styles.distance}>
                      {distance} {strings.miles}
                    </Text>
                    <TouchableOpacity onPress={() => this.map(item)}>
                      <Text style={styles.direction}>{strings.direction}</Text>
                    </TouchableOpacity>
                  </View>
                ) : (
                  // null
                  <View
                    style={{
                      left: Platform.OS === "ios" ? 15 : -5,
                      justifyContent: "center",
                      // alignItems: "center",
                      // alignSelf: "center",
                    }}
                  >
                    <Text style={styles.distance}>0.00 {strings.miles}</Text>
                    <TouchableOpacity onPress={() => this.map(item)}>
                      <Text style={styles.direction}>{strings.direction}</Text>
                    </TouchableOpacity>
                  </View>
                )}
              </View>
            </View>
            <Text
              style={{
                marginTop: "3%",
                color: "#060D0B",
                fontWeight: "700",
                fontSize: 14,
                lineHeight: 20,
              }}
            >
              {strings.expertise}: {item.Specilization}
            </Text>
          </View>
        </View>
      </Card>
    );
  };

  marker() {
    if (this.state.isFilter) {
      // alert(this.state.isFilter);
      return this.state.filterResult.map((item, index) => (
        <Marker
          key={index}
          coordinate={{
            latitude: item.HC_Lat ? parseFloat(item.HC_Lat) : 0,
            longitude: item.HC_Long ? parseFloat(item.HC_Long) : 0,
          }}
          tracksViewChanges={false}
          image={this.state.id === item.HC_Pkey ? flagYellowImg : flagBrownImg}
          onPress={() => {
            // alert("hiii");
            this.setState({
              show: true,
              data: item,
              id: item.HC_Pkey,
            });
            // this.state.flag = true;
            // this.render_Item(item, index);
          }}
        />
      ));
    } else {
      console.log("isFilter", this.state.isFilter);
      console.log("minalllresult", this.state.result);

      return this.state.result.map((item, index) => {
        return (
          <Marker
            key={item.HC_Pkey}
            coordinate={{
              latitude: item.HC_Lat ? parseFloat(item.HC_Lat) : 0,
              longitude: item.HC_Long ? parseFloat(item.HC_Long) : 0,
            }}
            tracksViewChanges={false}
            image={
              this.state.id === item.HC_Pkey ? flagYellowImg : flagBrownImg
            }
            onPress={() => {
              // alert("hiii");
              this.setState({
                show: true,
                data: item,
                id: item.HC_Pkey,
              });
              // item.MFlag = true;
              // this.render_Item(item, index);
            }}
          />
        );
      });
    }
  }

  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#fff",
          // backgroundColor: "#E5E5E5"
        }}
      >
        <Spinner visible={this.state.isLoading} />
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            marginHorizontal: 16,
            // backgroundColor: "pink",
            marginTop: 6,
          }}
        >
          <View style={{ justifyContent: "center" }}>
            <CustomInputMap
              // placeholder={strings.searchcode}
              placeholderTextColor="#828282"
              value={this.state.zipcode}
              onChangeText={(zipcode) => {
                this.setState({ zipcode });
              }}
            />
            <TouchableOpacity
              style={{ position: "absolute", right: 6 }}
              onPress={() => this.gethealthcenterdata()}
            >
              <AntDesign name="search1" size={25} color="#377867" />
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: "40%",
              // backgroundColor: "red",
              // flexDirection: "row",
              // justifyContent: "flex-end",
              alignItems: "flex-end",
            }}
          >
            {this.state.token ? (
              <View>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("Search", {
                      screen: "Profile",
                    })
                  }
                  // onPress={() => this.props.navigation.navigate("Profile")}
                  style={{
                    // backgroundColor: "pink",
                    justifyContent: "center",
                    alignItem: "center",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 16,
                      color: "#377867",
                      fontWeight: "700",
                      fontFamily: "Quicksand-Bold",
                    }}
                  >
                    {strings.myprofile}
                  </Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View style={{}}>
                <TouchableOpacity
                  style={{ justifyContent: "center", alignItem: "center" }}
                  onPress={() =>
                    this.props.navigation.navigate("Search", {
                      screen: "Login",
                    })
                  }
                >
                  <Text
                    style={{
                      fontSize: 13,
                      color: "#377867",
                      fontWeight: "700",
                      width: "100%",
                      fontFamily: "Quicksand-Bold",
                    }}
                  >
                    {strings.login}/{strings.signup}
                  </Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        </View>
        <View style={{ left: 0 }}>
          <View
            style={{
              justifyContent: "center",
              // left: Platform.OS === "ios" ? 20 : 5,
              marginTop: 10,
              marginHorizontal: 16,
            }}
          >
            <Text
              style={{
                // fontFamily: "Quicksand-Bold",
                fontSize: 16,
                fontWeight: "400",
                lineHeight: 22,
              }}
            >
              {strings.showing}{" "}
              <Text style={{ fontWeight: "bold" }}>{this.state.centers} </Text>
              {strings.nearcenter} {this.props.route.params.zipcode}
            </Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              // marginTop: "-5%",
              // marginBottom: "-5%",
              marginHorizontal: 16,
              paddingVertical: "4%",
              // right: 10,
            }}
          >
            <ScrollView
              showsHorizontalScrollIndicator={false}
              horizontal={true}
            >
              <CustomButton
                title={strings.depres}
                redius={40}
                color="#A1683A"
                backgroundColor={this.state.value1 ? "#EFD2CB" : "#F2F2F2"}
                onPress={() => {
                  this.getFilterData(1);
                  this.setState({
                    value1: !this.state.value1,
                    value2: false,
                    value3: false,
                    value4: false,
                    value5: false,
                  });
                }}
              />

              <View>
                <TouchableOpacity
                  onPress={() => {
                    this.getFilterData(2);
                    this.setState({
                      value2: !this.state.value2,
                      value1: false,
                      value3: false,
                      value4: false,
                      value5: false,
                    });
                  }}
                  style={{
                    backgroundColor: this.state.value2 ? "#EFD2CB" : "#F2F2F2",
                    padding: 10,
                    marginRight: 4,
                    justifyContent: "center",
                    alignItems: "center",
                    borderRadius: 40,
                  }}
                >
                  <Text
                    style={{
                      color: "#A1683A",
                      fontSize: 14,
                      fontWeight: "500",
                      lineHeight: 17.5,
                      // width: "90%",
                      // // left: this.state.langugageCode != 1 ? 10 : 0,
                      // alignSelf: "center",
                      fontFamily: "Quicksand-Bold",
                      // // borderWidth: 1,
                      // left: 5,
                    }}
                  >
                    {strings.anxt}
                  </Text>
                </TouchableOpacity>
              </View>

              <CustomButton
                title={strings.biop}
                redius={40}
                color="#A1683A"
                backgroundColor={this.state.value3 ? "#EFD2CB" : "#F2F2F2"}
                onPress={() => {
                  this.getFilterData(3);
                  this.setState({
                    value3: !this.state.value3,
                    value1: false,
                    value2: false,
                    value4: false,
                    value5: false,
                  });
                }}
              />
              <CustomButton
                title={strings.ptsds}
                redius={40}
                color="#A1683A"
                backgroundColor={this.state.value4 ? "#EFD2CB" : "#F2F2F2"}
                onPress={() => {
                  this.getFilterData(4);
                  this.setState({
                    value4: !this.state.value4,
                    value1: false,
                    value2: false,
                    value3: false,
                    value5: false,
                  });
                }}
              />
              <CustomButton
                title={strings.subsuse1}
                redius={40}
                color="#A1683A"
                backgroundColor={this.state.value5 ? "#EFD2CB" : "#F2F2F2"}
                onPress={() => {
                  this.getFilterData(5);
                  this.setState({
                    value5: !this.state.value5,
                    value1: false,
                    value2: false,
                    value3: false,
                    value4: false,
                  });
                }}
              />
            </ScrollView>
          </View>

          {this.state.isSelected ? (
            <View
              style={{
                borderColor: "lightgray",
                borderWidth: 0.5,
              }}
            >
              <ScrollView>
                <View style={{}}>
                  <FlatList
                    style={{
                      marginBottom: Platform.OS === "ios" ? "70%" : "100%",
                    }}
                    data={
                      this.state.isFilter
                        ? this.state.filterResult
                        : this.state.result
                    }
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item, index }) =>
                      this.render_Item(item, index)
                    }
                  />
                  <View
                    style={{ position: "absolute", right: 15, marginTop: 15 }}
                  >
                    <TouchableOpacity
                      onPress={() => this.setState({ isSelected: false })}
                      style={{
                        backgroundColor: "#A1683A",
                        padding: 10,
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <Text
                        style={{
                          color: "#FFFFFF",
                          fontSize: 14,
                          fontWeight: "700",
                          lineHeight: 20,
                          fontFamily: "Quicksand-Bold",
                          // left: this.state.langugageCode
                          //   ? Platform.OS === "ios"
                          //     ? null
                          //     : 5
                          //   : null,
                          left: this.state.langugageCode ? 0 : 2,
                          alignSelf: "center",
                        }}
                      >
                        {strings.mapview}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </ScrollView>
            </View>
          ) : (
            <View>
              <MapView
                // provider={PROVIDER_GOOGLE}
                style={styles.map}
                initialRegion={{
                  // latitude: this.state.currentLatitude,
                  // longitude: this.state.currentLongitude,
                  latitude: LATITUDE,
                  longitude: LONGITUDE,
                  latitudeDelta: LATITUDE_DELTA,
                  longitudeDelta: LONGITUDE_DELTA,
                }}
                // onPress={() =>
                //   this.setState({
                //     show: !this.state.show,
                //   })
                // }
              >
                {this.marker()}
              </MapView>
              <View>
                <TouchableOpacity
                  onPress={() => this.setState({ isSelected: true })}
                  style={{
                    backgroundColor: "#A1683A",
                    padding: 10,
                    justifyContent: "center",
                    alignItems: "center",
                    position: "absolute",
                    right: 15,
                    marginTop: 15,
                  }}
                >
                  <Text
                    style={{
                      color: "#FFFFFF",
                      fontSize: 14,
                      fontWeight: "700",
                      lineHeight: 20,
                      left: this.state.langugageCode ? 0 : 2,
                      alignSelf: "center",
                      fontFamily: "Quicksand-Bold",
                    }}
                  >
                    {strings.listview}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          )}

          {this.state.show && (
            <View
              style={{
                top:
                  Platform.OS == "ios"
                    ? deviceHeight - 500
                    : deviceHeight - 450,
              }}
            >
              {/* <Card
                style={{
                  height: (deviceHeight * 100) / 100,
                }}
              > */}
              <ActionsButton
                item={
                  this.state.isFilter
                    ? this.state.filterCenter
                    : this.state.data
                }
                showbutton={() =>
                  this.setState({
                    show: !this.state.show,
                  })
                }
                show={true}
                distance={patinetdistance}
                onPress={() => {
                  this.props.navigation.navigate("WebViewList");
                }}
                expert1={this.state.expertise1}
                expert2={this.state.expertise2}
                expert3={this.state.expertise3}
              />
              {/* </Card> */}
            </View>
          )}
        </View>
      </SafeAreaView>
    );
  }
}
