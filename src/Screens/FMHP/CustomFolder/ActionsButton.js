import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  Linking,
} from "react-native";
import { Card } from "react-native-elements";
import * as database from "./../../../Database/allSchemas";
import { healthdata } from "../FindHealthProvider";
import AsyncStorage from "@react-native-async-storage/async-storage";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import strings from "../../../Language/Language";
import Entypo from "react-native-vector-icons/Entypo";

export default class ActionsButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
    };
  }
  openContact(ldata) {
    console.log("ldata", ldata);
    if (ldata != "") {
      Linking.openURL(`tel:${ldata}`);
    }
  }
  async AsynStorageData(value) {
    if (value != null) {
      await AsyncStorage.setItem("webUrl", "0");
      await AsyncStorage.setItem("AsynstorageData", value);
      this.props.onPress();
    }
  }
  map(data) {
    let scheme = Platform.select({ ios: "maps:0,0?q=", android: "geo:0,0?q=" });
    let latLng = `${data.HC_Lat},${data.HC_Long}`;
    let label = data.HC_Name;
    let url = Platform.select({
      ios: `${scheme}${label}@${latLng}`,
      android: `${scheme}${latLng}(${label})`,
    });
    console.log("*****", url);
    Linking.openURL(url);
  }

  render() {
    const { item, show } = this.props;
    let distance = null;
    if (item.PatientDistance != null) {
      distance = parseFloat(item.PatientDistance);
      distance = distance.toFixed(2);
    }
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "center",
          width: "100%",
        }}
      >
        <TouchableOpacity
          style={{ position: "absolute", right: 15, zIndex: 1, paddingTop: 9 }}
          onPress={this.props.showbutton}
        >
          <Entypo name="cross" size={25} color="#377867" />
        </TouchableOpacity>
        <View
          style={{
            backgroundColor: "#fff",
            width: "95%",
            paddingHorizontal: 25,
            paddingVertical: 10,
          }}
        >
          <View
            style={{
              flexDirection: "row",
              // backgroundColor: "pink",
              justifyContent: "space-between",
            }}
          >
            <Text
              style={{
                color: "#060D0B",
                fontWeight: "700",
                fontSize: 18,
                lineHeight: 24,
                width: (deviceWidth * 60) / 100,
                fontFamily: "Quicksand-Bold",
              }}
            >
              {item.HC_Name}
            </Text>
          </View>
          <Text
            style={{
              // marginTop: "3%",
              color: "#060D0B",
              fontWeight: "400",
              fontSize: 14,
              lineHeight: 20,
              // width: (deviceWidth * 55) / 100,
            }}
          >
            {item.HC_Address}
          </Text>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              // backgroundColor: "pink",
            }}
          >
            <View>
              <TouchableOpacity
                // style={{ backgroundColor: "red" }}
                onPress={() => this.openContact(item.HC_Contactnum)}
              >
                <Text
                  style={{
                    color: "#060D0B",
                    fontWeight: "400",
                    fontSize: 12,
                  }}
                >
                  {item.HC_Contactnum}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.AsynStorageData(item.HC_Web_URL)}
              >
                <Text style={styles.direction}>{strings.visitwebsite}</Text>
              </TouchableOpacity>
            </View>
            <View>
              {distance != null ? (
                <View
                  style={{
                    // left: Platform.OS === "ios" ? 5 : -35,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Text style={styles.distance}>
                    {distance} {strings.miles}
                  </Text>
                  <TouchableOpacity onPress={() => this.map(item)}>
                    <Text style={styles.direction}>{strings.direction}</Text>
                  </TouchableOpacity>
                </View>
              ) : (
                <View
                  style={{
                    left: Platform.OS === "ios" ? 15 : -5,
                    justifyContent: "center",
                    // alignItems: "center",
                    // alignSelf: "center",
                  }}
                >
                  <Text style={styles.distance}>0.00 {strings.miles}</Text>
                  <TouchableOpacity onPress={() => this.map(item)}>
                    <Text style={styles.direction}>{strings.direction}</Text>
                  </TouchableOpacity>
                </View>
              )}
            </View>
          </View>
          <Text
            style={{
              marginTop: "3%",
              color: "#060D0B",
              fontWeight: "700",
              fontSize: 14,
              lineHeight: 20,
              fontFamily: "Quicksand-Bold",
            }}
          >
            {strings.expertise}: {item.Specilization}
            {/* //Anxiety Disorder, Bipolar */}
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  distance: {
    color: "#060D0B",
    fontSize: 14,
    lineHeight: 18,
    fontStyle: "italic",
  },
  direction: {
    color: "#A1683A",
    fontWeight: "700",
    fontSize: 16,
    lineHeight: 22,
  },
});
