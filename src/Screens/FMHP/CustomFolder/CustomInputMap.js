import React, { Component } from "react";
import { Text, View, TextInput, Dimensions } from "react-native";
const deviceWidth = Dimensions.get("window").width;
export default class CustomInputMap extends Component {
  render() {
    const {
      placeholder,
      heading,
      placeholderTextColor,
      value,
      onChangeText,
      keyboardType,
      onEndEditing,
      secureTextEntry,
      defaultValue,
      onSubmitEditing,
      placeholderStyle,
    } = this.props;
    return (
      <View
        style={{
          // marginTop: Platform.OS === "ios" ? 5 : 15,
          height: Platform.OS === "ios" ? 40 : 40,
          justifyContent: "center",
          borderWidth: 1,
          borderColor: "gray",
          width: (deviceWidth * 52) / 100,
          // alignSelf: "center",
          // right: 80,
          borderRadius: 3,
        }}
      >
        <TextInput
          onSubmitEditing={onSubmitEditing}
          placeholder={placeholder}
          value={value}
          defaultValue={defaultValue}
          keyboardType={keyboardType}
          placeholderTextColor={placeholderTextColor}
          onChangeText={onChangeText}
          onEndEditing={onEndEditing}
          secureTextEntry={secureTextEntry}
          placeholderStyle={placeholderStyle}
          style={{ paddingLeft: 10, color: "#000" }}
        />
      </View>
    );
  }
}
