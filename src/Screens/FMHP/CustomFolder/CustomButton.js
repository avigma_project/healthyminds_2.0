import React, { Component } from "react";
import { Text, View, TouchableOpacity } from "react-native";

export default class CustomButton extends Component {
  render() {
    return (
      <View
        style={{
          marginRight: 10,
          // backgroundColor: "red",
          // width: "10%",
          alignItems: "center",
        }}
      >
        <TouchableOpacity
          onPress={this.props.onPress}
          style={{
            backgroundColor: this.props.backgroundColor,
            padding: 10,
            justifyContent: "center",
            alignItems: "center",
            borderRadius: this.props.redius,
          }}
        >
          <Text
            style={{
              color: this.props.color,
              fontSize: 14,
              fontWeight: "500",
              lineHeight: 17.5,
              fontFamily: "Quicksand-Bold",
            }}
          >
            {this.props.title}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
