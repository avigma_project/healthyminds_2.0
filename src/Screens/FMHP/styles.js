import { StyleSheet, Dimensions } from "react-native";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
    width: "100%",
    height: (deviceHeight * 100) / 100,
    // marginTop: "-3%",
  },

  distance: {
    color: "#060D0B",
    fontSize: 14,
    lineHeight: 18,
    fontStyle: "italic",
  },
  direction: {
    color: "#A1683A",
    fontWeight: "700",
    fontSize: 16,
    lineHeight: 22,
  },
  card: {
    position: "absolute",
    bottom: 0,
  },
});
export default styles;
