import React, { Component } from "react";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  Image,
  FlatList,
  Keyboard,
  Linking,
  BackHandler,
  ToastAndroid,
  Platform,
  ScrollView,
} from "react-native";
import CustomBack from "../../CustomFolder/CustomBack";
import Myprofile from "../../CustomFolder/Myprofile";
import AsyncStorage from "@react-native-async-storage/async-storage";
import strings from "../../Language/Language";
import CustomInput from "../../CustomFolder/CustomInput";
import CustomInputMap from "./CustomFolder/CustomInputMap";
import AntDesign from "react-native-vector-icons/AntDesign";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import * as database from "../../Database/allSchemas";
import { userprofile, getdata } from "../../Api/function";
import Spinner from "react-native-loading-spinner-overlay";

export default class Welcome extends Component {
  state = {
    langugageCode: null,
    Ltype: 1,
    textValue: "Spanish",
    isLoading: false,
    token: "",
    zipcode: null,
  };
  // componentDidMount() {
  //   const { navigation } = this.props;
  //   this._unsubscribe = navigation.addListener("focus", () => {
  //     this.getToken();
  //   });
  // }
  componentDidMount() {
    const { navigation, route } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      // this.setState({ isLoader: true }, () => this.getLocation());

      this.getToken();
    });
    this.getLanguage();
  }
  backAction = () => {
    return true;
  };

  componentWillUnmount() {
    this._unsubscribe;
  }

  onsubmit(zipcode) {
    if (zipcode != null && zipcode != "") {
      this.props.navigation.navigate("FindHealthProvider", { zipcode });
    } else {
      if (Platform.OS === "android") {
        ToastAndroid.show("Please Enter City or Zipcode", ToastAndroid.SHORT);
      } else {
        alert("Please Enter City or Zipcode");
      }
    }
  }
  componentDidMount() {
    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      this.backAction
    );
    const { navigation, route } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      this.getToken();
      this.getLanguage();
    });
  }

  componentWillUnmount() {
    this._unsubscribe;
  }

  async GetMasterDetails() {
    console.log("ltype data", this.state.Ltype);
    this.setState({ isLoading: true });
    var data = JSON.stringify({
      Ltype: this.state.Ltype,
    });
    //debugger;
    try {
      getdata(data).then((response) => {
        console.log("response ---dfiffff", response);
        this.setState({ isLoading: false });
        database.deletePatientMaster().catch((error) => console.log(error));
        // database.deleteFinal_Scores_Schema().catch((Error) => {
        //   console.log(Error);
        // });
        database.deletePatientDepression().catch((error) => console.log(error));
        database
          .deletePatientDepression2()
          .catch((error) => console.log(error));
        database
          .deletePatientDepression3()
          .catch((error) => console.log(error));
        database
          .deletePatientDepression4()
          .catch((error) => console.log(error));
        database
          .deleteSubstanceUseMaster()
          .catch((error) => console.log(error));
        database
          .deleteSubstanceOptionMaster()
          .catch((error) => console.log(error));

        // database
        //   .deleteSubstanceHistoryMaster()
        //   .catch((error) => console.log(error));
        database
          .deletePatient_Treated_Schema()
          .catch((error) => console.log(error));
        database.deletePatientSurveyMaster_Schema().catch((error) => {
          console.log(error);
        });
        database.deletePatientHealthCenters_Schema().catch((error) => {
          console.log(error);
        });
        database.deletePatientTechniques_List_Schema_Schema().catch((error) => {
          console.log(error);
        });

        this.setState({ isLoading: false });

        if (response.data != "") {
          if (response.data[0] != "") {
            database
              .InsertPatientDepressionMaster(response.data[0])
              .catch((error) => console.log(error));

            // ToastAndroid.showWithGravity(
            //     "InsertPatientDepressionMaster",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
            console.log(response, "Spanish");
          }
          if (response.data[1] != "") {
            database
              .InsertPatientMoodDisorderMaster(response.data[1])
              .catch((error) => console.log(error));
            // ToastAndroid.showWithGravity(
            //     "InsertPatientMoodDisorderMaster",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
          }
          if (response.data[2] != "") {
            database
              .InsertPatientAnxietyDisorderMaster(response.data[2])
              .catch((error) => console.log(error));
            // ToastAndroid.showWithGravity(
            //     "InsertPatientAnxietyDisorderMaster",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
          }
          if (response.data[3] != "") {
            database
              .InsertPatientPTSDMaster(response.data[3])
              .catch((error) => console.log(error));
            // ToastAndroid.showWithGravity(
            //     "InsertPatientPTSDMaster",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
          }
          if (response.data[4] != "") {
            // console.log('repson 3', response.data[4]);
            database
              .InsertPatientTreatedMaster(response.data[4])
              .catch((error) => console.log(error));
            // ToastAndroid.showWithGravity(
            //     "InsertPatientTreatedMaster",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
          }
          if (response.data[5][0] != "") {
            // console.log('repson 3', response.data[5][0]);
            database
              .InsertPatientSurveyMaster(response.data[5][0])
              .catch((error) => console.log(error));
            // ToastAndroid.showWithGravity(
            //     "InsertPatientTreatedMaster",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
            // console.log("Ashok final response", response.data[5][0])
          }
          if (response.data[6][0] != "") {
            // console.log('repson 3', response.data[6][0]);
            database
              .InsertHealthCenterDetails(response.data[6][0])
              .catch((error) => console.log(error));
            // ToastAndroid.showWithGravity(
            //     "InsertHealthCenterDetails",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
            // console.log("Ashok  InsertHealthCenterDetails", response.data[6][0])
          }

          if (response.data[7][0] != "") {
            // console.log('repson 3', response.data[7][0]);
            database
              .InsertTechniques_List_SchemaDetails(response.data[7][0])
              .catch((error) => console.log(error));
            // ToastAndroid.showWithGravity(
            //     "InsertTechniques_List_SchemaDetails",
            //     ToastAndroid.SHORT,
            //     ToastAndroid.BOTTOM
            // )
            // console.log("Ashok  InsertTechniques_List_SchemaDetails", response.data[7][0])
          }
          if (response.data[8] != "") {
            database
              .InsertSubstanceOptionMaster(response.data[8])
              .catch((error) => console.log(error));
          }
          if (response.data[9] != "") {
            database
              .InsertSubstanceUseMaster(response.data[9])
              .catch((error) => console.log(error));
          }
          // if (response.data[10] != "") {
          //   // console.log(
          //   //   response.data[10],
          //   //   "response.data[10]auhashdiuhduihudhuihuihauidh"
          //   // );
          //   database
          //     .InsertPatientMaster(response.data[10])
          //     .catch((error) => console.log(error));
          // }
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      // console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
        this.setLanguage("en");
      } else {
        if (get_Data == "2") {
          strings.setLanguage("sp");
          this.setLanguage("sp");
        } else {
          strings.setLanguage("en");
          this.setLanguage("en");
        }
      }
    });
  }

  async setLanguage(languageCode) {
    // console.log(languageCode)
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        // console.log("refresh", get_Data);
        this.setState({
          Ltype: 1,
          language: false,
        });
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
        this.setState({
          Ltype: 2,
          language: false,
        });
      });
    }
    this.GetMasterDetails();
    // getLanguage(languageCode)
  }

  async componentDidUpdate() {
    // this.getLanguage()
    if (this.state.update === true) {
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log(get_Data, "Change language");
        if (get_Data === 1) {
          this.setState({ update: false });
          this.GetMasterDetails();
        } else {
          if (get_Data === 2) {
            this.setState({ update: false });
            this.GetMasterDetails();
          }
        }
      });
    }
  }

  getUserData = async (token) => {
    this.setState({ isLoading: true });
    var data = JSON.stringify({
      Type: 2,
    });
    try {
      const res = await userprofile(data, token);
      console.log(res, "resssss");
      this.setState({
        firstname: res[0][0].User_Name,
        isLoading: false,
      });
    } catch (error) {
      let message = "";
      if (error.response) {
        this.setState({ isLoading: false });
      } else {
        message = "";
      }
      console.log({ message });
    }
  };

  getToken = async () => {
    this.setState({ isLoading: true });
    let token;
    try {
      token = await AsyncStorage.getItem("token");
      if (token) {
        this.getUserData(token);
        this.GetMasterDetails();
        this.setState({ token: token });
      }
      // else {
      //   this.setState({ token: "", isLoading: false });
      //   database.deletePatientMaster().catch((error) => console.log(error));
      //   database.deleteFinal_Scores_Schema().catch((Error) => {
      //     console.log(Error);
      //   });
      //   database.deletePatientDepression().catch((error) => console.log(error));
      //   database
      //     .deletePatientDepression2()
      //     .catch((error) => console.log(error));
      //   database
      //     .deletePatientDepression3()
      //     .catch((error) => console.log(error));
      //   database
      //     .deletePatientDepression4()
      //     .catch((error) => console.log(error));
      //   database
      //     .deleteSubstanceUseMaster()
      //     .catch((error) => console.log(error));
      //   database
      //     .deleteSubstanceOptionMaster()
      //     .catch((error) => console.log(error));
      //   database
      //     .deletePatient_Treated_Schema()
      //     .catch((error) => console.log(error));
      //   database.deletePatientSurveyMaster_Schema().catch((error) => {
      //     console.log(error);
      //   });
      //   database.deletePatientHealthCenters_Schema().catch((error) => {
      //     console.log(error);
      //   });
      //   database.deletePatientTechniques_List_Schema_Schema().catch((error) => {
      //     console.log(error);
      //   });
      //   console.log("no token found");
      // }
      this.setState({ isLoading: false });
    } catch (e) {
      console.log(e);
    }
  };
  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>
        <ScrollView
          contentContainerStyle={{ marginHorizontal: 16 }}
          keyboardShouldPersistTaps={"handled"}
        >
          <Spinner visible={this.state.isLoading} />
          {this.state.token ? (
            <View style={{ marginTop: 0 }}>
              <Myprofile
                navigation={this.props.navigation}
                onPress={() =>
                  this.props.navigation.navigate("Search", {
                    screen: "Profile",
                  })
                }
              />
            </View>
          ) : (
            <View style={{ marginTop: 0 }}>
              <CustomBack
                navigation={this.props.navigation}
                onPress={() =>
                  this.props.navigation.navigate("Search", {
                    screen: "Login",
                  })
                }
              />
            </View>
          )}
          <View style={{ marginTop: 10 }}>
            <Text
              style={{
                fontSize: 20,
                lineHeight: 26,
                fontWeight: "700",
                fontFamily: "Quicksand-Bold",
                color: "#060D0B",
              }}
            >
              {strings.Findmhs}
            </Text>
          </View>
          <View style={{ justifyContent: "center", marginTop: 10 }}>
            <CustomInput
              searchtext={true}
              placeholder={strings.searchcode}
              placeholderTextColor="#828282"
              onChangeText={(zipcode) => {
                this.setState({
                  zipcode,
                });
              }}
            />
            <TouchableOpacity
              style={{
                position: "absolute",
                right: 20,
              }}
              onPress={() => this.onsubmit(this.state.zipcode)}
            >
              <AntDesign name="search1" size={25} color="#377867" />
            </TouchableOpacity>
          </View>
          <View
            style={{
              // marginTop: 24,
              alignItems: "center",
              // backgroundColor: "pink",
              // height: "0%",
              // justifyContent: "center",
              marginTop: 24,
              marginBottom: 55,
            }}
          >
            <Image
              resizeMode="stretch"
              source={require("../../Images/SearchP.png")}
              style={{
                width: "102%",
                // height: "110%",
                // backgroundColor: "pink",
              }}
            />
          </View>
          <View
            style={{
              width: "100%",
              // marginTop: 32,
            }}
          >
            <Text
              style={{
                fontSize: 16,
                fontWeight: "100",
                lineHeight: 22,
                fontFamily: "verdana",
                color: "#060D0B",
              }}
            >
              {strings.SearchWel}
            </Text>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
