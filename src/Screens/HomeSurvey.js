import React from "react";
import {
  View,
  Text,
  TextInput,
  Dimensions,
  StyleSheet,
  PixelRatio,
  Button,
  Image,
  ScrollView,
  SafeAreaView,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Actions } from "react-native-router-flux";
import Toast from "react-native-simple-toast";
import { connect } from "react-redux";
// import Database from '../Database/allSchemas';
import * as database from "../Database/allSchemas";
import strings from "../Language/Language";
import NativeBaseButton from "../components/NativeBaseButton";
import { Platform } from "react-native";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default class HomeSurvey extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      patname: "",
      email: "",
      contact: "",
      update: false,
    };
  }
  // componentWillMount(){
  //     this.getData();
  // }
  componentDidMount() {
    this.getLanguage();
    this.getData();
  }

  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
      } else {
        strings.setLanguage("sp");
      }
      this.getData();
      Actions.refresh();
    });
  }

  async setLanguage(languageCode) {
    console.log({ languageCode });
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        // console.log("refresh", get_Data);

        this.setState({ Ltype: 1 });
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
        this.setState({ Ltype: 2 });
      });
    }
    this.getData();
    Actions.refresh({ key: "tasks" });
    // getLanguage(languageCode)
  }

  async componentDidUpdate() {
    // console.log("Checking API hit", "componentDidUpdate method is called");
    // await AsyncStorage.getItem("refresh", (err, get_Data) => {
    //   console.log("AsyncStorage get_Data 23", get_Data);
    //   // this.GetMasterDetails();
    // });
    // this.getLanguage();
    // console.log("sssds", this.state.update);
    // if (this.state.update === true) {
    //   await AsyncStorage.getItem("refresh", (err, get_Data) => {
    //     console.log(get_Data, "Change language");
    //     if (get_Data === 1) {
    //       this.setState({ update: false });
    //       this.getLanguage();
    //     } else {
    //       if (get_Data === 2) {
    //         this.setState({ update: false });
    //         this.getLanguage();
    //       }
    //     }
    //   });
    // }
  }

  getData() {
    //debugger;
    database
      .queryBaseSurveyDetails()
      .then((res) => {
        const result = Object.values(res[0]);
        this.setState({ patname: result[0].Name, update: true });
      })
      .catch((error) => console.log(error));
  }
  sendNextpage() {
    //debugger;
    database
      .deletePatientSurveyBaseMaster_Schema()
      .catch((error) => console.log("delete", error));
    if (this.state.patname != "") {
      // Actions.PatientDetails()
      let data = {
        Survey_Cust_PKeyID: 1,
        Survey_Cust_Name: this.state.patname,
        Survey_Cust_Email: null,
        Survey_Cust_Mobile_Number: null,
      };
      console.log(data);
      database
        .InsertBaseSurveyDetails(data)
        .catch((error) => console.log("insert", error));
      Actions.Survey({ patname: this.state.patname });
    } else {
      Toast.show("Please fill all the details");
    }
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <ScrollView
          keyboardShouldPersistTaps="handled"
          style={{ marginTop: "5%" }}
        >
          <View
            style={{
              flex: 1,
              alignItmes: "center",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Image
              style={{
                width: (deviceWidth * 35) / 100,
                height: (deviceHeight * 20) / 100,
                marginRight: 10,
                borderRadius: 250 / 2,
              }}
              source={require("../Images/logo.png")}
            />
            {/* </View> */}
            {/* <Text style={styles.textStyles} >Participant No.123</Text> */}
          </View>
          <View
            style={{
              flex: 1,
              alignItmes: "center",
              alignItems: "flex-start",
              justifyContent: "center",
              marginTop: "5%",
            }}
          >
            {/* <View style={{justifyContent:'center',alignItems:'center',flex:1}}> */}

            <Text style={styles.textStyles}>{strings.HomeName}</Text>
            <TextInput
              value={this.state.patname}
              style={{
                marginLeft: 10,
                fontSize: 16,
                width: "90%",
                borderBottomWidth: Platform.OS === "ios" ? 1 : 0,
                padding: "3%",
              }}
              onChangeText={(patname) => {
                this.setState({ patname });
              }}
              underlineColorAndroid="black"
            />
            {/* <Text style={styles.textStyles} >What's your Email</Text>
                        <TextInput
                            value={this.state.email}
                            style={{ marginLeft: 10, fontSize: 16, width: '90%' ,borderBottomWidth: 1 ,padding:'3%'}}
                            onChangeText={(email) => { this.setState({ email }) }}
                            underlineColorAndroid="black"
                        />
                        <Text style={styles.textStyles} >What's your Phone Number</Text>

                        <TextInput
                            value={this.state.contact}
                            keyboardType="numeric"
                            style={{ marginLeft: 10, fontSize: 16, color: 'black', width: '90%',borderBottomWidth: 1 ,padding:'3%' }}
                            onChangeText={(contact) => { this.setState({ contact }) }}
                            underlineColorAndroid="black"
                            maxLength={10}
                        /> */}
          </View>
          {/* <View style={styles.button}> */}
          {/* <TouchableOpacity style={styles.button} onPress={() => this.sendNextpage()}>
                            <Text style={styles.buttonText}>Next</Text>
                        </TouchableOpacity> */}
          {/* <Button
                            style={styles.buttonText,{backgroundColor:'red'}}
                            onPress={() => this.sendTONextPage()}
                            color="#ffffff"
                            title="Next"
                        />
                    </View> */}
          {/* <View
            style={{
              alignItems: "center",
              marginTop: "5%",
              backgroundColor: "#4B96FB",
              width: "50%",
              alignSelf: "center",
              padding: "3%",
            }}
          >

            <Button
              onPress={() => this.sendNextpage()}
              title="Next"
              color="#ffffff"
              style={styles.button}
            />
          </View> */}
          <View style={{ flex: 0, alignItems: "center", marginTop: 20 }}>
            <NativeBaseButton
              onPress={() => this.sendNextpage()}
              label="Next"
              buttonStyle={{ width: 150, paddingHorizontal: 10 }}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  inputBox: {
    width: (deviceWidth * 85) / 100,
    height: (deviceHeight * 7) / 100,
    borderRadius: 5,
    // backgroundColor: '#EDF0F7',
    paddingVertical: 15,
  },
  textStyles: {
    fontSize: 25,
    marginTop: "5%",
    color: "#1D4A99",
    marginLeft: "3%",
  },
  button: {
    width: (deviceWidth * 60) / 100,
    height: (deviceHeight * 7) / 100,
    backgroundColor: "#4B96FB",
    borderRadius: 5,
    marginVertical: 12,
    paddingVertical: 12,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
  },
  buttonStyle: {
    color: "red",
    marginTop: 20,
    padding: 20,
    backgroundColor: "green",
  },
  buttonText: {
    fontSize: 18,
    color: "#ffffff",
    textAlign: "center",
  },
  avatarContainer: {
    borderColor: "#9B9B9B",
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },
  avatar: {
    borderRadius: 75,
    width: 120,
    height: 120,
  },
});
// const mapStateToProps = state => {
//     return {
//         ...state
//     };
// };

// module.exports = connect(mapStateToProps)(Home);
