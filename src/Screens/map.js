import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import MapView, {Marker} from 'react-native-maps';

class map extends Component {
  state = {
    coordinates: [
      {name: "1", latitude: 37.8025259, longitude: -122.4351431},
      {name: "2", latitude: 37.7896386, longitude: -122.421646},
      {name: "3", latitude: 37.7665248, longitude: -122.4161628},
      {name: "4", latitude: 37.7734153, longitude: -122.4577787},
      {name: "5", latitude: 37.7948605, longitude: -122.4596065},
      {name: "6", latitude: 37.8025259, longitude: -122.4351431}
    ]
  };
  render() {
    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          initialRegion={{
            latitude: 37.7665248,
            longitude: -122.4161628,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
        >
          {this.state.coordinates.map(marker => 
            (<Marker
            coordinate={{latitude: marker.latitude, longitude: marker.longitude}}
            title={marker.name}
            />)
          )}
        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  map: {
    height: '100%',
  },
});
export default map