import React from "react";
import {
  View,
  Text,
  Picker,
  StyleSheet,
  FlatList,
  Dimensions,
  TouchableOpacity,
} from "react-native";
import * as database from "../Database/allSchemas";
import SurveyPicker from "./SurveyPicker";
import { Actions } from "react-native-router-flux";
import { ScrollView } from "react-native-gesture-handler";
import SurveyFlip from "./SurveyFlip";
import SurveyTextInput from "./SurveyTextInput";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default class Survey extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      PickerValueHolder: "",
      quest: "",
    };
  }
  componentDidMount() {
    this.getData();
    console.log(this.state.quest);
  }
  getData() {
    database.queryAllPatientSurvey_Schema().then((res) => {
      const result = Object.values(res[0]);
      console.log("result", result);
      this.setState({ quest: result });
    });
  }

  surveyPicker(item) {
    // console.log("question survey", item.SUR_Question)
    // console.log("ans of survey",JSON.parse(item.Survey_Questions_Options_DTO))
    // console.log("pk",item.SUR_PKeyID)

    const ldata = JSON.parse(item.Survey_Questions_Options_DTO);
    return (
      <View style={{ margin: "5%" }}>
        <Text style={styles.textStyles}>{item.SUR_Question}</Text>
        {item.SUR_Control_Type == 1 ? (
          <SurveyPicker data={ldata} selectedQuest={item.SUR_PKeyID} />
        ) : null}
        {item.SUR_Control_Type == 2 ? (
          <SurveyFlip data={ldata} selectedQuest={item.SUR_PKeyID} />
        ) : null}
        {item.SUR_Control_Type == 3 ? (
          <SurveyTextInput data={ldata} selectedQuest={item.SUR_PKeyID} />
        ) : null}
      </View>
    );
  }
  render() {
    return (
      <View>
        <ScrollView>
          <View>
            <FlatList
              data={this.state.quest}
              style={{ marginTop: "5%", width: "95%" }}
              renderItem={({ item, index }) => this.surveyPicker(item)}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
          <View style={styles.button}>
            <TouchableOpacity
              onPress={() => Actions.CompletedSurvey()}
              style={{ justifyContent: "center", alignItems: "center" }}
            >
              <Text style={(styles.buttonText, { color: "white" })}>
                Submit
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
  },
  textStyles: {
    fontSize: 20,
    color: "#1D4A99",
    fontFamily: "Quicksand-Bold",
  },
  questionStyles: {
    fontSize: 25,
    color: "#1D4A99",
  },
  textStylesScore: {
    fontSize: 20,
    color: "#1D4A99",
    margin: "15%",
  },
  inputBox: {
    width: (deviceWidth * 85) / 100,
    height: (deviceHeight * 7) / 100,
    borderRadius: 5,
    // backgroundColor: '#EDF0F7',
    paddingVertical: 15,
  },
  pickerStyle: {
    justifyContent: "center",
    fontSize: 12,
  },
  // textStyles: {
  //     fontSize: 18,
  //     color: 'black',
  //     fontWeight: 'bold'
  // },
  skipbutton: {
    width: (deviceWidth * 30) / 100,
    height: (deviceHeight * 6) / 100,
    backgroundColor: "#4B96FB",
    borderRadius: 5,
    marginVertical: 9,
    paddingVertical: 9,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
  },
  button: {
    width: (deviceWidth * 30) / 100,
    height: (deviceHeight * 6) / 100,
    backgroundColor: "#4B96FB",
    borderRadius: 5,
    marginVertical: 9,
    paddingVertical: 9,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
  },
  _button: {
    width: (deviceWidth * 20) / 100,
    height: (deviceHeight * 4) / 100,
    backgroundColor: "#4B96FB",
    borderRadius: 5,
    marginVertical: 7,
    paddingVertical: 7,
  },
  button_Text: {
    fontSize: 12,
    color: "#ffffff",
    textAlign: "center",
  },
  buttonText: {
    fontSize: 16,
    color: "#ffffff",
    textAlign: "center",
  },
});
