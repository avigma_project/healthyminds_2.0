import React from "react";
import {
  Platform,
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Image,
  Button,
  ToastAndroid,
  BackHandler,
  ScrollView,
  Alert,
  SafeAreaView,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Actions, ActionConst } from "react-native-router-flux";
import { SliderBox } from "react-native-image-slider-box";
import { fetchapi } from "../Api/function";
import { Button as NativeBaseButton } from "native-base";
import * as database from "../Database/allSchemas";
import FastImage from "react-native-fast-image";

import axios from "axios";
import strings from "../Language/Language";
import Loader from "../ShowOptios/Loader";
import Toast from "react-native-simple-toast";
import Carousel from "react-native-snap-carousel";
// import {getLanguage} from '../Language/Language';

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export const SLIDER_WIDTH = Dimensions.get("window").width + 80;
export const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.7);

const CarouselItem = () => {
  return (
    <View style={{ flex: 1 }}>
      <Image source={require("../Images/screen1.png")} />
    </View>
  );
};

export default class Welcome extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [
        require("../Images/screen1.png"),
        require("../Images/screen2.jpg"),
        require("../Images/screen3.jpg"),
        require("../Images/screen4.png"),
        require("../Images/screen5.png"),
      ],
      Ltype: 1,
      language: true,
      // depression: '',
      selected: "#4B96FB",
      unSelcted: "#888888",
      // isLoading: false,
      update: true,
      // mdq: '',
      // anxiety: '',
      // ptsd: '',
    };
    // console.log(this.props.language)
  }
  UNSAFE_componentWillMount() {
    //debugger;
    console.log(this.props.language, "this.state.language");
    // BackHandler.removeEventListener('hardwareBackPress', this.onBackPress.bind(this));
    this.getLanguage();
    // this.showLanguage();
    // this.GetMasterDetails();
  }
  // componentWillUnmount(){
  //     BackHandler.removeEventListener('hardwareBackPress', this.onBackPress.bind(this));
  //     }
  //     onBackPress(){
  //         BackHandler.exitApp();
  //     }
  async GetMasterDetails() {
    console.log("ltype data", this.state.Ltype);
    // const data = JSON.stringify({"Ltype":2})
    // console.log("Ashok",data)
    // this.setState({ isLoading: true });
    //debugger;
    try {
      axios
        .post(
          "http://hospapi.ikaart.org/api/HealthyMindsApp/GetMasterDetails",
          { Ltype: this.state.Ltype },
          {}
        )
        .then((response) => {
          console.log("response ---dfiffff", response);
          // this.setState({isLoading:false})
          database
            .deletePatientDepression()
            .catch((error) => console.log(error));
          database
            .deletePatientDepression2()
            .catch((error) => console.log(error));
          database
            .deletePatientDepression3()
            .catch((error) => console.log(error));
          database
            .deletePatientDepression4()
            .catch((error) => console.log(error));
          database
            .deletePatient_Treated_Schema()
            .catch((error) => console.log(error));
          database.deletePatientSurveyMaster_Schema().catch((error) => {
            console.log(error);
          });
          database.deletePatientHealthCenters_Schema().catch((error) => {
            console.log(error);
          });
          database
            .deletePatientTechniques_List_Schema_Schema()
            .catch((error) => {
              console.log(error);
            });

          // this.setState({ isLoading: false });

          if (response.data != "") {
            if (response.data[0] != "") {
              database
                .InsertPatientDepressionMaster(response.data[0])
                .catch((error) => console.log(error));

              // ToastAndroid.showWithGravity(
              //     "InsertPatientDepressionMaster",
              //     ToastAndroid.SHORT,
              //     ToastAndroid.BOTTOM
              // )
              console.log(response, "Spanish");
            }
            if (response.data[1] != "") {
              database
                .InsertPatientMoodDisorderMaster(response.data[1])
                .catch((error) => console.log(error));
              // ToastAndroid.showWithGravity(
              //     "InsertPatientMoodDisorderMaster",
              //     ToastAndroid.SHORT,
              //     ToastAndroid.BOTTOM
              // )
            }
            if (response.data[2] != "") {
              database
                .InsertPatientAnxietyDisorderMaster(response.data[2])
                .catch((error) => console.log(error));
              // ToastAndroid.showWithGravity(
              //     "InsertPatientAnxietyDisorderMaster",
              //     ToastAndroid.SHORT,
              //     ToastAndroid.BOTTOM
              // )
            }
            if (response.data[3] != "") {
              database
                .InsertPatientPTSDMaster(response.data[3])
                .catch((error) => console.log(error));
              // ToastAndroid.showWithGravity(
              //     "InsertPatientPTSDMaster",
              //     ToastAndroid.SHORT,
              //     ToastAndroid.BOTTOM
              // )
            }
            if (response.data[4] != "") {
              // console.log('repson 3', response.data[4]);
              database
                .InsertPatientTreatedMaster(response.data[4])
                .catch((error) => console.log(error));
              // ToastAndroid.showWithGravity(
              //     "InsertPatientTreatedMaster",
              //     ToastAndroid.SHORT,
              //     ToastAndroid.BOTTOM
              // )
            }
            if (response.data[5][0] != "") {
              // console.log('repson 3', response.data[5][0]);
              database
                .InsertPatientSurveyMaster(response.data[5][0])
                .catch((error) => console.log(error));
              // ToastAndroid.showWithGravity(
              //     "InsertPatientTreatedMaster",
              //     ToastAndroid.SHORT,
              //     ToastAndroid.BOTTOM
              // )
              // console.log("Ashok final response", response.data[5][0])
            }
            if (response.data[6][0] != "") {
              // console.log('repson 3', response.data[6][0]);
              database
                .InsertHealthCenterDetails(response.data[6][0])
                .catch((error) => console.log(error));
              // ToastAndroid.showWithGravity(
              //     "InsertHealthCenterDetails",
              //     ToastAndroid.SHORT,
              //     ToastAndroid.BOTTOM
              // )
              // console.log("Ashok  InsertHealthCenterDetails", response.data[6][0])
            }

            if (response.data[7][0] != "") {
              // console.log('repson 3', response.data[7][0]);
              database
                .InsertTechniques_List_SchemaDetails(response.data[7][0])
                .catch((error) => console.log(error));
              // ToastAndroid.showWithGravity(
              //     "InsertTechniques_List_SchemaDetails",
              //     ToastAndroid.SHORT,
              //     ToastAndroid.BOTTOM
              // )
              // console.log("Ashok  InsertTechniques_List_SchemaDetails", response.data[7][0])
            }
          }
        });
    } catch (error) {
      console.log(error);
    }
  }
  // async onChangeLang(lang) {
  //     i18n.changeLanguage(lang);
  //     try {
  //         await AsyncStorage.setItem('@APP:languageCode', lang);
  //     } catch (error) {
  //         console.log(` Hi Errorrrr : ${error}`);
  //     }
  //     console.log(i18n.dir());
  // }
  async setLanguage(languageCode) {
    // console.log(languageCode)
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        // console.log("refresh", get_Data);

        this.setState({
          Ltype: 1,
          language: false,
          selected: "#4B96FB",
          unSelcted: "#888888",
        });
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
        this.setState({
          Ltype: 2,
          language: false,
          selected: "#888888",
          unSelcted: "#4B96FB",
        });
      });
    }
    this.GetMasterDetails();
    Actions.refresh({ key: "tasks" });
    // getLanguage(languageCode)
  }
  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      // console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
        this.setLanguage("en");
      } else {
        if (get_Data == "2") {
          strings.setLanguage("sp");
          this.setLanguage("sp");
        } else {
          strings.setLanguage("en");
          this.setLanguage("en");
        }
      }
    });
  }

  // async UNSAFE_componentWillcomUpdate() {
  //     // console.log("Checking API hit", "componentDidUpdate method is called");
  //     // this.setState({isLoading:true})
  //     await AsyncStorage.getItem("refresh", (err, get_Data) => {
  //         console.log(get_Data, "Change language")
  //         if (get_Data === 1) {
  //             this.GetMasterDetails();
  //         }
  //         else {
  //             if (get_Data === 2) {
  //                 this.GetMasterDetails();
  //             }
  //         }

  //     });
  // }
  async componentDidUpdate() {
    // this.getLanguage()
    if (this.state.update === true) {
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log(get_Data, "Change language");
        if (get_Data === 1) {
          this.setState({ update: false });
          this.GetMasterDetails();
        } else {
          if (get_Data === 2) {
            this.setState({ update: false });
            this.GetMasterDetails();
          }
        }
      });
    }
  }

  sendTONextPage() {
    //debugger;
    Actions.Home();
  }
  render() {
    const { t, i18n } = this.props;
    return (
      <SafeAreaView style={styles.container}>
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            height: "50%",
          }}
        >
          <SliderBox
            images={this.state.images}
            ImageComponent={FastImage}
            resizeMethod={"resize"}
            resizeMode={"stretch"}
            sliderBoxHeight={350}
            autoplay
            dotStyle={{
              width: 10,
              height: 10,
              borderRadius: 5,
              marginHorizontal: 0,
              padding: 0,
              margin: 0,
              backgroundColor: "rgba(128, 128, 128, 0.92)",
            }}
            inactiveDotColor="#90A4AE"
          />
        </View>

        <ScrollView>
          <View style={{ alignItems: "center" }}>
            <View
              style={{
                width: "90%",
                justifyContent: "center",
                marginTop: "5%",
                alignItems: "center",
              }}
            >
              <Text
                style={{
                  fontSize: 25,
                  fontWeight: "bold",
                  color: "#1D4A99",
                  fontFamily: "Quicksand-Bold",
                }}
              >
                {/* {t('common:currentLanguage', { lng: i18n.language })} */}
                {strings.title}
              </Text>
            </View>
          </View>
          <View style={{ alignItems: "center", justifyContent: "center" }}>
            <View
              style={{
                width: "100%",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              {/* <Text style={{ fontSize: 24, fontWeight: 'bold', color: '#1D4A99', }}>
                                Mental Health Check-Up
                            </Text> */}
              <Text
                style={{
                  fontSize: 18,
                  color: "#1D4A99",
                  marginTop: "5%",
                  justifyContent: "center",
                }}
              >
                {strings.description}
              </Text>
              <View style={{ width: "80%" }}>
                <Text
                  style={{
                    fontSize: 18,
                    color: "#1D4A99",
                    justifyContent: "center",
                  }}
                >
                  {strings.description2}
                </Text>
              </View>
            </View>
          </View>

          <View
            style={{
              marginVertical: 20,
              paddingHorizontal: 50,
              flex: 0,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <NativeBaseButton
              style={{
                backgroundColor: "#4B96FB",
                height: 50,
                paddingHorizontal: 30,
              }}
              onPress={() => {
                Actions.tabBar({ type: ActionConst.RESET });
                Actions.WelcomeScreen();
                // Actions.Home();
              }}
            >
              <Text style={{ textTransform: "uppercase", color: "#ffffff" }}>
                {strings.button1}
              </Text>
            </NativeBaseButton>
          </View>

          {/* <View
            style={{
              alignItems: "center",
              marginTop: "5%",
              // backgroundColor: "#4B96FB",
              width: "50%",
              alignSelf: "center",
              padding: "3%",
            }}
          >
            <Button
              onPress={() => {
                Actions.tabBar({ type: ActionConst.RESET });
                Actions.Home();
              }}
              title={strings.button1}
              // color="#ffffff"
              style={{
                alignItems: "center",
                marginTop: "5%",
                backgroundColor: "#4B96FB",
                // width: "50%",
                alignSelf: "center",
                paddingHorizontal: 20,
              }}
              // style={styles.button}
            />
          </View> */}
          {/* <TouchableOpacity onPress={()=>Actions.Selectlanguage()}>
                        <View style={{ width: '100%', alignSelf: "center", marginTop: '5%' }}>
                            <Text style={{ fontSize: 18, color: '#1D4A99', alignSelf: 'center' }}>{strings.changeLanguage}</Text>
                        </View>
                    </TouchableOpacity> */}
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              width: "90%",
              alignSelf: "center",
              marginBottom: 20,
            }}
          >
            <NativeBaseButton
              style={{
                backgroundColor: this.state.selected,
                height: 50,
                paddingHorizontal: 30,
              }}
              onPress={() => this.setLanguage("en")}
            >
              <Text style={{ textTransform: "uppercase", color: "#ffffff" }}>
                English
              </Text>
            </NativeBaseButton>
            <NativeBaseButton
              style={{
                backgroundColor: this.state.unSelcted,
                height: 50,
                paddingHorizontal: 30,
              }}
              onPress={() => this.setLanguage("sp")}
            >
              <Text style={{ textTransform: "uppercase", color: "#ffffff" }}>
                Spanish
              </Text>
            </NativeBaseButton>
            {/* <View
              style={{
                alignItems: "center",
                marginTop: "5%",
                backgroundColor: this.state.selected,
                width: "40%",
                alignSelf: "center",
                padding: "3%",
              }}
            >
              <Button
                onPress={() => this.setLanguage("en")}
                // onPress={() => Actions.Welcome({language:this.state.english})}
                title="English"
                style={{
                  width: 100,
                }}
                // color="#ffffff"
                // style={{
                //   alignItems: "center",
                //   marginTop: "5%",
                //   width: "50%",
                //   alignSelf: "center",
                //   padding: "3%",
                // }}
              />
            </View> */}
            {/* <View
              style={{
                alignItems: "center",
                marginTop: "5%",
                backgroundColor: this.state.unSelcted,
                width: "40%",
                alignSelf: "center",
                padding: "3%",
              }}
            >
              <Button
                onPress={() => this.setLanguage("sp")}
                // onPress={() => Actions.Welcome({language:this.state.spanish})}
                title="Spanish"
                color="#ffffff"
                style={{
                  alignItems: "center",
                  marginTop: "5%",
                  width: "50%",
                  alignSelf: "center",
                  padding: "3%",
                }}
                // style={styles.button}
              />
            </View> */}
          </View>
          {/* <Loader loading={this.state.isLoading} /> */}
        </ScrollView>
      </SafeAreaView>
    );
  }
}
// export default translate(['Welcome', 'common'], { wait: true })(Welcome);
// let isRTL = i18n.dir();
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    width: "100%",
  },
  textStyles: {
    fontSize: 18,
    color: "#1D4A99",
  },
  button: {
    width: (deviceWidth * 40) / 100,
    height: (deviceHeight * 7) / 100,
    backgroundColor: "#4B96FB",
    marginTop: "5%",
    borderRadius: 5,
    marginVertical: 12,
    paddingVertical: 12,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonText: {
    fontSize: 16,
    color: "#ffffff",
    textAlign: "center",
  },
});
