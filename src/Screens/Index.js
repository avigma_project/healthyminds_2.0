import React from "react";
import { Router, Scene, Stack, Tabs, Actions } from "react-native-router-flux";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  BackHandler,
  Alert,
  Dimensions,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Home from "./Home";
import PatientDetails from "./PatientDetails";

import HDSTool from "./HDSTool";
import Welcome from "./Welcome";
import MDQ from "./MDQ";
import ADS from "./ADS";
import PTSD from "./PTSD";
import SplashScreen from "./SplashScreen";
import Completed from "./Completed";
import CustomTabBar from "../view/CustomTabBar";
// import map from "./map";
import Dashboard from "./Dashboard";
import Profile from "./Profile";
import EditProfile from "./EditProfile";
import Survey from "./Survey";
import HomeSurvey from "./HomeSurvey";
import CompletedSurvey from "./CompletedSurvey";
import ListCenters from "./ListCenters";
import Techniques from "./Techniques";
import Selectlanguage from "./Selectlanguage";
import strings from "../Language/Language";
import Depression from "../ShowOptios/Depression";
import Bipolar from "../ShowOptios/Bipolar";
import Anxiety from "../ShowOptios/Anxiety";
import Stress from "../ShowOptios/Stress";
import WebViewList from "../Screens/WebViewList";
import Login from "../Auth/Login";
import WelcomeScreen from "../Auth/WelcomeScreen";
import SignUp from "../Auth/SignUp";
import FindPassword from "../Auth/FindPassword";
import ChangePassword from "../Auth/ChangePassword";
import GetStarted from "./MHS/GetStarted";
import SelectOptions from "./MHS/SelectOptions";
import BasicInformation from "./MHS/BasicInformation";

const screenWidth = Math.round(Dimensions.get("window").width);

const InboxIcon = () => {
  return <View />;
};
const skip = () => {
  return (
    <View style={{ backgroundColor: "red" }}>
      <TouchableOpacity onPress={() => Actions.Completed()}>
        <Text>Skip</Text>
      </TouchableOpacity>
    </View>
  );
};

const TabIcon = ({ focused }) => {
  return (
    <Icon
      name="home"
      style={{ color: focused ? "#34a103" : "#8c8c8c" }}
      size={25}
    />
  );
};
const TabIconHealthCenters = ({ focused }) => {
  return (
    <Icon
      name="map-marker"
      style={{ color: focused ? "#34a103" : "#8c8c8c" }}
      size={25}
    />
  );
};
const TabIconSurvey = ({ focused }) => {
  return (
    <MaterialCommunityIcons
      name="search-web"
      style={{ color: focused ? "#34a103" : "#8c8c8c" }}
      size={25}
    />
  );
};
const TabIconResources = ({ focused }) => {
  return (
    <MaterialCommunityIcons
      name="search-web"
      style={{ color: focused ? "#34a103" : "#8c8c8c" }}
      size={25}
    />
  );
};
export default class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      update: false,
    };
  }
  componentWillMount() {
    console.disableYellowBox = true;
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", function() {
      const scene = Actions.currentScene;
      console.log("screen name", scene);
      if (scene === "Welcome") {
        Alert.alert(
          "Exit App",
          "Are you sure ?",
          [
            {
              text: "Cancel",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel",
            },
            { text: "OK", onPress: () => BackHandler.exitApp() },
          ],
          {
            cancelable: false,
          }
        );
        return true;
      }
      // else if(scene === "Completed"){
      //   Actions.Welcome();
      // }
      Actions.pop();
      return true;
    });
  }

  render() {
    return (
      <Router>
        <Scene key="root">
          <Scene
            key="SplashScreen"
            component={SplashScreen}
            title="SplashScreen"
            hideNavBar={true}
            initial
          />

          {/* Tab Container */}
          <Scene
            key="tabBar"
            tabs={true}
            hideNavBar
            tabBarStyle={{ backgroundColor: "#FFFFFF" }}
            tabBarComponent={CustomTabBar}
          >
            {/* Tab and it's scenes */}

            <Scene
              key="WelcomeScreen"
              component={WelcomeScreen}
              title="WelcomeScreen"
              hideNavBar={true}
            />
            <Scene
              key="Login"
              component={Login}
              title="Login"
              hideNavBar={true}
            />
            <Scene
              key="SignUp"
              component={SignUp}
              title="SignUp"
              hideNavBar={true}
            />
            <Scene
              key="FindPassword"
              component={FindPassword}
              title="FindPassword"
              hideNavBar={true}
            />
            <Scene
              key="Dashboard"
              component={Dashboard}
              title="Dashboard"
              hideNavBar={true}
            />
            <Scene
              key="Profile"
              component={Profile}
              title="Profile"
              hideNavBar={true}
            />
            <Scene
              key="EditProfile"
              component={EditProfile}
              title="EditProfile"
              hideNavBar={true}
            />
            <Scene
              key="ChangePassword"
              component={ChangePassword}
              title="ChangePassword"
              hideNavBar={true}
            />
            <Scene
              key="GetStarted"
              component={GetStarted}
              title="GetStarted"
              hideNavBar={true}
            />
            <Scene
              key="SelectOptions"
              component={SelectOptions}
              title="ScreSelectOptionsening"
              hideNavBar={true}
            />
            <Scene
              key="BasicInformation"
              component={BasicInformation}
              title="BasicInformation"
              hideNavBar={true}
            />
            <Scene
              key="home"
              title={strings.Home}
              titleStyle={{ color: "black", fontSize: 12 }}
              icon={TabIcon}
            >
              <Scene
                key="Welcome"
                component={Welcome}
                title="Mental Health Check-Up"
                hideNavBar={true}
              />
              <Scene
                key="Home"
                clone
                component={Home}
                title="Mental Health Check-Up"
                style={style.textTitleStyle}
                back={false}
                hideNavBar={true}
                renderRightButton={InboxIcon}
                navigationBarStyle={{ backgroundColor: "#359f06" }}
                titleStyle={{ fontSize: 20 }}
              />
              <Scene
                key="ADS"
                component={ADS}
                title=" Anxiety Disorder Screen"
                // renderRightButton={InboxIcon}
                hideNavBar={false}
                // rightButtonTextStyle={{ marginRight: '2%' }}
                // renderRightButton={skip}
                titleStyle={{ fontSize: 20 }}
                rightButtonTextStyle={{ padding: "3%" }}
                onRight={() => Actions.Completed()}
                rightTitle="Skip"
                navigationBarStyle={{ backgroundColor: "#359f06" }}
              />
              <Scene
                key="PTSD"
                component={PTSD}
                title="PTSD SCREEN"
                // renderRightButton={InboxIcon}
                hideNavBar={false}
                // renderRightButton={skip}
                titleStyle={{ fontSize: 20 }}
                rightButtonTextStyle={{ padding: "3%" }}
                onRight={() => Actions.Completed()}
                navigationBarStyle={{ backgroundColor: "#359f06" }}
                rightTitle="Skip"
              />
              <Scene
                key="MDQ"
                component={MDQ}
                title="Mood Screening"
                hideNavBar={false}
                // renderRightButton={skip}
                // rightButtonTextStyle ={{paddingRight:'2%'}}
                titleStyle={{ fontSize: 20 }}
                rightButtonTextStyle={{ padding: "3%" }}
                onRight={() => Actions.Completed()}
                navigationBarStyle={{ backgroundColor: "#359f06" }}
                rightTitle="Skip"
              />
              <Scene
                key="PatientDetails"
                component={PatientDetails}
                title="Personal Details"
                renderRightButton={InboxIcon}
                titleStyle={{ fontSize: 20 }}
                navigationBarStyle={{ backgroundColor: "#359f06" }}
                hideNavBar={false}
              />
              <Scene
                key="Depression"
                component={Depression}
                title="Depression Screening"
                hideNavBar={false}
                renderRightButton={InboxIcon}
                titleStyle={{ fontSize: 20 }}
                navigationBarStyle={{ backgroundColor: "#359f06" }}
              />
              <Scene
                key="Bipolar"
                component={Bipolar}
                title="Mood Screening"
                hideNavBar={false}
                renderRightButton={InboxIcon}
                titleStyle={{ fontSize: 20 }}
                navigationBarStyle={{ backgroundColor: "#359f06" }}
              />
              <Scene
                key="Stress"
                component={Stress}
                title="PTSD SCREEN"
                hideNavBar={false}
                renderRightButton={InboxIcon}
                titleStyle={{ fontSize: 20 }}
                navigationBarStyle={{ backgroundColor: "#359f06" }}
              />
              <Scene
                key="Anxiety"
                component={Anxiety}
                title="Anxiety Disorder Screen"
                hideNavBar={false}
                renderRightButton={InboxIcon}
                titleStyle={{ fontSize: 20 }}
                navigationBarStyle={{ backgroundColor: "#359f06" }}
              />
              <Scene
                key="HDSTool"
                component={HDSTool}
                title="Depression Screening"
                hideNavBar={false}
                renderRightButton={InboxIcon}
                titleStyle={{ fontSize: 20 }}
                navigationBarStyle={{ backgroundColor: "#359f06" }}
              />
              {/* <Scene key="map" component={map} /> */}
              <Scene
                key="Completed"
                title="Results"
                titleStyle={{ fontSize: 20 }}
                navigationBarStyle={{ backgroundColor: "#359f06" }}
                component={Completed}
                hideNavBar={false}
              />
            </Scene>

            <Scene
              key="health-center"
              title={strings.Centers}
              titleStyle={{ color: "black", fontSize: 12 }}
              icon={TabIconHealthCenters}
            >
              <Scene
                clone
                key="ListCenters"
                component={ListCenters}
                title="HealthCare Centers"
                back={false}
                hideNavBar={true}
                navigationBarStyle={{ backgroundColor: "#359f06" }}
              />
              <Scene
                key="WebViewList"
                clone
                component={WebViewList}
                title="HealthCare Centers"
                // hideNavBar={true}
                // back={false}
                navigationBarStyle={{ backgroundColor: "#359f06" }}
              />
            </Scene>

            {/* Tab and it's scenes */}
            <Scene
              key="survey"
              title={strings.Survey}
              titleStyle={{ color: "black", fontSize: 12 }}
              icon={TabIconSurvey}
            >
              <Scene
                key="Survey"
                component={Survey}
                title="Survey"
                renderRightButton={InboxIcon}
                hideNavBar={false}
                navigationBarStyle={{ backgroundColor: "#359f06" }}
              />
              <Scene
                key="HomeSurvey"
                component={HomeSurvey}
                title="Survey"
                renderRightButton={InboxIcon}
                hideNavBar={false}
                navigationBarStyle={{ backgroundColor: "#359f06" }}
                initial
              />
              <Scene
                key="CompletedSurvey"
                title="Results"
                titleStyle={{ fontSize: 20 }}
                navigationBarStyle={{ backgroundColor: "#359f06" }}
                component={CompletedSurvey}
                hideNavBar={false}
              />
            </Scene>

            {/* Tab and it's scenes */}
            <Scene
              key="techiques"
              title={strings.Service}
              titleStyle={{ color: "black", fontSize: 12 }}
              icon={TabIconResources}
            >
              <Scene
                key="Techniques"
                component={Techniques}
                title="Service"
                hideNavBar={false}
                navigationBarStyle={{ backgroundColor: "#359f06" }}
              />
            </Scene>
          </Scene>
        </Scene>
      </Router>
    );
  }
}
const style = StyleSheet.create({
  navigationBarTitleStyle: {
    flex: 1,
    textAlign: "center",
  },
  textTitleStyle: {
    flex: 1,
    textAlign: "center",
  },
});
