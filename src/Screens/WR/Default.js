import React, { Component } from "react";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  Image,
  useWindowDimensions,
  FlatList,
  Linking,
  Keyboard,
} from "react-native";
import Myprofile from "../../CustomFolder/Myprofile";
import { getblogdata, getblogsearchdata } from "../../Api/function";
import AsyncStorage from "@react-native-async-storage/async-storage";
import strings from "../../Language/Language";
import AntDesign from "react-native-vector-icons/AntDesign";
import Entypo from "react-native-vector-icons/Entypo";
import CustomInputMap from "../FMHP/CustomFolder/CustomInputMap";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
// const { width } = useWindowDimensions();

export default class Default extends Component {
  constructor(props) {
    super(props);
    this.textInput = React.createRef();
    this.state = {
      token: "",
      result: [],
      image: [],
      search: [],
      searchData: [],
      isSearch: false,
      Blog_Language: 1,
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      this.getToken();
      this.getLanguage();
    });
  }

  componentWillUnmount() {
    this._unsubscribe;
  }

  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      // console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        this.setState({
          Blog_Language: 1,
        });
      } else if (get_Data == "2") {
        this.setState({
          Blog_Language: 2,
        });
      }
    });
    this.getBlogData();
  }

  getToken = async () => {
    let token;
    token = await AsyncStorage.getItem("token");
    this.setState({
      token: token,
    });
    console.log("this.state.token", this.state.token, token);
  };

  getBlogData = async () => {
    this.setState({ isSearch: false });
    var data = JSON.stringify({
      Type: 3,
      Blog_Language: this.state.Blog_Language,
    });
    // try {
    console.log("miallll", this.state.Blog_Language);
    const res = await getblogdata(data);
    console.log(res, "Blogs");
    this.setState({ result: res[0] });

    // // } catch (error) {
    // console.log("hihihihihihih", { e: error.response.data.error });
    // let message = "";
    // if (error.response) {
    //   this.setState({ isLoading: false });
    // } else {
    //   message = "";
    // }
    // console.log({ message });
    // }
  };

  getBlogSearchData = async () => {
    Keyboard.dismiss();
    this.setState({ isSearch: true });
    var data = JSON.stringify({
      Type: 1,
      Blog_Title: this.state.search,
    });
    try {
      const res = await getblogsearchdata(data);
      if (this.state.search != "" && this.state.search != []) {
        this.setState({ searchData: res[0] });
      } else {
        alert("No Resources found for current search");
      }

      console.log(res, "searchData");
    } catch (error) {
      // console.log("hihihihihihih", { e: error.response.data.error });
      let message = "";
      if (error.response) {
        this.setState({ isLoading: false });
      } else {
        message = "";
      }
      console.log({ message });
    }
  };

  render_Item(item, index) {
    // console.log(`==>${index}`, item);
    return (
      <View
        style={{
          // paddingHorizontal: Platform.OS === "ios" ? 15 : 10,
          // borderWidth: 0.5,
          // borderColor: "#C4C4C4",
          marginTop: 15,
          marginHorizontal: 16,
          shadowOffset: { width: 0.5, height: 0.5 },
          shadowColor: "gray",
          shadowOpacity: 0.2,
          elevation: 1,
          backgroundColor: "#fff",
          // padding: 10,
          flexDirection: "column",
        }}
      >
        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate("Description", { item })
          }
        >
          {item.Blog_ImagePath && (
            <Image
              source={{
                uri: item.Blog_ImagePath
                  ? item.Blog_ImagePath
                  : "https://www.phoca.cz/images/projects/phoca-gallery-r.png",
              }}
              resizeMode="stretch"
              style={{
                width: "100%",
                height: 200,
                overflow: "visible",
              }}
            />
          )}
        </TouchableOpacity>

        <View style={{ padding: 10 }}>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate("Description", { item })
            }
          >
            <Text
              style={{
                color: "#A1683A",
                fontSize: 18,
                fontWeight: "700",
                lineHeight: 24,
                fontFamily: "Quicksand-Bold",
              }}
            >
              {item.Blog_Title}
            </Text>
          </TouchableOpacity>
          <View style={{ flexDirection: "row" }}>
            <Text style={styles.text}>
              {item.Blog_AuthorName} {"\u2981"}
            </Text>
            {/* <Text style={styles.text}>3mins read {"\u2981"}</Text> */}
            <Text style={styles.text}>{item.Blog_CreatedOn}</Text>
          </View>
        </View>
      </View>
    );
  }

  contact = () => {
    Linking.openURL("https://www.healthelivin.org/contact-us/");
  };

  footer = () => {
    return (
      <TouchableOpacity
        style={{ marginVertical: 50, alignSelf: "center" }}
        onPress={() => this.contact()}
      >
        <Text
          style={{
            color: "#A1683B",
            fontWeight: "700",
            lineHeight: 24,
            fontFamily: "Quicksand-Bold",
          }}
        >
          {strings.contact}
        </Text>
      </TouchableOpacity>
    );
  };

  // searchFilterFunction = (text) => {
  //   // Check if searched text is not blank
  //   if (text) {
  //     // Inserted text is not blank
  //     // Filter the masterDataSource and update FilteredDataSource
  //     const newData = this.state.searchData.filter(function(item) {
  //       // Applying filter for the inserted text in search bar
  //       // return console.log(item.User_Name);
  //       console.log(item.Blog_Title);
  //       const itemData = item.Blog_Title
  //         ? item.Blog_Title.toUpperCase()
  //         : "".toUpperCase();
  //       const textData = text.toUpperCase();
  //       return itemData.indexOf(textData) > -1;
  //     });
  //     console.log(newData);
  //     // const dataGroup = this.getContact(newData);
  //     // this.props.setContacts(dataGroup);
  //     this.setState({
  //       searchData: newData,
  //     });
  //   } else {
  //     // const dataGroup = this.getContact(this.state.data);
  //     // this.props.setContacts(dataGroup);
  //     this.setState({
  //       searchData: this.state.backupData,
  //     });
  //   }
  // };

  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#ffff",
          // backgroundColor: "#E5E5E5"
        }}
      >
        <View
          style={{
            justifyContent: "space-between",
            flexDirection: "row",
            // backgroundColor: "pink",
            alignItems: "center",
            marginHorizontal: 16,
            marginTop: 6,
          }}
        >
          <View style={{ justifyContent: "center" }}>
            <CustomInputMap
              placeholder={strings.Search}
              placeholderTextColor="#828282"
              value={this.state.search}
              autoCapitalize="none"
              onChangeText={(search) => {
                this.setState({ search: search });
              }}
            />
            {this.state.isSearch ? (
              <TouchableOpacity
                style={{
                  position: "absolute",
                  right: 20,
                }}
                onPress={() =>
                  this.setState({ isSearch: false, searchData: [], search: "" })
                }
              >
                <Entypo name="cross" size={25} color="#377867" />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                style={{
                  position: "absolute",
                  right: 20,
                }}
                onPress={() => this.getBlogSearchData()}
              >
                <AntDesign name="search1" size={25} color="#377867" />
              </TouchableOpacity>
            )}
          </View>
          <View style={{}}>
            {this.state.token ? (
              <View style={{}}>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("Search", {
                      screen: "Profile",
                    })
                  }
                  style={{
                    justifyContent: "center",
                    alignItem: "center",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 16,
                      color: "#377867",
                      fontWeight: "700",
                      fontFamily: "Quicksand-Bold",
                    }}
                  >
                    {strings.myprofile}
                  </Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View style={{}}>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("Search", {
                      screen: "Login",
                    })
                  }
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Text
                    style={{
                      fontSize: 13,
                      color: "#377867",
                      fontWeight: "700",
                      fontFamily: "Quicksand-Bold",
                    }}
                  >
                    {strings.login}/{strings.signup}
                  </Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        </View>

        <View
          style={{
            marginTop: 15,
            paddingHorizontal: Platform.OS === "ios" ? 22 : 15,
          }}
        >
          <Text
            style={{
              fontSize: 25,
              fontWeight: "700",
              lineHeight: 32,
              color: "#060D0B",
              fontFamily: "Quicksand-Bold",
            }}
          >
            {strings.resources}
          </Text>
        </View>

        <FlatList
          // style={{ paddingHorizontal: 7 }}
          data={this.state.isSearch ? this.state.searchData : this.state.result}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => this.render_Item(item, index)}
          ListFooterComponent={this.footer}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    color: "#060D0B",
    fontSize: 14,
    lineHeight: 18,
  },
});
