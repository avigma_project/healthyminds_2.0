import React, { Component } from "react";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  Image,
  useWindowDimensions,
  FlatList,
  Linking,
  ScrollView,
} from "react-native";
import Myprofile from "../../CustomFolder/Myprofile";
import { getblogdata } from "../../Api/function";
import AsyncStorage from "@react-native-async-storage/async-storage";
import strings from "../../Language/Language";
import AntDesign from "react-native-vector-icons/AntDesign";
import CustomInputMap from "../FMHP/CustomFolder/CustomInputMap";
import HTMLView from "react-native-htmlview";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import { WebView } from "react-native-webview";
// const source = {
//   html: Zthis.props.route.params.item.Blog_Description,
// };

export default class Description extends Component {
  render() {
    const htmlContent = `<div>${this.props.route.params.item.Blog_Description}</div>`;
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#fff",
          // backgroundColor: "#E5E5E5"
        }}
      >
        <ScrollView>
          <View>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack()}
              style={{
                width: "6%",
                top: Platform.OS === "ios" ? 10 : 20,
                left: 15,
              }}
            >
              <AntDesign name="left" size={25} color="#377867" />
            </TouchableOpacity>
          </View>

          <View
            style={{
              marginTop: 25,
              marginHorizontal: 16,
              shadowOffset: { width: 0.5, height: 0.5 },
              shadowColor: "gray",
              shadowOpacity: 0.2,
              elevation: 1,
              backgroundColor: "#fff",
              // padding: 10,
              flexDirection: "column",
            }}
          >
            <View style={{}}>
              <Text
                style={{
                  color: "#060D0B",
                  fontSize: 20,
                  fontWeight: "700",
                  lineHeight: 26,
                  paddingVertical: 5,
                  paddingHorizontal: 10,
                }}
              >
                {this.props.route.params.item.Blog_Title}
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                paddingVertical: 5,
                paddingHorizontal: 10,
              }}
            >
              <Text style={styles.text}>
                {this.props.route.params.item.Blog_AuthorName} {"\u2981"}
              </Text>
              {/* <Text style={styles.text}>3mins read {"\u2981"}</Text> */}
              <Text style={styles.text}>
                {this.props.route.params.item.Blog_CreatedOn}
              </Text>
            </View>
            <Image
              source={{ uri: this.props.route.params.item.Blog_ImagePath }}
              resizeMode="stretch"
              style={{
                width: "100%",
                height: 200,
              }}
            />
            <View
              style={{ paddingHorizontal: 22, marginTop: 10, marginBottom: 10 }}
            >
              <HTMLView
                value={htmlContent}
                stylesheet={styles}
                lineBreak={true}
              />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  text: {
    color: "#060D0B",
    fontSize: 14,
    lineHeight: 18,
  },
  div: {
    color: "#060D0B",
    fontSize: 16,
    lineHeight: 22,
  },
});
