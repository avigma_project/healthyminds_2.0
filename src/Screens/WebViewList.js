import React from "react";
import { StyleSheet, View, SafeAreaView } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Actions } from "react-native-router-flux";

import { WebView } from "react-native-webview";
import Loader from "../ShowOptios/Loader";

export default class WebViewList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      webUrl: "",
      isLoading: true,
      stopRef: true,
    };
  }

  componentDidMount() {
    // AsyncStorage.setItem("webUrl","0")
    this.refreshData();
  }
  async getData() {
    AsyncStorage.getItem("webUrl", (err, get_Data) => {
      console.log(get_Data);
      if (get_Data == 0) {
        this.refreshData();
        AsyncStorage.setItem("webUrl", "1");
      }
    });
  }

  // componentDidUpdate(){
  //     AsyncStorage.getItem("webUrl", (err, get_Data) => {
  //         console.log(get_Data)
  //         if (get_Data == 0) {
  //             this.refreshData()
  //             // Actions.refresh({ key: 'tasks', });

  //         }
  //     })
  // }

  async refreshData() {
    await AsyncStorage.getItem("AsynstorageData", (err, get_Data) => {
      console.log("refresh", get_Data);
      this.setState({ webUrl: get_Data, isLoading: false, stopRef: false });
      // Actions.refresh({ key: 'tasks', });
    });
    //    await AsyncStorage.setItem("webUrl","1")
    Actions.refresh({ key: "tasks" });
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <WebView
          source={{ uri: this.state.webUrl }}
          startInLoadingState={true}
          onLoad={() => this.setState({ isLoading: true })}
          renderLoading={() => <Loader loading={this.state.isLoading} />}
          onLoadEnd={() => this.setState({ isLoading: false })}
          onLoadProgress={() => <Loader loading={this.state.isLoading} />}
        />
        <Loader loading={this.state.isLoading} />
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "red",
  },
  textStyles: {
    fontSize: 20,
    color: "#1D4A99",
  },
  container: {
    marginTop: "5%",
  },
});
