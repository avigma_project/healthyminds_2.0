import React from "react";
import {
  View,
  Text,
  Animated,
  Image,
  Dimensions,
  ToastAndroid,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  Linking,
  Platform,
  Alert,
} from "react-native";
import * as database from "../Database/allSchemas";
const h = Dimensions.get("window").height;
height = h * 2;
import AsyncStorage from "@react-native-async-storage/async-storage";
import strings from "../Language/Language";
import { Toast } from "native-base";
import axios from "axios";
import { API } from "../Api/ApiUrl";
import { userprofile, getdata } from "../Api/function";
export default class SplashScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
    };
  }
  componentDidMount = async () => {
    setTimeout(() => this.props.navigation.navigate("Screening"), 5000); ///minal
  };

  contact = () => {
    Linking.openURL("https://www.healthelivin.org/contact-us/");
  };
  render() {
    return (
      <View style={styles.con}>
        <Animated.Image
          source={require("../Images/HealthELivingLogoTransparentBackground.png")}
          style={styles.icon}
        />
        <Animated.View style={styles.circle} />
      </View>
    );
  }
}

const styles = {
  con: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
  },
  circle: {
    width: height,
    height,
    // backgroundColor: '#4B96FB',
    borderRedius: h,
    position: "absolute",
    zIndex: -1,
  },
  icon: {
    width: "90%",
    height: "50%",
  },
};
