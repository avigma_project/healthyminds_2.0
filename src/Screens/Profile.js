import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Platform,
  BackHandler,
  Alert,
} from "react-native";
import moment from "moment";
import CustomHeader from "../CustomFolder/CustomHeader";
import { ActionSheetCustom as ActionSheet } from "react-native-actionsheet";
import ImagePicker from "react-native-image-crop-picker";
import CustomView from "../CustomFolder/CustomView";
import CustomInput from "../CustomFolder/CustomInput";
import Spinner from "react-native-loading-spinner-overlay";
import { userprofile } from "../Api/function";
import strings from "../Language/Language";
import { registerStoreImage } from "../Api/function";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Router, Scene, Stack, Tabs, Actions } from "react-native-router-flux";
import axios from "axios";

const options = [
  "Cancel",
  <View>
    {/* <EvilIcons
      name="pencil"
      size={35}
      //   style={{ marginRight: 20 }}
      // color=""
    /> */}
    <Text style={{ color: "black" }}>{strings.gallery}</Text>
  </View>,
  <Text style={{ color: "black" }}>{strings.camera}</Text>,
];

export default class Profile extends Component {
  constructor() {
    super();
    this.state = {
      defaultValue: undefined,
      base64: "",
      fileName: "image",
      imagePath: "",
      userData: {
        username: null,
        firstname: null,
        dob: "",
        userid: null,
      },
      isLoading: false,
      token: null,
      msg: null,
      UserId: null,
      null: "N/A",
      textValue: "Spanish",
    };
  }
  // backAction = () => {
  //   Alert.alert("Hold on!", "Are you sure you want to go back?", [
  //     {
  //       text: "Cancel",
  //       onPress: () => null,
  //       style: "cancel",
  //     },
  //     { text: "YES", onPress: () => BackHandler.exitApp() },
  //   ]);
  //   return true;
  // };
  componentDidMount() {
    const { navigation } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      this.getToken();
      this.getLanguage();
    });

    // this.focusListener = navigation.addListener("didFocus", () => {
    //   // The screen is focused
    //   // Call any action
    //   this.getToken();
    // });
  }

  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
      } else {
        strings.setLanguage("sp");
      }
    });
  }

  async setLanguage(languageCode) {
    console.log({ languageCode });
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        this.setState({ Ltype: 1 });
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
        this.setState({ Ltype: 2 });
      });
    }
  }

  onPress = () => {
    // alert("hiiii");
    if (this.state.textValue == "Spanish") {
      this.setState({
        textValue: "English",
      });
      this.setLanguage("sp");
    } else {
      this.setState({
        textValue: "Spanish",
      });
      this.setLanguage("en");
    }
  };

  componentWillUnmount() {
    this._unsubscribe;
    // this.backHandler();
    // this.focusListener.remove();
  }
  getUserData = async (token) => {
    this.setState({
      ErrorUserName: null,
      isLoading: true,
    });
    var data = JSON.stringify({
      Type: 2,
    });
    try {
      const res = await userprofile(data, token);
      this.setState({
        userData: {
          username: res[0][0].User_Email,
          firstname: res[0][0].User_Name,
          userid: res[0][0].User_PkeyID,
          dob: res[0][0].User_DOB,
        },
        UserId: res[0][0].User_PkeyID,
        imagePath: res[0][0].User_Image_Path,
        ErrorUserEmail: null,
        ErrorPassword: null,
        isLoading: false,
      });
    } catch (error) {
      console.log("hihihihihihih", { e: error.response.data.error });
      let message = "";
      if (error.response) {
        this.setState({ isLoading: false });
      } else {
        message = "";
      }
      console.log({ message });
    }
  };
  logOut = async (token) => {
    this.props.navigation.reset({
      index: 0,
      routes: [{ name: "Welcome" }],
    });
    await AsyncStorage.removeItem("token");
    delete axios.defaults.headers.common["TOKEN"];
    // navigation.navigate("Search", {
    //   screen: "Welcome",
    // });
  };
  onOpenImage = () => this.ActionSheet.show();
  ImageGallery = async () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
      includeBase64: true,
      multiple: false,
      compressImageQuality: 0.5,
    }).then((image) => {
      console.log(image.data);
      this.setState(
        {
          base64: image.data,
          fileName:
            Platform.OS === "ios" ? image.filename : "images" + new Date(),
          imagePath: image.path,
        },
        () => {
          this.uploadImage();
        }
      );
    });
  };
  ImageCamera = async () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
      includeBase64: true,
      multiple: false,
      compressImageQuality: 0.5,
    }).then((image) => {
      console.log(image);
      this.setState(
        {
          base64: image.data,
          fileName:
            Platform.OS === "ios" ? image.filename : "images" + new Date(),
          imagePath: image.path,
        },
        () => {
          this.uploadImage();
        }
      );
    });
  };
  uploadImage = async () => {
    const { base64 } = this.state;
    let data = JSON.stringify({
      Type: 6,
      User_Image_Base: "data:image/png;base64, " + base64,
    });
    try {
      const token = await AsyncStorage.getItem("token");
      const res = await registerStoreImage(data, token);
      console.log("resImage:", res);
    } catch (error) {
      if (error.request) {
        console.log(error.request);
      } else if (error.responce) {
        console.log(error.responce);
      } else {
        console.log(error);
      }
    }
  };

  getToken = async () => {
    let token;
    try {
      token = await AsyncStorage.getItem("token");
      if (token) {
        this.getUserData(token);
      } else {
        console.log("no token found");
      }
    } catch (e) {
      console.log(e);
    }
    console.log("userTokannnnnn", token);
  };
  // Validation = () => {
  //   this.setState({ isLoading: false });
  //   // debugger;
  //   const invalidFields = [];
  //   if (!this.state.firstname) {
  //     invalidFields.push("firstname");
  //     this.setState({ ErrorFirstName: "First Name is required" });
  //   } else {
  //     console.log("else");
  //     this.setState({ ErrorFirstName: null });
  //   }
  //   if (!this.state.username) {
  //     invalidFields.push("username");
  //     this.setState({ ErrorUserName: "Email address is required" });
  //   } else {
  //     console.log("else");
  //     this.setState({ ErrorUserName: null });
  //   }
  //   let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  //   if (reg.test(this.state.username) === false && this.state.username) {
  //     invalidFields.push("ErrorUserEmail");
  //     this.setState({ ErrorUserEmail: "Please enter valid email" });
  //   } else {
  //     this.setState({ ErrorUserEmail: null });
  //   }
  //   return invalidFields.length > 0;
  // };
  // ChangeName = () => {
  //   const validate = this.Validation();
  //   console.log("validate", validate);
  //   if (!validate) {
  //     this.setState({
  //       showfirstname: !this.state.showfirstname,
  //     });
  //   }
  // };
  // FirstNameChange = (firstname) => {
  //   this.setState({
  //     userData: {
  //       ...this.state.userData,
  //       firstname,
  //     },
  //   });
  // };
  // ChangeDob = () => {
  //   this.setState({
  //     showdob: !this.state.showdob,
  //   });
  // };
  // DOBChange = (dob) => {
  //   this.setState({
  //     userData: {
  //       ...this.state.userData,
  //       dob,
  //     },
  //   });
  // };
  // ChangeEmail = () => {
  //   const validate = this.Validation();
  //   if (!validate) {
  //     this.setState({
  //       showemail: !this.state.showemail,
  //     });
  //   }
  // };

  // EmailChange = (username) => {
  //   this.setState({
  //     userData: {
  //       ...this.state.userData,
  //       username,
  //     },
  //   });
  // };

  render() {
    const { isLoading, token, userData } = this.state;
    console.log(userData);
    return (
      <SafeAreaView>
        <View
          style={{
            backgroundColor: "#fff",
            // backgroundColor: "#E5E5E5",
            height: "100%",
          }}
        >
          <Spinner visible={this.state.isLoading} />
          <View>
            <CustomHeader
              title={strings.profile}
              save={false}
              navigation={this.props.navigation}
            />
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("EditProfile")}
              style={{
                width: 35,
                marginLeft: "85%",
                marginTop: "3%",
                position: "absolute",
                zIndex: 111,
                // backgroundColor: "red",
              }}
            >
              <Text
                style={{
                  color: "#377867",
                  fontSize: 16,
                  fontWeight: "700",
                  lineHeight: 22,
                  fontFamily: "Quicksand-Bold",
                }}
              >
                {strings.edit}
              </Text>
            </TouchableOpacity>
          </View>

          <ScrollView>
            <View>
              <View
                style={{
                  // backgroundColor: 'pink',
                  justifyContent: "center",
                  alignItems: "center",
                  marginTop: 35,
                }}
              >
                <TouchableOpacity
                  onPress={() => this.onOpenImage()}
                  style={{ position: "absolute", zIndex: 100 }}
                >
                  <Text
                    style={{
                      color: "#377867",
                      fontSize: 16,
                      fontWeight: "700",
                      lineHeight: 22,
                      fontFamily: "Quicksand-Bold",
                    }}
                  >
                    {strings.upload}
                  </Text>
                </TouchableOpacity>
                <Image
                  style={{
                    height: 200,
                    width: 200,
                    borderRadius: 150,
                    borderColor: "#BDBDBD",
                    borderWidth: 1,
                  }}
                  source={{
                    uri: this.state.imagePath
                      ? this.state.imagePath
                      : "https://t4.ftcdn.net/jpg/03/32/59/65/360_F_332596535_lAdLhf6KzbW6PWXBWeIFTovTii1drkbT.jpg",
                  }}
                />
              </View>
              <ActionSheet
                ref={(o) => (this.ActionSheet = o)}
                title={
                  <Text style={{ color: "#000", fontSize: 18 }}>
                    Profile Photo
                  </Text>
                }
                options={options}
                cancelButtonIndex={0}
                destructiveButtonIndex={4}
                useNativeDriver={true}
                onPress={(index) => {
                  if (index === 0) {
                    // cancel action
                  } else if (index === 1) {
                    this.ImageGallery();
                  } else if (index === 2) {
                    this.ImageCamera();
                  }
                }}
              />
            </View>
            <View style={{ marginTop: 20 }}>
              <View style={{ marginLeft: 17, marginBottom: 8 }}>
                <Text style={styles.heading}>{strings.name}</Text>
                <Text style={styles.value}>
                  {this.state.userData.firstname}
                </Text>
              </View>
              {/* <CustomInput
                editable={false}
                heading="Your Name"
                value={this.state.userData.firstname}
              /> */}

              {this.state.userData.dob === null ? (
                <View
                  style={{ marginLeft: 17, marginBottom: 8, marginTop: 10 }}
                >
                  <Text style={styles.heading}>{strings.ydob}</Text>
                  <Text style={styles.value}>{this.state.null}</Text>
                </View>
              ) : (
                // <CustomInput
                //   heading="Your Date of Birth"
                //   value={this.state.null}
                // />
                <View style={{ marginLeft: 17, marginBottom: 8 }}>
                  <Text style={styles.heading}>{strings.ydob}</Text>
                  <Text style={styles.value}>
                    {this.state.userData.dob
                      ? moment(this.state.userData.dob).format("MM/DD/YYYY")
                      : this.state.userData.dob}
                  </Text>
                </View>
                // <CustomInput
                //   heading="Your Date of Birth"
                //   value={
                //     this.state.userData.dob
                //       ? moment(this.state.userData.dob).format("MM/DD/YYYY")
                //       : this.state.userData.dob
                //   }
                // />
              )}
              <View style={{ marginLeft: 17, marginBottom: 8, marginTop: 10 }}>
                <Text style={styles.heading}>{strings.email}</Text>
                <Text style={styles.value}>{this.state.userData.username}</Text>
              </View>
              {/* <CustomInput
                heading="Your Email Address"
                value={this.state.userData.username}
              /> */}
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("ChangePassword")}
                style={{ marginTop: 20, marginHorizontal: "5%" }}
              >
                <Text
                  style={{
                    color: "#377867",
                    fontSize: 16,
                    fontWeight: "700",
                    lineHeight: 22,
                    fontFamily: "Quicksand-Bold",
                  }}
                >
                  {strings.changepass}
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={{
                  marginTop: 20,
                  marginBottom: 40,
                  marginHorizontal: "5%",
                }}
                onPress={() => this.logOut()}
              >
                <Text
                  style={{
                    color: "#A1683B",
                    fontSize: 18,
                    fontWeight: "700",
                    lineHeight: 24,
                    fontFamily: "Quicksand-Bold",
                  }}
                >
                  {strings.logout}
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={{
                  backgroundColor: "#F8D470",
                  paddingHorizontal: 12,
                  paddingVertical: 8,
                  width: "50%",
                  justifyContent: "center",
                  alignItems: "center",
                  borderRadius: 20,

                  marginHorizontal: "5%",
                  marginBottom: 50,
                }}
                onPress={this.onPress}
              >
                <Text
                  style={{
                    color: "#A1683A",
                    fontWeight: "bold",
                    lineHeight: 20,
                    fontSize: 14,
                    fontFamily: "Quicksand-Bold",
                  }}
                >
                  {this.state.textValue}
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  errortext: {
    color: "red",
    marginTop: 5,
    marginLeft: "10%",
  },
  heading: {
    fontSize: 16,
    lineHeight: 22,
    color: "#060D0B",
    fontFamily: "verdana",
    fontWeight: "400",
  },
  value: {
    fontSize: 15,
    lineHeight: 20,
    color: "#060D0B",
    fontFamily: "verdana",
    fontWeight: "400",
    marginTop: 10,
    // borderBottomWidth: 1,
  },
});
