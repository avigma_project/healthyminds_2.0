import React, { Component } from "react";
import { View, Text, StyleSheet, Platform, Alert } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Picker, Form } from "native-base";
import * as database from "../Database/allSchemas";
import { connect } from "react-redux";
import ScoreView from "./ScoreView";
import { addScore } from "../actions/score";
export let selectedHDST = [];
export let newArray = [];
export let showAlert = false;
// export let selectedHDST =''
import DropDownPicker from "react-native-dropdown-picker";
import strings from "../Language/Language";

class PickerExample extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
      TotalScore: 0,
      selectedPK: 0,
    };
  }
  componentDidMount() {
    this.getLanguage();
  }
  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
        this.setLanguage("en");
      } else {
        strings.setLanguage("sp");
        this.setLanguage("sp");
      }
    });
  }
  async setLanguage(languageCode) {
    console.log(languageCode);
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
      });
    }

    Actions.refresh({ key: "tasks" });
    // getLanguage(languageCode)
  }

  updatevalue = (value) => {
    //debugger
    selectedHDST = [];

    if (this.props.selected_value == 8) {
      const { valueUpdate } = this.props;
      valueUpdate(value);
      showAlert = true;
    }
    var patient_depression;
    this.setState({ value: value });
    var value = Number(value);
    var Score = value;
    var Staff_Score = value;
    var DST_PKeyID = this.props.selected_value;
    var ldata = { value, DST_PKeyID, Score, Staff_Score };
    var subval = 0;
    selectedHDST.push(ldata);
    this.setState({ selectedPK: DST_PKeyID });

    database
      .UpdatePatientDepressionMaster(selectedHDST)
      .catch((error) => console.error(error));

    database.queryAllPatient_Depression().then((res) => {
      const result = Object.values(res[0]);
      console.log("resultresult", result);

      for (var i = 0; i < result.length; i++) {
        subval = subval + result[i].Score;
      }
      console.log("sub val ", subval);
      this.setState({ TotalScore: subval }, () => {
        console.log("Set Total Score", this.state.TotalScore);
        this.props.score(subval);
        let data = {
          Id: 1,
          Title: "Depression Screening",
          Score: this.state.TotalScore,
        };
        database
          .InsertFinalScores(data)
          .then((data) =>
            console.log(`Depression screening: ${JSON.stringify(data)}`)
          )
          .catch((error) =>
            console.log(`Error in depression screening: ${error}`)
          );
      });
    });

    this.showAlert(DST_PKeyID, value);
    // if (DST_PKeyID === 8) {
    //   if (value === 1) {
    //     showAlert = true
    //       // Alert.alert("", strings.emergency, [{ text: "RETURN TO SCREENING" }], {
    //       //     cancelable: false,
    //       // }, 3000000000);
    //   }
    //   if (value === 2) {
    //     showAlert = true
    //       // Alert.alert("", strings.emergency, [{ text: "RETURN TO SCREENING" }], {
    //       //     cancelable: false,
    //       // }, 3000000000);
    //   }
    //   if (value === 3) {
    //     showAlert = true
    //       // Alert.alert("", strings.emergency, [{ text: "RETURN TO SCREENING" }], {
    //       //     cancelable: false,
    //       // }, 3000000000);
    //   }
    // }
  };
  showAlert = async (DST_PKeyID, value) => {
    if (DST_PKeyID === 8) {
      if (value === 1 && value === 2 && value === 3) {
        await AsyncStorage.setItem("alert", "1");
      }
    }
  };

  // showAlert(value){
  //     if (this.state.selectedPK === 8) {
  //           if (value.Staff_Score === 1) {
  //               Alert.alert("", strings.emergency, [{ text: "RETURN TO SCREENING" }], {
  //                   cancelable: false,
  //               }, 3000000000);
  //           }
  //           if (value.Staff_Score === 2) {
  //               Alert.alert("", strings.emergency, [{ text: "RETURN TO SCREENING" }], {
  //                   cancelable: false,
  //               }, 3000000000);
  //           }
  //           if (value.Staff_Score === 3) {
  //               Alert.alert("", strings.emergency, [{ text: "RETURN TO SCREENING" }], {
  //                   cancelable: false,
  //               }, 3000000000);
  //           }
  //         }
  //   }

  render() {
    return (
      <View>
        <View>
          <Picker
            style={styles.pickerStyle}
            mode="dropdown"
            selectedValue={
              this.props.defaultSelectedValue
                ? this.props.defaultSelectedValue
                : this.state.value
            }
            placeholder={strings.selectOptions}
            onValueChange={this.updatevalue}
          >
            {/* <Picker.Item label="Please select an option..." value={0} /> */}
            <Picker.Item label={strings.little} value={0} />
            <Picker.Item label={strings.Some} value={1} />
            <Picker.Item label={strings.Most} value={2} />
            <Picker.Item label={strings.All} value={3} />
          </Picker>
          {/* <DropDownPicker
            items={[
              { label: 'None or little of the time', value: 0 },
              { label: 'Some of the time', value: 1 },
              { label: 'Most of the time', value:2 },
              { label: 'All of the time', value:3 },

            ]}
            defaultValue={this.props.defaultSelectedValue
              ? this.props.defaultSelectedValue
              : this.state.value}
            containerStyle={{ backgroundColor: 'black', marginRight: '5%' }}
            style={{ backgroundColor: '#fafafa', }}
            // dropDownStyle={{ backgroundColor: "red" }}
            dropDownMaxHeight={'100%'}
            // containerStyle={{height:'0%'}}
            dropDownStyle={{ padding: "10%" }}
            onChangeItem={item => this.updatevalue(item.value)}

          /> */}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    alignSelf: "center",
    color: "red",
  },
  pickerStyle: {
    justifyContent: "center",
    fontSize: 12,
    // borderBottomWidth:1
  },
});

const mapStateToProps = (state) => {
  console.log("HSDTool state: ", state);
  return {
    score: state.scoreReducer.score,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    score: (score) => dispatch(addScore(score)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PickerExample);
