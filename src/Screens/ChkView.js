import React from "react";
import { View, TextInput, CheckBox } from "react-native";

export const chkboxDetails = [];

export default class ChkView extends React.Component {
  state = { value: false, show: false, Treated: "", selectedOptions: "" };

  updatevalue = (value) => {
    //debugger;
    console.log("selected_value", this.props.selected_value);
    this.setState({ value: value });
    var MDQ_Option = this.props.selected_value.value;
    var Id = this.props.selected_value.id;
    var value = value;
    var data = { Id, MDQ_Option, value };

    chkboxDetails.push(data);
    console.log(chkboxDetails);
  };

  render() {
    return (
      <View>
        <View>
          <CheckBox
            // value={this.props.showChecked ? true : this.state.value}
            value={this.state.value}
            onValueChange={(data) => this.updatevalue(data)}
          />
        </View>
      </View>
    );
  }
}
// export function inputView() {
//     if (this.state.show) {
//         return (
//             <View>
//                 <TextInput
//                     placeholder="Treated for ?"
//                     style={{ marginLeft: 10, fontSize: 16, width: '90%' }}
//                     onChangeText={(Treated) => { this.setState({ Treated }) }}
//                     underlineColorAndroid="black"
//                 />
//             </View>
//         )
//     }
// }
