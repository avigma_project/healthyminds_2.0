import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Linking,
} from "react-native";
import CustomButton from "../CustomFolder/CustomButton";
import Header from "../CustomFolder/Header";
import strings from "../Language/Language";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { userprofile } from "../Api/function";

export default class DashBoard extends Component {
  state = {
    textValue: "Spanish",
    firstname: null,
    isLoading: false,
  };
  componentDidMount() {
    const { navigation } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      this.getToken();
    });

    // this.focusListener = navigation.addListener("didFocus", () => {
    //   // The screen is focused
    //   // Call any action
    //   this.getToken();
    // });
  }
  componentWillUnmount() {
    this._unsubscribe;

    // this.backHandler.remove();
    // this.focusListener.remove();
  }
  // componentDidMount() {
  //   this.getLanguage();
  // }
  getUserData = async (token) => {
    this.setState({
      isLoading: true,
    });
    var data = JSON.stringify({
      Type: 2,
    });
    try {
      const res = await userprofile(data, token);
      this.setState({
        firstname: res[0][0].User_Name,
        isLoading: false,
      });
    } catch (error) {
      console.log("hihihihihihih", { e: error.response.data.error });
      let message = "";
      if (error.response) {
        this.setState({ isLoading: false });
      } else {
        message = "";
      }
      console.log({ message });
    }
  };
  getToken = async () => {
    let token;
    try {
      token = await AsyncStorage.getItem("token");
      if (token) {
        this.getUserData(token);
      } else {
        console.log("no token found");
      }
    } catch (e) {
      console.log(e);
    }
    console.log("userTokannnnnn", token);
  };
  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
      } else {
        strings.setLanguage("sp");
      }
    });
  }

  async setLanguage(languageCode) {
    console.log({ languageCode });
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        this.setState({ Ltype: 1 });
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
        this.setState({ Ltype: 2 });
      });
    }
  }

  onPress = () => {
    // alert("hiiii");
    if (this.state.textValue == "Spanish") {
      this.setState({
        textValue: "English",
      });
      this.setLanguage("sp");
    } else {
      this.setState({
        textValue: "Spanish",
      });
      this.setLanguage("en");
    }
  };
  contact = () => {
    Linking.openURL("https://www.healthelivin.org/contact-us/");
  };

  render() {
    return (
      <SafeAreaView>
        <View
          style={{
            height: "100%",
            backgroundColor: "#fff",
            // backgroundColor: "#E5E5E5"
          }}
        >
          <View
            style={{
              width: "100%",
              // backgroundColor: 'red',
              justifyContent: "flex-end",
              flexDirection: "row",
              marginTop: 8,
              marginBottom: 16,
            }}
          >
            <TouchableOpacity
              style={{
                backgroundColor: "#F8D470",
                paddingHorizontal: 12,
                paddingVertical: 8,
                // width: '20%',
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 20,
                marginRight: 20,
              }}
              onPress={this.onPress}
            >
              <Text
                style={{
                  color: "#A1683A",
                  fontWeight: "bold",
                  lineHeight: 20,
                  fontSize: 14,
                  fontFamily: "Quicksand-Bold",
                }}
              >
                {this.state.textValue}
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              justifyContent: "center",
              // backgroundColor: 'red',
              //   alignItems: 'center',
              marginLeft: 16,
            }}
          >
            <View style={{ marginBottom: 16 }}>
              <Text
                style={{
                  fontSize: 25,
                  fontWeight: "700",
                  lineHeight: 32,
                  fontFamily: "Quicksand-Bold",
                }}
              >
                {strings.dashwelcome}, {this.state.firstname}!
              </Text>
            </View>
            <Text
              style={{
                fontSize: 16,
                fontWeight: "400",
                lineHeight: 22,
                // //fontFamily: "OpenSans-Regular",
              }}
            >
              {strings.dashwelcometext}
            </Text>
          </View>
          <View style={{ marginHorizontal: 33 }}>
            <View style={{ marginTop: 32 }}>
              <CustomButton
                title={strings.button2}
                redius={10}
                width="100%"
                height={55}
              />
            </View>
            <View style={{ marginTop: 24 }}>
              <CustomButton
                title={strings.button3}
                redius={10}
                width="100%"
                height={55}
                onPress={() => this.props.navigation.navigate("GetStarted")}
              />
            </View>
            <View style={{ marginTop: 24 }}>
              <CustomButton
                title={strings.button4}
                redius={10}
                width="100%"
                height={55}
              />
            </View>
          </View>
          <View
            style={{
              //   flexDirection: 'row',
              // backgroundColor: 'red',
              justifyContent: "center",
              marginTop: 32,
              alignItems: "center",
            }}
          >
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("Search", {
                  screen: "Profile",
                })
              }
            >
              <Text
                style={{
                  color: "#377867",
                  fontSize: 18,
                  fontWeight: "700",
                  lineHeight: 24,
                  fontFamily: "Quicksand-Bold",
                }}
              >
                {strings.gotoprofile}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ marginTop: 32 }}
              onPress={() =>
                this.props.navigation.navigate("Search", {
                  screen: "Login",
                })
              }
            >
              <Text
                style={{
                  color: "#A1683B",
                  fontSize: 18,
                  fontWeight: "700",
                  lineHeight: 24,
                  fontFamily: "Quicksand-Bold",
                }}
              >
                {strings.logout}
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{ position: "absolute", bottom: 32, alignSelf: "center" }}
          >
            <View>
              <TouchableOpacity onPress={() => this.contact()}>
                <Text
                  style={{
                    color: "#A1683B",
                    fontWeight: "700",
                    lineHeight: 24,
                    fontFamily: "Quicksand-Bold",
                  }}
                >
                  {strings.contact}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
