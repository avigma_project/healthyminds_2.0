import React, { Component } from "react";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  ScrollView,
} from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import CustomHeader from "../CustomFolder/CustomHeader";
import CustomInput from "../CustomFolder/CustomInput";
import CustomButton from "../CustomFolder/CustomButton";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from "moment";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { userprofile } from "../Api/function";
import { updateprofile } from "../Api/function";
import { Toast } from "native-base";
import strings from "../Language/Language";

export default class EditProfile extends Component {
  constructor() {
    super();
    this.state = {
      isDateTimePickerVisible: false,
      userData: {
        username: null,
        chosenDate: "",
        firstname: null,
        userid: null,
      },
      ErrorUserName: null,
      ErrorUserEmail: null,
      ErrorName: null,
      isLoading: false,
      // token: null,
      UserId: null,
      null: "N/A",
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      this.getToken();
    });

    // this.focusListener = navigation.addListener("didFocus", () => {
    //   // The screen is focused
    //   // Call any action
    //   this.getToken();
    // });
  }
  componentWillUnmount() {
    this._unsubscribe;

    // this.focusListener.remove();
  }
  getUserData = async (token) => {
    this.setState({
      ErrorUserName: null,
      isLoading: true,
    });
    var data = JSON.stringify({
      Type: 2,
    });
    try {
      const res = await userprofile(data, token);
      console.log("res: user profile", res[0][0]);
      this.setState({
        userData: {
          username: res[0][0].User_Email,
          firstname: res[0][0].User_Name,
          userid: res[0][0].User_PkeyID,
          chosenDate: res[0][0].User_DOB,
        },
        UserId: res[0][0].User_PkeyID,
        imagePath: res[0][0].User_Image_Path,
        ErrorUserEmail: null,
        ErrorPassword: null,
        isLoading: false,
      });
    } catch (error) {
      console.log("hihihihihihih", { e: error.response.data.error });
      let message = "";
      if (error.response) {
        this.setState({ isLoading: false });
      } else {
        message = "";
      }
      console.log({ message });
    }
  };

  onUsernameChange = (username) => {
    this.setState({
      userData: {
        ...this.state.userData,
        username,
      },
    });
  };

  onNameChange = (firstname) => {
    this.setState({
      userData: {
        ...this.state.userData,
        firstname,
      },
    });
  };

  Validation = () => {
    this.setState({ isLoading: false });
    const { firstname, username, password, cpassword } = this.state.userData;
    console.log("hihihihiihihhi", this.state.userData);
    // debugger;
    const invalidFields = [];
    if (!firstname) {
      invalidFields.push("firstname");
      this.setState({ ErrorName: "Name is required" });
    } else {
      console.log("else");
      this.setState({ ErrorName: null });
    }
    if (!username) {
      invalidFields.push("username");
      this.setState({ ErrorUserName: "Email address is required" });
    } else {
      console.log("else");
      this.setState({ ErrorUserName: null });
    }
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(this.state.username) === false && this.state.username) {
      invalidFields.push("ErrorUserEmail");
      this.setState({ ErrorUserEmail: "Please enter valid email" });
    } else {
      this.setState({ ErrorUserEmail: null });
    }
    return invalidFields.length > 0;
  };
  getToken = async () => {
    let token;
    try {
      token = await AsyncStorage.getItem("token");
      if (token) {
        // await AsyncStorage.removeItem("token");
        this.getUserData(token);
        // console.log(token);
      } else {
        console.log("no token found");
      }
    } catch (e) {
      console.log(e);
    }
    // console.log("userTokannnnnn", token);
  };
  UpdateProfileData = async () => {
    let token;
    this.setState({ isLoading: false });
    const validate = this.Validation();

    if (!validate) {
      const { username, firstname, userid, chosenDate } = this.state.userData;

      this.setState({ isLoading: true });
      let data = {
        User_PkeyID: userid,
        User_Email: username,
        User_DOB: chosenDate,
        User_Name: firstname,
        Type: 2,
      };
      console.log("dataaaaaa", data);

      try {
        token = await AsyncStorage.getItem("token");
        const res = await updateprofile(data, token);
        console.log("resssssssssssssssssss: ", res);
        this.setState({ isLoading: false });
        this.showMessage("User updated successfully");
        this.props.navigation.navigate("WelcomeScreen");
      } catch (error) {
        console.log({ e: error.response.data.error });
        let message = "";
        if (error.request) {
          this.setState({ isLoading: false });
          this.showMessage("Unable to Update ,error in request");
        }
        if (error.response) {
          const {
            data: { error_description },
          } = error.response;
          message = error_description;
          if (error.response.data.error === "-99") {
            this.showMessage("User is already exist");
          }
          this.setState({ isLoading: false });
        }
        if (error) {
          this.setState({ isLoading: false });
          this.showMessage("Unable to Update");
        }
        console.log({ message });
      }
    }
  };

  showMessage = (message) => {
    if (message !== "" && message !== null && message !== undefined) {
      Toast.show({
        text: message,
        // style: styles.toasttxt,
        duration: 3000,
      });
    }
  };

  ShowDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  HideDateTimePicker = () =>
    this.setState({
      isDateTimePickerVisible: false,
    });

  HandleDatePicked = (date) => {
    console.log(date, "dadadd");
    this.setState({
      userData: {
        ...this.state.userData,
        chosenDate: moment(date).format(""),
      },
      isDateTimePickerVisible: false,
    });
  };

  render() {
    const { isLoading, token, userData } = this.state;
    console.log("renderData", userData);
    return (
      <SafeAreaView>
        <ScrollView
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{ height: "100%", backgroundColor: "white" }}
        >
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
              height: 50,
            }}
          >
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack()}
              style={{
                width: "5%",
                right: 15,
                // backgroundColor: 'red'
              }}
            >
              <AntDesign name="left" size={30} />
            </TouchableOpacity>
            <View
              style={{
                width: "80%",
                alignItems: "center",
                right: 10,
                // backgroundColor: 'green',
              }}
            >
              <Text
                style={{
                  fontFamily: "Quicksand-Bold",
                  lineHeight: 26,
                  color: "#060D0B",
                  fontWeight: "bold",
                  fontSize: 18,
                }}
              >
                {strings.editprofile}
              </Text>
            </View>
          </View>
          <View style={{ marginHorizontal: 16 }}>
            <CustomInput
              placeholder={strings.name}
              heading={strings.nam}
              value={this.state.userData.firstname}
              placeholderTextColor="gray"
              onChangeText={this.onNameChange}
            />
          </View>
          <View style={{ marginHorizontal: 16 }}>
            {this.state.userData.chosenDate === null ? (
              <CustomInput
                placeholder={strings.ydob}
                heading={strings.dbirth}
                value={this.state.null}
                // value={this.state.userData.chosenDate}
                placeholderTextColor="gray"
                // onChangeText={() => this.HandleDatePicked()}
                // onEndEditing={this.ChangeDob}
              />
            ) : (
              <CustomInput
                placeholder={strings.ydob}
                heading={strings.dbirth}
                value={
                  this.state.userData.chosenDate
                    ? moment(this.state.userData.chosenDate).format(
                        "MM/DD/YYYY"
                      )
                    : this.state.userData.chosenDate
                }
                // value={this.state.userData.chosenDate}
                placeholderTextColor="gray"
                // onChangeText={() => this.HandleDatePicked()}
                // onEndEditing={this.ChangeDob}
              />
            )}

            <TouchableOpacity
              onPress={this.ShowDateTimePicker}
              style={{
                position: "absolute",
                right: 15,
                top: 52,
              }}
            >
              <AntDesign name="calendar" size={25} color="#8c8c8c" />
            </TouchableOpacity>
            <DateTimePickerModal
              isVisible={this.state.isDateTimePickerVisible}
              onConfirm={this.HandleDatePicked}
              onCancel={this.HideDateTimePicker}
            />
          </View>
          <View style={{ marginHorizontal: 16 }}>
            <CustomInput
              placeholder={strings.email}
              heading={strings.emal}
              value={this.state.userData.username}
              placeholderTextColor="gray"
              // onChangeText={this.onUsernameChange}
            />
          </View>

          <View style={{ marginTop: 35 }}>
            <CustomButton
              title={strings.save}
              redius={40}
              width={230}
              height={45}
              onPress={() => this.UpdateProfileData()}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
