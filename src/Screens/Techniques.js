import React from "react";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  PixelRatio,
  FlatList,
  TextInput,
  ScrollView,
  Linking,
} from "react-native";
import { Actions } from "react-native-router-flux";
// import Picker, { selectedHDST } from './PickerView';
import * as database from "../Database/allSchemas";
// import Datafile from '../reducers/Datafile';
// import {connect} from 'react-redux';
// import { emptyScore } from "../actions/score"
import { Card } from "react-native-elements";
// import selectedHDST from './Picker';
import ScoreView from "./ScoreView";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default class HDSTool extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      result: [],
    };
  }

  componentDidMount() {
    this.getData();
  }
  getData() {
    //debugger;
    database.queryTechniques_List_SchemaDetails().then((res) => {
      const result = Object.values(res[0]);
      console.log("result", result);
      this.setState({ result: result });
    });
  }

  render_Item(item, index) {
    // Added TIP_Desc instead of TIP_Web_URL as mentioned by Sandip
    return (
      <TouchableOpacity onPress={() => Linking.openURL(item.TIP_Desc)}>
        <Card>
          <View style={styles.container}>
            <Text
              style={{
                color: "#1D4A99",
                fontWeight: "bold",
                fontFamily: "Quicksand-Bold",
              }}
            >
              {item.TIP_Name}
            </Text>
            <Text style={{ marginTop: "3%", color: "gray" }}>
              {item.TIP_Desc}
            </Text>
          </View>
        </Card>
      </TouchableOpacity>
    );
  }
  render() {
    return (
      <ScrollView style={{ flex: 1 }}>
        {/* <View style={styles.container}> */}
        <FlatList
          data={this.state.result}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => this.render_Item(item, index)} //(this.treatedChkbox(item),console.log('depression',item))}
        />
        {/* </View> */}
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
  },
  textStyles: {
    fontSize: 20,
    color: "#1D4A99",
  },
  container: {
    marginTop: "5%",
  },
});

// const mapStateToProps = (state) => {
//     console.log("HSDTool state: ", state);
//     return {
//         score: state.scoreReducer.score
//     }
// }

// const mapDispatchToProps = (dispatch) => {
//   return {
//     remove: () => dispatch(emptyScore())
//   }
// }

// export default connect(mapStateToProps, mapDispatchToProps)(HDSTool);
