import React from "react";
import { SafeAreaView, View, Button } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { SliderBox } from "react-native-image-slider-box";
import { Actions } from "react-native-router-flux";
import strings from "../Language/Language";

export default class Selectlanguage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [
        require("../Images/screen1.png"),
        require("../Images/screen2.jpg"),
        require("../Images/screen3.jpg"),
        require("../Images/screen4.png"),
        require("../Images/screen5.png"),
      ],
      Ltype: 1,

      spanish: "sp",
      english: "en",
      selected: "#4B96FB",
      unSelcted: "#888888",
    };
  }
  // componentDidMount(){
  //     this.getLanguage()
  // }

  async setLanguage(languageCode) {
    // console.log(languageCode)
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        // console.log("refresh", get_Data);

        this.setState({
          Ltype: 1,
          language: false,
          selected: "#4B96FB",
          unSelcted: "#888888",
        });
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
        this.setState({
          Ltype: 2,
          language: false,
          selected: "#888888",
          unSelcted: "#4B96FB",
        });
      });
    }
    // this.GetMasterDetails();
    Actions.Welcome();
    // getLanguage(languageCode)
  }

  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      // console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
        this.setLanguage("en");
      } else {
        if (get_Data == "2") {
          strings.setLanguage("sp");
          this.setLanguage("sp");
        } else {
          strings.setLanguage("en");
          this.setLanguage("en");
        }
      }
    });
  }
  render() {
    return (
      <SafeAreaView style={{ alignSelf: "center", width: "90%", flex: 1 }}>
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            height: "60%",
          }}
        >
          <SliderBox
            images={this.state.images}
            onCurrentImagePressed={(index) =>
              console.warn(`image ${index} pressed`)
            }
            ImageComponentStyle={{ height: "100%" }}
            resizeMethod={"resize"}
            resizeMode={"stretch"}
            height="100%"
            autoplay
            dotStyle={{
              width: 10,
              height: 10,
              borderRadius: 5,
              marginHorizontal: 0,
              padding: 0,
              margin: 0,
              backgroundColor: "rgba(128, 128, 128, 0.92)",
            }}
            inactiveDotColor="#90A4AE"
          />
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            marginTop: "30%",
            width: "100%",
          }}
        >
          <View
            style={{
              alignItems: "center",
              marginTop: "5%",
              backgroundColor: this.state.selected,
              width: "40%",
              alignSelf: "center",
              padding: "3%",
            }}
          >
            <Button
              onPress={() => this.setLanguage("en")}
              // onPress={() => Actions.Welcome({language:this.state.english})}
              title="English"
              color="#ffffff"
              style={{
                alignItems: "center",
                marginTop: "5%",
                width: "50%",
                alignSelf: "center",
                padding: "3%",
              }}
            />
          </View>
          <View
            style={{
              alignItems: "center",
              marginTop: "5%",
              backgroundColor: this.state.unSelcted,
              width: "40%",
              alignSelf: "center",
              padding: "3%",
            }}
          >
            <Button
              onPress={() => this.setLanguage("sp")}
              // onPress={() => Actions.Welcome({language:this.state.spanish})}
              title="Spanish"
              color="#ffffff"
              style={{
                alignItems: "center",
                marginTop: "5%",
                width: "50%",
                alignSelf: "center",
                padding: "3%",
              }}
              // style={styles.button}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
