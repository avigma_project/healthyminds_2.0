import React from "react";
import { View, Text, Picker } from "react-native";
import * as database from "../Database/allSchemas";

export const surveyList = [];

class SurveyPicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      PickerValueHolder: "",
    };
  }
  updateValue = (value) => {
    //debugger;
    this.setState({ PickerValueHolder: value });
    var Sur_Value = value;
    var SUR_PKeyID = this.props.selectedQuest;
    let ldata = { SUR_PKeyID, Sur_Value };

    surveyList.push(ldata);

    database.UpdatePatientSurveyMaster_final(surveyList).catch((error) => {
      console.log(error);
    });
  };
  render() {
    return (
      <View>
        <Picker
          selectedValue={this.state.PickerValueHolder}
          onValueChange={this.updateValue}
          // onValueChange={(itemValue, itemIndex) => this.setState({ PickerValueHolder: itemValue })}
        >
          {this.props.data != null
            ? this.props.data.map((item, key) => (
                <Picker.Item
                  label={item.sur_OPT_VAL}
                  value={item.sur_OPT_VAL}
                  key={key}
                />
              ))
            : null}
        </Picker>
      </View>
    );
  }
}
export default SurveyPicker;
