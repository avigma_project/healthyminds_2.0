import React, { PureComponent } from "react";
import {
  View,
  Text,
  Platform,
  Dimensions,
  Button,
  TouchableOpacity,
  Keyboard,
  StyleSheet,
  BackHandler,
  PixelRatio,
  FlatList,
  TextInput,
  ScrollView,
  ToastAndroid,
  SafeAreaView,
  Alert,
} from "react-native";
import { Button as NativeBaseButton } from "native-base";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Actions } from "react-native-router-flux";
import { Picker } from "native-base";
import FlipToggle from "react-native-flip-toggle-button";
// import Picker from '@react-native-community/picker'
import { CheckBox } from "native-base";
import * as database from "../Database/allSchemas";
import { Datafile } from "../reducers/Datafile";
import Toast from "react-native-simple-toast";
import CheckboxView, { chkbox, inputView, show_View } from "./CheckboxView";
// import {show_ViewTextInput} from '../Database/Helper';
import DropDownPicker from "react-native-dropdown-picker";
// import ModalPicker from 'react-native-modal-picker'
// import ModalDropdown from 'react-native-modal-dropdown';
import strings from "../Language/Language";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const ldata = [];

export default class PatientDeatils extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      identity: "",
      partnership: "",
      ethnic: "",
      suicide: "",
      medication: "",
      isChecked_suicide: false,
      isChecked_medication: false,
      treatment: [],
      value: "",
      chkbox: false,
      checkbox: [],
      updateChkbox: [],
      patname: "",
      zipcode: "",
      age: "",
      Treated: "",
      show: false,
      other_val_show: false,
      value_other: "",
      err: "",
    };
    // console.log("checking value let clear 1",this.state.identity)
    // console.log("checking value let clear 1",this.state.identity)
    // this.setState({
    //     identity: '', partnership: '', ethnic: '', isChecked_suicide: false,
    //     isChecked_medication: false
    // })
  }
  // componentWillMount(){
  //     this.getData()
  // }
  componentDidMount() {
    //debugger
    // this.getBasicData()
    console.log("componentDidMount called");

    this.setState({
      identity: "",
      partnership: "",
      ethnic: "",
      isChecked_suicide: false,
      isChecked_medication: false,
    });
    this.getLanguage();
    this.getData();

    // console.log('props', this.props.patname, this.props.age, this.props.zipcode);
  }

  async getLanguage() {
    await AsyncStorage.getItem("refresh", (err, get_Data) => {
      console.log("refresh", get_Data);
      if (get_Data == "1" && get_Data != null) {
        strings.setLanguage("en");
        this.setLanguage("en");
      } else {
        strings.setLanguage("sp");
        this.setLanguage("sp");
      }
    });
  }
  async setLanguage(languageCode) {
    console.log(languageCode);
    strings.setLanguage(languageCode);
    await AsyncStorage.setItem("language", languageCode);
    if (languageCode == "en") {
      await AsyncStorage.setItem("refresh", "1");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
      });
    } else {
      await AsyncStorage.setItem("refresh", "2");
      await AsyncStorage.getItem("refresh", (err, get_Data) => {
        console.log("refresh", get_Data);
      });
    }
    this.getData();
    Actions.refresh({ key: "tasks" });
    // getLanguage(languageCode)
  }

  async getBasicData() {
    //debugger
    try {
      await database
        .queryBaseDetails()
        .then((res) => {
          const result = Object.values(res[0]);
          this.setState({
            patname: result[0].Name,
            age: result[0].age,
            zipcode: result[0].zipcode,
          });
          console.log("base", result);
        })
        .catch((error) => console.log(error));
    } catch (error) {
      console.log(error);
    }
  }

  async componentDidUpdate() {
    console.log("componentDidUpdate method is called");

    await AsyncStorage.getItem("PatientDetials", (err, ldata) => {
      console.log("AsyncStorage profile PatientDetials", ldata);

      // if (ldata == "1") {
      //     console.log("Profile+++++", "Checking profile");
      //     AsyncStorage.setItem("PatientDetials", "0");
      //     this.getData()

      // }
      if (ldata == "2") {
        AsyncStorage.setItem("PatientDetials", "0");
        this.setState({
          identity: "",
          partnership: "",
          ethnic: "",
          isChecked_suicide: false,
          isChecked_medication: false,
        });
      }
    });
  }

  getData() {
    //debugger
    try {
      database
        .queryBaseDetails()
        .then((res) => {
          const result = Object.values(res[0]);
          // console.log({ result });
          this.setState({
            patname: result[0].Name,
            age: result[0].age,
            zipcode: result[0].zipcode,
          });
          console.log("base", result);
        })
        .catch((error) => console.log(error));

      database
        .queryAllPatient_Treated_Schema()
        .then((res) => {
          const result = Object.values(res[0]);

          console.log("Checking values", JSON.stringify(result));

          this.setState({ treatment: result });
        })
        .catch((error) => console.log(error));

      database
        .queryAllPatient_Master()
        .then((res) => {
          const result = Object.values(res[0]);
          console.log("back ++++++++++++++", result);
          if (result != []) {
            this.setState({
              identity: result[0].Gender,
              partnership: result[0].partnership_Status,
              update: true,
              ethnic: result[0].Ethnic_identity,
              isChecked_medication: result[0].Medication,
              isChecked_suicide: result[0].Suicide,
            });
          } else {
          }
        })
        .catch((error) => console.log(error));
    } catch (error) {
      console.log(error);
    }
    // database
    // .queryAllPatient_Treated_Schema()
    // .then(res => {
    //     const result = Object.values((res[0]));
    // }).catch(error => console.log(error));
  }

  setSelectedValue(value, index) {
    // https://stackoverflow.com/questions/35397678/bind-picker-to-list-of-picker-item-in-react-native
    // map for picker
    Keyboard.dismiss();
    console.warn(value, index);
    //debugger;
    if (index == 1) {
      if (value != "") {
        this.setState({ identity: value });
      } else {
        this.setState({ identity: 1 });
      }
    } else if (index == 2) {
      if (value != "") {
        this.setState({ partnership: value });
      } else {
        this.setState({ partnership: 1 });
      }
    } else if (index == 3) {
      if (value != "") {
        this.setState({ ethnic: value });
      } else {
        this.setState({ ethnic: 1 });
      }
    } else {
    }
  }
  // checkboxVal() {

  // }
  isCheckedValue(value, index) {
    // https://stackoverflow.com/questions/54111540/react-native-checkbox-unable-to-uncheck-the-checked-box
    //debugger;
    console.warn(value, index);
    if (index == 1) {
      this.setState({ isChecked_medication: value });
    } else if (index == 2) {
      this.setState({ isChecked_suicide: value });
      if (value) {
        Alert.alert("", strings.emergency, [{ text: "RETURN TO SCREENING" }], {
          cancelable: false,
        });
      }
    }
  }
  validateCheckBox(value) {
    console.log("checkbox", value);
    const chkbox = [
      {
        Patid: null,
        Treated_Master_IsActive: value.Treated_Master_IsActive,
        Treated_Master_Option: value.Treated_Master_Option,
        Treated_Master_PKeyId: value.Treated_Master_PKeyId,
        Type: null,
        other_Val: null,
        value: null,
      },
    ];
    // this.setState({ chkbox: !this.state.chkbox }),console.log('box',this.state.chkbox)
    this.setState({ chkbox: !this.state.chkbox });
    if (value != "") {
      this.setState({ checkbox: chkbox });
    }
  }
  // checkbox render item
  treatedChkbox(data) {
    //debugger
    console.log("chk", data);
    // console.warn('chk',data.Treated_Master_IsActive);
    if (data.Treated_Master_PKeyId <= 12) {
      return (
        <View
          style={{
            flexDirection: "row",
            width: (deviceWidth * 80) / 100,
            marginTop: "2%",
          }}
        >
          <View>
            <CheckboxView selected_value={data} showChecked={data.value} />
          </View>
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              marginLeft: "4%",
            }}
          >
            <Text style={{ fontSize: 16 }} numberOfLines={2}>
              {data.Treated_Master_Option}
            </Text>
          </View>
        </View>
      );
    } else {
      if (data.Treated_Master_PKeyId > 13 && data.Treated_Master_PKeyId != 26) {
        return (
          <View
            style={{
              flexDirection: "row",
              width: (deviceWidth * 80) / 100,
              marginTop: "2%",
            }}
          >
            <View>
              <CheckboxView selected_value={data} showChecked={data.value} />
            </View>
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                marginLeft: "4%",
              }}
            >
              <Text style={{ fontSize: 16 }} numberOfLines={2}>
                {data.Treated_Master_Option}
              </Text>
            </View>
          </View>
        );
      }
    }
  }

  // validate next button
  async validateUpdate() {
    // console.log(chkbox);
    this.checkUpdate();
    console.log(chkbox);
    if (this.state.identity != "" && this.state.identity > "0") {
      if (this.state.partnership != "" && this.state.partnership > "0") {
        if (this.state.ethnic != "" && this.state.ethnic > "0") {
          let data = {
            Id: 1,
            Name: this.state.patname,
            Zip: this.state.zipcode,
            age: this.state.age,
            Gender: this.state.identity,
            partnership_Status: this.state.partnership,
            Ethnic_identity: this.state.ethnic,
            Suicide: this.state.isChecked_suicide,
            Medication: this.state.isChecked_medication,
          };
          console.log("zipcode", data);
          database.deletePatientMaster().catch((error) => console.log(error));

          database
            .InsertPatientMaster(data)
            .catch((error) => console.log(error));
          database
            .UpdatePatientTreatedMaster(chkbox)
            .catch((error) => console.log(error));

          // this.setState({
          //     identity: '', partnership: '', ethnic: '', isChecked_suicide: false,
          //     isChecked_medication: false
          // })

          // await AsyncStorage.setItem("PatientDetials", "1");
          Actions.Depression();
          // Actions.HDSTool();
        } else {
          ToastAndroid.showWithGravity(
            "Please fill all details",
            ToastAndroid.SHORT,
            ToastAndroid.BOTTOM
          );
          Alert.alert(
            "",
            "Please fill all details",
            [
              {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel",
              },
              { text: "OK", onPress: () => console.log("OK Pressed") },
            ],
            { cancelable: false }
          );
        }
      } else {
        ToastAndroid.showWithGravity(
          "Please fill all details",
          ToastAndroid.SHORT,
          ToastAndroid.BOTTOM
        );
        Alert.alert(
          "",
          "Please fill all details",
          [
            {
              text: "Cancel",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel",
            },
            { text: "OK", onPress: () => console.log("OK Pressed") },
          ],
          { cancelable: false }
        );
      }
    } else {
      ToastAndroid.showWithGravity(
        "Please fill all details",
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM
      );
      Alert.alert(
        "",
        "Please fill all details",
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel",
          },
          { text: "OK", onPress: () => console.log("OK Pressed") },
        ],
        { cancelable: false }
      );
    }
  }
  // render_FlatList_footer=(data)=>{
  //     if(data.Treated_Master_Option == "Others")
  //     {
  //         return(
  //             <View>
  //                 <TextInput
  //                  placeholder="Treated for ?"
  //                  style={{ marginLeft: 10, fontSize: 16, width: '90%' }}
  //                  onChangeText={(Treated) => {this.setState({Treated})}}
  //                  underlineColorAndroid="black"
  //                 />
  //             </View>
  //         )
  //     }
  // }
  checkUpdate() {
    //debugger
    if (this.state.other_val_show == true && this.state.value_other != "") {
      let ldata = {
        Treated_Master_PKeyId: 13,
        value: true,
        other_Val: this.state.value_other,
      };
      chkbox.push(ldata);
    } else if (this.state.value_other == "") {
      this.setState({ err: "Please fill required field" });
    } else {
    }
  }
  updatevalue(value) {
    //debugger
    this.setState({ other_val_show: value });
    if (value == true) {
      this.setState({ other_val_show: true, show: true });
    } else {
      this.setState({ other_val_show: false, show: false });
    }
    console.log(this.state.other, "checkboxValue");
  }
  inputView() {
    //debugger
    if (this.state.show)
      return (
        <View style={{ marginTop: "10%" }}>
          <TextInput
            placeholder="Treated for ?"
            style={{
              marginLeft: 10,
              fontSize: 16,
              width: "90%",
              marginTop: "2%",
              borderBottomWidth: 1,
            }}
            onChangeText={(value_other) => {
              this.setState({ value_other });
            }}
            underlineColorAndroid="black"
          />
          <Text>{this.state.err}</Text>
        </View>
      );
  }
  render() {
    // let index = 0;
    // const dropdown_data1 = [{ key: index++, label: 'Female' },
    // { key: index++, label: 'Male' },
    // { key: index++, label: 'Transgender' },
    // { key: index++, label: 'Other' }]
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView keyboardShouldPersistTaps="handled" style={{ flex: 1 }}>
          <View
            style={{
              flexDirection: "column",
              marginTop: "5%",
              marginLeft: "5%",
            }}
          >
            {/* <Text style={styles.textStyles} >What is your identity</Text> */}
            <Text style={styles.textStyles}>{strings.identity}</Text>

            <Picker
              textStyle={{ fontSize: 15 }}
              style={styles.pickerStyle}
              placeholder={strings.selectOptions}
              placeholderStyle={{ fontSize: 15 }}
              selectedValue={this.state.identity}
              onValueChange={(value) => this.setSelectedValue(value, 1)}
            >
              {/* <Picker.Item label='Please select an option...' value="0" /> */}
              <Picker.Item label={strings.Female1} value={strings.Female1} />
              <Picker.Item label={strings.Male1} value={strings.Male1} />
              <Picker.Item
                label={strings.Transgender1}
                value={strings.Transgender1}
              />
              <Picker.Item label={strings.Female} value={strings.Female} />
              <Picker.Item label={strings.Male} value={strings.Male} />
              <Picker.Item
                label={strings.Transgender}
                value={strings.Transgender}
              />
              {/* <Picker.Item label={strings.other} value={strings.other} /> */}
            </Picker>
          </View>

          <View
            style={{
              flexDirection: "column",
              marginTop: "5%",
              marginLeft: "5%",
            }}
          >
            {/* <Text style={styles.textStyles} >What is your partnership status?</Text> */}
            <Text style={styles.textStyles}>{strings.status}</Text>

            <Picker
              textStyle={{ fontSize: 15 }}
              style={styles.pickerStyle}
              placeholder={strings.selectOptions}
              placeholderStyle={{ fontSize: 15 }}
              selectedValue={this.state.partnership}
              onValueChange={(value) => this.setSelectedValue(value, 2)}
            >
              {/* <Picker.Item label='Please select an option...' value="0" /> */}
              <Picker.Item label={strings.married} value={strings.married} />
              <Picker.Item label={strings.partner} value={strings.partner} />
              <Picker.Item label={strings.living} value={strings.living} />
              <Picker.Item
                label={strings.separated}
                value={strings.separated}
              />
              <Picker.Item label={strings.single} value={strings.single} />
              <Picker.Item label={strings.widowed} value={strings.widowed} />
            </Picker>
          </View>
          <View
            style={{
              flexDirection: "column",
              marginTop: "5%",
              marginLeft: "5%",
            }}
          >
            {/* <Text style={styles.textStyles} >What is your racial/ethnic identity?</Text> */}
            <Text style={styles.textStyles}>{strings.racial}</Text>
            <Picker
              textStyle={{ fontSize: 15 }}
              style={styles.pickerStyle}
              selectedValue={this.state.ethnic}
              placeholder={strings.selectOptions}
              placeholderStyle={{ fontSize: 15 }}
              onValueChange={(value) => this.setSelectedValue(value, 3)}
            >
              {/* <Picker.Item label='Please select an option...' value="0" /> */}
              <Picker.Item label={strings.black} value={strings.black} />
              <Picker.Item label={strings.indian} value={strings.indian} />
              <Picker.Item label={strings.African} value={strings.African} />
              <Picker.Item label={strings.native} value={strings.native} />
              <Picker.Item label={strings.white} value={strings.white} />
              <Picker.Item label={strings.asian} value={strings.asian} />
              <Picker.Item label={strings.Alaska} value={strings.Alaska} />
              {/* <Picker.Item label={strings.Hawaiian} value={strings.Hawaiian} /> */}
              <Picker.Item
                label={strings.identidad}
                value={strings.identidad}
              />
            </Picker>
          </View>

          <View style={{ flexDirection: "column", marginLeft: "5%" }}>
            <Text style={styles.textStyles}>{strings.treatment}</Text>
            {/* {this.getList()} */}
            {/* <FlatList
                                data={this.state.treatment}
                                numColumns={2}
                                style={{ marginTop: '5%' }}
                                renderItem={({ item, index }) => this.treatedChkbox(item)}
                                keyExtractor={(item, index) => index.toString()}
                            />
                             */}
            <FlatList
              data={this.state.treatment}
              // numColumns={2}
              // style={{ marginTop: '5%', width: deviceWidth * 90 / 100,}}
              renderItem={({ item, index }) => this.treatedChkbox(item)}
              keyExtractor={(item, index) => index.toString()}
              // ListFooterComponent={this.inputView()}
            />
          </View>
          <View
            style={{ flexDirection: "row", marginTop: "2%", marginLeft: "5%" }}
          >
            <CheckBox
              // value={this.props.showChecked ? true : this.state.value}
              // value={this.state.other_val_show}
              checked={this.state.other_val_show}
              // onValueChange={(other_val_show) => this.updatevalue(other_val_show)}
              onPress={(other_val_show) =>
                this.setState({
                  other_val_show: !this.state.other_val_show,
                  show: !this.state.other_val_show,
                })
              }
            />
            <Text style={{ fontSize: 16, marginTop: "1%", marginLeft: "4%" }}>
              {strings.other}
            </Text>
          </View>
          {this.state.show == true
            ? this.inputView()
            : // <View>
              //     <TextInput
              //         placeholder="Treated for ?"
              //         style={{ marginLeft: 10, fontSize: 16, width: '90%' }}
              //         onChangeText={(value_other) => { this.setState({ value_other }) }}
              //         underlineColorAndroid="black"
              //     />
              // </View>
              null}

          {/* <Show_InputView /> */}

          <View style={{ flexDirection: "column", marginLeft: "5%" }}>
            <View style={{ flexDirection: "row", width: "100%" }}>
              <View style={{ width: "70%", marginTop: "5%" }}>
                <Text style={styles.textStyles}>{strings.treatment}</Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  padding: "3%",
                  justifyContent: "space-around",
                }}
              >
                <FlipToggle
                  value={this.state.isChecked_medication}
                  buttonWidth={90}
                  buttonHeight={30}
                  buttonRadius={50}
                  buttonOffColor={"gray"}
                  sliderOffColor={"#ffffff"}
                  buttonOnColor={"#4B96FB"}
                  sliderOnColor={"#fff"}
                  labelStyle={{ color: "#fff", fontSize: 10 }}
                  onLabel={strings.Yes}
                  offLabel={strings.No}
                  onToggle={(value) => {
                    this.isCheckedValue(value, 1);
                    this.setState({ isChecked_medication: value });
                  }}
                />
              </View>
            </View>
            {/* <TextInput
                    style={{ marginLeft: 10, fontSize: 16, width: '90%' }}
                    // onChangeText={(Username) => {this.setState({Username})}}
                    underlineColorAndroid="black"
                /> */}
          </View>
          <View
            style={{
              flexDirection: "column",
              marginTop: "3%",
              marginLeft: "5%",
            }}
          >
            <View style={{ flexDirection: "row", width: "100%" }}>
              <View style={{ width: "70%", marginTop: "2%" }}>
                <Text style={styles.textStyles}>{strings.suicide}</Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  padding: "3%",
                  justifyContent: "space-around",
                }}
              >
                <FlipToggle
                  value={this.state.isChecked_suicide}
                  buttonWidth={90}
                  buttonHeight={30}
                  buttonRadius={50}
                  buttonOffColor={"gray"}
                  sliderOffColor={"#ffffff"}
                  buttonOnColor={"#4B96FB"}
                  sliderOnColor={"#fff"}
                  labelStyle={{ color: "#fff", fontSize: 10 }}
                  onLabel={strings.Yes}
                  offLabel={strings.No}
                  onToggle={(value) => {
                    this.isCheckedValue(value, 2);
                    this.setState({ isChecked_suicide: value });
                  }}
                />
              </View>
            </View>
            {/* <TextInput
                    style={{ marginLeft: 10, fontSize: 16, width: '90%' }}
                    // onChangeText={(Username) => {this.setState({Username})}}
                    underlineColorAndroid="black"
                /> */}
          </View>

          <View
            style={{
              // borderWidth: 1,
              marginVertical: 15,
              flex: 0,
              justifyContent: "space-around",
              alignItems: "center",
              flexDirection: "row",
            }}
          >
            <NativeBaseButton
              style={{
                backgroundColor: "#4B96FB",
                height: 50,
                paddingHorizontal: 50,
                marginHorizontal: 20,
              }}
              onPress={() => Actions.Home()}
            >
              <Text style={{ textTransform: "uppercase", color: "#ffffff" }}>
                {strings.previous}
              </Text>
            </NativeBaseButton>
            <NativeBaseButton
              style={{
                backgroundColor: "#4B96FB",
                height: 50,
                paddingHorizontal: 50,
                marginHorizontal: 20,
              }}
              onPress={() => this.validateUpdate()}
            >
              <Text style={{ textTransform: "uppercase", color: "#ffffff" }}>
                {strings.Next}
              </Text>
            </NativeBaseButton>
          </View>

          {/* <View
            style={{
              flexDirection: "row",
              justifyContent: "space-around",
              marginTop: "5%",
              width: "100%",
            }}
          >
            <View
              style={{
                alignItems: "center",
                marginTop: "5%",
                backgroundColor: "#4B96FB",
                width: "40%",
                alignSelf: "center",
                padding: "3%",
              }}
            >
              <Button
                onPress={() => Actions.Home()}
                title={strings.previous}
                color="#ffffff"
                style={styles.button}
              />
            </View>
            <View
              style={{
                alignItems: "center",
                marginTop: "5%",
                backgroundColor: "#4B96FB",
                width: "40%",
                alignSelf: "center",
                padding: "3%",
              }}
            >
              <Button
                onPress={() => this.validateUpdate()}
                title={strings.Next}
                color="#ffffff"
                style={styles.button}
              />
            </View>
          </View> */}

          {/* <View style={{ alignItems: 'flex-end', flexDirection: 'row', justifyContent: 'space-between', marginRight: '4%', marginLeft: '4%', marginTop: '5%' }}>
                        <TouchableOpacity style={styles.button} onPress={() => Actions.Home()}>
                            <Text style={styles.buttonText}>Previous</Text>
                        </TouchableOpacity>
                        <Button
                           onPress={() => Actions.Home()}
                            title="Previous"
                            color="#ffffff"
                            style={styles.button}
                        />
                        <TouchableOpacity style={styles.button} onPress={() => this.validateUpdate()}>
                            <Text style={styles.buttonText}>Next</Text>
                        </TouchableOpacity> */}
          {/* <Button
                            onPress={() => this.validateUpdate()}
                            title="Next"
                            color="#ffffff"
                            style={styles.button}
                        /> */}
          {/* </View> */}
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    height: "100%",
  },
  inputBox: {
    width: (deviceWidth * 85) / 100,
    height: (deviceHeight * 7) / 100,
    borderRadius: 5,
    // backgroundColor: '#EDF0F7',
    paddingVertical: 15,
  },
  textStyles: {
    fontSize: 20,
    color: "#1D4A99",
  },
  pickerStyle: {
    color: "#344953",
    justifyContent: "center",
    // borderBottomWidth:1,
    // marginRight:'5%'
  },
  button: {
    width: (deviceWidth * 30) / 100,
    height: (deviceHeight * 8) / 100,
    backgroundColor: "#4B96FB",
    borderRadius: 5,
    marginVertical: 12,
    paddingVertical: 12,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
  },
  _button: {
    width: (deviceWidth * 20) / 100,
    height: (deviceHeight * 4) / 100,
    backgroundColor: "#4B96FB",
    borderRadius: 5,
    marginVertical: 7,
    paddingVertical: 7,
  },
  button_Text: {
    fontSize: 12,
    color: "#ffffff",
    textAlign: "center",
  },
  buttonText: {
    fontSize: 16,
    color: "#ffffff",
    textAlign: "center",
  },
  avatarContainer: {
    borderColor: "#9B9B9B",
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },
  avatar: {
    borderRadius: 75,
    width: 120,
    height: 120,
  },
});
