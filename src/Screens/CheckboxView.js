import React from "react";
import { View, TextInput } from "react-native";
import Helper from "../Database/Helper";
import { CheckBox } from "native-base";
// import Checkbox from 'react-simple-checkbox';
export const chkbox = [];
export let show_View = false;
export default class CheckboxView extends React.Component {
  state = { value: false, show: false, Treated: "", selectedOptions: "" };

  updatevalue = (value) => {
    //debugger;
    console.log("statevalue", this.state.value);
    console.log("props value", this.props.selected_value.value);
    var selectedOptions = this.props.selected_value.Treated_Master_Option;
    this.setState({ value: value });
    var value = value;
    // var other_Val =this.state.Treated
    var Treated_Master_PKeyId = this.props.selected_value.Treated_Master_PKeyId;
    var ldata = { value, Treated_Master_PKeyId };

    chkbox.push(ldata);
    // selectedHDST = ldata;
    console.log(chkbox);
    // return selectedHDST;
    this.setState({ selectedOptions: selectedOptions });
    // this.ShowData(Treated_Master_PKeyId, value);
    // this.Show_InputView(Treated_Master_PKeyId,value);
  };
  show(Treated_Master_PKeyId, value) {
    //debugger

    if (value == true && Treated_Master_PKeyId === 13) {
      this.setState({ show: true });
      const ldata = true;
      // this.ShowData(ldata)
      // Helper.show_ViewTextInput =true
    } else {
      this.setState({ show: false });
      const ldata = false;
      // Helper.show_ViewTextInput =false
      // this.ShowData(ldata)
    }
  }
  updateTextValue(ldata) {
    //debugger;
    var other_Val = ldata;
    chkbox.push(other_Val);
  }
  inputView() {
    if (this.state.show) {
      return (
        <View style={{ width: "100%", backgroundColor: "" }}>
          <TextInput
            placeholder="Treated for ?"
            style={{ marginLeft: 10, fontSize: 16, width: "90%" }}
            onChangeText={(Treated) => {
              this.setState({ Treated }), this.updateTextValue(Treated);
            }}
            underlineColorAndroid="black"
          />
        </View>
      );
    }
  }

  render() {
    return (
      <View style={{ backgroundColor: "" }}>
        <View>
          <CheckBox
            // checked={this.props.showChecked ? true : this.state.value}
            checked={this.state.value}
            // onPress={(data) => this.updatevalue(data)}
            onPress={() => this.updatevalue(!this.state.value)}
          />
        </View>
        {/* <View style={{ width: '100%' }}>
                {this.state.show ? (this.inputView()) : null}
            </View> */}
      </View>
    );
  }
}
export function Show_InputView(ldata) {
  if (ldata) {
    return (
      <View style={{ width: "100%", backgroundColor: "" }}>
        <TextInput
          placeholder="Treated for ?"
          style={{ marginLeft: 10, fontSize: 16, width: "90%" }}
          onChangeText={(Treated) => {
            this.setState({ Treated }), this.updateTextValue(Treated);
          }}
          underlineColorAndroid="black"
        />
      </View>
    );
  }
}

export function ShowData(Treated_Master_PKeyId, value) {
  if (value == true && Treated_Master_PKeyId === 13) {
    // this.setState({ show: true });
    // const ldata = true
    // this.ShowData(ldata)
    show_View = true;
    // Helper.show_ViewTextInput =true
  } else {
    // this.setState({ show: false });
    // const ldata = false
    show_View = false;
    // Helper.show_ViewTextInput =false
    // this.ShowData(ldata)
  }
}
