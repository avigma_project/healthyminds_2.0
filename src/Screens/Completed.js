import React from "react";
import {
  View,
  Text,
  Dimensions,
  FlatList,
  BackHandler,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import * as database from "../Database/allSchemas";
import { fetchapi } from "../Api/function";
import { Actions } from "react-native-router-flux";
import { AnimatedCircularProgress } from "react-native-circular-progress";
import strings from "../Language/Language";

const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;
// const data = [
//                 {title: "Depression Screening", score: 32, description: "A complete evalutation is not recommended"},
//                 {title: "Mood Disorder", score: 7, description: "A complete evalutation is not recommended"},
//                 {title: "General Anxiety Disorder", score: 9, description: "A complete evalutation is not recommended"},
//                 {title: "P.T.S.D.", score: 76, description: "A complete evalutation is strongly recommended"}
// ];
const lvalue = [];
export default class Completed extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      patient_base: "",
      patient_master: "",
      result: [],
      mood_Disorder_1: "",
      mood_Disorder_2: "",
    };
  }
  componentWillMount() {
    //debugger
    BackHandler.addEventListener(
      "hardwareBackPress",
      this.backHandler.bind(this)
    );
  }
  // componentWillUnmount(){
  //   BackHandler.removeEventListener('hardwareBackPress', this.onBackPress.bind(this));
  //   }
  componentWillUnmount() {
    //debugger
    BackHandler.addEventListener(
      "hardwareBackPress",
      this.backHandler.bind(this)
    );
  }
  componentDidMount() {
    //debugger
    this.Urldata();
    this.getScore();
    this.getData();
  }
  backHandler() {
    Actions.Welcome();
  }
  Urldata() {
    database.queryHealthCenterDetails().then((res) => {
      const result = Object.values(res[0]);
      console.log("List of centers", result);
      this.setState({ result: result });
    });
  }

  getData = () => {
    //debugger;
    var patient_base,
      patient_master,
      patient_treated_schema,
      patient_depression,
      patient_mooddisorder,
      patient_anxiety,
      patient_ptsd,
      Depression_1,
      Depression_2,
      Score,
      Survey;
    database
      .queryBaseDetails()
      .then((res) => {
        let patient_base_val = Object.values(res[0]);
        patient_base = JSON.stringify(patient_base_val);
        // console.log('patient_Base', patient_base);

        database
          .queryAllPatient_Master()
          .then((res) => {
            patient_master = Object.values(res[0]);
            database
              .queryAllPatient_Treated_Schema()
              .then((res) => {
                patient_treated_schema = Object.values(res[0]);

                database
                  .queryAllPatient_Depression()
                  .then((res) => {
                    patient_depression = Object.values(res[0]);

                    database
                      .queryAllPatient_MoodDisorder_Schema()
                      .then((res) => {
                        patient_mooddisorder = Object.values(res[0]);
                        database
                          .queryAllPatient_AnxietyDisorder_Schema()
                          .then((res) => {
                            patient_anxiety = Object.values(res[0]);
                            database
                              .queryAllPatient_PTSD_Schema()
                              .then((res) => {
                                patient_ptsd = Object.values(res[0]);

                                database
                                  .queryAllPatient_MoodDisorder_Schema_2()
                                  .then((res) => {
                                    Depression_1 = Object.values(res[0]);
                                    // lvalue.push(Depression_1)
                                    this.setState({
                                      mood_Disorder_1: Depression_1[0],
                                    });
                                    database
                                      .queryAllPatient_MoodDisorder_Schema_3()
                                      .then((res) => {
                                        Depression_2 = Object.values(res[0]);
                                        // lvalue.push(Depression_2)
                                        this.setState({
                                          mood_Disorder_2: Depression_2[0],
                                        });

                                        database
                                          .queryAllFinalScore()
                                          .then((res) => {
                                            Score = Object.values(res[0]);

                                            database
                                              .queryAllPatientSurvey_Schema()
                                              .then((res) => {
                                                Survey = Object.values(res[0]);
                                                console.log(Survey);
                                                fetchapi({
                                                  url:
                                                    "HealthyMindsApp/PostPatientDetails",

                                                  patient_Master: JSON.stringify(
                                                    patient_master
                                                  ),
                                                  patient_treated_schema: JSON.stringify(
                                                    patient_treated_schema
                                                  ),
                                                  patient_depression: JSON.stringify(
                                                    patient_depression
                                                  ),
                                                  patient_mooddisorder: JSON.stringify(
                                                    patient_mooddisorder
                                                  ),
                                                  patient_anxiety: JSON.stringify(
                                                    patient_anxiety
                                                  ),
                                                  patient_ptsd: JSON.stringify(
                                                    patient_ptsd
                                                  ),
                                                  Depression_1: JSON.stringify(
                                                    Depression_1
                                                  ),
                                                  Depression_2: JSON.stringify(
                                                    Depression_2
                                                  ),
                                                  Survey: JSON.stringify(
                                                    Survey
                                                  ),

                                                  Score: JSON.stringify(Score),
                                                }).then((response) => {
                                                  console.log(
                                                    "Response +++++++++++++++++++++++",
                                                    response
                                                  );
                                                  //updating default value
                                                  console.log(
                                                    "api response",
                                                    response
                                                  );
                                                  database
                                                    .UpdatePatient_PTSD_Schema_final(
                                                      patient_ptsd
                                                    )
                                                    .catch((error) =>
                                                      console.log(error)
                                                    );
                                                  database
                                                    .UpdatePatientDepressionMaster_final(
                                                      patient_depression
                                                    )
                                                    .catch((Error) => {
                                                      console.log(Error);
                                                    });
                                                  database
                                                    .UpdatePatient_AnxietyDisorder_Schema_final(
                                                      patient_anxiety
                                                    )
                                                    .catch((error) => {
                                                      console.log(error);
                                                    });
                                                  database
                                                    .UpdatePatientTreatedMaster_final(
                                                      patient_treated_schema
                                                    )
                                                    .catch((Error) => {
                                                      console.log(Error);
                                                    });
                                                  database
                                                    .UpdatePatientMoodDisorderMaster_final(
                                                      patient_mooddisorder
                                                    )
                                                    .catch((error) => {
                                                      console.log(error);
                                                    });

                                                  database
                                                    .UpdatePatientSurveyMaster_final(
                                                      Survey
                                                    )
                                                    .catch((error) => {
                                                      console.log(error);
                                                    });

                                                  // database
                                                  // .UpdatePatientBaseMaster_final(patient_base)
                                                  // .catch(error=>{console.log(error)})
                                                  // database
                                                  // .UpdatePatientMaster_final(patient_master)

                                                  // database
                                                  //   .deleteBaseDetails()
                                                  //   .catch(error => { console.log(error) })
                                                  database
                                                    .deletePatientMaster()
                                                    .catch((error) => {
                                                      console.log(error);
                                                    });

                                                  // database
                                                  // .UpdatePatientMoodDisorderMaster_2_final(Depression_1)
                                                  // .catch(error=>{console.log(error)})
                                                  // database
                                                  // .UpdatePatientMoodDisorderMaster_3_final(Depression_2)
                                                  // .catch(error=>{console.log(error)})

                                                  database
                                                    .deletePatientDepression_2(
                                                      Depression_1
                                                    )
                                                    .catch((error) => {
                                                      console.log(error);
                                                    });

                                                  // database
                                                  //   .deletePatientDepression_3(
                                                  //     Depression_2
                                                  //   )
                                                  //   .catch((Error) => {
                                                  //     console.log(Error);
                                                  //   });
                                                });
                                                this.storeData();
                                              });

                                            console.log(
                                              JSON.stringify(patient_master)
                                            );
                                          })
                                          .catch((error) => console.log(error));
                                      })
                                      .catch((error) => console.log(error));
                                  })
                                  .catch((error) => console.log(error));
                              })
                              .catch((error) => console.log(error));
                          })
                          .catch((error) => console.log(error));
                      })
                      .catch((error) => console.log(error));
                  })
                  .catch((error) => console.log(error));
              })
              .catch((error) => console.log(error));
          })
          .catch((error) => console.log(error));
      })
      .catch((error) => console.log(error));
  };

  async storeData() {
    await AsyncStorage.getItem("PatientDetials", (err, ldata) => {
      console.log("AsyncStorage profile", ldata);
    });
    await AsyncStorage.setItem("PatientDetials", "2");
  }

  UpdateDatabase() {
    //debugger

    database.deleteBaseDetails().then((error) => {
      console.log(error);
    });

    database.deletePatientMaster().then((error) => {
      console.log(error);
    });
  }
  getScore = () => {
    //debugger
    console.log("resp", lvalue);
    database
      .queryAllFinalScore()
      .then((result) => {
        console.log(`Getting all the score value: ${JSON.stringify(result)}`);
        this.setState({ result: result });
      })
      .catch((error) => {
        console.log(`Error in getting score value: ${error}`);
      });
    // database
    // .queryAllPatient_MoodDisorder_Schema_2()
    // .then(result => {
    //   console.log(`mod ${JSON.stringify(result)}`);
    //   this.setState({ mood_Disorder_1: result});
    // })
    // .catch(error => {
    //   console.log(`Error in getting score value: ${error}`);
    // });
    // database
    // .queryAllPatient_MoodDisorder_Schema_3()
    // .then(result => {
    //   console.log(`mod2: ${JSON.stringify(result)}`);
    //   this.setState({ mood_Disorder_2: result });
    // })
    // .catch(error => {
    //   console.log(`Error in getting score value: ${error}`);
    // });
  };

  getDescription = (title, score) => {
    console.log("Title: ", title, "Score: ", score);
    if (title === "Depression Screening") {
      if (score <= 8) {
        // return 'A complete evaluation is not recommended';
        return (
          <View style={{ width: (deviceWidth * 55) / 100 }}>
            <Text>
              {strings.depressive}
              {strings.notRecommended}
            </Text>
          </View>
        );
      } else if (score <= 16 && score >= 9) {
        // return 'A complete evalutaion is recommended';
        return (
          <View>
            <TouchableOpacity
              style={styles.button}
              onPress={() => Actions.ListCenters()}
            >
              <Text style={styles.buttonText}>Click here for Evaluation</Text>
            </TouchableOpacity>
            <View style={{ width: (deviceWidth * 55) / 100 }}>
              <Text style={{ fontWeight: "bold" }}>
                {strings.depressive1} {strings.Evaluationrecommended}
              </Text>
            </View>
          </View>
        );
      } else if (score <= 30 && score >= 17) {
        // return 'A complete evalutaion is strongly recommended';
        return (
          <View>
            <TouchableOpacity
              style={styles.button}
              onPress={() => Actions.ListCenters()}
            >
              <Text style={styles.buttonText}>Click here for Evaluation</Text>
            </TouchableOpacity>
            <View style={{ width: (deviceWidth * 55) / 100 }}>
              <Text style={{ fontWeight: "bold" }}>
                {strings.depressive2}
                {strings.Evaluation}
              </Text>
            </View>
          </View>

          // <View>
          //   <Text>A complete evalutaion is strongly recommended</Text>
          // </View>
        );
      }
    }
    if (title === "Mood Disorder") {
      // return 'Description';
      // console.log('lval', lvalue && this.state.mood_Disorder_1.value)
      const ldata = lvalue;
      // console.log('data', this.state.mood_Disorder_2.value)
      if (
        this.state.mood_Disorder_1 != undefined &&
        this.state.mood_Disorder_2 != undefined
      ) {
        if (
          score >= 7 &&
          score <= 13 &&
          this.state.mood_Disorder_1.value >= 3 &&
          this.state.mood_Disorder_2.value == true
        ) {
          // return 'need to be evaluated for bipolar disorder.'
          return (
            <View>
              <TouchableOpacity
                style={styles.button}
                onPress={() => Actions.ListCenters()}
              >
                <Text style={styles.buttonText}>Click here for Evaluation</Text>
              </TouchableOpacity>
              <View style={{ width: (deviceWidth * 55) / 100 }}>
                <Text style={{ fontWeight: "bold" }}>{strings.Need}</Text>
              </View>
            </View>
            // <View>
            //   <Text>need to be evaluated for bipolar disorder.</Text>
            // </View>
          );
        } else {
          return (
            <View style={{ width: (deviceWidth * 55) / 100 }}>
              <Text>{strings.notRecommended}</Text>
            </View>
          );
        }
      } else {
        return (
          <View style={{ width: (deviceWidth * 55) / 100 }}>
            <Text>{strings.notRecommended}</Text>
          </View>
        );
      }
    }
    if (title === "Generalized Anxiety Disorder") {
      if (score <= 5) {
        // return 'A complete evaluation is not recommended';
        return (
          <View style={{ width: (deviceWidth * 55) / 100 }}>
            <Text>A complete Evaluation is not recommended</Text>
          </View>
        );
      } else {
        // return 'A complete evalutaion is recommended';
        return (
          <View>
            <TouchableOpacity
              style={styles.button}
              onPress={() => Actions.ListCenters()}
            >
              <Text style={styles.buttonText}>Click here for Evaluation</Text>
            </TouchableOpacity>
            <View style={{ width: (deviceWidth * 55) / 100 }}>
              <Text style={{ fontWeight: "bold" }}>
                {strings.Evaluationrecommended}
              </Text>
            </View>
          </View>
        );
      }
    }
    if (title === "P.T.S.D.") {
      if (score <= 1) {
        // return 'A complete evaluation is not recommended';
        return (
          <View style={{ width: (deviceWidth * 55) / 100 }}>
            <Text>
              {strings.SymptomsPTSD1}
              {strings.notRecommended}
            </Text>
          </View>
        );
      } else if (score <= 3 && score >= 2) {
        //Changes done due to the requriment of Button added by sandeep sir

        // this.showBtn();
        // return 'Further evaluation is recommended';
        // return this.showBtn()
        return (
          <View>
            <TouchableOpacity
              style={styles.button}
              onPress={() => Actions.ListCenters()}
            >
              <Text style={styles.buttonText}>Click here for Evaluation</Text>
            </TouchableOpacity>
            <View style={{ width: (deviceWidth * 55) / 100 }}>
              <Text style={{ fontWeight: "bold" }}>
                {strings.SymptomsPTSD23}
                {strings.reco}
              </Text>
            </View>
          </View>
        );
      } else if (score == 4) {
        // return 'A complete evalutaion is strongly recommended';
        return (
          <View>
            <TouchableOpacity
              style={styles.button}
              onPress={() => Actions.ListCenters()}
            >
              <Text style={styles.buttonText}>Click here for Evaluation</Text>
            </TouchableOpacity>
            <View style={{ width: (deviceWidth * 55) / 100 }}>
              <Text style={{ fontWeight: "bold" }}>
                {strings.SymptomsPTSD4}
                {strings.Evaluation}
              </Text>
              {/* <Text style={{ fontWeight: "bold" }}>
                {strings.Evaluation}
              </Text> */}
            </View>
          </View>
        );
      }
    }
  };
  showBtn() {
    return (
      <View>
        <TouchableOpacity
          style={styles.button}
          onPress={() => Actions.ListCenters()}
        >
          <Text style={styles.buttonText}>Click here for Evaluation</Text>
        </TouchableOpacity>
        <View style={{ width: (deviceWidth * 55) / 100 }}>
          <Text>{strings.Evaluation}</Text>
        </View>
      </View>
    );
  }
  renderRow = ({ item }) => {
    console.log(`Getting all the score item: ${JSON.stringify(item)}`);
    console.log(item.Title, "mobo");
    return (
      <View style={{ backgroundColor: "#ffffff", flex: 1, padding: 20 }}>
        <View>
          <Text style={{ fontSize: 25, color: "#1D4A99" }}>{item.Title}</Text>
        </View>
        <View style={{ flexDirection: "row" }}>
          <AnimatedCircularProgress
            size={100}
            width={15}
            // fill={item.Score}
            fill={(item.Score * 100) / 30}
            tintColor="#1D4A99"
            backgroundColor="#AFAFAF"
          >
            {(fill) => <Text>{item.Score}</Text>}
          </AnimatedCircularProgress>
          <View style={{ flex: 1, alignItems: "center", margin: 9 }}>
            {/* {item.Title == 'P.T.S.D.' && item.Score >= 2 && item.Score <= 3 ?
              <View>
                <TouchableOpacity style={styles.button}>
                  <Text style={styles.buttonText}>Click here for Evaluatiion</Text>
                </TouchableOpacity>
                <Text style={{ fontWeight: 'bold' }}> Further evaluation is recommended </Text>
              </View>
              : null
            }
            {item.Title == 'P.T.S.D.' && item.Score == 4 ?
              <View>
                <TouchableOpacity style={styles.button}>
                  <Text style={styles.buttonText}>Click here for Evaluatiion</Text>
                </TouchableOpacity>
                <Text style={{ fontWeight: 'bold' }}> A complete evalutaion is strongly recommended</Text>
              </View>
              : null
            } */}
            <View style={{ textAlign: "left" }}>
              {this.getDescription(item.Title, item.Score)}
            </View>
          </View>
        </View>
      </View>
    );
  };
  render() {
    const { result } = this.state;
    return (
      <View>
        <ScrollView>
          <FlatList
            data={result[0]}
            renderItem={this.renderRow}
            keyExtractor={(item, index) => index.toString()}
          />
          {/* <TouchableOpacity style={styles.button} onPress={()=>Actions.Welcome()}>
          <Text style={styles.buttonText}>Finish</Text>
        </TouchableOpacity> */}
          <View style={{ width: "100%" }}>
            <View style={{ width: "98%", margin: "3%" }}>
              <Text>{strings.result1}</Text>
            </View>
            <View style={{ width: "98%", margin: "3%" }}>
              <Text>{strings.result2}</Text>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  button: {
    width: (deviceWidth * 55) / 100,
    height: (deviceHeight * 7) / 100,
    backgroundColor: "#4B96FB",
    borderRadius: 5,
    marginVertical: 12,
    paddingVertical: 12,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
  },
  buttonStyle: {
    color: "red",
    marginTop: 20,
    padding: 20,
  },
  buttonText: {
    fontSize: 18,
    color: "#ffffff",
    textAlign: "center",
  },
});
