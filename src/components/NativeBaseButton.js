import React from "react";
import { Button, Text } from "native-base";
import { StyleSheet } from "react-native";

const NativeBaseButton = ({ label, labelStyle, buttonStyle, ...props }) => {
  return (
    <Button {...props} style={[styles.button, { ...buttonStyle }]}>
      <Text style={[styles.text, { ...labelStyle }]}>{label}</Text>
    </Button>
  );
};

export default NativeBaseButton;
const styles = StyleSheet.create({
  button: {
    backgroundColor: "#4B96FB",
    height: 50,
    paddingHorizontal: 30,
    justifyContent: "center",
  },
  text: {
    textTransform: "uppercase",
    color: "#ffffff",
    textAlign: "center",
  },
});
