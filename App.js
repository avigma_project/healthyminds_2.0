import "react-native-gesture-handler";
import { Root } from "native-base";
import React, { Component } from "react";
import { Provider, useDispatch, useSelector } from "react-redux";
import { PersistGate } from "redux-persist/lib/integration/react";
import { NavigationContainer } from "@react-navigation/native";
import { StatusBar, StyleSheet, Dimensions, View } from "react-native";
import store, { persistor } from "./src/config/store";
import MyStack from "./src/Navigation/StackNavigation";
const deviceHeight = Dimensions.get("window").height;

const MyStatusBar = ({ backgroundColor, ...props }) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);
const App = () => {
  // alert((deviceHeight / 100) * 4);
  const [token, setToken] = React.useState(null);
  const userToken = useSelector((state) => state.authReducer.userToken);
  const mode = useSelector((state) => state.authReducer.registerMode);
  return (
    // <Root>
    //   <Provider store={store}>
    //     {/* <Routes /> */}
    //   </Provider>
    // </Root>
    <Root>
      <NavigationContainer>
        <MyStatusBar
          backgroundColor="#fff"
          barStyle="light-content"
          animated={true}
        />
        <View style={styles.appBar} />
        <MyStack />
      </NavigationContainer>
    </Root>
  );
};

const AppWrapper = () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <App />
    </PersistGate>
  </Provider>
);
export default AppWrapper;
const STATUSBAR_HEIGHT = StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === "ios" ? (deviceHeight / 100) * 4.9 : 0;

const styles = StyleSheet.create({
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
  appBar: {
    backgroundColor: "#fff",
    height: APPBAR_HEIGHT,
  },
});
