/**
 * @format
 */

import {AppRegistry} from 'react-native';
// import Home from './src/Screens/Home';
import App from './App'
// import Index from './src/Screens/Index'
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
